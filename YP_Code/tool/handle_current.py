import numpy as np
from numpy import diff
from datetime import datetime
from mypickle import save2pickle, load_pickle
import matplotlib.pyplot as plt
from plot_current import plot_current
from plot_current2 import plot_current2
import argparse
import struct
import math
from statistics import mean
from itertools import groupby

if __name__ == "__main__":
    # delat_t = 0.01
    PWM = 20000 # pwm頻率 : 20k
    now = datetime.now()
    default_len = 13
    default_file_marker = now.strftime("%m_%d_%H_%M_%S")
    parser = argparse.ArgumentParser()
    parser.add_argument("-mk", "--marker", help="Enter the marker about file, it will add behine the file", default=default_file_marker, type=str)
    parser.add_argument("-len", "--length", help="Comma \",\" split length, example \"1,2,3\" is three", default=default_len, type=int)

    args = parser.parse_args()

    file_marker = args.marker

    # define PWM PERIOD COUNT
    PERIOD_COUNT = 900
    # define each phase resistor
    ra = 0.25
    rb = 0.25
    COMMA_DATA_LEN = args.length
    # para_index = {'title': 0, 'ia': 1, 'ib': 2, 'angle': 3, 'pwma': 4, 'pwmb': 5}
    para_index = {'title': 0, 'i_ar': 1, 'i_af': 2, 'i_br': 3, 'i_bf': 4, 'angle_com': 5, 'dutyA': 6, 'dutyB': 7, 'max_duty': 8, 'pwm_freq': 9}
    
    print("Handle data ...")
    f = open("data" + "/" + 'raw_data_' + file_marker + '.txt', "r")
    raw_data = f.readlines()
    f.close()
    data_length = len(raw_data)
    print("Data has ", data_length)

    # collect data
    i_ar = np.zeros([data_length, 1], dtype=np.float32)
    i_af = np.zeros([data_length, 1], dtype=np.float32)
    # sign_ar = np.zeros([data_length, 1], dtype=np.float32)
    # sign_af = np.zeros([data_length, 1], dtype=np.float32)
    
    i_br = np.zeros([data_length, 1], dtype=np.float32)
    i_bf = np.zeros([data_length, 1], dtype=np.float32)
    # sign_br = np.zeros([data_length, 1], dtype=np.float32)
    # sign_bf = np.zeros([data_length, 1], dtype=np.float32)
    
    current_A = np.zeros([data_length, 1], dtype=np.float32)
    current_A_slope = np.zeros([data_length, 1], dtype=np.float32)
    current_B = np.zeros([data_length, 1], dtype=np.float32)
    current_B_slope = np.zeros([data_length, 1], dtype=np.float32)
    current_A_deri = np.zeros([1, data_length], dtype=np.float32)
    current_B_deri = np.zeros([1, data_length], dtype=np.float32)
    angle_com = np.zeros([data_length, 1], dtype=np.int16)

    dutyA = np.zeros([data_length, 1], dtype=np.float32)
    dutyB = np.zeros([data_length, 1], dtype=np.float32)
    max_duty = np.zeros([data_length, 1], dtype=np.float32)
    pwm_freq = np.zeros([data_length, 1], dtype=np.float32)

    inductorA = np.zeros([data_length, 1], dtype=np.float32)
    inductorB = np.zeros([data_length, 1], dtype=np.float32)

    # ib = np.zeros([data_length, 1], dtype=np.float32)
    # angle = np.zeros([data_length, 1], dtype=np.float32)
    # pwma = np.zeros([data_length, 1], dtype=np.float32)
    # pwmb = np.zeros([data_length, 1], dtype=np.float32)
    save_data = {}


    current_center = 2.32
    cnt = 0
    c1 = 0
    dc = 0
    dc_cnt = 0
    rpc_fg = 0
    CONF_CNT = 5
    a = []
    for data in raw_data:
        data_list = data.split(',')
        if len(data_list) == COMMA_DATA_LEN:
            i_ar[cnt] = float(data_list[para_index['i_ar']])
            i_af[cnt] = float(data_list[para_index['i_af']])
            i_br[cnt] = float(data_list[para_index['i_br']])
            i_bf[cnt] = float(data_list[para_index['i_bf']])
            angle_com[cnt] = int(data_list[para_index['angle_com']],16)
            dutyA[cnt] = float(data_list[para_index['dutyA']])
            dutyB[cnt] = float(data_list[para_index['dutyB']])
            max_duty[cnt] = float(data_list[para_index['max_duty']])
            pwm_freq[cnt] = float(data_list[para_index['pwm_freq']])
            cnt += 1
        else:
            print("Warning !!! List size not match !!!, ", cnt)
            print(data_list)

    # Change PWM to duty 0~1
    # pwma = pwma / PERIOD_COUNT
    # pwmb = pwmb / PERIOD_COUNT

    # Handle current positive or negative by decide PWM duty
    # 20 is gain (AD8206), 0.1 is current sense resistor

    for n,i in enumerate(dutyA):
        dutyA[n] = (0.5 + 0.5 * ((math.sin(((2*3.14)/pwm_freq[1])*(dutyA[n]+1)+3.14)) * max_duty[1]))
        dutyB[n] = (0.5 + 0.5 * ((math.cos(((2*3.14)/pwm_freq[1])*(dutyB[n]+1)+3.14)) * max_duty[1]))

    i_ar = -(((0.075+(i_ar*5/1024))-current_center)/(20*ra))
    i_af = -(((0.075+(i_af*5/1024))-current_center)/(20*ra))
    i_br = -(((0.075+(i_br*5/1024))-current_center)/(20*rb))
    i_bf = -(((0.075+(i_bf*5/1024))-current_center)/(20*rb))
    
    current_A = -(i_af - i_ar)/2
    current_A_slope = -(i_af + i_ar)/1/(0.00005)
    # current_A_slope = diff(((current_A)).flatten())
    current_B = -(i_bf - i_br)/2
    current_B_slope = -(i_bf + i_br)/1/(0.00005)
    # current_B_slope = diff((current_B).flatten())

    angle_com = angle_com.astype(np.float32)
    angle_com = angle_com*360/16384

    current_A_deri = diff((current_A).flatten())
    current_B_deri = diff((current_B).flatten())
    # current_A_slope = current_A_deri.reshape((data_length-1,1))
    # current_B_slope = current_B_deri.reshape((data_length-1,1))
    inductorA = (5-4.66*current_A)/current_A_slope
    inductorB = (5-4.87*current_B)/current_B_slope

    
    data_length = len(i_ar)
    avg_angle_com = np.zeros([int(pwm_freq[1]),1], dtype=np.float32)
    avg_current_A = np.zeros([int(pwm_freq[1]),1], dtype=np.float32)
    avg_current_A_slope = np.zeros([int(pwm_freq[1]),1], dtype=np.float32)
    avg_current_B = np.zeros([int(pwm_freq[1]),1], dtype=np.float32)
    avg_current_B_slope = np.zeros([int(pwm_freq[1]),1], dtype=np.float32)

    for i in range(int(data_length/pwm_freq[1])):        
        avg_current_A = avg_current_A + current_A[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[0])]
        avg_current_B = avg_current_B + current_B[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[0])]
        avg_current_A_slope = avg_current_A_slope + current_A_slope[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[0])]
        avg_current_B_slope = avg_current_B_slope + current_B_slope[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[0])]
        avg_angle_com = avg_angle_com + angle_com[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[0])]
    
    avg_current_A = avg_current_A/int(data_length/pwm_freq[1])
    avg_current_B = avg_current_B/int(data_length/pwm_freq[1])
    avg_current_A_slope = avg_current_A_slope/int(data_length/pwm_freq[1])
    avg_current_B_slope = avg_current_B_slope/int(data_length/pwm_freq[1-1])
    avg_angle_com = avg_angle_com/int(data_length/pwm_freq[1])

    # avg_inductor_A = np.zeros([int(pwm_freq[1]),1], dtype=np.float32)
    # avg_inductor_B = np.zeros([int(pwm_freq[1]),1], dtype=np.float32)
    # avg_inductor_A = (5-4.66*avg_current_A)/avg_current_A_slope
    # avg_inductor_B = (5-4.66*avg_current_B)/avg_current_B_slope
    
    # use new mothod to calculate average
    inductance_A = np.zeros([data_length, 2], dtype=np.float32)
    inductance_B = np.zeros([data_length, 2], dtype=np.float32)
    current_A_data = np.zeros([data_length, 2], dtype=np.float32)
    current_B_data = np.zeros([data_length, 2], dtype=np.float32)
    angle_com = [round(num, 1) for num in angle_com.flatten()]
    angle_com = np.reshape(angle_com, (1, len(angle_com))).T
    
    for i in range(len(angle_com)):
        inductance_A[i] = np.hstack((angle_com[i],current_A_slope[i]))
        inductance_B[i] = np.hstack((angle_com[i],current_B_slope[i]))
        current_A_data[i] = np.hstack((angle_com[i],current_A[i]))
        current_B_data[i] = np.hstack((angle_com[i],current_B[i]))
    
    inductance_A = sorted(inductance_A,key=lambda x:x[0])
    inductance_A = np.array(inductance_A)
    inductance_B = sorted(inductance_B,key=lambda x:x[0])
    inductance_B = np.array(inductance_B)
    current_A_data = sorted(current_A_data,key=lambda x:x[0])
    current_A_data = np.array(current_A_data)
    current_B_data = sorted(current_B_data,key=lambda x:x[0])
    current_B_data = np.array(current_B_data)
    
    inductance_A = [[key, mean(map(lambda x: x[1], list(group)))] for key, group in groupby(inductance_A, lambda x:x[0])]
    inductance_B = [[key, mean(map(lambda x: x[1], list(group)))] for key, group in groupby(inductance_B, lambda x:x[0])]
    inductance_A = np.array(inductance_A)
    inductance_B = np.array(inductance_B)
    
    print(len(inductance_A))
    # print((inductance_A))

    current_A_data = [[key, mean(map(lambda x: x[1], list(group)))] for key, group in groupby(current_A_data, lambda x:x[0])]
    current_B_data = [[key, mean(map(lambda x: x[1], list(group)))] for key, group in groupby(current_B_data, lambda x:x[0])]
    current_A_data = np.array(current_A_data)
    current_B_data = np.array(current_B_data)
    
    print((current_A_data.shape))
    # print((current_A_data))
    avg_inductor_A = np.zeros([len(inductance_A),1], dtype=np.float32)
    avg_inductor_B = np.zeros([len(inductance_A),1], dtype=np.float32)
    avg_inductor_A = (5-4.66*current_A_data[:,1])/inductance_A[:,1]
    avg_inductor_B = (5-4.66*current_B_data[:,1])/inductance_A[:,1]
    
    save_data["i_ar"] = i_ar
    save_data["i_af"] = i_af
    
    save_data["i_br"] = i_br
    save_data["i_bf"] = i_bf
    
    save_data["current_A"] = current_A
    # iBB
    # save_data["current_A"] = -current_A
    save_data["current_A_slope"] = current_A_slope

    save_data["current_B"] = current_B
    # save_data["current_B"] = -current_B
    save_data["current_B_slope"] = current_B_slope
    
    save_data["angle_com"] = angle_com

    save_data["dutyA"] = dutyA
    save_data["dutyB"] = dutyB

    save_data["max_duty"] = max_duty
    save_data["pwm_freq"] = pwm_freq

    save_data["inductorA"] = inductorA
    save_data["inductorB"] = inductorB

    save_data["avg_current_A"] = avg_current_A
    save_data["avg_current_A_slope"] = avg_current_A_slope
    save_data["avg_current_B"] = avg_current_B
    save_data["avg_current_B_slope"] = avg_current_B_slope

    save_data["avg_angle_com"] = avg_angle_com

    save_data["avg_inductor_A"] = avg_inductor_A
    save_data["avg_inductor_B"] = avg_inductor_B
    
    save_data["current_A_deri"] = current_A_deri
    save_data["current_B_deri"] = current_B_deri

    save_data["inductance_A"] = inductance_A
    save_data["inductance_B"] = inductance_B

    save_data["current_A_data"] = current_A_data
    save_data["current_B_data"] = current_B_data

    delat_t = (1/PWM)*pwm_freq[1]*16 * (data_length/pwm_freq[1])  # 16是一個特定duty有16個pwm

    print("Saving data to pickle file ...")
    save2pickle("data"+'/data_'+file_marker+'.pickle', save_data)

    print("Saving figure ...")
    plot_current(file_marker, delat_t)
    plot_current2(file_marker, delat_t)

    