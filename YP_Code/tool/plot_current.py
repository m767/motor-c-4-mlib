import pickle
import matplotlib.pyplot as plt
from mypickle import save2pickle, load_pickle
import os
import numpy as np
import collections
from functools import cmp_to_key

FIG_DIR = "figure"
def plot_current(file_marker, DELTA_T):
    try:
        os.mkdir(FIG_DIR)
    except:
        pass
    try:
        os.mkdir(FIG_DIR+"/"+file_marker)
    except:
        pass

    save_data = load_pickle("data/data_"+file_marker+'.pickle')

    i_ar = save_data["i_ar"]
    i_af = save_data["i_af"]
    # sign_ar = save_data["sign_ar"]
    # sign_af = save_data["sign_af"]

    i_br = save_data["i_br"]
    i_bf = save_data["i_bf"]
    # sign_br = save_data["sign_br"]
    # sign_bf = save_data["sign_bf"]

    current_A = save_data["current_A"]
    current_A_slope = save_data["current_A_slope"]
    
    current_B = save_data["current_B"]
    current_B_slope = save_data["current_B_slope"]

    angle_com = save_data["angle_com"]

    dutyA = save_data["dutyA"]
    dutyB = save_data["dutyB"]

    max_duty = save_data["max_duty"]
    pwm_freq = save_data["pwm_freq"]
    inductorA = save_data["inductorA"]
    inductorB = save_data["inductorB"]

    current_A_deri = save_data["current_A_deri"]
    current_B_deri = save_data["current_B_deri"]

    inductance_A = save_data["inductance_A"]
    inductance_B = save_data["inductance_B"]

    current_A_data = save_data["current_A_data"]
    current_B_data = save_data["current_B_data"]
    # ib = save_data["ib"]
    # angle = save_data["angle"]
    # pwma = save_data["pwma"]
    # pwmb = save_data["pwmb"]

    # t = np.linspace(0, i_ar.shape[0]-1, i_ar.shape[0]) * DELTA_T
    # t = np.linspace(0, current_A.shape[0]-1, current_A.shape[0]) * DELTA_T
    data_length = len(i_ar)
    t = np.linspace(0, int(data_length/pwm_freq[1])*0.0008, data_length)
    ### ==================================================================================== ###
    # fig = plt.figure(figsize=(20, 24))
    # # first figure
    # plt.subplot(311)
    # plt.plot(t, angle_com)
    # plt.grid(True)
    # plt.ylabel('angle(deg) ', fontsize=24, position=(0,1), rotation="horizontal")
    # plt.title('Sensing Angle'+ str(angle_com[-2]-angle_com[0]), fontsize=28)
    # plt.xticks(fontsize=20)
    # plt.yticks(fontsize=20)
    # plt.legend(['motor angle'], fontsize=20)
    # plt.text(1, 5, 'boxed italics text in data coords', style='italic')
    # fig.align_labels()
    # # second figure
    # plt.subplot(312)
    # plt.plot(t, dutyA)
    # plt.plot(t, dutyB)
    # plt.grid(True)
    # plt.ylabel('duty ', fontsize=24, position=(0,1), rotation="horizontal")
    # plt.title('Phase A B PWM', fontsize=28)
    # plt.xticks(fontsize=20)
    # plt.yticks(fontsize=20)
    # plt.legend(['pwmA','pwmB'], fontsize=20)
    # fig.align_labels()
    # # third figure
    # plt.subplot(313)
    # plt.plot(t, current_A)
    # plt.plot(t, current_B)
    # # plt.plot(t, dutyA)
    # # plt.plot(t, dutyB)
    # plt.grid(True)
    # plt.ylabel('I(A) ', fontsize=24, position=(0,1), rotation="horizontal")
    # plt.title('Phase A B current', fontsize=28)
    # plt.xticks(fontsize=20)
    # plt.yticks(fontsize=20)
    # plt.legend(['current_A','current_B'], fontsize=20)
    # fig.align_labels()
    # plt.savefig(FIG_DIR+"/"+file_marker + '/current_pwm_phaseAB_' + file_marker + '.png', dpi=300)

    # ### ==================================================================================== ###
    # fig = plt.figure(figsize=(20, 24))
    # # first figure
    # plt.subplot(311)
    # plt.plot(t, angle_com)
    # plt.grid(True)
    # plt.ylabel('angle(deg) ', fontsize=24, position=(0,1), rotation="horizontal")
    # plt.title('Sensing Angle', fontsize=28)
    # plt.xticks(fontsize=20)
    # plt.yticks(fontsize=20)
    # plt.legend(['motor angle'], fontsize=20)
    # fig.align_labels()
    # # second figure
    # plt.subplot(312)
    # plt.plot(t, dutyA)
    # plt.plot(t, dutyB)
    # plt.grid(True)
    # plt.ylabel('duty ', fontsize=24, position=(0,1), rotation="horizontal")
    # plt.title('Phase A B PWM', fontsize=28)
    # plt.xticks(fontsize=20)
    # plt.yticks(fontsize=20)
    # plt.legend(['pwmA','pwmB'], fontsize=20)
    # fig.align_labels()
    # # third figure
    # plt.subplot(313)
    # plt.plot(t, current_A_slope)
    # plt.plot(t, current_B_slope)
    # plt.grid(True)
    # plt.ylabel('A ', fontsize=24, position=(0,1), rotation="horizontal")
    # plt.title('Phase A B current slope', fontsize=28)
    # plt.xticks(fontsize=20)
    # plt.yticks(fontsize=20)
    # plt.legend(['current_A_slope','current_B_slope'], fontsize=20)
    # fig.align_labels()
    # plt.savefig(FIG_DIR+"/"+file_marker + '/current_slope_pwm_phaseB_' + file_marker + '.png', dpi=300)

    fig = plt.figure(figsize=(20, 38))
    fig.suptitle('Micro-stepping (1/' + str(int(pwm_freq[1]))+')', fontsize=50)
        # first figure
        # plt.subplot(511)
        # plt.plot(t, dutyA)
        # plt.plot(t, dutyB)
        # plt.grid(True)
        # plt.ylabel('duty ', fontsize=24, position=(0,1), rotation="horizontal")
        # plt.xlabel('t(s) ', fontsize=24, position=(1,0), rotation="horizontal")
        # plt.title('Phase A B PWM', fontsize=28)
        # plt.xticks(fontsize=20)
        # plt.yticks(fontsize=20)
        # plt.legend(['pwmA','pwmB'], fontsize=20)
        # fig.align_labels()
    # first figure
    plt.subplot(511)
    plt.plot(t, angle_com)
    plt.grid(True)
    plt.ylabel('angle(deg) ', fontsize=24, position=(0,1.1), rotation="horizontal")
    plt.xlabel('t(s) ', fontsize=24, position=(1,0), rotation="horizontal")
    # plt.title('Sensing Angle'+ str(angle_com[int(i*pwm_freq[1])]-angle_com[int(i*pwm_freq[1]+pwm_freq[1]-2)]), fontsize=28)
    plt.title('Sensing Angle', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(['motor angle'], fontsize=20)
    plt.text(1, 5, 'boxed italics text in data coords', style='italic')
    fig.align_labels()
    # second figure
    plt.subplot(512)
    plt.plot(t, current_A)
    plt.plot(t, current_B)
    plt.grid(True)
    plt.ylabel('I(A) ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('t(s) ', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Phase A B current', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(['current_A','current_B'], fontsize=20)
    fig.align_labels()
    # third figure
    plt.subplot(513)
    # plt.plot(t, current_A)
    # plt.plot(t, current_B)
    plt.plot(current_A_data[:,0],current_A_data[:,1], '.')
    plt.plot(current_B_data[:,0],current_B_data[:,1], '.')
    plt.grid(True)
    plt.ylabel('I(A) ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('Angle(Degree) ', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Phase A B current', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(['current_A','current_B'], fontsize=20)
    fig.align_labels()
    # third figure
    plt.subplot(514)
    plt.plot(t, current_A_slope)
    plt.plot(t, current_B_slope)
    plt.grid(True)
    plt.ylabel('V/s ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('t(s) ', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Phase A B current slope', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(['current_A_slope','current_B_slope'], fontsize=20)
    fig.align_labels()
    # third figure
    plt.subplot(515)
    # plt.plot(t[0:-1], current_A_deri)
    # plt.plot(t[0:-1], current_B_deri)
    # plt.plot(angle_com,current_A, '.')
    # plt.plot(angle_com,current_B, '.')
    plt.plot(inductance_A[:,0],inductance_A[:,1], '.')
    plt.plot(inductance_B[:,0],inductance_B[:,1], '.')
    # plt.plot(angle_com,current_B_slope, '.')
    plt.grid(True)
    plt.ylabel(' ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('Angle(Degree) ', fontsize=24, position=(1,0), rotation="horizontal")
    plt.ylabel('V/s ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.title('Phase A B current slope ', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(['current A inductance','current B inductance'], fontsize=20)
    fig.align_labels()
    plt.savefig(FIG_DIR+"/"+file_marker + '/current_pwm_phaseAB_' + file_marker + '.png', dpi=300)
