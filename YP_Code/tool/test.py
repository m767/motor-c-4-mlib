from itertools import groupby
from statistics import mean # 3 or greater

# if using 2.7
# def mean(x): return sum(x)/len(list(x))

a = [['foo', 13], 
['foo', 15], 
['bar', 14], 
['bar', 16],
['bar', 5]]

a = [[key, mean(map(lambda x: x[1], list(group)))] for key, group in groupby(a, lambda x:x[0])]
print(a)