import pickle
import matplotlib.pyplot as plt
from mypickle import save2pickle, load_pickle
import os
import numpy as np
import math

FIG_DIR = "figure"
def plot_current2(file_marker, DELTA_T):
    try:
        os.mkdir(FIG_DIR)
    except:
        pass
    try:
        os.mkdir(FIG_DIR+"/"+file_marker)
    except:
        pass

    save_data = load_pickle("data/data_"+file_marker+'.pickle')

    i_ar = save_data["i_ar"]
    i_af = save_data["i_af"]

    i_br = save_data["i_br"]
    i_bf = save_data["i_bf"]

    current_A = save_data["current_A"]
    current_A_slope = save_data["current_A_slope"]
    
    current_B = save_data["current_B"]
    current_B_slope = save_data["current_B_slope"]

    angle_com = save_data["angle_com"]

    dutyA = save_data["dutyA"]
    dutyB = save_data["dutyB"]
    
    max_duty = save_data["max_duty"]
    pwm_freq = save_data["pwm_freq"]

    inductorA = save_data["inductorA"]
    inductorB = save_data["inductorB"]

    avg_current_A = save_data["avg_current_A"]
    avg_current_A_slope = save_data["avg_current_A_slope"]
    avg_current_B = save_data["avg_current_B"]
    avg_current_B_slope = save_data["avg_current_B_slope"]

    avg_angle_com = save_data["avg_angle_com"]

    avg_inductor_A = save_data["avg_inductor_A"]
    avg_inductor_B = save_data["avg_inductor_B"]

    inductance_A = save_data["inductance_A"]
    inductance_B = save_data["inductance_B"]

    current_A_data = save_data["current_A_data"]
    current_B_data = save_data["current_B_data"]

    data_length = len(i_ar)
    # avg_angle_com = np.zeros([int(pwm_freq[1]),1], dtype=np.float32)
    # avg_current_A = np.zeros([int(pwm_freq[1]),1], dtype=np.float32)
    # avg_current_A_slope = np.zeros([int(pwm_freq[1]),1], dtype=np.float32)
    # avg_current_B = np.zeros([int(pwm_freq[1]),1], dtype=np.float32)
    # avg_current_B_slope = np.zeros([int(pwm_freq[1]),1], dtype=np.float32)
    # for i in range(int(data_length/pwm_freq[1])):        
    #     avg_current_A=avg_current_A + current_A[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[0])]
    #     avg_current_B=avg_current_B + current_B[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[0])]
    #     avg_current_A_slope=avg_current_A_slope + current_A_slope[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[0])]
    #     avg_current_B_slope=avg_current_B_slope + current_B_slope[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[0])]
    #     avg_angle_com=avg_angle_com + angle_com[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[0])]
    
    # avg_current_A = avg_current_A/int(data_length/pwm_freq[1])
    # avg_current_B = avg_current_B/int(data_length/pwm_freq[1])
    # avg_current_A_slope = avg_current_A_slope/int(data_length/pwm_freq[1])
    # avg_current_B_slope = avg_current_B_slope/int(data_length/pwm_freq[1-1])
    # avg_angle_com = avg_angle_com/int(data_length/pwm_freq[1])

    # avg_current_A = dutyA[0:int(pwm_freq[1])]
    # avg_current_B = dutyB[0:int(pwm_freq[1])]
    # for n,i in enumerate(avg_current_A_slope):
    #     avg_current_A[n] =  (0 + 0.6 * ((math.cos((((2*3.14)/pwm_freq[1]))*(n)+1.9)) *1))
    #     avg_current_B[n] = -(0 + 0.6 * ((math.sin((((2*3.14)/pwm_freq[1]))*(n)+1.9)) *1))
    #     avg_current_A_slope[n] = (-0.07 + 0.1 * ((math.cos(((4*3.14)/pwm_freq[1])*(n))) * 1))
    #     avg_current_B_slope[n] = -(avg_current_A_slope[n]) -0.14 
    
    # avg_inductor_A = np.zeros([int(pwm_freq[1]),1], dtype=np.float32)
    # avg_inductor_B = np.zeros([int(pwm_freq[1]),1], dtype=np.float32)
    # avg_inductor_A = (5-4.66*avg_current_A)/avg_current_A_slope
    # avg_inductor_B = (5-4.66*avg_current_B)/avg_current_B_slope
    for i in range(int(pwm_freq[1])):
        if inductorA[i] >=1000:
            inductorA[i] = 1000
        elif inductorA[i] <=-1000:
            inductorA[i] = -1000
        if inductorB[i] >=1000:
            inductorB[i] = 1000
        elif inductorB[i] <=-1000:
            inductorB[i] = -1000
    # t = np.linspace(0, i_ar.shape[0]-1, i_ar.shape[0]) * DELTA_T
    # t = np.linspace(0, current_A.shape[0]-1, current_A.shape[0]) * DELTA_T
    # t = np.linspace(0, DELTA_T, current_A.shape[0])
    t = np.linspace(0, int(pwm_freq[1])*0.0008, int(pwm_freq[1])-1)
    t0 = np.linspace(0, int(pwm_freq[1]), current_A.shape[0])
    
    print("Figure have", int(data_length/pwm_freq[1]), "part")
    ### ==================================================================================== ###
    # for i in range(int(data_length/pwm_freq[1])):
        # fig = plt.figure(figsize=(20, 38))
        # fig.suptitle('Micro-stepping (1/' + str(int(pwm_freq[1]))+')', fontsize=50)
        # first figure
        # plt.subplot(511)
        # plt.plot(t[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[1])], dutyA[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[1])])
        # plt.plot(t[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[1])], dutyB[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[1])])
        # plt.grid(True)
        # plt.ylabel('duty ', fontsize=24, position=(0,1), rotation="horizontal")
        # plt.xlabel('t(s) ', fontsize=24, position=(1,0), rotation="horizontal")
        # plt.title('Phase A B PWM', fontsize=28)
        # plt.xticks(fontsize=20)
        # plt.yticks(fontsize=20)
        # plt.legend(['pwmA','pwmB'], fontsize=20)
        # fig.align_labels()
        # second figure
        # plt.subplot(512)
        # plt.plot(t[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[1])], angle_com[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[1])])
        # plt.grid(True)
        # plt.ylabel('angle(deg) ', fontsize=24, position=(0,1.1), rotation="horizontal")
        # plt.xlabel('t(s) ', fontsize=24, position=(1,0), rotation="horizontal")
        # plt.title('Sensing Angle'+ str(angle_com[int(i*pwm_freq[1])]-angle_com[int(i*pwm_freq[1]+pwm_freq[1]-2)]), fontsize=28)
        # plt.title('Sensing Angle', fontsize=28)
        # plt.xticks(fontsize=20)
        # plt.yticks(fontsize=20)
        # plt.legend(['motor angle'], fontsize=20)
        # plt.text(1, 5, 'boxed italics text in data coords', style='italic')
        # fig.align_labels()
        # third figure
        # plt.subplot(513)
        # plt.plot(t[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[1])], current_A[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[1])])
        # plt.plot(t[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[1])], current_B[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[1])])
        # plt.grid(True)
        # plt.ylabel('I(A) ', fontsize=24, position=(0,1), rotation="horizontal")
        # plt.xlabel('t(s) ', fontsize=24, position=(1,0), rotation="horizontal")
        # plt.title('Phase A B current', fontsize=28)
        # plt.xticks(fontsize=20)
        # plt.yticks(fontsize=20)
        # plt.legend(['current_A','current_B'], fontsize=20)
        # fig.align_labels()
        # forth figure
        # plt.subplot(514)
        # plt.plot(t[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[1])], current_A_slope[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[1])])
        # plt.plot(t[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[1])], current_B_slope[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[1])])
        # plt.grid(True)
        # plt.ylabel(' ', fontsize=24, position=(0,1), rotation="horizontal")
        # plt.xlabel('t(s) ', fontsize=24, position=(1,0), rotation="horizontal")
        # plt.title('Phase A B current slope', fontsize=28)
        # plt.xticks(fontsize=20)
        # plt.yticks(fontsize=20)
        # plt.legend(['current_A_slope','current_B_slope'], fontsize=20)
        # fig.align_labels()
        # fifth figure
        # plt.subplot(515)
        # plt.plot(t[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[1])], inductorA[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[1])])
        # plt.plot(t[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[1])], inductorB[int(i*pwm_freq[1]):int(i*pwm_freq[1]+pwm_freq[1])])
        # plt.grid(True)
        # plt.ylabel(' ', fontsize=24, position=(0,1), rotation="horizontal")
        # plt.xlabel('t(s) ', fontsize=24, position=(1,0), rotation="horizontal")
        # plt.title('Phase A B current slope', fontsize=28)
        # plt.xticks(fontsize=20)
        # plt.yticks(fontsize=20)
        # plt.legend(['current_A_slope','current_B_slope'], fontsize=20)
        # fig.align_labels()
        # plt.savefig(FIG_DIR+"/"+file_marker + '/current_pwm_phaseAB_' + file_marker + '_' + 'part' + str(i) + '.png', dpi=300)
        # print("Part", str(i),"Done !")

    ### ==================================================================================== ###
    fig = plt.figure(figsize=(20, 38))
    fig.suptitle('Micro-stepping (1/' + str(int(pwm_freq[1]))+')', fontsize=50)
    # first figure
    plt.subplot(511)
    plt.plot(t, avg_angle_com[0:-1])
    plt.grid(True)
    plt.xlabel('t(s) ', fontsize=24, position=(1,0), rotation="horizontal")
    plt.ylabel('angle(deg) ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.title('Sensing Angle', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(['motor angle'], fontsize=20)
    fig.align_labels()
    # second figure
    plt.subplot(512)
    plt.plot(t, avg_current_A[0:-1])
    plt.plot(t, avg_current_B[0:-1])
    plt.grid(True)
    plt.xlabel('t(s) ', fontsize=24, position=(1,0), rotation="horizontal")
    plt.ylabel('I(A) ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.title('Phase A B current', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(['current_A','current_B'], fontsize=20)
    plt.text(0, -2, 'Position std: '+str(np.std(avg_angle_com[0:-1],ddof=1))+'\n'+'Amax:'+str(max(avg_current_A[0:-1]))+', '+'Amin'+str(min(avg_current_A[0:-1]))+'\n'+'Bmax:'+str(max(avg_current_B[0:-1]))+', '+'Bmin'+str(min(avg_current_B[0:-1])), fontsize=20)
    fig.align_labels()
    # plt.savefig(FIG_DIR+"/"+file_marker + '/current_avg_pwm_phaseB_' + file_marker + '.png', dpi=300)
    fig.align_labels()
    # third figure
    plt.subplot(513)
    plt.plot(t, avg_current_A_slope[0:-1])
    plt.plot(t, avg_current_B_slope[0:-1])
    plt.grid(True)
    plt.xlabel('t(s) ', fontsize=24, position=(1,0), rotation="horizontal")
    plt.ylabel('I/s ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.title('Phase A B current slope', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(['current_A_slope','current_B_slope'], fontsize=20)
    fig.align_labels()
    # forth figure
    plt.subplot(514)
    # plt.plot(t, avg_inductor_A[0:-1])
    # plt.plot(t, avg_inductor_B[0:-1])
    plt.plot(current_A_data[:,0],avg_inductor_A)
    plt.plot(current_A_data[:,0],avg_inductor_B)
    plt.grid(True)
    plt.xlabel('Angle(Degree) ', fontsize=24, position=(1,0), rotation="horizontal")
    plt.ylabel('L(H)', fontsize=24, position=(0,1), rotation="horizontal")
    plt.title('L = (V-Ri)*dt/di ', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(['current_A_slope','current_B_slope'], fontsize=20)
    fig.align_labels()
    plt.savefig(FIG_DIR+"/"+file_marker + '/current_avg_pwm_phaseB_' + file_marker + '.png', dpi=300)

    ### ==================================================================================== ###

    # rms_ia_square = np.mean((ia*ia))
    # rms_ib_square = np.mean((ib*ib))
    # P_loss_A = rms_ia_square*6.485
    # P_loss_B = rms_ib_square*6.165
    # P_in_A = np.mean(np.abs(ia*5))
    # P_in_B = np.mean(np.abs(ib*5))
    # P_efficient_A = (P_in_A - P_loss_A) / P_in_A * 100
    # P_efficient_B = (P_in_B - P_loss_B) / P_in_B * 100
    # P_all = (P_in_A + P_in_B - P_loss_A - P_loss_B) / (P_in_A + P_in_B) * 100
    speed = 7 * pwm_freq[1] / (20000/16)

    report_str = ""
    # report_str += "Data size is " +  str(ia.shape[0]) + "\n"
    # report_str += "===== sensor ele angle =====" + "\n"
    # report_str += "max: " + str(np.max(sele_dangle)) + "\n"
    # report_str += "min: " + str(np.min(sele_dangle)) + "\n"
    # report_str += "mean: " + str(np.mean(sele_dangle)) + "\n"
    # report_str += "===== command ele angle =====" + "\n"
    # report_str += "max: " + str(np.max(cele_dangle)) + "\n"
    # report_str += "min: " + str(np.min(cele_dangle)) + "\n"
    # report_str += "mean: " + str(np.mean(cele_dangle)) + "\n"
    # report_str += "===== theta error (degree) =====" + "\n"
    # report_str += "max: " + str(np.max(th_er)) + "\n"
    # report_str += "min: " + str(np.min(th_er)) + "\n"
    # report_str += "mean: " + str(np.mean(th_er)) + "\n"
    # report_str += "std: " + str(np.std(th_er)) + "\n"
    # report_str += "MSE: " + str(np.mean(np.square(th_er))) + "\n"
    # report_str += "N_STEP: " + str(1.8/(np.max(th_er))) + ", " + str(1.8/(np.min(th_er))) + "\n"
    # report_str += "===== consume power (watt) =====" + "\n"
    # report_str += "max: " + str(np.max(power)) + "\n"
    # report_str += "min: " + str(np.min(power)) + "\n"
    # report_str += "mean: " + str(np.mean(power)) + "\n"
    # report_str += "std: " + str(np.std(power)) + "\n"
    # report_str += "sum: " + str(np.sum(np.abs(power))) + " J\n"
    # report_str += "===== current RMS Square (watt) =====" + "\n"
    # report_str += "rms_ia_square: " + str(rms_ia_square) + " A\n"
    # report_str += "rms_ib_square: " + str(rms_ib_square) + " A\n"
    # report_str += "===== current RMS (watt) =====" + "\n"
    # report_str += "rms_ia: " + str(np.sqrt(rms_ia_square)) + " A\n"
    # report_str += "rms_ib: " + str(np.sqrt(rms_ib_square)) + " A\n"
    # report_str += "===== P_loss (watt) =====" + "\n"
    # report_str += "P_loss_A: " + str(rms_ia_square*6.485) + " W\n"
    # report_str += "P_loss_B: " + str(rms_ia_square*6.165) + " W\n"
    # report_str += "===== P_in (watt) =====" + "\n"
    # report_str += "P_in_A: " + str(P_in_A) + " W\n"
    # report_str += "P_in_B: " + str(P_in_B) + " W\n"
    # report_str += "===== Eta (H) (%) =====" + "\n"
    # report_str += "P_efficient_A: " + str(P_efficient_A) + " %\n"
    # report_str += "P_efficient_B: " + str(P_efficient_B) + " %\n"
    # report_str += "P_all: " + str(P_all) + " %\n"

    report_str += "=====  SPEED (omega) =====" + "\n"
    report_str += "Speed: " + str(speed) + " %\n"
    print(report_str)

    # f = open(FIG_DIR + "/" + file_marker + "/report_" + file_marker + ".txt", "w")
    # f.write(report_str)
    # f.close()