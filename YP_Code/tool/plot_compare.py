import pickle
import matplotlib.pyplot as plt
from mypickle import save2pickle, load_pickle
import os
import numpy as np
from numpy import diff
import collections
from functools import cmp_to_key

FIG_DIR = "figure"
# def plot_control(file_marker, DELTA_T):
if __name__ == "__main__":
    try:
        os.mkdir(FIG_DIR)
    except:
        pass
    try:
        os.mkdir(FIG_DIR+"/"+"compare")
    except:
        pass
    
    #1/8
    save_data1 = load_pickle("data/data_"+"open_8step_10"+'.pickle')
    save_data2 = load_pickle("data/data_"+"open_8step_11_06"+'.pickle')
    save_data3 = load_pickle("data/data_"+"open_8step_12_08"+'.pickle')
    save_data4 = load_pickle("data/data_"+"open_8step_14_095"+'.pickle')
    #1/16 
    # save_data1 = load_pickle("data/data_"+"open_16step_5_04"+'.pickle')
    # save_data2 = load_pickle("data/data_"+"open_16step_3_06"+'.pickle')
    # save_data3 = load_pickle("data/data_"+"open_16step_2_08"+'.pickle')
    # save_data4 = load_pickle("data/data_"+"open_16step_1_095"+'.pickle')

    PWMA1 = save_data1["PWMA"] 
    PWMB1 = save_data1["PWMB"]
    current_A1 = save_data1["current_A"]
    current_A_slope1 = save_data1["current_A_slope"]    
    current_B1 = save_data1["current_B"]
    current_B_slope1 = save_data1["current_B_slope"]

    PWMA4 = save_data4["PWMA"] 
    PWMB4 = save_data4["PWMB"]
    current_A4 = save_data4["current_A"]
    current_A_slope4 = save_data4["current_A_slope"]    
    current_B4 = save_data4["current_B"]
    current_B_slope4 = save_data4["current_B_slope"]

    PWMA2 = save_data2["PWMA"] 
    PWMB2 = save_data2["PWMB"]
    current_A2 = save_data2["current_A"]
    current_A_slope2 = save_data2["current_A_slope"]    
    current_B2 = save_data2["current_B"]
    current_B_slope2 = save_data2["current_B_slope"]

    PWMA3 = save_data3["PWMA"] 
    PWMB3 = save_data3["PWMB"]
    current_A3 = save_data3["current_A"]
    current_A_slope3 = save_data3["current_A_slope"]    
    current_B3 = save_data3["current_B"]
    current_B_slope3 = save_data3["current_B_slope"]

    # i_ar = save_data1["i_ar"]
    # i_af = save_data1["i_af"]
    # i_br = save_data1["i_br"]
    # i_bf = save_data1["i_bf"]
    # fir_output = save_data1["fir_output"]
    # th_er = save_data1["th_er"]
    # th_cum = save_data1["th_cum"]  
    # I_ki = save_data1["I_ki"] 
    # PI_kp = save_data1["PI_kp"]
    # PI_ki = save_data1["PI_ki"] 

    current_A_deri1 = diff((current_A1).flatten())/(0.00005)
    current_B_deri1 = diff((-current_B1).flatten())/(0.00005)
    current_A_deri2 = diff((current_A2).flatten())/(0.00005)
    current_B_deri2 = diff((-current_B2).flatten())/(0.00005)
    current_A_deri3 = diff((current_A3).flatten())/(0.00005)
    current_B_deri3 = diff((-current_B3).flatten())/(0.00005)
    current_A_deri4 = diff((current_A4).flatten())/(0.00005)
    current_B_deri4 = diff((-current_B4).flatten())/(0.00005)
    # data_length = len(fir_output)
    # inductance_A = np.zeros([data_length, 2], dtype=np.float32)
    # inductance_B = np.zeros([data_length, 2], dtype=np.float32)
    # current_A_data = np.zeros([data_length, 2], dtype=np.float32)
    # current_B_data = np.zeros([data_length, 2], dtype=np.float32)
    # angle_com = [round(num, 1) for num in fir_output.flatten()]
    # angle_com = np.reshape(angle_com, (1, len(angle_com))).T

    # for i in range(len(angle_com)):
    #     inductance_A[i] = np.hstack((angle_com[i],current_A_slope[i]))
    #     inductance_B[i] = np.hstack((angle_com[i],current_B_slope[i]))
    #     current_A_data[i] = np.hstack((angle_com[i],current_A[i]))
    #     current_B_data[i] = np.hstack((angle_com[i],current_B[i]))

    # inductance_A = sorted(inductance_A,key=lambda x:x[0])
    # inductance_A = np.array(inductance_A)
    # inductance_B = sorted(inductance_B,key=lambda x:x[0])
    # inductance_B = np.array(inductance_B)
    # current_A_data = sorted(current_A_data,key=lambda x:x[0])
    # current_A_data = np.array(current_A_data)
    # current_B_data = sorted(current_B_data,key=lambda x:x[0])
    # current_B_data = np.array(current_B_data)

    # inductance_A = np.array(inductance_A)
    # inductance_B = np.array(inductance_B)
    # current_A_data = np.array(current_A_data)
    # current_B_data = np.array(current_B_data)


    # t = np.linspace(0, i_ar.shape[0]-1, i_ar.shape[0]) * DELTA_T
    # t = np.linspace(0, current_A.shape[0]-1, current_A.shape[0]) * DELTA_T
    data_length = len(PWMA1)
    t = np.linspace(0, 360, data_length) #取樣週期是固定的1,1ms
    ### ==================================================================================== ###
    
    fig = plt.figure(figsize=(20, 45))
    # 1/8
    fig.suptitle('Micro-stepping 1/8 Stepping\n' , fontsize=50)
    # # 1/16
    # fig.suptitle('Micro-stepping 1/16 Stepping\n' , fontsize=50)
    # first figure
    plt.subplot(411)
    plt.plot(t, PWMA1)
    plt.plot(t, PWMB1)
    plt.plot(t, PWMA2)
    plt.plot(t, PWMB2)
    plt.plot(t, PWMA3)
    plt.plot(t, PWMB3)
    plt.plot(t, PWMA4)
    plt.plot(t, PWMB4)
    plt.grid(True)
    plt.ylabel('Duty ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('ele angle(degree) ', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Phase A B PWM', fontsize=28,)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    # 1/8
    plt.legend(['pwmA, Irms=0.21','pwmB, Irms=0.20','pwmA, Irms=0.31','pwmB, Irms=0.30','pwmA, Irms=0.40','pwmB, Irms=0.38','pwmA, Irms=0.47','pwmB, Irms=0.43'], fontsize=17)
    # # 1/16
    # plt.legend(['pwmA, Irms=0.22','pwmB, Irms=0.20','pwmA, Irms=0.31','pwmB, Irms=0.31','pwmA, Irms=0.40','pwmB, Irms=0.38','pwmA, Irms=0.47','pwmB, Irms=0.40'], fontsize=20)
    fig.align_labels()

    # first figure
    plt.subplot(412)
    plt.plot(t, current_A1)
    plt.plot(t, current_B1)
    plt.plot(t, current_A2)
    plt.plot(t, current_B2)
    plt.plot(t, current_A3)
    plt.plot(t, current_B3)
    plt.plot(t, current_A4)
    plt.plot(t, current_B4)
    plt.grid(True)
    plt.ylabel('I(A) ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('ele angle(degree) ', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Phase A B Current', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    # 1/8
    plt.legend(['pwmA, Irms=0.21','pwmB, Irms=0.20','pwmA, Irms=0.31','pwmB, Irms=0.30','pwmA, Irms=0.40','pwmB, Irms=0.38','pwmA, Irms=0.47','pwmB, Irms=0.43'], fontsize=17)
    # # 1/16
    # plt.legend(['pwmA, Irms=0.22','pwmB, Irms=0.20','pwmA, Irms=0.31','pwmB, Irms=0.31','pwmA, Irms=0.40','pwmB, Irms=0.38','pwmA, Irms=0.47','pwmB, Irms=0.40'], fontsize=20)
    fig.align_labels()
    
    # second figure
    plt.subplot(413)
    plt.plot(t, current_A_slope1)
    plt.plot(t, current_B_slope1)
    plt.plot(t, current_A_slope2)
    plt.plot(t, current_B_slope2)
    plt.plot(t, current_A_slope3)
    plt.plot(t, current_B_slope3)
    plt.plot(t, current_A_slope4)
    plt.plot(t, current_B_slope4)
    plt.grid(True)
    plt.ylabel('I/s ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('ele angle(degree) ', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Phase A B Current Slope', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    # 1/8
    plt.legend(['pwmA, Irms=0.21','pwmB, Irms=0.20','pwmA, Irms=0.31','pwmB, Irms=0.30','pwmA, Irms=0.40','pwmB, Irms=0.38','pwmA, Irms=0.47','pwmB, Irms=0.43'], fontsize=17)
    # 1/16
    # plt.legend(['pwmA, Irms=0.22','pwmB, Irms=0.20','pwmA, Irms=0.31','pwmB, Irms=0.31','pwmA, Irms=0.40','pwmB, Irms=0.38','pwmA, Irms=0.47','pwmB, Irms=0.40'], fontsize=20)
    fig.align_labels()

    # second figure
    plt.subplot(414)
    plt.plot(t[:-1], current_A_deri1)
    plt.plot(t[:-1], current_B_deri1)
    plt.plot(t[:-1], current_A_deri2)
    plt.plot(t[:-1], current_B_deri2)
    plt.plot(t[:-1], current_A_deri3)
    plt.plot(t[:-1], current_B_deri3)
    plt.plot(t[:-1], current_A_deri4)
    plt.plot(t[:-1], current_B_deri4)
    plt.grid(True)
    plt.ylabel('I/s ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('ele angle(degree) ', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Phase A B Current Derivative', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    # 1/8
    plt.legend(['pwmA, Irms=0.21','pwmB, Irms=0.20','pwmA, Irms=0.31','pwmB, Irms=0.30','pwmA, Irms=0.40','pwmB, Irms=0.38','pwmA, Irms=0.47','pwmB, Irms=0.43'], fontsize=17)
    # 1/16
    # plt.legend(['pwmA, Irms=0.22','pwmB, Irms=0.20','pwmA, Irms=0.31','pwmB, Irms=0.31','pwmA, Irms=0.40','pwmB, Irms=0.38','pwmA, Irms=0.47','pwmB, Irms=0.40'], fontsize=20)
    fig.align_labels()

    plt.subplots_adjust(hspace=0.3)
    # 1/8
    plt.savefig(FIG_DIR+"/"+"compare" + '/' + "compare_8step" + '.png', dpi=300)
    # # 1/16
    # plt.savefig(FIG_DIR+"/"+"compare" + '/' + "compare_16step" + '.png', dpi=300)
