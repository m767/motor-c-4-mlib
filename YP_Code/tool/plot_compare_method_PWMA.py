import pickle
import matplotlib.pyplot as plt
from mypickle import save2pickle, load_pickle
import os
import numpy as np
from numpy import diff
import collections
from functools import cmp_to_key

FIG_DIR = "figure"
# def plot_control(file_marker, DELTA_T):
if __name__ == "__main__":
    try:
        os.mkdir(FIG_DIR)
    except:
        pass
    try:
        os.mkdir(FIG_DIR+"/"+"compare")
    except:
        pass
    
    #1/8
    # save_data1 = load_pickle("data/data_"+"open_8step_15"+'.pickle')
    # save_data2 = load_pickle("data/data_"+"old_8step_2_out"+'.pickle')
    # save_data3 = load_pickle("data/data_"+"new_8step_5_in"+'.pickle')
    # save_data4 = load_pickle("data/data_"+"old_8step_7_in"+'.pickle')
    #1/16 
    save_data1 = load_pickle("data/data_"+"open_16step_6"+'.pickle')
    save_data2 = load_pickle("data/data_"+"new_16step_1_out"+'.pickle')
    save_data3 = load_pickle("data/data_"+"new_16step_4_in"+'.pickle')
    save_data4 = load_pickle("data/data_"+"old_16step_3_in"+'.pickle')

    PWMA1 = save_data1["PWMA"] 
    PWMB1 = save_data1["PWMB"]
    current_A1 = save_data1["current_A"]
    current_A_slope1 = save_data1["current_A_slope"]    
    current_B1 = save_data1["current_B"]
    current_B_slope1 = save_data1["current_B_slope"]

    PWMA4 = save_data4["PWMA"] 
    PWMB4 = save_data4["PWMB"]
    current_A4 = save_data4["current_A"]
    current_A_slope4 = save_data4["current_A_slope"]    
    current_B4 = save_data4["current_B"]
    current_B_slope4 = save_data4["current_B_slope"]

    PWMA2 = save_data2["PWMA"] 
    PWMB2 = save_data2["PWMB"]
    current_A2 = save_data2["current_A"]
    current_A_slope2 = save_data2["current_A_slope"]    
    current_B2 = save_data2["current_B"]
    current_B_slope2 = save_data2["current_B_slope"]

    PWMA3 = save_data3["PWMA"] 
    PWMB3 = save_data3["PWMB"]
    current_A3 = save_data3["current_A"]
    current_A_slope3 = save_data3["current_A_slope"]    
    current_B3 = save_data3["current_B"]
    current_B_slope3 = save_data3["current_B_slope"]

    current_A_deri1 = diff((current_A1).flatten())/(0.00005)
    current_B_deri1 = diff((-current_B1).flatten())/(0.00005)
    current_A_deri2 = diff((current_A2).flatten())/(0.00005)
    current_B_deri2 = diff((-current_B2).flatten())/(0.00005)
    current_A_deri3 = diff((current_A3).flatten())/(0.00005)
    current_B_deri3 = diff((-current_B3).flatten())/(0.00005)
    current_A_deri4 = diff((current_A4).flatten())/(0.00005)
    current_B_deri4 = diff((-current_B4).flatten())/(0.00005)


    # t = np.linspace(0, i_ar.shape[0]-1, i_ar.shape[0]) * DELTA_T
    # t = np.linspace(0, current_A.shape[0]-1, current_A.shape[0]) * DELTA_T
    data_length = len(PWMA1)
    t = np.linspace(0, 360, data_length) #取樣週期是固定的1,1ms
    ### ==================================================================================== ###
    
    fig = plt.figure(figsize=(20, 45))
    # 1/8
    # fig.suptitle('Micro-stepping 1/8 Stepping\n' , fontsize=50)
    # # 1/16
    fig.suptitle('Micro-stepping 1/16 Stepping\n' , fontsize=50)
    # first figure
    plt.subplot(411)
    plt.plot(t, PWMA1)
    # plt.plot(t, PWMB1)
    plt.plot(t, PWMA2)
    # plt.plot(t, PWMB2)
    plt.plot(t, PWMA3)
    # plt.plot(t, PWMB3)
    plt.plot(t, PWMA4)
    # plt.plot(t, PWMB4)
    plt.grid(True)
    plt.ylabel('Duty ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('ele angle(degree) ', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Phase A PWM', fontsize=28,)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    # 1/8
    # plt.legend(['pwmA, Method 1','pwmB, Method 1','pwmA, Method 2','pwmB, Method 2','pwmA, Method 3','pwmB, Method 3','pwmA, Method 4','pwmB, Method 4'], fontsize=17)
    plt.legend(['pwmA, Method 1','pwmA, Method 2','pwmA, Method 3','pwmA, Method 4'], fontsize=20)
    # plt.legend(['pwmB, Method 1','pwmB, Method 2','pwmB, Method 3','pwmB, Method 4'], fontsize=17)
    # # 1/16
    # plt.legend(['pwmA, Irms=0.22','pwmB, Irms=0.20','pwmA, Irms=0.31','pwmB, Irms=0.31','pwmA, Irms=0.40','pwmB, Irms=0.38','pwmA, Irms=0.47','pwmB, Irms=0.40'], fontsize=20)
    fig.align_labels()

    # first figure
    plt.subplot(412)
    plt.plot(t, current_A1)
    # plt.plot(t, current_B1)
    plt.plot(t, current_A2)
    # plt.plot(t, current_B2)
    plt.plot(t, current_A3)
    # plt.plot(t, current_B3)
    plt.plot(t, current_A4)
    # plt.plot(t, current_B4)
    plt.grid(True)
    plt.ylabel('I(A) ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('ele angle(degree) ', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Phase A Current', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    # 1/8
    # plt.legend(['pwmA, Method 1','pwmB, Method 1','pwmA, Method 2','pwmB, Method 2','pwmA, Method 3','pwmB, Method 3','pwmA, Method 4','pwmB, Method 4'], fontsize=17)    
    plt.legend(['pwmA, Method 1','pwmA, Method 2','pwmA, Method 3','pwmA, Method 4'], fontsize=20)
    # plt.legend(['pwmB, Method 1','pwmB, Method 2','pwmB, Method 3','pwmB, Method 4'], fontsize=17)# 
    # 1/16
    # plt.legend(['pwmA, Irms=0.22','pwmB, Irms=0.20','pwmA, Irms=0.31','pwmB, Irms=0.31','pwmA, Irms=0.40','pwmB, Irms=0.38','pwmA, Irms=0.47','pwmB, Irms=0.40'], fontsize=20)
    fig.align_labels()
    
    # second figure
    plt.subplot(413)
    plt.plot(t, current_A_slope1)
    # plt.plot(t, current_B_slope1)
    plt.plot(t, current_A_slope2)
    # plt.plot(t, current_B_slope2)
    plt.plot(t, current_A_slope3)
    # plt.plot(t, current_B_slope3)
    plt.plot(t, current_A_slope4)
    # plt.plot(t, current_B_slope4)
    plt.grid(True)
    plt.ylabel('I/s ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('ele angle(degree) ', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Phase A Current Slope', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    # 1/8
    # plt.legend(['pwmA, Irms=0.21','pwmB, Irms=0.20','pwmA, Irms=0.31','pwmB, Irms=0.30','pwmA, Irms=0.40','pwmB, Irms=0.38','pwmA, Irms=0.47','pwmB, Irms=0.43'], fontsize=17)
    plt.legend(['pwmA, Method 1','pwmA, Method 2','pwmA, Method 3','pwmA, Method 4'], fontsize=20)
    # plt.legend(['pwmB, Method 1','pwmB, Method 2','pwmB, Method 3','pwmB, Method 4'], fontsize=17)    
    # 1/16
    # plt.legend(['pwmA, Irms=0.22','pwmB, Irms=0.20','pwmA, Irms=0.31','pwmB, Irms=0.31','pwmA, Irms=0.40','pwmB, Irms=0.38','pwmA, Irms=0.47','pwmB, Irms=0.40'], fontsize=20)
    fig.align_labels()

    # second figure
    plt.subplot(414)
    plt.plot(t[:-1], current_A_deri1)
    # plt.plot(t[:-1], current_B_deri1)
    plt.plot(t[:-1], current_A_deri2)
    # plt.plot(t[:-1], current_B_deri2)
    plt.plot(t[:-1], current_A_deri3)
    # plt.plot(t[:-1], current_B_deri3)
    plt.plot(t[:-1], current_A_deri4)
    # plt.plot(t[:-1], current_B_deri4)
    plt.grid(True)
    plt.ylabel('I/s ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('ele angle(degree) ', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Phase A Current Derivative', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    # 1/8
    # plt.legend(['pwmA, Irms=0.21','pwmB, Irms=0.20','pwmA, Irms=0.31','pwmB, Irms=0.30','pwmA, Irms=0.40','pwmB, Irms=0.38','pwmA, Irms=0.47','pwmB, Irms=0.43'], fontsize=17)
    plt.legend(['pwmA, Method 1','pwmA, Method 2','pwmA, Method 3','pwmA, Method 4'], fontsize=20)
    # plt.legend(['pwmB, Method 1','pwmB, Method 2','pwmB, Method 3','pwmB, Method 4'], fontsize=17)    
    # 1/16
    # plt.legend(['pwmA, Irms=0.22','pwmB, Irms=0.20','pwmA, Irms=0.31','pwmB, Irms=0.31','pwmA, Irms=0.40','pwmB, Irms=0.38','pwmA, Irms=0.47','pwmB, Irms=0.40'], fontsize=20)
    fig.align_labels()

    plt.subplots_adjust(hspace=0.3)
    # 1/8
    # plt.savefig(FIG_DIR+"/"+"compare" + '/' + "compare_8step_method_PWMA" + '.png', dpi=300)
    # plt.savefig(FIG_DIR+"/"+"compare" + '/' + "compare_8step_method_PWMB" + '.png', dpi=300)
    # # 1/16
    plt.savefig(FIG_DIR+"/"+"compare" + '/' + "compare_16step_method_PWMA" + '.png', dpi=300)
