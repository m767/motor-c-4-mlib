import numpy as np
from numpy import diff
from datetime import datetime
from mypickle import save2pickle, load_pickle
import matplotlib.pyplot as plt
from plot_current import plot_current
from plot_current2 import plot_current2
from plot_control import plot_control
import argparse
import struct
import math
from statistics import mean
from itertools import groupby

if __name__ == "__main__":
    delat_t = 0.01
    PWM = 20000  # pwm頻率 : 20k
    now = datetime.now()
    default_len = 13
    default_file_marker = now.strftime("%m_%d_%H_%M_%S")
    parser = argparse.ArgumentParser()
    parser.add_argument("-mk", "--marker", help="Enter the marker about file, it will add behine the file",
                        default=default_file_marker, type=str)
    parser.add_argument(
        "-len", "--length", help="Comma \",\" split length, example \"1,2,3\" is three", default=default_len, type=int)

    args = parser.parse_args()

    file_marker = args.marker

    # define PWM PERIOD COUNT
    PERIOD_COUNT = 900
    # define each phase resistor
    ra = 0.15
    rb = 0.15
    COMMA_DATA_LEN = args.length
    # para_index = {'title': 0, 'ia': 1, 'ib': 2, 'angle': 3, 'pwma': 4, 'pwmb': 5}
    para_index = {'title': 0, 'i_ar': 1, 'i_af': 2, 'i_br': 3, 'i_bf': 4, 'Sangle': 5,
                  'Cangle': 6, 'th_cum': 7, 'PWMA': 8, 'PWMB': 9, 'I_ki': 10, 'PI_kp': 11, 'PI_ki': 12}

    print("Handle data ...")
    f = open("data" + "/" + 'raw_data_' + file_marker + '.txt', "r")
    raw_data = f.readlines()
    f.close()
    data_length = len(raw_data)
    print("Data has ", data_length)

    # collect data
    i_ar = np.zeros([data_length], dtype=np.float32)
    i_af = np.zeros([data_length], dtype=np.float32)
    i_br = np.zeros([data_length], dtype=np.float32)
    i_bf = np.zeros([data_length], dtype=np.float32)

    current_A = np.zeros([data_length], dtype=np.float32)
    current_A_slope = np.zeros([data_length], dtype=np.float32)
    current_B = np.zeros([data_length], dtype=np.float32)
    current_B_slope = np.zeros([data_length], dtype=np.float32)

    Sangle = np.zeros([data_length], dtype=np.uint16 )
    Cangle = np.zeros([data_length], dtype=np.uint16)

    th_cum = np.zeros([data_length], dtype=np.float32)
    PWMA = np.zeros([data_length], dtype=np.float32)
    PWMB = np.zeros([data_length], dtype=np.float32)

    I_ki = np.zeros([data_length], dtype=np.float32)
    PI_kp = np.zeros([data_length], dtype=np.float32)
    PI_ki = np.zeros([data_length], dtype=np.float32)

    save_data = {}

    current_center = 2.4
    cnt = 0
    c1 = 0
    dc = 0
    dc_cnt = 0
    rpc_fg = 0
    CONF_CNT = 5
    a = []
    for data in raw_data:
        data_list = data.split(',')
        if len(data_list) == COMMA_DATA_LEN:
            i_ar[cnt] = float(data_list[para_index['i_ar']])
            i_af[cnt] = float(data_list[para_index['i_af']])
            i_br[cnt] = float(data_list[para_index['i_br']])
            i_bf[cnt] = float(data_list[para_index['i_bf']])
            Sangle[cnt] = float(data_list[para_index['Sangle']])
            Cangle[cnt] = float(data_list[para_index['Cangle']])
            th_cum[cnt] = float(data_list[para_index['th_cum']])
            PWMA[cnt] = float(data_list[para_index['PWMA']])
            PWMB[cnt] = float(data_list[para_index['PWMB']])
            I_ki[cnt] = float(data_list[para_index['I_ki']])
            PI_kp[cnt] = float(data_list[para_index['PI_kp']])
            PI_ki[cnt] = float(data_list[para_index['PI_ki']])
            cnt += 1
        else:
            print("Warning !!! List size not match !!!, ", cnt)
            print(data_list)

    # ========= CURRENT RAW DATA =========
    # new_out
    i_ar = -(((0.075+(i_ar*5/1024))-current_center)/(20*ra))
    i_af = -(((0.075+(i_af*5/1024))-current_center)/(20*ra))
    i_br = -(((0.075+(i_br*5/1024))-current_center)/(20*rb))
    i_bf = -(((0.075+(i_bf*5/1024))-current_center)/(20*rb))

    # out
    # i_ar = -(((0.075+(i_ar*5/1024))-current_center)/(20*ra))
    # i_af = -(((0.075+(i_af*5/1024))-current_center)/(20*ra))
    # i_br = -(((0.075+(i_br*5/1024))-current_center)/(20*rb))
    # i_bf = -(((0.075+(i_bf*5/1024))-current_center)/(20*rb))

    # in
    # i_ar = -((((i_ar*4.8/1024))-current_center)/(20*ra))
    # i_af = -((((i_af*4.8/1024))-current_center)/(20*ra))
    # i_br = -((((i_br*4.8/1024))-current_center)/(20*rb))
    # i_bf = -((((i_bf*4.8/1024))-current_center)/(20*rb))

    # ========= CURRENT DATA =========
    # new_out
    current_A = ((i_af - i_ar)/2)*0.6/0.43
    current_B = ((i_bf - i_br)/2)*0.6/0.43
    current_A_slope = -(i_af + i_ar)/1/(0.00005)
    current_B_slope = -(i_bf + i_br)/1/(0.00005)

    # old_out  不能用
    # current_A = (i_ar)/1
    # current_B = (i_br)/1

    # new_in
    # 需要/2 在呈上0.6/0.43 這個倍率才是正確值
    # current_A = ((i_ar + i_af)/2)*0.6/0.43
    # current_B = -((i_bf + i_br)/2)*0.6/0.43
    # current_A_slope = (i_ar - i_af)/1/(0.000035)
    # current_B_slope = (i_bf - i_br)/1/(0.000035)

    # old_in
    # current_A = (i_ar)
    # current_B = -(i_br)

    Sangle = Sangle.astype(np.float32)
    Sangle = Sangle
    #Cangle = Cangle*360/16364
    #th_cum = th_cum*360/16364
    Cangle = Cangle.astype(np.float32)
    Cangle = Cangle
    th_cum = Sangle.astype(np.float32)
    th_cum = th_cum/360*7.2#360度電子角=7.2度機械角
    PWMA = PWMA / 68
    PWMB = PWMB / 68
    PWMA[PWMA == 0] = 0.01
    PWMB[PWMB == 0] = 0.01
    save_data["i_ar"] = i_ar
    save_data["i_af"] = i_af

    save_data["i_br"] = i_br
    save_data["i_bf"] = i_bf

    save_data["current_A"] = current_A
    save_data["current_A_slope"] = current_A_slope
    save_data["current_B"] = current_B
    save_data["current_B_slope"] = current_B_slope

    save_data["Sangle"] = Sangle
    save_data["Cangle"] = Cangle
    save_data["th_cum"] = th_cum
    save_data["PWMA"] = PWMA
    save_data["PWMB"] = PWMB

    save_data["I_ki"] = I_ki
    save_data["PI_kp"] = PI_kp
    save_data["PI_ki"] = PI_ki

    print("Saving data to pickle file ...")
    save2pickle("data"+'/data_'+file_marker+'.pickle', save_data)

    print("Saving figure ...")
    plot_control(file_marker, delat_t)
