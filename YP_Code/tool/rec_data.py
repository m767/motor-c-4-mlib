import serial
import os
import sys
import numpy as np
from datetime import datetime
from mypickle import save2pickle, load_pickle
import argparse
import time

if __name__ == "__main__":
    now = datetime.now()
    default_file_marker = now.strftime("%m_%d_%H_%M_%S")
    default_len = 13

    parser = argparse.ArgumentParser()
    parser.add_argument("-sz", "--size", help="Receive data size you want", default=1000, type=int)
    parser.add_argument("-mk", "--marker", help="Enter the marker about file, it will add behine the file", default=default_file_marker, type=str)
    parser.add_argument("-p", "--port", help="COM port choose, e.g. COM8", default="7", type=str)
    parser.add_argument("-len", "--length", help="Comma \",\" split length, example \"1,2,3\" is three", default=default_len, type=int)
    args = parser.parse_args()

    file_marker = args.marker
    GRAB_DATA_SIZE = args.size
    COMMA_DATA_LEN = args.length
    COM_PORT = args.port
    raw_data_file_name = 'raw_data_' + file_marker + '.txt'
    print('Write to file: ' + raw_data_file_name)
    print('Grab data size: ' + str(GRAB_DATA_SIZE))
    print("parameter size: ", COMMA_DATA_LEN)

    DATA_DIR = 'data'
    try:
        os.mkdir(DATA_DIR)
    except:
        pass
    print("Open ", COM_PORT)
    
    ser = serial 
    try:
        # ser = serial.Serial(COM_PORT, 38400, timeout= 0.01)
        ser = serial.Serial(
            port        = COM_PORT,
            baudrate    = 38400,
            timeout     = 1,
        )
        print("set done")
        ser.flushInput()
        ser.flushOutput()
        if(ser.isOpen() == False):
            ser.open()
        print ("port is opened!")
    except serial.SerialException as e:
        #There is no new data from serial port
        print(e)
        sys.exit(1)
    except TypeError as e:
        print(e)
        ser.port.close()
        sys.exit(1)
    except IOError: # if port is already opened, close it and open it again and print message
        ser.close()
        ser.open()
        print ("port was already open, was closed and opened again!")

    # ser = serial.Serial(COM_PORT, 38400, timeout= 0.01)
    # ser.setDTR(False)
    # time.sleep(0.02)
    # ser.setDTR(True)
    # if ser.isOpen():
    #     print("Serial ok")
    # else:
    #     print("serial wrong")

    cnt = 0
    temp_str = ''
    # ser.flush()
    exit_fg = False
    for _, __, fn in os.walk("./"+DATA_DIR):
        if raw_data_file_name in fn:
            exit_fg = True
    if exit_fg == True:
        print("Warning the file already exits !!!")
        print("Overwrite ? (Y/N)")
        yes_no = input()
        if yes_no.lower() == "y" or yes_no.lower() == "yes":
            # Erase
            f = open(DATA_DIR + "/" + raw_data_file_name, "w")
            f.close()
        else:
            sys.exit()
    else:
        f = open(DATA_DIR + "/" + raw_data_file_name, "w")
        f.close()
    print("Create File Done !")
    # Open
    f = open(DATA_DIR + "/" + raw_data_file_name, "ab")
    print("Open File Done !")
    while cnt <= (GRAB_DATA_SIZE):
        raw_data = ser.readline()
        # print(raw_data)
        try:
            if len(raw_data.decode("utf-8").split(",")) == COMMA_DATA_LEN:
                # print(raw_data)
                f.write(raw_data[0:-1])

                cnt += 1
                if cnt % 100 == 0:
                    print("waiting ... ", cnt)
            else:
                print("length not match --> ", len(raw_data.decode("utf-8").split(",")))
                print(raw_data)
        except Exception as e:
            print(raw_data)
            print(e)
    print("Let's Close !")
    ser.flush()
    ser.flushInput()
    ser.flushOutput()
    if ser.isOpen():
        ser.close()
    
    if not f.closed:
        f.close()
    
    print("ser.closed\n",ser.closed)
    print("Done !")



