import pickle
import matplotlib.pyplot as plt
from mypickle import save2pickle, load_pickle
import os
import numpy as np
from numpy import diff
import collections
from functools import cmp_to_key

FIG_DIR = "figure"
def plot_control(file_marker, DELTA_T):
    try:
        os.mkdir(FIG_DIR)
    except:
        pass
    try:
        os.mkdir(FIG_DIR+"/"+file_marker)
    except:
        pass

    save_data = load_pickle("data/data_"+file_marker+'.pickle')

    i_ar = save_data["i_ar"]
    i_af = save_data["i_af"]
    i_br = save_data["i_br"]
    i_bf = save_data["i_bf"]

    current_A = save_data["current_A"]
    current_A_slope = save_data["current_A_slope"]    
    current_B = save_data["current_B"]
    current_B_slope = save_data["current_B_slope"]

    Sangle = save_data["Sangle"]
    # print(Sangle)
    Cangle = save_data["Cangle"]
    th_cum = save_data["th_cum"]
    PWMA = save_data["PWMA"] 
    PWMB = save_data["PWMB"]
    
    I_ki = save_data["I_ki"] 
    PI_kp = save_data["PI_kp"]
    PI_ki = save_data["PI_ki"] 

    Sangle_command = Cangle
    power = (current_A*current_A*6.48)+(current_B*current_B*6.16)
    current_A_deri = diff((current_A).flatten())/(0.00005)
    current_B_deri = diff((-current_B).flatten())/(0.00005)

    data_length = len(Sangle)
    inductance_A = np.zeros([data_length, 2], dtype=np.float32)
    inductance_B = np.zeros([data_length, 2], dtype=np.float32)
    current_A_data = np.zeros([data_length, 2], dtype=np.float32)
    current_B_data = np.zeros([data_length, 2], dtype=np.float32)
    angle_com = [round(num, 1) for num in Sangle.flatten()]
    angle_com = np.reshape(angle_com, (1, len(angle_com))).T

    for i in range(len(angle_com)):
        inductance_A[i] = np.hstack((angle_com[i],current_A_slope[i]))
        inductance_B[i] = np.hstack((angle_com[i],current_B_slope[i]))
        current_A_data[i] = np.hstack((angle_com[i],current_A[i]))
        current_B_data[i] = np.hstack((angle_com[i],current_B[i]))

    inductance_A = sorted(inductance_A,key=lambda x:x[0])
    inductance_A = np.array(inductance_A)
    inductance_B = sorted(inductance_B,key=lambda x:x[0])
    inductance_B = np.array(inductance_B)
    current_A_data = sorted(current_A_data,key=lambda x:x[0])
    current_A_data = np.array(current_A_data)
    current_B_data = sorted(current_B_data,key=lambda x:x[0])
    current_B_data = np.array(current_B_data)

    inductance_A = np.array(inductance_A)
    inductance_B = np.array(inductance_B)
    current_A_data = np.array(current_A_data)
    current_B_data = np.array(current_B_data)

    avg_inductor_A = np.zeros([len(inductance_A),1], dtype=np.float32)
    avg_inductor_B = np.zeros([len(inductance_A),1], dtype=np.float32)
    avg_inductor_A = (5-4.66*current_A_data[:,1])/inductance_A[:,1]
    avg_inductor_B = (5-4.66*current_B_data[:,1])/inductance_A[:,1]

    # t = np.linspace(0, i_ar.shape[0]-1, i_ar.shape[0]) * DELTA_T
    # t = np.linspace(0, current_A.shape[0]-1, current_A.shape[0]) * DELTA_T
    data_length = len(Sangle)
    t = np.linspace(0, data_length, data_length) #取樣週期是固定的1,1ms
    ### ==================================================================================== ###
    
    fig = plt.figure(figsize=(20, 55))
    fig.suptitle('Micro-stepping \n' + 'i_ki : ' + str(round(float(I_ki[1]),4)) + ' PI_kp : ' + str(round(float(PI_kp[1]),4)) + ' PI_ki : ' + str(round(float(PI_ki[1]),4)) + "\n" + ' max_er : ' + str(round(np.max(Cangle),3)) +' min_er : ' + str(round(np.min(Cangle),3)) + ' std_er : ' +  str(round(np.std(Cangle),3)) , fontsize=50)
    # first figure
    plt.subplot(611)
    plt.plot(t, Sangle)
    
    plt.plot(t, Sangle_command)
    plt.grid(True)
    plt.ylabel('Encoder(2^14) ', fontsize=24, position=(0,1.1), rotation="horizontal")
    plt.xlabel('full microstep ', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Sensing Encoder', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(['Sensing Angle','Command Angle'], fontsize=20)
    # plt.legend(['motor angle'], fontsize=20)
    plt.text(1, 5, 'Encoder Fir Output', style='italic')
    fig.align_labels()

    # second figure
    plt.subplot(612)
    plt.plot(t, Sangle-Cangle)
    plt.grid(True)
    plt.ylabel('Encoder(2^14) ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('full microstep ', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Encoder Error', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(['Encoder Error'], fontsize=20)
    fig.align_labels() 

    # third figure
    plt.subplot(613)
    plt.plot(t, PWMA)
    plt.plot(t, PWMB)
    plt.grid(True)
    plt.ylabel('Duty ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('full microstep ', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Phase A B PWM', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(['pwmA','pwmB'], fontsize=20)
    fig.align_labels()
    # forth figure
    # plt.subplot(714)
    # # plt.plot(t, current_A)
    # # plt.plot(t, current_B)
    # # plt.plot(t,th_cum)
    # plt.plot(t,power)
    # plt.grid(True)
    # # plt.ylabel('Angle(deg) ', fontsize=24, position=(0,1), rotation="horizontal")
    # # plt.xlabel('t(100ms) ', fontsize=24, position=(1,0), rotation="horizontal")
    # # plt.title('Theta Error accumulation', fontsize=28)
    # plt.ylabel('P (W) ', fontsize=24, position=(0,1), rotation="horizontal")
    # plt.xlabel('t(100ms) ', fontsize=24, position=(1,0), rotation="horizontal")
    # plt.title('1A1B 2A2B Power', fontsize=28)
    # plt.xticks(fontsize=20)
    # plt.yticks(fontsize=20)
    # fig.align_labels()

    # first figure
    plt.subplot(614)
    plt.plot(t[:-1], current_A[1:])
    plt.plot(t[:-1], current_B[1:])
    plt.grid(True)
    plt.ylabel('I(A) ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('full microstep ', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Phase A B current', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(['current_A','current_B'], fontsize=20)
    fig.align_labels()
    # second figure
    plt.subplot(615)
    plt.plot(t[:-1], current_A_slope[1:])
    plt.plot(t[:-1], current_B_slope[1:])
    # plt.plot(t[0:-1], current_B_slope[0:-1])
    # plt.plot(t[1:], current_A_deri)
    # plt.plot(t[0:-1], current_B_deri)
    plt.grid(True)
    plt.ylabel('I/s ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('full microstep', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Phase A B current slope', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    # plt.legend(['current_A_slope','current_B_slope'], fontsize=20)
    plt.legend(['current_A_slope','current_B_slope'], fontsize=20)
    fig.align_labels()

    # second figure
    plt.subplot(616)
    plt.plot(t[1:], current_A_deri)
    plt.plot(t[1:], current_B_deri)
    plt.grid(True)
    plt.ylabel('I/s ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('full microstep ', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Phase A B current derivative', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    # plt.legend(['current_A_slope','current_B_slope'], fontsize=20)
    plt.legend(['current_A_deri','current_B_deri'], fontsize=20)
    fig.align_labels()

    # # seventh figure
    # plt.subplot(814)
    # # plt.plot(t, current_A)
    # # plt.plot(t, current_B)
    # plt.plot(current_A_data[:,0],current_A_data[:,1], '.')
    # plt.plot(current_B_data[:,0],current_B_data[:,1], '.')
    # plt.grid(True)
    # plt.ylabel('I(A) ', fontsize=24, position=(0,1), rotation="horizontal")
    # plt.xlabel('Angle(Degree) ', fontsize=24, position=(1,0), rotation="horizontal")
    # plt.title('Phase A B current', fontsize=28)
    # plt.xticks(fontsize=20)
    # plt.yticks(fontsize=20)
    # plt.legend(['current_A','current_B'], fontsize=20)
    # fig.align_labels()

    # plt.subplot(817)
    # # plt.plot(t[0:-1], current_A_deri)
    # # plt.plot(t[0:-1], current_B_deri)
    # # plt.plot(angle_com,current_A, '.')
    # # plt.plot(angle_com,current_B, '.')
    # plt.plot(inductance_A[:,0],inductance_A[:,1], '.')
    # plt.plot(inductance_B[:,0],inductance_B[:,1], '.')
    # # plt.plot(angle_com,current_B_slope, '.')
    # plt.grid(True)
    # plt.ylabel(' ', fontsize=24, position=(0,1), rotation="horizontal")
    # plt.xlabel('Angle(Degree) ', fontsize=24, position=(1,0), rotation="horizontal")
    # plt.ylabel('V/s ', fontsize=24, position=(0,1), rotation="horizontal")
    # plt.title('Phase A B current slope ', fontsize=28)
    # plt.xticks(fontsize=20)
    # plt.yticks(fontsize=20)
    # plt.legend(['current A inductance','current B inductance'], fontsize=20)
    # fig.align_labels()

    # plt.subplot(818)
    # plt.plot(current_A_data[:,0],avg_inductor_A)
    # plt.plot(current_A_data[:,0],avg_inductor_B)
    # plt.grid(True)
    # plt.xlabel('Angle(Degree) ', fontsize=24, position=(1,0), rotation="horizontal")
    # plt.ylabel('L(H)', fontsize=24, position=(0,1), rotation="horizontal")
    # plt.title('L = (V-Ri)*dt/di ', fontsize=28)
    # plt.xticks(fontsize=20)
    # plt.yticks(fontsize=20)
    # plt.legend(['current_A_slope','current_B_slope'], fontsize=20)
    # fig.align_labels()

    
    plt.subplots_adjust(hspace=0.3)
    
    plt.savefig(FIG_DIR+"/"+file_marker + '/current_pwm_phaseAB_' + file_marker + '.png', dpi=300)

    ### ==================================================================================== ###

    rms_ia_square = np.mean((current_A*current_A))
    rms_ib_square = np.mean((current_B*current_B))
    P_loss_A = rms_ia_square*6.485
    P_loss_B = rms_ib_square*6.165
    P_in_A = np.mean(np.abs(current_A*5))
    P_in_B = np.mean(np.abs(current_A*5))
    P_efficient_A = (P_in_A - P_loss_A) / P_in_A * 100
    P_efficient_B = (P_in_B - P_loss_B) / P_in_B * 100
    P_all = (P_in_A + P_in_B - P_loss_A - P_loss_B) / (P_in_A + P_in_B) * 100
    omega = (abs(Sangle[0]-Sangle[-1]))/(current_A.shape[0]*0.0012)
    rpm = omega*60/360
    report_str = ""
    report_str += "Data size is " +  str(current_A.shape[0]) + "\n"
    report_str += "===== velocity =====" + "\n"
    report_str += "theta/s: " + str(omega) + "\n"
    report_str += "RPM: " + str(rpm) + "\n"
    report_str += "===== sensor ele angle =====" + "\n"
    report_str += "max: " + str(np.max(Sangle)) + "\n"
    report_str += "min: " + str(np.min(Sangle)) + "\n"
    report_str += "mean: " + str(np.mean(Sangle)) + "\n"
    report_str += "===== command ele angle =====" + "\n"
    report_str += "max: " + str(np.max(Sangle_command)) + "\n"
    report_str += "min: " + str(np.min(Sangle_command)) + "\n"
    report_str += "mean: " + str(np.mean(Sangle_command)) + "\n"
    report_str += "===== theta error (degree) =====" + "\n"
    report_str += "max: " + str(np.max(Cangle)) + "\n"
    report_str += "min: " + str(np.min(Cangle)) + "\n"
    report_str += "mean: " + str(np.mean(Cangle)) + "\n"
    report_str += "std: " + str(np.std(Cangle)) + "\n"
    report_str += "MSE: " + str(np.mean(np.square(Cangle))) + "\n"
    report_str += "N_STEP: " + str(1.8/(np.max(Cangle))) + ", " + str(1.8/(np.min(Cangle))) + "\n"
    report_str += "===== consume power (watt) =====" + "\n"
    report_str += "max: " + str(np.max(power)) + "\n"
    report_str += "min: " + str(np.min(power)) + "\n"
    report_str += "mean: " + str(np.mean(power)) + "\n"
    report_str += "std: " + str(np.std(power)) + "\n"
    report_str += "sum: " + str(np.sum(np.abs(power))) + " J\n"
    report_str += "===== current  =====" + "\n"
    report_str += "ia_max: " + str(np.max(current_A)) + " A\n"
    report_str += "ia_min: " + str(np.min(current_A)) + " A\n"
    report_str += "ia_mean: " + str(np.mean(current_A)) + " A\n"
    report_str += "ib_max: " + str(np.max(current_B)) + " A\n"
    report_str += "ib_min: " + str(np.min(current_B)) + " A\n"
    report_str += "ib_mean: " + str(np.mean(current_B)) + " A\n"
    report_str += "===== current slope =====" + "\n"
    report_str += "ia_slope_max: " + str(np.max(current_A_slope)) + " A\n"
    report_str += "ia_slope_min: " + str(np.min(current_A_slope)) + " A\n"
    report_str += "ia_slope_mean: " + str(np.mean(current_A_slope)) + " A\n"
    report_str += "ib_slope_max: " + str(np.max(current_B_slope)) + " A\n"
    report_str += "ib_slope_min: " + str(np.min(current_B_slope)) + " A\n"
    report_str += "ib_slope_mean: " + str(np.mean(current_B_slope)) + " A\n"
    report_str += "===== current RMS Square (watt) =====" + "\n"
    report_str += "rms_ia_square: " + str(rms_ia_square) + " A\n"
    report_str += "rms_ib_square: " + str(rms_ib_square) + " A\n"
    report_str += "===== current RMS (watt) =====" + "\n"
    report_str += "rms_ia: " + str(np.sqrt(rms_ia_square)) + " A\n"
    report_str += "rms_ib: " + str(np.sqrt(rms_ib_square)) + " A\n"
    report_str += "===== P_loss (watt) =====" + "\n"
    report_str += "P_loss_A: " + str(rms_ia_square*6.485) + " W\n"
    report_str += "P_loss_B: " + str(rms_ia_square*6.165) + " W\n"
    report_str += "===== P_in (watt) =====" + "\n"
    report_str += "P_in_A: " + str(P_in_A) + " W\n"
    report_str += "P_in_B: " + str(P_in_B) + " W\n"
    report_str += "===== Eta (H) (%) =====" + "\n"
    report_str += "P_efficient_A: " + str(P_efficient_A) + " %\n"
    report_str += "P_efficient_B: " + str(P_efficient_B) + " %\n"
    report_str += "P_all: " + str(P_all) + " %\n"

    print(report_str)

    f = open(FIG_DIR + "/" + file_marker + "/report_" + file_marker + ".txt", "w")
    f.write(report_str)
    f.close()
