/**
 * @file test_identification.c
 * @author nacmvmclab (nacmvmclab@gmail.com)
 * @date 2021.08.20
 * @brief 開迴路
 *
 */
#ifndef __AVR_ATmega128__
#    define __AVR_ATmega128__
#endif
#include <avr/io.h>
#include <math.h>
#include <util/delay.h>

#include "../bsp/control_board.h"
#include "../bsp/hal_as5047d.h"
#include "../lib/c4mlib.h"

#define N       8
#define work_FR 20  //工作除頻
#define wait_FR 40  //等待除頻
#define TASK1   0
#define TASK2   1
#define TASK3   2
#define TASK4   3
// (V1*0.000122-0.606)*S1+(V2*0.000122-0.0606)*S2
#define SAMPLEHOLD_NUM 4
#define PWM_freq       32 /* PWM更新頻率 */
#define PWM_set        16
#define COLLECT_CYCLE                                                          \
    1 /* n個完整svpwm再collect一次，一個svpwm有PWM_freq筆資料 4*/
#define PRINT_TRIG  4 /* n個完整svpwm再print一次，一個svpwm有PWM_freq筆資料 4*/
#define PRINT_CYCLE 8 /* n個完整svpwm再print一次，一個svpwm有PWM_freq筆資料 \
                         4*/
#define Pi          3.14159
#define MAX_DUTY    0.85
#define DUTY_OFFSET 0.5

#define ENABLE_SAMPLEHOLD()  PORTD &= ~(1 << 4);
#define DISABLE_SAMPLEHOLD() PORTD |= (1 << 4);
#define GET_PWM_CYCLE()      REGGET(&ICR1L, 2, &tempDuty);
#define GET_ADC_SIGNED()     (PIND & 0x40) >> 6
#define NSLEEP_ON()          PORTD |= (1 << 6);

#define measure_center 240  // degree
#define eletrical_ang_revol                                                    \
    8  //一個完整的電子角 機械角理論上走1.8 * 4(2相共4個極)= 7.2度，多取到8
#define upper_limit (45.5) * (measure_center + (eletrical_ang_revol / 2))
#define lower_limit (45.5) * (measure_center - (eletrical_ang_revol / 2))

void ADCTrigger1(void);
void ADCTrigger2(void);
void ADCTrigger3(void);
void ADCTrigger4(void);

void ADCgetSigned(void);
void updateDuty(void);
void SenseCurrent(void);

void EncodergetAngle(void);
// volatile uint16_t PWM_freq = 32;/* PWM更新頻率 */

volatile uint8_t flag1 = 0, flag2 = 0;
uint16_t ADC_DATA                               = 0;  // ADC結果
uint16_t tempDuty                               = 0;
volatile uint8_t isr_count                      = 0;
volatile uint8_t print_count                    = 0;
uint8_t updateDuty_count                        = 0;
float dutyA[PWM_freq]                           = {};
float dutyB[PWM_freq]                           = {};
uint16_t adcResult[4][PWM_freq * COLLECT_CYCLE] = {0};
uint16_t data[4][240];
uint16_t angle_com[PWM_freq * COLLECT_CYCLE] = {0};
char adcsign[4][PWM_freq * COLLECT_CYCLE]    = {0};
uint8_t printf_trig                          = 0;
volatile uint8_t flag                        = 0;
uint16_t init_angle_com                      = 0;
uint8_t init_fg                              = 20;

void test(void* FBData_p);
void test1(void* FBData_p);
void test2(void* FBData_p);
void test3(void* FBData_p);
void Drv8847_init(void);
void internalTimer_init(void);
void Timer1_init(void);
void Duty_init(void);
void SVPWM_init(void);

#define PWM              1
#define USE_WITHOUT_FREQ 1
// #define TIMER1_asPWM 1

extern as50474_t as5047d; /* AS5047D motor encoder IC */

int main(void) {
    DDRB |= (1 << 4);
    PORTB &= ~(1 << 4);
    // DDRB = 0xF0;
    // DDRD |= 0x70;
    C4M_DEVICE_set();
    // printf("start  this is test encoder\n");

    /* 馬達硬體設定 */
    Drv8847_init();

/* 計時器硬體設定 */
#ifdef TIMER1_asPWM
    Timer1_init();
#endif

    /* 啟用pwm1計時器硬體設定佈局 */
    PWM1_HW_LAY();

    /* 設定timer1硬體 */
    hardware_set(&PWM1HWSet_str);

    /* SVPWM初始化 */
    SVPWM_init();

    /* DUTY初始化 */
    Duty_init();

    // /* ADC硬體設定佈局 (已經在control_bord執行過了)*/
    // ADC_HW_LAY();

    // /* ADC硬體設定 */
    // hardware_set(&ADCHWSet_str);

    // /* 周邊硬體設定 */
    board_init();
    SPCR = 215;
    // 32,64用212,16用215
    as5047d.init();
    NSLEEP_ON();
    sei();

    DISABLE_SAMPLEHOLD();
    GET_PWM_CYCLE();
    while (1) {
        ;
    }
}

void Drv8847_init(void) {
    DDRD |= (1 << 4);   // set Mode PIN to output PD4
    PORTD |= (1 << 4);  // set Mode PIN & nsleep to High
    DDRD |= (1 << 6);   // set Mode PIN to input PD6 for signA rising
    // DDRD &= ~(1<<7); //set Mode PIN to input PD7 for signA falling
    // DDRD &= ~(1<<0); //set Mode PIN to input PD0 for signB rising
    // DDRD &= ~(1<<1); //set Mode PIN to input PD1 for signB falling
}

void Duty_init(void) {
    uint16_t tempDuty;
    REGGET(&ICR1L, 2, &tempDuty);
    for (int i = 0; i < sizeof(dutyA) / sizeof(float); i++) {
        dutyA[i] = (float)tempDuty * dutyA[i];
        dutyB[i] = (float)tempDuty * dutyB[i];
    }
}

void SVPWM_init(void) {
    for (uint8_t i = 0; i < PWM_freq; i++) {
        dutyA[i] =
            (float)(DUTY_OFFSET +
                    0.5 * ((float)(sin((double)((2 * Pi) / PWM_freq) * i)) *
                           MAX_DUTY));
        dutyB[i] =
            (float)(DUTY_OFFSET +
                    0.5 * ((float)(cos((double)((2 * Pi) / PWM_freq) * i)) *
                           MAX_DUTY));
    }
}

void internalTimer_init(void) {
    TIM1_HW_LAY();                 //啟用timer1計時器硬體設定佈局
    hardware_set(&TIM1HWSet_str);  //設定timer2硬體
}

void Timer1_init(void) {
    DDRB   = 0xF0;  // set PB5 6 7 to Output
    TCCR1B = 0x18;  // 0b00011000, Mode 12(CTC to ICR1)
    TCCR1A =
        0x54;     // 0b01010100, Mode 12(CTC to ICR1), toggle when compare match
    ICR1   = 16;  // f = 11059200/(2*N*(1+ICR1))
    OCR1AL = 9;
    OCR1BL = 4;
    TCCR1B |= 2;   // 0b00011101, N=8
    TIMSK = 0x10;  // OCIE1A=1, Output compare A match interrupt
    TCNT1 = 0;     // init counter
}

void ADCTrigger1(void) {
    REGFPT(&ADMUX, 0x1F, 0, 0x00);  //選擇輸入訊號為ADC0(Port F0)/sigleE_0_x1
    _delay_us(2);
    REGFPT(&ADCSRA, 0x40, 6, 1);  //觸發ADC轉換
}

void ADCTrigger2(void) {
    REGFPT(&ADMUX, 0x1F, 0, 0x01);  //選擇輸入訊號為ADC1(Port F1)/sigleE_1_x1
    _delay_us(2);
    REGFPT(&ADCSRA, 0x40, 6, 1);  //觸發ADC轉換
}

void ADCTrigger3(void) {
    REGFPT(&ADMUX, 0x1F, 0, 0x02);  //選擇輸入訊號為ADC2(Port F2)/sigleE_2_x1
    _delay_us(2);
    REGFPT(&ADCSRA, 0x40, 6, 1);  //觸發ADC轉換
}

void ADCTrigger4(void) {
    REGFPT(&ADMUX, 0x1F, 0, 0x03);  //選擇輸入訊號為ADC3(Port F3)/sigleE_3_x1
    _delay_us(2);
    REGFPT(&ADCSRA, 0x40, 6, 1);  //觸發ADC轉換
}

void ADCgetSigned(void) {
    REGFGT(&PIND, 0x40, 6,
           &adcsign[0][(PWM_freq * (printf_trig)) + updateDuty_count]);
    REGFGT(&PIND, 0x80, 7,
           &adcsign[1][(PWM_freq * (printf_trig)) + updateDuty_count]);
    REGFGT(&PIND, 0x01, 0,
           &adcsign[2][(PWM_freq * (printf_trig)) + updateDuty_count]);
    REGFGT(&PIND, 0x02, 1,
           &adcsign[3][(PWM_freq * (printf_trig)) + updateDuty_count]);
    adcsign[0][(PWM_freq * (printf_trig)) + updateDuty_count] |=
        (updateDuty_count << 1);
    // for(uint8_t i=0;i<4;i++){
    // 	if(adcsign[i][(PWM_freq*(printf_trig))+updateDuty_count]==1){
    // 		adcsign[i][(PWM_freq*(printf_trig))+updateDuty_count]=-1;
    // 	}
    // 	if(adcsign[i][(PWM_freq*(printf_trig))+updateDuty_count]==0){
    // 		adcsign[i][(PWM_freq*(printf_trig))+updateDuty_count]=1;
    // 	}
    // }
}

void EncodergetAngle(void) {
    // cli();
    angle_com[(PWM_freq * (printf_trig)) + updateDuty_count] =
        as5047d.instance->read_anglecom();
    // printf("[485],%x\n",angle_com[(PWM_freq*(printf_trig))+updateDuty_count]);
    // sei();
    // static uint8_t q=0;
    // angle_com[q] = as5047d.instance->read_anglecom();
    // q++;
    // if(q>=32){
    // 	q=0;
    // 	for(int j=0;j<32;j++){
    // 		printf("%f\n",(float)(angle_com[j]>>3)*360/2048);
    // 	}
    // }
    if (init_fg > 1) {
        init_fg--;
    }
    if (init_fg == 1) {
        init_angle_com =
            angle_com[(PWM_freq * (printf_trig)) + updateDuty_count];
        init_fg--;
    }
}

void updateDuty(void) {
    ENABLE_SAMPLEHOLD();

    // PORTB |= (1<<4);
    // ADCgetSigned();
    // PORTB &= ~(1<<4);
    EncodergetAngle();
    cli();
    /* 更改A相duty */
    OCR1A = (uint16_t)dutyA[updateDuty_count];
    /* 更改B相duty */
    OCR1B = (uint16_t)dutyB[updateDuty_count];
    sei();
    updateDuty_count++;
    if (updateDuty_count == PWM_freq) {
        updateDuty_count = 0;
        printf_trig++;
        if (printf_trig == COLLECT_CYCLE) {
            printf_trig = 0;
        }
    }
}

void SenseCurrent(void) {
    cli();
    // TCNT1=0;
    /* 更改A相duty為總周期的一半 */
    OCR1A = (tempDuty >> 1);
    /* 更改B相duty為總周期的一半 */
    OCR1B = (tempDuty >> 1);
    sei();
}

ISR(TIMER1_OVF_vect) {
    isr_count++;
    switch (isr_count) {
        case 1: {
            PORTB |= (1 << 4);
            ADCTrigger1();
            PORTB &= ~(1 << 4);
            break;
        }
        case 2: {
            PORTB |= (1 << 4);
            ADCTrigger2();
            PORTB &= ~(1 << 4);
            break;
        }
        case 3: {
            PORTB |= (1 << 4);
            ADCTrigger3();
            PORTB &= ~(1 << 4);
            break;
        }
        case 4: {
            PORTB |= (1 << 4);
            ADCTrigger4();
            PORTB &= ~(1 << 4);
            break;
        }
        case (PWM_set - 1): {
            // start sense
            SenseCurrent();
            break;
        }
        case PWM_set: {
            // change duty
            updateDuty();
            isr_count = 0;
            break;
        }
        default: {
            break;
        }
    }
}
// #define use_HMI 1
// #define around 1

ISR(ADC_vect) {
    REGGET(&ADCL, 2, &ADC_DATA);  //讀取ADC轉換結果並記憶
    adcResult[isr_count - 1][(PWM_freq * (printf_trig)) + updateDuty_count] =
        ADC_DATA;
    if (isr_count == SAMPLEHOLD_NUM) {
        DISABLE_SAMPLEHOLD();
    }
    // printf("[!], %d, %d, %d\n",isr_count,printf_trig,updateDuty_count);
    if ((isr_count) == (SAMPLEHOLD_NUM) && printf_trig == (COLLECT_CYCLE - 1) &&
        updateDuty_count == (PWM_freq - 1)) {
        // print_count++;
        // if(print_count == PRINT_TRIG){
        // 	#ifdef use_HMI
        // 		HMI_snput_matrix(HMI_TYPE_UI16,4,120,adcResult);
        // 		HMI_snput_matrix(HMI_TYPE_I8,4,120,adcsign);
        // 		HMI_snput_matrix(HMI_TYPE_UI16,1,120,angle_com);
        // 	#else
        // 		for(uint8_t i=0;i<COLLECT_CYCLE;i++){
        // 			for(uint8_t j=0;j<PWM_freq;j++){
        // 				printf("[485],%d,%d,%d,%d,%x,%f,%f,%f,%d\n",adcResult[0][(i*PWM_freq)+j],
        // adcResult[1][(i*PWM_freq)+j], adcResult[2][(i*PWM_freq)+j],
        // adcResult[3][(i*PWM_freq)+j],angle_com[(i*PWM_freq)+j],(float)j,(float)j,MAX_DUTY,PWM_freq);
        // 			}
        // 		}
        // 	#endif

        // 	#ifdef around
        // 		if(angle_com[(PWM_freq*(printf_trig))+updateDuty_count] <
        // (init_angle_com+0x03ff) &&
        // angle_com[(PWM_freq*(printf_trig))+updateDuty_count] >
        // (init_angle_com-0x03ff)){
        // 			// printf("init %x, now angle
        // =%x\n",init_angle_com,angle_com[(PWM_freq*(printf_trig))+updateDuty_count]);
        // 			HMI_snput_matrix(HMI_TYPE_UI16,4,120,adcResult);
        // 			HMI_snput_matrix(HMI_TYPE_UI8,4,120,adcsign);
        // 			HMI_snput_matrix(HMI_TYPE_UI16,1,120,angle_com);
        // 		}
        // 	#elif noaround
        // 		// if(angle_com[(PWM_freq*(printf_trig))+updateDuty_count] <
        // (init_angle_com+0x03ff) &&
        // angle_com[(PWM_freq*(printf_trig))+updateDuty_count] >
        // (init_angle_com-0x03ff)){
        // 		// 	for(uint16_t i=0;i<PWM_freq*COLLECT_CYCLE;i++){
        // 		//
        // printf("[485],%d,%d,%d,%d,%d,%d,%d,%d,%x\n",adcResult[0][i],adcResult[1][i],adcsign[0][i],adcsign[1][i],adcResult[2][i],adcResult[3][i],adcsign[2][i],adcsign[3][i],angle_com[i]);
        // 		// 	}
        // 		// }
        // 	#endif

        // }
        // if(print_count == PRINT_CYCLE){
        // 	print_count=0;
        // }
        // PORTB ^= (1<<4);
        if (upper_limit >= angle_com[PWM_freq * COLLECT_CYCLE - 1] &&
            angle_com[PWM_freq * COLLECT_CYCLE - 1] >= lower_limit) {
            for (uint8_t i = 0; i < COLLECT_CYCLE; i++) {
                for (uint8_t j = 0; j < PWM_freq; j++) {
                    printf("[485],%d,%d,%d,%d,%x,%f,%f,%f,%d\n",
                           adcResult[0][(i * PWM_freq) + j],
                           adcResult[1][(i * PWM_freq) + j],
                           adcResult[2][(i * PWM_freq) + j],
                           adcResult[3][(i * PWM_freq) + j],
                           angle_com[(i * PWM_freq) + j], (float)j, (float)j,
                           MAX_DUTY, PWM_freq);
                    // printf("%f\n",(float)angle_com[(i*PWM_freq)+j]*360/16384);
                }
            }
        }
    }
}
ISR(SPI_STC_vect) {
}