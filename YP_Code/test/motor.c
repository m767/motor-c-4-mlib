/**
 * @file test_identification_fb.c
 * @author nacmvmclab (nacmvmclab@gmail.com)
 * @date 2021.08.20
 * @brief 上升緣下降緣量測電流

 *
 * PWM1     B5 OC1A
 * PWM2     B6 OC1B
 */

#ifndef __AVR_ATmega128__
#    define __AVR_ATmega128__
#endif
#include <avr/io.h>
#include <math.h>
#include <util/delay.h>

// #include "../bsp/control_board.h"
#include "../bsp/hal_as5047d.h"
#include "../control/control_config.h"
#include "../lib/c4mlib.h"
#define NSLEEP_ON() PORTD |= (1 << 6);

extern as50474_t as5047d; /* AS5047D motor encoder IC */

int main(void) {
    C4M_DEVICE_set();
    // spi initial
    init_hw_as5047d();
    SPCR = 213;
    // Encoder initial
    as5047d.init();

    float command = 0;
    printf("START\n");
    NSLEEP_ON()
    // REGFPT(&DDRB, 0x60, 0, 0xff);
    // REGFPT(&PORTB, 0x60, 0, (1 << PB5) | (0 << PB6));  // step_BCoid
    REGFPT(&DDRF, 0x0F, 0, 0xff);
    REGFPT(&PORTF, 0x0F, 0, (1 << PF0) | (0 << PF1) | (0 << PF2) | (0 << PF3));
    _delay_ms(200);

    //先讀一筆值
    as5047d.update();
    as5047d.update();
    command = as5047d.angle;
    printf("%d,%.2lf\n", as5047d.angle, command);
    _delay_ms(100);

    while (1) {
        step();
        _delay_ms(200);

        as5047d.update();
        command += 81.92;

        if (command > 16384) {
            command -= 16384;
        }
        printf("%d,%.2lf\n", as5047d.angle, command);
        _delay_ms(100);
    }
}

void step() {
    static uint8_t AB = 2;
    switch (AB) {
        case 1:
            REGFPT(&PORTF, 0x0F, 0,
                   (1 << PF0) | (0 << PF1) | (0 << PF2) | (0 << PF3));
                   AB=2;
            break;

        case 2:
            REGFPT(&PORTF, 0x0F, 0,
                   (0 << PF0) | (1 << PF1) | (0 << PF2) | (0 << PF3));
                   AB=3;
            break;
        case 3:
            REGFPT(&PORTF, 0x0F, 0,
                   (0 << PF0) | (0 << PF1) | (1 << PF2) | (0 << PF3));
                   AB=4;
            break;
        case 4:
            REGFPT(&PORTF, 0x0F, 0,
                   (0 << PF0) | (0 << PF1) | (0 << PF2) | (1 << PF3));
                   AB=1;
            break;
    }
}

// void step() {
//     static uint8_t AB = 0;
//     if (AB) {
//         step_ACoid();
//         // printf("1\n");
//     }
//     else {
//         step_BCoid();
//         // printf("2\n");
//     }
//     AB ^= 0x01;
// }

// void step_ACoid() {
//     //半步暫態
//     REGFPT(&PORTB, 0x60, 0, (1 << PB5) | (1 << PB6));
//     _delay_ms(50);
//     //全步進
//     REGFPT(&PORTB, 0x60, 0, (0 << PB5) | (1 << PB6));
//     printf("%d\n",PORTB);
// }

// void step_BCoid() {
//     //半步暫態
//     REGFPT(&PORTB, 0x60, 0, (0 << PB5) | (0 << PB6));
//     _delay_ms(50);
//     //全步進
//     REGFPT(&PORTB, 0x60, 0, (1 << PB5) | (0 << PB6));
//     printf("%d\n",PORTB);
// }