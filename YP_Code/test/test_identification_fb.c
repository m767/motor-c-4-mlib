/**
 * @file test_identification_fb.c
 * @author nacmvmclab (nacmvmclab@gmail.com)
 * @date 2021.08.20
 * @brief 上升緣下降緣量測電流
 * ENABLE_EXI_ANGLE_I
 * ENABLE_CURRENT_PI
 *
 * PWM1     B5 OC1A
 * PWM2     B6 OC1B
 * S&H_HOLD D4
 * CS       D5
 * NSLEEP   D6
 *
 * TEST_PIN0 B4
 * TEST_PIN2 D7
 * TEST_PIN3 D0
 * TEST_PIN4 D1
 */
#ifndef __AVR_ATmega128__
#    define __AVR_ATmega128__
#endif
#include <avr/io.h>
#include <math.h>
#include <util/delay.h>

// #include "../bsp/control_board.h"
#include "../bsp/hal_as5047d.h"
#include "../control/control_config.h"
#include "../lib/c4mlib.h"

/* Include control library */
#include "../control/adj_velocity.h"
#include "../control/control.h"
#include "../control/ele_angle.h"
#include "../control/fir_filter.h"
#include "../control/i_excite_angle.h"
#include "../control/pi_current.h"
#include "../control/step_accumulator.h"
#include "../control/svpwm.h"

// (V1*0.000122-0.606)*S1+(V2*0.000122-0.0606)*S2
#define PWM_set 7
// #define COLLECT_CYCLE  1  //
// n個完整svpwm再collect一次，一個svpwm有PWM_freq筆資料 #define PRINT_TRIG     4
// // n個完整svpwm再print一次，一個svpwm有PWM_freq筆資料 #define PRINT_CYCLE 8
// // n個完整svpwm再print一次，一個svpwm有PWM_freq筆資料
#define Pi       3.14159
#define MAX_DUTY (4.5 / 12.0)

#define SAMPLE_SAMPLEHOLD() PORTD &= ~(1 << 4);  // Set Low(Sample)
#define HOLD_SAMPLEHOLD()   PORTD |= (1 << 4);   // Set High(Hold)
#define GET_PWM_CYCLE()     REGGET(&ICR1, 2, &tempDuty0);
#define GET_ADC_SIGNED()    (PIND & 0x40) >> 6
#define NSLEEP_ON()         PORTD |= (1 << 6);

#define measure_center 240  // degree
#define eletrical_ang_revol                                                    \
    8  //一個完整的電子角 機械角理論上走1.8 * 4(2相共4個極)= 7.2度，多取到8
#define upper_limit (45.5) * (measure_center + (eletrical_ang_revol / 2))
#define lower_limit (45.5) * (measure_center - (eletrical_ang_revol / 2))

#define print_data_num      64
#define COMMAND_CHANGE_FREQ 1

void ADCTrigger1(void);
void ADCTrigger2(void);
void ADCTrigger3(void);
void ADCTrigger4(void);
void updateDuty(void);
void SenseCurrent(void);
void Drv8847_init(void);
void Data_init(void);

uint16_t ADC_DATA           = 0;  // ADC結果
uint16_t tempDuty0          = 0;
volatile uint16_t isr_count = 0;

uint8_t print_index = 0;

#define PWM              1
#define USE_WITHOUT_FREQ 1
// #define ENABLE_EXI_ANGLE_I 1
// #define ENABLE_CURRENT_PI 1
// #define debug_printf 1

extern as50474_t as5047d; /* AS5047D motor encoder IC */

// function block init
CANGLEStr_t CANGLE_str                       = CANGLEINI0;
SANGLEStr_t SANGLE_str                       = SANGLEINI0;
I_EXCITEStr_t I_EXCITE_str                   = I_EXCITEINI0;
SVPWMStr_t SVPWM_str                         = SVPWMINI0;
ADJ_VELOCITYStr_t ADJ_VELOCITY_str           = ADJ_VELOCITYINI0;
STEP_CACCUMULATORStr_t STEP_CACCUMULATOR_str = STEP_CACCUMULATORINI0;
STEP_SACCUMULATORStr_t STEP_SACCUMULATOR_str = STEP_SACCUMULATORINI0;
PI_CURRENTStr_t PI_CURRENT_str               = PI_CURRENTINI0;
mv_avgStr_t mv_avg_str_Enc                   = MV_AVGINI_ENC;
mv_avgStr_t mv_avg_str_PWMA                  = MV_AVGINI_PWMA;
mv_avgStr_t mv_avg_str_PWMB                  = MV_AVGINI_PWMB;
fir_t enc_fir; /* 馬達編碼器 FIR filter object */

uint16_t Sangle[print_data_num] = {0}; /*FIR編碼器濾波輸出*/
// float Sele_dangle[print_data_num]={0};  /*感測電子角(度度量)*/
// float Cele_dangle[print_data_num]={0};  /*命令電子角(度度量)*/
// float th_esvpwm[print_data_num]={0};  /*激磁角度*/
// float i_svpwm[print_data_num]={0};  /*激磁電流*/
uint16_t Cangle[print_data_num]       = {0}; /*激磁電流誤差*/
float th_cum[print_data_num]          = {0}; /*角度累計誤差*/
uint8_t PWMA[print_data_num]          = {0}; /*A相PWM Duty*/
uint8_t PWMB[print_data_num]          = {0}; /*B相PWM Duty*/
uint16_t adcResult[4][print_data_num] = {0};
double error                          = 0;

int main(void) {
    C4M_DEVICE_set();

    printf("start  this is test_identification_fb.c\n");

    /* 馬達硬體設定 */
    DDRD |= (1 << 4) | (1 << 6);  // set D4(S&H) D6(NSleep) output

    PWM1_HW_LAY();  // pwm1 initial
    hardware_set(&PWM1HWSet_str);

    // TIM3_HW_LAY();
    // hardware_set(&TIM1_3HWSet_str);

    // /* 周邊硬體設定 */
    init_hw_as5047d();  // spi initial
    SPCR = 213;
    // SPCR = 85;//01010101
    as5047d.init();               // Encoder initial
    ADC_HW_LAY();                 // adc initial
    hardware_set(&ADCHWSet_str);  // 32,64用212,16用215
    /*控制演算法初始化*/
    control_init();  //功能方塊內的連線+初始化
    Data_init();     //功能方塊 與 功能方塊 之間的連線
    control_handle();
    updateDuty();
    // OCR1A=34;
    // OCR1B=54;

    // isr_count=8;
    // REGFPT(&TIMSK, 0x04, 2, 1);  // Disable timer0  Overflow Interrupt
    // sei();
    NSLEEP_ON();  // open DRV8847
    _delay_ms(100);
    /*************************************************************/
    as5047d.update();  // update encoder angle to be first machanical angle
    STEP_SACCUMULATOR_str.th_init =
        as5047d.angle; /*set_saccum_th_init(&s_accum, as5047d.angle);*/
    //初始化init_mach_angle
    // init_sangle(&sangle, as5047d.angle);
    SANGLE_str.init_mach_angle = as5047d.angle;
    CANGLE_str.init_ele_angle  = 0; /*init_cangle(&cangle, N_STEP, 0);*/
    STEP_CACCUMULATOR_str.c_theta_total =
        as5047d.angle / 16384.0 * 360.0 / 1.8 * (float)N_STEP;
    error = (as5047d.angle / 16384.0 * 360.0) -
            (STEP_CACCUMULATOR_str.c_theta_total / (float)N_STEP * 1.8);
    /**************************************************************/
    REGFPT(&TIMSK, 0x04, 2, 1);  // Enable timer0  Overflow Interrupt
    isr_count=100;
    sei();
    SAMPLE_SAMPLEHOLD();
    GET_PWM_CYCLE();  //讀取pwm.cfg中設定的ICR1存成全域變數tempDuty0
    while (1) {
        ;
    }
}

ISR(TIMER1_OVF_vect) {
    isr_count++;
    switch (isr_count) {
        case 1: {
            // ADCTrigger1();
            control_handle();
            as5047d.update();
            break;
        }
        case 2: {
            SenseCurrent();
            break;
        }
        case 3: {
            HOLD_SAMPLEHOLD();
            updateDuty();
            ADCTrigger1();

            // ADCTrigger3();
            // control_handle();
            break;
        }
        case 4: {
            ADCTrigger2();
            // ADCTrigger4();
            break;
        }
        case 5: {
            ADCTrigger3();
            // ADCTrigger4();
            break;
        }
        case (PWM_set - 1): {
            ADCTrigger4();
            SAMPLE_SAMPLEHOLD();
            // start sense
            // SenseCurrent();
            break;
        }
        case PWM_set: {
            // change duty
            // updateDuty();
            // ADCTrigger4();
            // SAMPLE_SAMPLEHOLD();
            // PORTD |= (1 << 0);
            control_print();
            // PORTD &= ~(1 << 0);
            // isr_count = 0;
            break;
        }
        case 1000: {
            isr_count = 0;
            break;
        }
        default: {
            break;
        }
    }
}

void control_init(void) {
    // function block init
    ADJ_VELOCITY_LAY(ADJ_VELOCITY_str);
    I_EXCITE_LAY(I_EXCITE_str);
    PI_CURRENT_LAY(PI_CURRENT_str);
    STEP_CACCUMULATOR_LAY(STEP_CACCUMULATOR_str);
    STEP_SACCUMULATOR_LAY(STEP_SACCUMULATOR_str);

    _delay_ms(5);
    CANGLE_LAY(CANGLE_str);
    SANGLE_LAY(SANGLE_str);

    SVPWM_LAY(SVPWM_str);

    /* Initialize machanical angle */
    for (int i = 0; i < 16; i++) {
        as5047d.update();  // update encoder angle to be first machanical angle
    }

    STEP_SACCUMULATOR_str.th_init =
        as5047d.angle; /*set_saccum_th_init(&s_accum, as5047d.angle);*/
    //初始化init_mach_angle
    // init_sangle(&sangle, as5047d.angle);
    SANGLE_str.init_mach_angle = as5047d.angle;
    CANGLE_str.init_ele_angle  = 0; /*init_cangle(&cangle, N_STEP, 0);*/
    STEP_CACCUMULATOR_str.c_theta_total =
        as5047d.angle / 16384.0 * 360.0 / 1.8 * (float)N_STEP;
    error = (as5047d.angle / 16384.0 * 360.0) -
            (STEP_CACCUMULATOR_str.c_theta_total / (float)N_STEP * 1.8);
    // printf("cc_angle : %f %f \n",
    // STEP_CACCUMULATOR_str.c_theta_total/(float)N_STEP*1.8,as5047d.angle/16384.0*360.0);

#ifdef debug_printf
    printf("init_mach_angle : %x\n", SANGLE_str.init_mach_angle);
#endif
}

void Data_init(void) {                               //各funcion的傳參連結
    SANGLE_str.uIn_p[0]            = &as5047d.angle; /*fir.output是float*/
    STEP_SACCUMULATOR_str.uIn_p[0] = SANGLE_str.uIn_p[0];

    I_EXCITE_str.uIn_p[0] = &SANGLE_str.ele_dangle;
    I_EXCITE_str.uIn_p[1] = &CANGLE_str.ele_dangle;

    PI_CURRENT_str.uIn_p[0] = &I_EXCITE_str.th_er;
    PI_CURRENT_str.uIn_p[1] = &I_EXCITE_str.th_cum;

    SVPWM_str.uIn_p[0] =
        &I_EXCITE_str.th_esvpwm;  //沒回饋=CANGLE_str.ele_dangle
    SVPWM_str.uIn_p[1] =
        &PI_CURRENT_str
             .i_svpwm;  //沒回饋=(+I_SVPWM_HIGH)or (-I_SVPWM_HIGH) //0.9

    CANGLE_str.uIn_p[0]            = &ADJ_VELOCITY_str.add_step;
    STEP_CACCUMULATOR_str.uIn_p[0] = &ADJ_VELOCITY_str.add_step;
}

//==========================命令control_print==============================
//=========================================================================
void control_print(void) {  //沒有print是1.03ms
    static uint8_t prec_cnt = 0;
    // Sangle[print_index] =
    //     as5047d.angle / 16384.0 * 360.0; /*FIR編碼器濾波輸出*/

    // Cangle[print_index] = STEP_CACCUMULATOR_str.c_theta_total / (float)N_STEP
    // *
    //                       1.8;  //+error;  /*激磁電流誤差*/
    Sangle[print_index] = as5047d.angle;

    Cangle[print_index] = STEP_CACCUMULATOR_str.c_theta_total / (float)N_STEP *
                          81.92;  //激磁電流誤差 81.92=16384/200

    // th_er[print_index]  = I_EXCITE_str.th_er;  /*激磁電流誤差*/
    // printf("cc_angle : %f %f \n",
    // STEP_CACCUMULATOR_str.c_theta_total/(float)N_STEP*1.8,as5047d.angle/16384.0*360.0);
    th_cum[print_index] = I_EXCITE_str.th_cum; /*角度累計誤差*/
    PWMA[print_index]   = SVPWM_str.pwmA;      /*A相PWM Duty*/
    PWMB[print_index]   = SVPWM_str.pwmB;      /*B相PWM Duty*/

    // if (++print_index == print_data_num) {
    // if((SANGLE_str.poles<0xF) && (SANGLE_str.poles>0xA)){
    // if ((Sangle[print_index - 1] <= upper_limit) &&
    //     (Sangle[print_index - 1] >= lower_limit)) {
    // if ((PWMA[0] <= 36) && (PWMA[0] >= 32)) {
    //     for (uint8_t i = 0; i < print_data_num; i++) {
    uint8_t i = 0;
    printf("[485],%d,%d,%d,%d,%d,%d,%.2f,%d,%d,%f,%f,%f\n", adcResult[0][i],
           adcResult[1][i], adcResult[2][i], adcResult[3][i], Sangle[i],
           Cangle[i], th_cum[i], PWMA[i], PWMB[i], I_EXCITE_str.pid.ki,
           PI_CURRENT_str.pid.kp, PI_CURRENT_str.pid.ki);
    // printf("%d %d\n",OCR1A,OCR1B);//51-13=38
    // }
    // printf("\n");
    //     print_index = 0;
    // }

    if (++prec_cnt == COMMAND_CHANGE_FREQ) {
        ADJ_VELOCITY_step(&ADJ_VELOCITY_str);
        CANGLE_step(
            &CANGLE_str); /*update_cangle(&cangle, get_cangle_inc(&adj_v));*/
        STEP_CACCUMULATOR_step(&STEP_CACCUMULATOR_str);
        prec_cnt = 0;
    }
}

/**
 * @brief 新增微步step
 * 新增微步計算器
 * @param void_p ADJ_VELOCITYStr_t
 */
void ADJ_VELOCITY_step(void* void_p) {  // get_cangle_inc
    ADJ_VELOCITYStr_t* Str_p = (ADJ_VELOCITYStr_t*)void_p;
    // if要調整的相位超過設定上限
    if (Str_p->residual_phase > Str_p->limit) {
        Str_p->w_phase = Str_p->limit;  //相位角速度(w)=limit
        Str_p->sm      = SM_OVERLIMIT;  //狀態2
    }
    else {
        Str_p->w_phase = Str_p->residual_phase;  //相位角速度(w)=要調整的相位
        Str_p->sm = SM_RUNNING;                  //狀態1
    }
    //剩餘的相位=剩餘的相位-(相位角速度*單位時間)
    Str_p->residual_phase = Str_p->residual_phase - Str_p->w_phase;
    //計算下次要調整的相位
    Str_p->residual_phase += Str_p->add;
    Str_p->add      = 0;
    Str_p->add_step = Str_p->w_back + Str_p->w_phase;
#ifdef debug_printf
    printf("ADJ_VELOCITY_step : // add_step: %x\n", Str_p->add_step);
#endif
}

/**
 * @brief Command電子角除餘器
 * C電子角除餘器
 * CANGLE
 * @param void_p CANGLEStr_t
 * Str_p->uIn_p[0]=ADJ_VELOCITY_str.add_step
 */
void CANGLE_step(void* void_p) {
    CANGLEStr_t* Str_p = (CANGLEStr_t*)void_p;
    Str_p->ele_angle += *Str_p->uIn_p[0];  // ADJ_VELOCITY_str.add_step
    // Str_p->ele_limit:一圈的微步數
    //檢查:ele_angle不超過+-一圈(超過歸0)
    if (Str_p->ele_angle > Str_p->ele_limit ||
        Str_p->ele_angle < -Str_p->ele_limit) {
        Str_p->ele_angle = 0;
    }
    /* 360度餘數 */
    // Str_p->K_degree:(90/Nstep)(電子角/微步)
    Str_p->ele_dangle = (int16_t)(Str_p->ele_angle * Str_p->K_degree) % 360;
#ifdef debug_printf
    printf("Cele_degree angle : %f, ele_angle : %x\n", Str_p->ele_dangle,
           Str_p->ele_angle);
#endif
    // TODO:使用完CANGLE_step要再呼叫下式
    // STEP_CACCUMULATOR_step(&STEP_CACCUMULATOR_str);
}

/**
 * @brief Command微步累加器
 * 收數據用->累加ADJ_VELOCITY_str.add_step再乘上係數轉成長度
 * 命令微步累加器
 *
 * @param void_p STEP_CACCUMULATORStr_t
 */
void STEP_CACCUMULATOR_step(void* void_p) {
    STEP_CACCUMULATORStr_t* Str_p = (STEP_CACCUMULATORStr_t*)void_p;
    Str_p->th_inc = *Str_p->uIn_p[0];  //&ADJ_VELOCITY_str.add_step

    /* accumulate theta */
    Str_p->c_theta_total += Str_p->th_inc;
    Str_p->c_theta_total = Str_p->c_theta_total % (200 * N_STEP);
    /* transfer theta to length */
    Str_p->c_length = (float)Str_p->c_theta_total * Str_p->k_theta2legnth;
}

//==========================回饋control_handle==============================
//==========================================================================
void control_handle(void) {  // 0.7ms
    // S電子角除餘器
    /*原型是updare_sangle，後面緊跟STEP_SACCUMULATOR_step*/
    SANGLE_step(&SANGLE_str);
    STEP_SACCUMULATOR_step(&STEP_SACCUMULATOR_str);
    //激磁角回饋
    I_EXCITE_step(&I_EXCITE_str);
    //角差PI調電流
    PI_CURRENT_step(&PI_CURRENT_str);
    //調整SVPWM_str->pwmA  SVPWM_str->pwmB
    SVPWM_step(&SVPWM_str);

#ifdef debug_printf
    printf("pmwa : %d, pwmb: %d\n", mv_avg_str_PWMA.mv_output,
           mv_avg_str_PWMB.mv_output);
#endif

    // cli();
    // /* 更改A相duty */
    // OCR1A=mv_avg_str_PWMA.mv_output;
    // /* 更改B相duty */
    // OCR1B=mv_avg_str_PWMB.mv_output;
    // sei();

    // cli();
    // /* 更改A相duty */
    // OCR1A=(uint8_t)SVPWM_str.pwmA;
    // /* 更改B相duty */
    // OCR1B=(uint8_t)SVPWM_str.pwmB;
    // sei();
}

/**
 * @brief Sensor電子角除餘器
 * SANGLE
 * @param void_p SANGLEStr_t
 */
void SANGLE_step(void* void_p) {  // update_sangle
    SANGLEStr_t* Str_p = (SANGLEStr_t*)void_p;
    Str_p->enc_angle   = *Str_p->uIn_p[0];  // as5047d.angle
#ifdef debug_printf
    printf("SANGLE_step : // enc_angle : %x ", Str_p->enc_angle);
#endif
    //馬達目前位置-馬達初始位置
    int16_t temp_mach = Str_p->enc_angle - Str_p->init_mach_angle;
    if (temp_mach < 0) {
        temp_mach += MAX_ENCODER_VALUE;
    }
    Str_p->delta_angle = temp_mach - Str_p->mach_angle;
    Str_p->mach_angle  = temp_mach;

    if (abs_int(Str_p->delta_angle) > HALF_ENCODER_VALUE) {
        if (Str_p->delta_angle < 0) {
            Str_p->delta_angle += MAX_ENCODER_VALUE;
        }
        else {
            Str_p->delta_angle -= MAX_ENCODER_VALUE;
        }
    }
    Str_p->ele_angle += Str_p->delta_angle;
    if (Str_p->ele_angle > ENC_ELE_ANGLE_TH_L) {
        Str_p->ele_angle -= ENC_ELE_ANGLE_TH_H;
        Str_p->err_count += ENC_ELE_ANGLE_ER;
        if (Str_p->err_count > ENC_ELE_ANGLE_ER_TH) {
            Str_p->ele_angle++;
            Str_p->err_count -= EMC_ELE_AMGLE_COUNT;
        }
        Str_p->poles++;
        if (Str_p->poles >= MAX_ELETRICAL_ROTOR_ANGLE) {
            Str_p->poles -= MAX_ELETRICAL_ROTOR_ANGLE;
        }
    }
    else if (Str_p->ele_angle < -ENC_ELE_ANGLE_TH_L) {
        Str_p->ele_angle += ENC_ELE_ANGLE_TH_H;
        Str_p->err_count -= ENC_ELE_ANGLE_ER;
        if (Str_p->err_count < -ENC_ELE_ANGLE_ER_TH) {
            Str_p->ele_angle--;
            Str_p->err_count += EMC_ELE_AMGLE_COUNT;
        }
        Str_p->poles--;
        if (Str_p->poles < 0) {
            Str_p->poles += MAX_ELETRICAL_ROTOR_ANGLE;
        }
    }

    /* 順時針為正 */
    Str_p->ele_dangle = (float)Str_p->ele_angle * Str_p->K_degree;
#ifdef debug_printf
    printf("angle : %x, degree : %f\n", Str_p->ele_angle, Str_p->ele_dangle);
#endif
    // TODO:使用完SANGLE_step要在呼叫下式
    // STEP_SACCUMULATOR_step(&STEP_SACCUMULATOR_str);
}

/**
 * @brief 實際微步累加器
 * STEP_SACCUMULATOR工作方塊步級執行函式
 * @param void_p
 */
void STEP_SACCUMULATOR_step(void* void_p) {  // update_step_saccum
    STEP_SACCUMULATORStr_t* Str_p =
        (STEP_SACCUMULATORStr_t*)void_p; /*typeset the str pointer */
    Str_p->th_s = (uint16_t)roundf(*Str_p->uIn_p[0]);
    /* save last delta theta value */
    Str_p->delta_th = (uint16_t)roundf(*Str_p->uIn_p[0]) - Str_p->last_th_s;
    if (abs_int(Str_p->delta_th) > SENSOR_HALF) {
        /* ZERO to MAX */
        if (Str_p->delta_th > 0) {
            Str_p->cycles++;
        }
        /* MAX to ZERO */
        else {
            Str_p->cycles--;
        }
    }
    /* transfer theta to length */
    Str_p->s_length =
        (float)(Str_p->cycles * SENSOR_RES +
                (uint16_t)roundf(*Str_p->uIn_p[0]) - Str_p->th_init) *
        Str_p->k_theta2legnth;
    /* update last sensor theta */
    Str_p->last_th_s = (uint16_t)roundf(*Str_p->uIn_p[0]);
}

/**
 * @brief I_EXCITE_step
 *
 * @param void_p
 *
 * I_EXCITE_str.uIn_p[0] = &SANGLE_str.ele_dangle;
 * I_EXCITE_str.uIn_p[1] = &CANGLE_str.ele_dangle;
 */
void I_EXCITE_step(void* void_p) {  // cal_exc_ang_correct
    I_EXCITEStr_t* Str_p = (I_EXCITEStr_t*)void_p;
    Str_p->e_sdegree     = (*Str_p->uIn_p[0]);
    Str_p->e_cdegree     = (*Str_p->uIn_p[1]);
#ifdef debug_printf
    printf("I_EXCITE_step : // e_sdegree : %f, e_cdegree : %f  ",
           (*Str_p->uIn_p[0]), (*Str_p->uIn_p[1]));
#endif
    /* 計算C電子角(命令) 與 S電子角誤差(感測) (度度量) */
    /* th_er > 0 領先, th_er < 0 落後 */
    Str_p->th_er = (*Str_p->uIn_p[0]) - (*Str_p->uIn_p[1]);
    if (abs_float(Str_p->th_er) >= 180) {
        if ((*Str_p->uIn_p[0]) > (*Str_p->uIn_p[1])) {
            /* 馬達落後 */
            Str_p->th_er -= 360;
        }
        else {
            /* 馬達領先 */
            Str_p->th_er += 360;
        }
    }

    Str_p->th_cum += Str_p->th_er; /* 累計誤差 */

    /* 限制上下界 */
    if (Str_p->th_cum >= Str_p->cum_limit)
        Str_p->th_cum = Str_p->cum_limit;
    if (Str_p->th_cum <= -Str_p->cum_limit)
        Str_p->th_cum = -Str_p->cum_limit;
#ifdef debug_printf
    printf("the_er : %f, th_cum %f  ", Str_p->th_er, Str_p->th_cum);
#endif
#ifdef ENABLE_EXI_ANGLE_I
    /* 計算 th_esvpwm 值 (角差I回饋) */
    Str_p->th_esvpwm = (*Str_p->uIn_p[1]) - Str_p->pid.ki * Str_p->th_cum;
    /* theta svpwm to positive */
    if (Str_p->th_esvpwm > 360)
        Str_p->th_esvpwm -= 360;
    else if (Str_p->th_esvpwm < 0)
        Str_p->th_esvpwm += 360;
#    ifdef debug_printf
    printf("Using EXI angle I \n");
#    endif
    /* Uncomment the below code, it will cause system unstable sometimes */
    /* Constrain to  (e_sdegree - 90) ~ (e_sdegree + 90)*/
    // float er_sensor_svpwm = e_sdegree - fb_exc_angle->th_esvpwm;
    // if(er_sensor_svpwm > 180) {
    //     er_sensor_svpwm -= 360;
    // }
    // else if(er_sensor_svpwm < -180){
    //     er_sensor_svpwm += 360;
    // }
    // if(er_sensor_svpwm > 90) {
    //     /* Over 90 degree */
    //     fb_exc_angle->th_cum += SVPWM_LIMIT_UNIT;
    // }
    // else if(er_sensor_svpwm < -90) {
    //     fb_exc_angle->th_cum -= SVPWM_LIMIT_UNIT;
    // }

#else
    Str_p->th_esvpwm = (*Str_p->uIn_p[1]);
#    ifdef debug_printf
    printf("Not Using EXI angle I \n");
#    endif
#endif

    /* Calculate and save omega (rad/s) */
    float delta_theta = (*Str_p->uIn_p[0]) - Str_p->last_th;
    if (delta_theta > 180) {
        delta_theta -= 360;
    }
    else if (delta_theta < -180) {
        delta_theta += 360;
    }
    Str_p->w = delta_theta * DELTA_THETA_TO_OMEGA;

    Str_p->last_th = (*Str_p->uIn_p[0]);
}

/**
 * @brief PI_CURRENT_step
 *
 * @param void_p
 *
 * PI_CURRENT_str.uIn_p[0] = &I_EXCITE_str.th_er;
 * PI_CURRENT_str.uIn_p[1] = &I_EXCITE_str.th_cum;
 */
void PI_CURRENT_step(void* void_p) {  // cal_current_correct
    PI_CURRENTStr_t* Str_p =
        (PI_CURRENTStr_t*)void_p; /*typeset the str pointer */
    Str_p->th_er  = *Str_p->uIn_p[0];
    Str_p->th_cum = *Str_p->uIn_p[1];
#ifdef debug_printf
    printf("PI_CURRENT_step : // th_er : %f, th_cum : %f  ", *Str_p->uIn_p[0],
           *Str_p->uIn_p[1]);
#endif
#ifdef ENABLE_CURRENT_PI
    /* C電子角(命令) 與 S電子角誤差(感測)、 累計誤差，作電流PI回饋 */
    Str_p->i_svpwm = I_SVPWM_EXI_BASE - (Str_p->pid.kp * (*Str_p->uIn_p[0])) -
                     (Str_p->pid.ki * (*Str_p->uIn_p[1]));

    /* 限制上下界 */
    if (Str_p->i_svpwm < Str_p->low_limit)
        Str_p->i_svpwm = Str_p->low_limit;
    if (Str_p->i_svpwm > Str_p->high_limit)
        Str_p->i_svpwm = Str_p->high_limit;
#    ifdef debug_printf
    printf("ENABLE_CURRENT_PI, isvpwm : %f\n", Str_p->i_svpwm);
#    endif
#else
    if (Str_p->th_er < 0) {
        Str_p->i_svpwm = -Str_p->low_limit;
    }
    else {
        Str_p->i_svpwm = Str_p->high_limit;
    }
#    ifdef debug_printf
    printf("DISABLE_CURRENT_PI, isvpwm : %f\n", Str_p->i_svpwm);
#    endif
#endif
}

/**
 * @brief SVPWM_step
 *
 * @param void_p
 *
 * SVPWM_str.uIn_p[0] = &I_EXCITE_str.th_esvpwm;//CANGLE_str.ele_dangle
 * SVPWM_str.uIn_p[1] = &PI_CURRENT_str.i_svpwm;//沒回饋=(+I_SVPWM_HIGH)or
 * (-I_SVPWM_HIGH) //0.9
 */
void SVPWM_step(void* void_p) {              // cal_pwmAB
    SVPWMStr_t* Str_p = (SVPWMStr_t*)void_p; /*typeset the str pointer */
    Str_p->th_esvpwm  = *Str_p->uIn_p[0];
    Str_p->i_svpwm    = *Str_p->uIn_p[1];
#ifdef debug_printf
    printf("SVPWM_step : // th_esvpwm : %f, i_svpwm : %f  ", Str_p->th_esvpwm,
           Str_p->i_svpwm);
#endif
    float temp_ang = *Str_p->uIn_p[0] * (3.14159 / 180);  //單位轉換：角度換弧度
    float ia_svpwm;
    float ib_svpwm;
#ifdef ENABLE_CURRENT_PI
    /* 由角差I回饋 和 電流PI回饋計算SVPWM */
    if (*Str_p->uIn_p[1] < 0) {
        ia_svpwm = (-*Str_p->uIn_p[1]) * I_SVPWM_GAIN * sinf(temp_ang);
        ib_svpwm = (-*Str_p->uIn_p[1]) * I_SVPWM_GAIN * cosf(temp_ang);
    }
    else {
        ia_svpwm = (*Str_p->uIn_p[1]) * I_SVPWM_GAIN * sinf(temp_ang);
        ib_svpwm = (*Str_p->uIn_p[1]) * I_SVPWM_GAIN * cosf(temp_ang);
    }

#    ifdef debug_printf
    printf("using ENABLE_CURRENT_PI, ia_svpwm : %f, ib_svpwm : %f ", ia_svpwm,
           ia_svpwm);
#    endif
#else
    ia_svpwm = MAX_DUTY * sinf(temp_ang);
    ib_svpwm = MAX_DUTY * cosf(temp_ang);

#    ifdef debug_printf
    printf("Not using ENABLE_CURRENT_PI, ia_svpwm : %f, ib_svpwm : %f ",
           ia_svpwm, ia_svpwm);
#    endif
#endif
    Str_p->pwmA = ((int8_t)(ia_svpwm * PERIOD_COUNT) + PERIOD_COUNT) >> 1;
    Str_p->pwmB = ((int8_t)(ib_svpwm * PERIOD_COUNT) + PERIOD_COUNT) >> 1;
#ifdef debug_printf
    printf("pwmA : %d, pwmB : %d\n", Str_p->pwmA, Str_p->pwmB);
#endif

    // TODO:移植set mv avg
    // set_mv_avg(&pwma_mv_avg, pwmAB->pwm1);
    // set_mv_avg(&pwmb_mv_avg, pwmAB->pwm2);
}

//==================================工具===================================
//=========================================================================
void updateDuty(void) {
    cli();
    /* 更改A相duty */
    OCR1A = (uint8_t)SVPWM_str.pwmA;
    /* 更改B相duty */
    OCR1B = (uint8_t)SVPWM_str.pwmB;
    sei();
}

void SenseCurrent(void) {
    cli();
    // TCNT1=0;
    /* 更改A相duty為總周期的一半 */
    OCR1A = (tempDuty0 >> 1);
    /* 更改B相duty為總周期的一半 */
    OCR1B = (tempDuty0 >> 1);
    sei();
}

//==================================ADC====================================
//=========================================================================
ISR(ADC_vect) {
    REGGET(&ADCL, 2, &ADC_DATA);  //讀取ADC轉換結果並記憶
    switch (isr_count) {
        case 3: {
            adcResult[0][print_index] = ADC_DATA;
            break;
        }
        case 4: {
            adcResult[1][print_index] = ADC_DATA;
            break;
        }
        case 5: {
            adcResult[2][print_index] = ADC_DATA;
            break;
        }
        case (PWM_set - 1): {
            adcResult[3][print_index] = ADC_DATA;
            break;
        }
        default:
            break;
    }
}

/**
 * @brief A Rising
 *
 */
void ADCTrigger1(void) {
    REGFPT(&ADMUX, 0x1F, 0, 0x00);  //選擇輸入訊號為ADC0(Port F0)/sigleE_0_x1
    _delay_us(2);
    REGFPT(&ADCSRA, 0x40, 6, 1);  //觸發ADC轉換
}

/**
 * @brief A Falling
 *
 */
void ADCTrigger2(void) {
    REGFPT(&ADMUX, 0x1F, 0, 0x01);  //選擇輸入訊號為ADC1(Port F1)/sigleE_1_x1
    _delay_us(2);
    REGFPT(&ADCSRA, 0x40, 6, 1);  //觸發ADC轉換
}

/**
 * @brief B Rising
 *
 */
void ADCTrigger3(void) {
    REGFPT(&ADMUX, 0x1F, 0, 0x02);  //選擇輸入訊號為ADC2(Port F2)/sigleE_2_x1
    _delay_us(2);
    REGFPT(&ADCSRA, 0x40, 6, 1);  //觸發ADC轉換
}

/**
 * @brief B Falling
 *
 */
void ADCTrigger4(void) {
    REGFPT(&ADMUX, 0x1F, 0, 0x03);  //選擇輸入訊號為ADC3(Port F3)/sigleE_3_x1
    _delay_us(2);
    REGFPT(&ADCSRA, 0x40, 6, 1);  //觸發ADC轉換
}

// void FIR_Handle(void) {  // 0.17ms
//     as5047d.update();
// #ifdef debug_printf
//     printf("\nencoder : %x\n", as5047d.angle);
// #endif
//     /* Encoder boundary threshold constrain */
//     /* Moving average */
//     // if((as5047d.angle > 16370) || (as5047d.angle < 10)) {
//     //     uint16_t temp = enc_mv_avg.mv_output;
//     //     set_mv_avg(&enc_mv_avg, as5047d.angle);
//     //     if(enc_mv_avg.mv_output < 16370 && enc_mv_avg.mv_output > 20) {
//     //         enc_mv_avg.mv_output = temp;
//     //     }
//     // }
//     // else {
//     //     set_mv_avg(&enc_mv_avg, as5047d.angle);
//     // }
//     /* FIR filter */
//     if ((as5047d.angle > 16370) || (as5047d.angle < 10)) {
//         float temp = enc_fir.Sangle;
//         set_fir(&enc_fir, (float)as5047d.angle);
//         if (enc_fir.Sangle < 16370 && enc_fir.Sangle > 20) {
//             enc_fir.Sangle = temp;
//         }
//     }
//     else {
//         set_fir(&enc_fir, (float)as5047d.angle);
//     }
// }
