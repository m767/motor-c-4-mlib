#ifndef __AVR_ATmega128__
#    define __AVR_ATmega128__
#endif
// #include <avr/io.h>
// #include <util/delay.h>
// #include "lib/c4mlib.h"
// #include <math.h>

#include "../bsp/control_board.h"
#include "../bsp/hal_as5047d.h"

extern as50474_t as5047d; /* AS5047D motor encoder IC */

int main(void) {
    C4M_DEVICE_set();
    printf("\nstart test spi\n");
    board_init();
    printf("hardware_set report \n");
    printf("SPI2SPEED = %d\n", ((SPSR & 0x01) >> 0));
    printf("SPR0_1    = %d\n", ((SPCR & 0x03) >> 0));
    printf("CPHASE    = %d\n", ((SPCR & 0x04) >> 2));
    printf("CPOLAR    = %d\n", ((SPCR & 0x08) >> 3));
    printf("MSSELECT  = %d\n", ((SPCR & 0x10) >> 4));
    printf("DORDER    = %d\n", ((SPCR & 0x20) >> 5));
    printf("DDx0_3    = %d\n", ((DDRB & 0x0F) >> 0));
    printf("SPE       = %d\n", ((SPCR & 0x40) >> 6));
    printf("SPIE      = %d\n", ((SPCR & 0x80) >> 7));
    printf("SPCR  = %d\n", SPCR);
    SPCR = 215;
    printf("SPCR  = %d\n", SPCR);
    as5047d.init();
    sei();

    uint16_t angle;
    uint16_t angle_com;
    uint16_t dia;
    printf("in while\n");
    while (1) {
        angle     = as5047d.instance->read_angle();
        angle_com = as5047d.instance->read_anglecom();
        _delay_ms(10);
        printf("%x %x\n", angle, angle_com);
        // printf("%f\n",(float)angle*360/16384);
    }
}
ISR(SPI_STC_vect) {
}