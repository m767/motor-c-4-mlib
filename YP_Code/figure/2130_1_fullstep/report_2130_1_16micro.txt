Data size is 201
===== velocity =====
theta/s: 323.38308457711446
RPM: 53.89718076285241
===== sensor ele angle =====
max: 16305.0
min: 9.0
mean: 8146.955
===== command ele angle =====
max: 16302.0
min: 0.0
mean: 8122.6416
===== theta error (degree) =====
max: 16302.0
min: 0.0
mean: 8122.6416
std: 4734.3164
MSE: 88391060.0
N_STEP: 0.0001104158998895841, inf
===== consume power (watt) =====
max: 2.2002797
min: 0.067302436
mean: 1.5344145
std: 0.36095136
sum: 308.41733 J
===== current  =====
ia_max: 0.53256726 A
ia_min: -0.58253086 A
ia_mean: -0.046941217 A
ib_max: 0.5450581 A
ib_min: -0.45307952 A
ib_mean: 0.020722147 A
===== current slope =====
ia_slope_max: 12489.584 A
ia_slope_min: 5295.5723 A
ia_slope_mean: 6954.2705 A
ib_slope_max: 19195.312 A
ib_slope_min: 3342.4473 A
ib_slope_mean: 7794.9565 A
===== current RMS Square (watt) =====
rms_ia_square: 0.12909803 A
rms_ib_square: 0.11328884 A
===== current RMS (watt) =====
rms_ia: 0.35930213 A
rms_ib: 0.3365841 A
===== P_loss (watt) =====
P_loss_A: 0.8372007112205029 W
P_loss_B: 0.7958893422782422 W
===== P_in (watt) =====
P_in_A: 1.3432393 W
P_in_B: 1.3432393 W
===== Eta (H) (%) =====
P_efficient_A: 37.67300386416005 %
P_efficient_B: 48.00437212091632 %
P_all: 42.83868799253819 %
