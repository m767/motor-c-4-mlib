Data size is 301
===== velocity =====
theta/s: 0.05538281412219685
RPM: 0.009230469020366143
===== sensor ele angle =====
max: 138.87
min: 138.58
mean: 138.72661
===== command ele angle =====
max: 138.71
min: 138.71
mean: 138.71
===== theta error (degree) =====
max: 138.71
min: 138.71
mean: 138.71
std: 0.0
MSE: 19240.469
N_STEP: 0.01297671337954055, 0.01297671337954055
===== consume power (watt) =====
max: 1.796718
min: 0.023598
mean: 0.9908385
std: 0.5710959
sum: 298.2424 J
===== current  =====
ia_max: -0.04428597 A
ia_min: -0.06926781 A
ia_mean: -0.05853113 A
ib_max: 0.18282159 A
ib_min: -0.53597385 A
ib_mean: -0.34292865 A
===== current slope =====
ia_slope_max: 6890.625 A
ia_slope_min: 6174.4785 A
ia_slope_mean: 6634.4253 A
ib_slope_max: 8778.644 A
ib_slope_min: -13552.084 A
ib_slope_mean: 3780.2239 A
===== current RMS Square (watt) =====
rms_ia_square: 0.0034486689 A
rms_ib_square: 0.15722258 A
===== current RMS (watt) =====
rms_ia: 0.05872537 A
rms_ib: 0.39651304 A
===== P_loss (watt) =====
P_loss_A: 0.022364617604762316 W
P_loss_B: 0.021261043567210435 W
===== P_in (watt) =====
P_in_A: 0.29265562 W
P_in_B: 0.29265562 W
===== Eta (H) (%) =====
P_efficient_A: 92.35804259145186 %
P_efficient_B: -231.20062400374425 %
P_all: -69.4212907061462 %
