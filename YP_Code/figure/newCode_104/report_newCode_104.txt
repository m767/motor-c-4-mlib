Data size is 301
===== velocity =====
theta/s: 94.90586594489723
RPM: 15.817644324149539
===== sensor ele angle =====
max: 359.12
min: 324.84
mean: 341.91913
===== command ele angle =====
max: 358.76
min: 325.01
mean: 341.88705
===== theta error (degree) =====
max: 358.76
min: 325.01
mean: 341.88705
std: 9.774975
MSE: 116982.3
N_STEP: 0.0050172816116710595, 0.005538290963093835
===== consume power (watt) =====
max: 1.0661479
min: 0.58372486
mean: 0.8248282
std: 0.124838576
sum: 248.27328 J
===== current  =====
ia_max: 0.33271256 A
ia_min: -0.39857376 A
ia_mean: -0.043259837 A
ib_max: 0.38948944 A
ib_min: -0.34974563 A
ib_mean: 0.023944383 A
===== current slope =====
ia_slope_max: 7281.2495 A
ia_slope_min: 4384.114 A
ia_slope_mean: 5628.8794 A
ib_slope_max: 7932.291 A
ib_slope_min: 5035.156 A
ib_slope_mean: 6282.7334 A
===== current RMS Square (watt) =====
rms_ia_square: 0.06700416 A
rms_ib_square: 0.06341578 A
===== current RMS (watt) =====
rms_ia: 0.25885162 A
rms_ib: 0.25182492 A
===== P_loss (watt) =====
P_loss_A: 0.43452197171747686 W
P_loss_B: 0.41308064080774787 W
===== P_in (watt) =====
P_in_A: 1.1552873 W
P_in_B: 1.1552873 W
===== Eta (H) (%) =====
P_efficient_A: 62.38840463414841 %
P_efficient_B: 66.15921430352859 %
P_all: 64.2738094688385 %
