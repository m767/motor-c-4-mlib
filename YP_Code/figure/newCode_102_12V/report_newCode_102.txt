Data size is 301
===== velocity =====
theta/s: 92.27571033296132
RPM: 15.379285055493554
===== sensor ele angle =====
max: 270.9
min: 237.57
mean: 254.15259
===== command ele angle =====
max: 271.46
min: 237.71
mean: 254.58698
===== theta error (degree) =====
max: 271.46
min: 237.71
mean: 254.58698
std: 9.775474
MSE: 64910.086
N_STEP: 0.006630811377955372, 0.007572251689709764
===== consume power (watt) =====
max: 1.9880495
min: 1.0059377
mean: 1.4219704
std: 0.2280709
sum: 428.0131 J
===== current  =====
ia_max: 0.45080847 A
ia_min: -0.5450582 A
ia_mean: -0.021899661 A
ib_max: 0.5064498 A
ib_min: -0.47579035 A
ib_mean: 0.022118477 A
===== current slope =====
ia_slope_max: 9332.029 A
ia_slope_min: 5002.6035 A
ia_slope_mean: 6934.6396 A
ib_slope_max: 10145.831 A
ib_slope_min: 6174.4785 A
ib_slope_mean: 7730.598 A
===== current RMS Square (watt) =====
rms_ia_square: 0.111858405 A
rms_ib_square: 0.11317013 A
===== current RMS (watt) =====
rms_ia: 0.3344524 A
rms_ib: 0.3364077 A
===== P_loss (watt) =====
P_loss_A: 0.7254017575457693 W
P_loss_B: 0.6896070678904652 W
===== P_in (watt) =====
P_in_A: 1.4789721 W
P_in_B: 1.4789721 W
===== Eta (H) (%) =====
P_efficient_A: 50.952302031565345 %
P_efficient_B: 52.8257581932544 %
P_all: 51.88903011240987 %
