Data size is 2002
===== velocity =====
theta/s: 304.2790542790543
RPM: 50.713175713175715
===== sensor ele angle =====
max: 16350.0
min: 51.0
mean: 8200.67
===== command ele angle =====
max: 16302.0
min: 0.0
mean: 8144.995
===== theta error (degree) =====
max: 16302.0
min: 0.0
mean: 8144.995
std: 4730.5156
MSE: 88718720.0
N_STEP: 0.0001104158998895841, inf
===== consume power (watt) =====
max: 2.0479763
min: 1.0358018
mean: 1.372822
std: 0.27394888
sum: 2748.3896 J
===== current  =====
ia_max: 0.44853738 A
ia_min: -0.5620911 A
ia_mean: -0.046017077 A
ib_max: 0.5155341 A
ib_min: -0.44853738 A
ib_mean: 0.009930582 A
===== current slope =====
ia_slope_max: 9266.926 A
ia_slope_min: 4872.3955 A
ia_slope_mean: 6834.3984 A
ib_slope_max: 9722.654 A
ib_slope_min: 6076.8223 A
ib_slope_mean: 7866.6333 A
===== current RMS Square (watt) =====
rms_ia_square: 0.11194732 A
rms_ib_square: 0.105097935 A
===== current RMS (watt) =====
rms_ia: 0.3345853 A
rms_ib: 0.3241881 A
===== P_loss (watt) =====
P_loss_A: 0.7259783728048206 W
P_loss_B: 0.6901552302762866 W
===== P_in (watt) =====
P_in_A: 1.2571232 W
P_in_B: 1.2571232 W
===== Eta (H) (%) =====
P_efficient_A: 42.25081882270034 %
P_efficient_B: 48.459407129183084 %
P_all: 45.35511297594172 %
