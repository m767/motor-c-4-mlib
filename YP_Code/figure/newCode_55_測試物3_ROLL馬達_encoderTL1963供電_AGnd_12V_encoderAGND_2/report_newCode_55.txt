Data size is 301
===== velocity =====
theta/s: 96.48395931205877
RPM: 16.08065988534313
===== sensor ele angle =====
max: 291.47
min: 256.57
mean: 275.8728
===== command ele angle =====
max: 294.19
min: 260.44
mean: 277.3123
===== theta error (degree) =====
max: 294.19
min: 260.44
mean: 277.3123
std: 9.775206
MSE: 76997.65
N_STEP: 0.006118494799491039, 0.006911380675497282
===== consume power (watt) =====
max: 1.9351683
min: 0.914009
mean: 1.3425045
std: 0.24329671
sum: 404.09387 J
===== current  =====
ia_max: 0.44399527 A
ia_min: -0.5371094 A
ia_mean: -0.022774892 A
ib_max: 0.48033252 A
ib_min: -0.4712482 A
ib_mean: 0.020741494 A
===== current slope =====
ia_slope_max: 9266.926 A
ia_slope_min: 4872.3955 A
ia_slope_mean: 6885.7573 A
ib_slope_max: 10243.487 A
ib_slope_min: 6207.031 A
ib_slope_mean: 7803.3804 A
===== current RMS Square (watt) =====
rms_ia_square: 0.10981749 A
rms_ib_square: 0.10241676 A
===== current RMS (watt) =====
rms_ia: 0.33138722 A
rms_ib: 0.3200262 A
===== P_loss (watt) =====
P_loss_A: 0.7121664225310087 W
P_loss_B: 0.6770248257368803 W
===== P_in (watt) =====
P_in_A: 1.4640703 W
P_in_B: 1.4640703 W
===== Eta (H) (%) =====
P_efficient_A: 51.35708901823326 %
P_efficient_B: 56.873701745253356 %
P_all: 54.115395381743305 %
