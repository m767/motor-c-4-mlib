Data size is 301
===== velocity =====
theta/s: 1.4396574542107905
RPM: 0.23994290903513174
===== sensor ele angle =====
max: 144.73
min: 143.44
mean: 144.00877
===== command ele angle =====
max: 144.0
min: 144.0
mean: 144.0
===== theta error (degree) =====
max: 144.0
min: 144.0
mean: 144.0
std: 0.0
MSE: 20736.0
N_STEP: 0.0125, 0.0125
===== consume power (watt) =====
max: 1.7309822
min: 0.056005392
mean: 1.1644429
std: 0.4480097
sum: 350.4973 J
===== current  =====
ia_max: -0.04542151 A
ia_min: -0.06926781 A
ia_mean: -0.059625164 A
ib_max: 0.1919059 A
ib_min: -0.5268895 A
ib_mean: -0.40641692 A
===== current slope =====
ia_slope_max: 6760.416 A
ia_slope_min: 6174.4785 A
ia_slope_mean: 6561.534 A
ib_slope_max: 8778.644 A
ib_slope_min: -13421.877 A
ib_slope_mean: 5896.3257 A
===== current RMS Square (watt) =====
rms_ia_square: 0.0035658842 A
rms_ib_square: 0.18528183 A
===== current RMS (watt) =====
rms_ia: 0.059715025 A
rms_ib: 0.43044376 A
===== P_loss (watt) =====
P_loss_A: 0.023124759006313982 W
P_loss_B: 0.02198367606382817 W
===== P_in (watt) =====
P_in_A: 0.2981258 W
P_in_B: 0.2981258 W
===== Eta (H) (%) =====
P_efficient_A: 92.2432883242213 %
P_efficient_B: -283.1478042507769 %
P_all: -95.4522579632778 %
