Data size is 301
===== velocity =====
theta/s: 0.05534056941272149
RPM: 0.00922342823545358
===== sensor ele angle =====
max: 348.51
min: 347.83
mean: 348.178
===== command ele angle =====
max: 348.07
min: 348.07
mean: 348.07004
===== theta error (degree) =====
max: 348.07
min: 348.07
mean: 348.07004
std: 3.0517578e-05
MSE: 121152.74
N_STEP: 0.0051713734654630665, 0.0051713734654630665
===== consume power (watt) =====
max: 1.5948658
min: 0.93271446
mean: 1.3643517
std: 0.11504055
sum: 410.66986 J
===== current  =====
ia_max: 0.027252905 A
ia_min: -0.07153887 A
ia_mean: -0.036061816 A
ib_max: -0.3838118 A
ib_min: -0.5087209 A
ib_mean: -0.4684149 A
===== current slope =====
ia_slope_max: 6011.7183 A
ia_slope_min: 3114.5828 A
ia_slope_mean: 4966.5903 A
ib_slope_max: 9006.508 A
ib_slope_min: 5458.333 A
ib_slope_mean: 7651.3257 A
===== current RMS Square (watt) =====
rms_ia_square: 0.0015619394 A
rms_ib_square: 0.2198426 A
===== current RMS (watt) =====
rms_ia: 0.039521378 A
rms_ib: 0.46887377 A
===== P_loss (watt) =====
P_loss_A: 0.010129176795599052 W
P_loss_B: 0.009629356198129244 W
===== P_in (watt) =====
P_in_A: 0.18796737 W
P_in_B: 0.18796737 W
===== Eta (H) (%) =====
P_efficient_A: 94.61120484346576 %
P_efficient_B: -621.0451368318613 %
P_all: -263.2169659941978 %
