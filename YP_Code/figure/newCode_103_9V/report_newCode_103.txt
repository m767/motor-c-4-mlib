Data size is 301
===== velocity =====
theta/s: 91.74967921057414
RPM: 15.291613201762356
===== sensor ele angle =====
max: 313.53
min: 280.39
mean: 296.83368
===== command ele angle =====
max: 314.1
min: 280.35
mean: 297.22476
===== theta error (degree) =====
max: 314.1
min: 280.35
mean: 297.22476
std: 9.775269
MSE: 88438.11
N_STEP: 0.0057306589144311805, 0.006420545606606384
===== consume power (watt) =====
max: 1.537331
min: 0.5740321
mean: 0.82074845
std: 0.1310607
sum: 247.04529 J
===== current  =====
ia_max: 0.33725473 A
ia_min: -0.4087936 A
ia_mean: -0.015769262 A
ib_max: 0.39403164 A
ib_min: -0.49850103 A
ib_mean: 0.017202832 A
===== current slope =====
ia_slope_max: 7411.458 A
ia_slope_min: 4253.906 A
ia_slope_mean: 5757.682 A
ib_slope_max: 8062.4995 A
ib_slope_min: 5132.812 A
ib_slope_mean: 6344.268 A
===== current RMS Square (watt) =====
rms_ia_square: 0.06352538 A
rms_ib_square: 0.06641301 A
===== current RMS (watt) =====
rms_ia: 0.2520424 A
rms_ib: 0.2577072 A
===== P_loss (watt) =====
P_loss_A: 0.41196208089590075 W
P_loss_B: 0.391633959710598 W
===== P_in (watt) =====
P_in_A: 1.1155432 W
P_in_B: 1.1155432 W
===== Eta (H) (%) =====
P_efficient_A: 63.07072072072072 %
P_efficient_B: 63.297147493335814 %
P_all: 63.183934107028264 %
