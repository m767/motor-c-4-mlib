Data size is 301
===== velocity =====
theta/s: 89.11958696570244
RPM: 14.85326449428374
===== sensor ele angle =====
max: 112.63
min: 80.44
mean: 98.17774
===== command ele angle =====
max: 146.02
min: 112.27
mean: 129.1495
===== theta error (degree) =====
max: 146.02
min: 112.27
mean: 129.1495
std: 9.7754135
MSE: 16775.152
N_STEP: 0.012327078121716479, 0.016032778603553694
===== consume power (watt) =====
max: 1.8784047
min: 0.08284764
mean: 1.354439
std: 0.24981764
sum: 407.68616 J
===== current  =====
ia_max: 0.44058862 A
ia_min: -0.53143173 A
ia_mean: -0.023050286 A
ib_max: 0.50304323 A
ib_min: -0.48941678 A
ib_mean: 0.02520065 A
===== current slope =====
ia_slope_max: 9201.823 A
ia_slope_min: 4872.3955 A
ia_slope_mean: 6864.453 A
ib_slope_max: 18218.75 A
ib_slope_min: 6141.9263 A
ib_slope_mean: 7809.437 A
===== current RMS Square (watt) =====
rms_ia_square: 0.10619829 A
rms_ib_square: 0.108161405 A
===== current RMS (watt) =====
rms_ia: 0.3258808 A
rms_ib: 0.328879 A
===== P_loss (watt) =====
P_loss_A: 0.6886959009245038 W
P_loss_B: 0.6547124486044049 W
===== P_in (watt) =====
P_in_A: 1.439266 W
P_in_B: 1.439266 W
===== Eta (H) (%) =====
P_efficient_A: 52.14950419207437 %
P_efficient_B: 53.66978197821531 %
P_all: 52.90964308514484 %
