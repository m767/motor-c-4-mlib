Data size is 301
===== velocity =====
theta/s: 88.9258316056937
RPM: 14.820971934282284
===== sensor ele angle =====
max: 226.71
min: 194.55
mean: 209.97229
===== command ele angle =====
max: 226.8
min: 194.51
mean: 209.97403
===== theta error (degree) =====
max: 226.8
min: 194.51
mean: 209.97403
std: 9.695512
MSE: 44183.094
N_STEP: 0.007936507829716492, 0.00925402319075558
===== consume power (watt) =====
max: 1.6437273
min: 0.5740321
mean: 0.8512505
std: 0.1854031
sum: 256.2264 J
===== current  =====
ia_max: 0.33271256 A
ia_min: -0.41333574 A
ia_mean: -0.009650183 A
ib_max: 0.38721842 A
ib_min: -0.5155341 A
ib_mean: -0.016206877 A
===== current slope =====
ia_slope_max: 7411.458 A
ia_slope_min: 4319.01 A
ia_slope_mean: 5791.424 A
ib_slope_max: 8062.4995 A
ib_slope_min: 5035.156 A
ib_slope_mean: 6440.735 A
===== current RMS Square (watt) =====
rms_ia_square: 0.062242147 A
rms_ib_square: 0.072714515 A
===== current RMS (watt) =====
rms_ia: 0.24948376 A
rms_ib: 0.2696563 A
===== P_loss (watt) =====
P_loss_A: 0.4036403205804527 W
P_loss_B: 0.383722833674401 W
===== P_in (watt) =====
P_in_A: 1.0885696 W
P_in_B: 1.0885696 W
===== Eta (H) (%) =====
P_efficient_A: 62.92011963812906 %
P_efficient_B: 58.81889699736102 %
P_all: 60.869508317745044 %
