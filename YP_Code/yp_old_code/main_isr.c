#ifndef __AVR_ATmega128__
    #define __AVR_ATmega128__
#endif
#include <avr/io.h>
#include <util/delay.h>
#include "../lib/c4mlib.h"
#include <math.h>
#define N 8
#define work_FR 20 //工作除頻
#define wait_FR 40 //等待除頻
#define TASK1 0
#define TASK2 1
#define TASK3 2
#define TASK4 3
// (V1*0.000122-0.606)*S1+(V2*0.000122-0.0606)*S2 
#define SAMPLEHOLD_NUM 4
#define PWM_freq 12/* PWM更新頻率 */
#define PRINT_CYCLE 20 /* n個完整svpwm再print一次，一個svpwm十筆資料 */
#define Pi 3.14159
#define MAX_DUTY 1
#define DUTY_OFFSET 0.5

#define ENABLE_SAMPLEHOLD()		PORTD &= ~(1<<4);
#define DISABLE_SAMPLEHOLD()	PORTD |= (1<<4);
#define GET_PWM_CYCLE() REGGET(&ICR1L,2,&tempDuty);
#define GET_ADC_SIGNED()		(PIND&0x40)>>6
typedef struct{
	uint16_t *InData_p;
	uint16_t *DataList_p;
	uint16_t DataLength;
	volatile uint8_t DataCount;
	uint8_t TaskId;
	uint8_t NextTaskNum;
	uint8_t *NextTask_p;
	volatile uint8_t TrigCount;
}ADCPostProStr_t;

#define ADCPOSTPRO_LAY(ADCPostPro_str, ListNum, NextTaskNum_size, InDataAdd)\
uint16_t ADCPostPro_str##_DataList[ListNum];\
uint8_t ADCPostPro_str##_NextTaskList[NextTaskNum_size];\
ADCPostProStr_t ADCPostPro_str={\
	.InData_p = InDataAdd,\
	.DataList_p = ADCPostPro_str##_DataList,\
	.DataLength = ListNum,\
	.DataCount = 0,\
	.TaskId = 0,\
	.NextTaskNum = NextTaskNum_size,\
	.NextTask_p = ADCPostPro_str##_NextTaskList,\
	.TrigCount = 0\
}\

typedef struct{
	uint16_t *OutData_p;
	uint16_t *DataList_p;
	uint16_t DataLength;
	volatile uint8_t DataCount;
	uint8_t TaskId;
	uint8_t NextTaskNum;
	uint8_t *NextTask_p;
	volatile uint8_t TrigCount;
}PWMPreProStr_t;

#define PWMPrePro_LAY(PWMPrePro_str, ListNum, NextTaskNum_size, OutDataAdd)\
uint16_t PWMPrePro_str##_DataList[ListNum]={0};\
uint8_t PWMPrePro_str##_NextTaskList[NextTaskNum_size]={0};\
PWMPreProStr_t PWMPrePro_str={\
	.OutData_p = OutDataAdd,\
	.DataList_p = PWMPrePro_str##_DataList,\
	.DataLength = ListNum,\
	.DataCount = 0,\
	.TaskId = 0,\
	.NextTaskNum = NextTaskNum_size,\
	.NextTask_p = PWMPrePro_str##_NextTaskList,\
	.TrigCount = 0\
}\

typedef struct{
	ADCPostProStr_t* ADCPostPro;
	PWMPreProStr_t* PWMPrePro;
}ALL_Stuct;
#define ALL_Stuct_LAY(ALLStrName,ADCPostPro_ADD,PWMPrePro_ADD)\
ALL_Stuct ALLStrName={\
	.ADCPostPro = ADCPostPro_ADD,\
	.PWMPrePro = PWMPrePro_ADD\
}

void ADCTrigger(void);
void ADCTrigger1(void);
void ADCTrigger2(void);
void ADCTrigger3(void);
void ADCTrigger4(void);
void ADCPostPro_step(void* VoidStr_p);
void ADCgetData(void* FBData_p);
void ADCgetSigned(void);
void updateDuty(void);
void SenseCurrent(void);

uint16_t period[N] = {wait_FR};
volatile uint8_t flag1=0,flag2=0;
uint16_t ADC_DATA=0;	//ADC結果
uint16_t PWM_DATA=0;
uint16_t tempDuty=0;
volatile uint8_t isr_count=0;
uint8_t updateDuty_count=0;
float dutyA[PWM_freq]={};
float dutyB[PWM_freq]={};
uint16_t adcResult[4][PWM_freq*PRINT_CYCLE]={0};
char adcsign[4][PWM_freq*PRINT_CYCLE]={0};
uint8_t printf_trig = 0;
volatile uint8_t flag=0;
void test(void* FBData_p);
void test1(void* FBData_p);
void test2(void* FBData_p);
void test3(void* FBData_p);
void Drv8847_init(void);
void internalTimer_init(void);
void Timer1_init(void);
void Duty_init(void);
void SVPWM_init(void);

#define PWM 1
#define USE_WITHOUT_FREQ 1
// #define TIMER1_asPWM 1
int main(void){
    DDRB = 0xF0;
	DDRD |= 0x71;
    C4M_DEVICE_set();
    // printf("start\n");

    /* 馬達硬體設定 */
    Drv8847_init();
    /* 計時器硬體設定 */    
    #ifdef TIMER1_asPWM
	Timer1_init();
	#endif
	/* 啟用pwm1計時器硬體設定佈局 */
	PWM1_HW_LAY();

	/* 設定timer1硬體 */
	hardware_set(&PWM1HWSet_str);
	
	/* SVPWM初始化 */
	SVPWM_init();

	/* ADC硬體設定佈局 */
	ADC_HW_LAY();

	/* ADC硬體設定 */
	hardware_set(&ADCHWSet_str);//
	Duty_init();
    sei();
	DISABLE_SAMPLEHOLD();
	GET_PWM_CYCLE();
	// printf("size:%d\n",sizeof(adcResult));
	// printf("%d , %d, %d",TCCR1A,TCCR1B,TCCR1C);
    while(1){
        ;
    }
}

void Drv8847_init(void){
    DDRD |= (1<<4); //set Mode PIN to output PD4
    PORTD |= (1<<4); //set Mode PIN & nsleep to High
	DDRD &= ~(1<<6); //set Mode PIN to input PD6 for signA rising
	DDRD &= ~(1<<7); //set Mode PIN to input PD7 for signA falling
}

void Duty_init(void){
	uint16_t tempDuty;
	REGGET(&ICR1L,2,&tempDuty);
	for(int i=0;i<sizeof(dutyA)/sizeof(float);i++){
		dutyA[i]=(float)tempDuty*dutyA[i];
		dutyB[i]=(float)tempDuty*dutyB[i];
	}
	
}

void SVPWM_init(void){
	for(uint8_t i=0;i<PWM_freq;i++){
		dutyA[i]= DUTY_OFFSET + 0.5 * ((float)(sin((double)((2*Pi)/PWM_freq)*i)) * MAX_DUTY);
		dutyB[i]= DUTY_OFFSET + 0.5 * ((float)(cos((double)((2*Pi)/PWM_freq)*i)) * MAX_DUTY);
	}
}

void internalTimer_init(void){
  	TIM1_HW_LAY();//啟用timer1計時器硬體設定佈局
	hardware_set(&TIM1HWSet_str);//設定timer2硬體
}

void Timer1_init(void){
    DDRB = 0xF0; //set PB5 6 7 to Output
    TCCR1B = 0x18; // 0b00011000, Mode 12(CTC to ICR1)
	TCCR1A = 0x54; // 0b01010100, Mode 12(CTC to ICR1), toggle when compare match	
    ICR1=16; // f = 11059200/(2*N*(1+ICR1))
    OCR1AL=9;
    OCR1BL=4;
	TCCR1B |= 2;//0b00011101, N=8
    TIMSK =0x10; //OCIE1A=1, Output compare A match interrupt
    TCNT1=0; //init counter
}

void ADCPostPro_step(void* VoidStr_p)
{
	ADCPostProStr_t* ADCPostProStr_t_new = (ADCPostProStr_t*)VoidStr_p;
	if(ADCPostProStr_t_new->DataCount >= ADCPostProStr_t_new->DataLength)//if full
	{
		cli();
		// HMI_snput_matrix(HMI_TYPE_UI16, 1, ADCPostProStr_t_new->DataLength, ADCPostProStr_t_new->DataList_p);
		ADCPostProStr_t_new->DataCount=0;
		ADCPostProStr_t_new->TrigCount=0;
		sei();
	}
	ADCPostProStr_t_new->TrigCount++;
	if(flag1==0)
	{
		PORTD &= ~(1<<4);
		flag1=1;
		TRIG_NEXT_TASK(TASK3);
	}
	else if(flag1==1)
	{
		PORTD |= (1<<4);
		flag1=0;
		TRIG_NEXT_TASK(TASK1);
	}
}
void ADCTrigger(void){
	switch (isr_count)
	{
		case 0:{
			PORTD |= (1<<5);
			REGFPT(&ADMUX, 0x1F, 0, 0x00);//選擇輸入訊號為ADC0(Port F0)/sigleE_0_x1
			REGFPT(&ADCSRA, 0x40, 6, 1);//觸發ADC轉換
			break;
		}
		case 1:{
			PORTD |= (1<<5);
			REGFPT(&ADMUX, 0x1F, 0, 0x01);//選擇輸入訊號為ADC0(Port F0)/sigleE_0_x1
			REGFPT(&ADCSRA, 0x40, 6, 1);//觸發ADC轉換
			break;
		}
		case 2:{
			PORTD |= (1<<5);
			REGFPT(&ADMUX, 0x1F, 0, 0x02);//選擇輸入訊號為ADC0(Port F0)/sigleE_0_x1
			REGFPT(&ADCSRA, 0x40, 6, 1);//觸發ADC轉換
			break;
		}
		case 3:{
			PORTD |= (1<<5);
			REGFPT(&ADMUX, 0x1F, 0, 0x03);//選擇輸入訊號為ADC0(Port F0)/sigleE_0_x1
			REGFPT(&ADCSRA, 0x40, 6, 1);//觸發ADC轉換
			break;
		}
		default:{
			break;
		}
	}
}
void ADCTrigger1(void)
{
	REGFPT(&ADMUX, 0x1F, 0, 0x00);//選擇輸入訊號為ADC0(Port F0)/sigleE_0_x1
	_delay_us(1);
	REGFPT(&ADCSRA, 0x40, 6, 1);//觸發ADC轉換
}

void ADCTrigger2(void)
{
	REGFPT(&ADMUX, 0x1F, 0, 0x01);//選擇輸入訊號為ADC1(Port F1)/sigleE_1_x1
	_delay_us(1);
	REGFPT(&ADCSRA, 0x40, 6, 1);//觸發ADC轉換
}

void ADCTrigger3(void)
{
	REGFPT(&ADMUX, 0x1F, 0, 0x02);//選擇輸入訊號為ADC2(Port F2)/sigleE_2_x1
	_delay_us(1);
	REGFPT(&ADCSRA, 0x40, 6, 1);//觸發ADC轉換
}

void ADCTrigger4(void)
{
	REGFPT(&ADMUX, 0x1F, 0, 0x03);//選擇輸入訊號為ADC3(Port F3)/sigleE_3_x1
	_delay_us(1);
	REGFPT(&ADCSRA, 0x40, 6, 1);//觸發ADC轉換
}
void ADCgetData(void* FBData_p)
{
	ADCPostProStr_t* ADCPostProStr_t_new =  (ADCPostProStr_t*)FBData_p;
	REGGET(&ADCL, 2, &ADC_DATA);//讀取ADC轉換結果並記憶
	ADCPostProStr_t_new->DataList_p[ADCPostProStr_t_new->DataCount]=*(ADCPostProStr_t_new->InData_p);
	ADCPostProStr_t_new->DataCount++;
}
void ADCgetSigned(void){
	REGFGT(&PIND,0x40,6,&adcsign[0][(PWM_freq*(printf_trig))+updateDuty_count]);
	REGFGT(&PIND,0x80,7,&adcsign[1][(PWM_freq*(printf_trig))+updateDuty_count]);
}

void updateDuty(void){
	ENABLE_SAMPLEHOLD();
	ADCgetSigned();
	cli();
	// TCNT1=0;
	/* 更改A相duty */
	OCR1A=(uint16_t)dutyA[updateDuty_count];
	/* 更改B相duty */
	OCR1B=(uint16_t)dutyB[updateDuty_count];
	sei();
	updateDuty_count++;
	if(updateDuty_count==PWM_freq){
		updateDuty_count=0;
		printf_trig++;
		if(printf_trig==PRINT_CYCLE){
			printf_trig=0;
		}
	}
}

void SenseCurrent(void){
	cli();
	// TCNT1=0;
	/* 更改A相duty為總周期的一半 */
	OCR1A=(tempDuty>>1);
	/* 更改B相duty為總周期的一半 */
	OCR1B=(tempDuty>>1);
	// ENABLE_SAMPLEHOLD();
	sei();
}

ISR(TIMER1_OVF_vect){
	/*
	isr_count++;
	if(isr_count==PWM_freq+1){
		//start sense
		SenseCurrent();
	}
	if(isr_count==(PWM_freq+2)){
		//change duty
		updateDuty();
		isr_count=0;
	}
	ADCTrigger();*/

	isr_count++;
	switch (isr_count)
	{
		case 1:{
				PORTD |= (1<<5);
				ADCTrigger1();
				break;
		}
		case 2:{
				PORTD |= (1<<5);
				ADCTrigger2();
				break;
		}
		case 3:{
				PORTD |= (1<<5);
				ADCTrigger3();
				break;
		}
		case 4:{
				PORTD |= (1<<5);
				ADCTrigger4();
				break;
		}
		case 10:{
				//start sense
				SenseCurrent();
				break;
		}
		case 11:{
				//change duty
				updateDuty();
				isr_count=0;
				break;
		}
		default:{
			break;
		}
	}
}

ISR(ADC_vect){
	REGGET(&ADCL, 2, &ADC_DATA);//讀取ADC轉換結果並記憶
	adcResult[isr_count-1][(PWM_freq*(printf_trig))+updateDuty_count]=ADC_DATA;
	// printf("%d,%d,%d,%d,%d\n",(isr_count-1),(10*(printf_trig))+updateDuty_count,printf_trig,updateDuty_count,adcResult[isr_count-1][(10*(printf_trig))+updateDuty_count]);
	// adcResult[isr_count-1][updateDuty_count]=ADC_DATA;
	PORTD &= ~(1<<5);
	if(isr_count==SAMPLEHOLD_NUM){
		DISABLE_SAMPLEHOLD();
	}
	// printf("[!], %d, %d, %d\n",isr_count,printf_trig,updateDuty_count);
	if((isr_count)==(SAMPLEHOLD_NUM) && printf_trig==(PRINT_CYCLE-1) && updateDuty_count==(PWM_freq-1)){
		PORTD |= (1<<0);
		// for(uint8_t i=0;i<PWM_freq*PRINT_CYCLE;i++){
		// // 	// printf(", %2.2f ",0.075+(float)(adcResult[0][i]+adcResult[1][i])*5/2048);
		// 	printf("[485],%d,%d,%d,%d\n",adcResult[0][i],adcResult[1][i],adcsign[0][i],adcsign[1][i]);
		// }
		// HMI_snput_matrix(HMI_TYPE_UI16,4,20,adcResult);
		PORTD &= ~(1<<0);
	}
}