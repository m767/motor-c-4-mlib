#ifndef __AVR_ATmega128__
    #define __AVR_ATmega128__
#endif
#include <avr/io.h>
#include <util/delay.h>
#include "c4mlib.h"
#define N 8
#define work_FR 20 //工作除頻
#define wait_FR 40 //等待除頻
#define TASK1 0
#define TASK2 1
#define TASK3 2
#define TASK4 3

typedef struct{
	uint16_t *InData_p;
	uint16_t *DataList_p;
	uint16_t DataLength;
	volatile uint8_t DataCount;
	uint8_t TaskId;
	uint8_t NextTaskNum;
	uint8_t *NextTask_p;
	volatile uint8_t TrigCount;
}ADCPostProStr_t;

#define ADCPOSTPRO_LAY(ADCPostPro_str, ListNum, NextTaskNum_size, InDataAdd)\
uint16_t ADCPostPro_str##_DataList[ListNum];\
uint8_t ADCPostPro_str##_NextTaskList[NextTaskNum_size];\
ADCPostProStr_t ADCPostPro_str={\
	.InData_p = InDataAdd,\
	.DataList_p = ADCPostPro_str##_DataList,\
	.DataLength = ListNum,\
	.DataCount = 0,\
	.TaskId = 0,\
	.NextTaskNum = NextTaskNum_size,\
	.NextTask_p = ADCPostPro_str##_NextTaskList,\
	.TrigCount = 0\
}\

typedef struct{
	uint16_t *OutData_p;
	uint16_t *DataList_p;
	uint16_t DataLength;
	volatile uint8_t DataCount;
	uint8_t TaskId;
	uint8_t NextTaskNum;
	uint8_t *NextTask_p;
	volatile uint8_t TrigCount;
}PWMPreProStr_t;

#define PWMPrePro_LAY(PWMPrePro_str, ListNum, NextTaskNum_size, OutDataAdd)\
uint16_t PWMPrePro_str##_DataList[ListNum]={0};\
uint8_t PWMPrePro_str##_NextTaskList[NextTaskNum_size]={0};\
PWMPreProStr_t PWMPrePro_str={\
	.OutData_p = OutDataAdd,\
	.DataList_p = PWMPrePro_str##_DataList,\
	.DataLength = ListNum,\
	.DataCount = 0,\
	.TaskId = 0,\
	.NextTaskNum = NextTaskNum_size,\
	.NextTask_p = PWMPrePro_str##_NextTaskList,\
	.TrigCount = 0\
}\

typedef struct{
	ADCPostProStr_t* ADCPostPro;
	PWMPreProStr_t* PWMPrePro;
}ALL_Stuct;
#define ALL_Stuct_LAY(ALLStrName,ADCPostPro_ADD,PWMPrePro_ADD)\
ALL_Stuct ALLStrName={\
	.ADCPostPro = ADCPostPro_ADD,\
	.PWMPrePro = PWMPrePro_ADD\
}

void ADCTrigger1(void* FBData_p);
void ADCTrigger2(void* FBData_p);
void ADCPostPro_step(void* VoidStr_p);
void ADCgetData(void* FBData_p);
void updateDuty(void* VoidStr_p);

uint8_t TR=0, TE=0, FR1=0, FR2=0, FR3=0, FR4=0, PR1=0, PR2=0, PR3=0, PR4=0, ADC1=0;
uint8_t PWM1=0,PWM2=0;
uint8_t P1=0,P2=N/4, P3=N/2, P4=(N/2)+(N/4);//(0.25, 0.5, 0.75)
uint16_t period[N] = {wait_FR};
volatile uint8_t flag1=0,flag2=0;
uint16_t ADC_DATA=0;
uint16_t PWM_DATA=0;

uint8_t apple=0;

float duty[]={0.5, 0.853, 1, 0.853, 0.5, 0.146, 0, 0.146};
volatile uint8_t flag=0;
void test(void* FBData_p);
void test1(void* FBData_p);
void test2(void* FBData_p);
void test3(void* FBData_p);
void Drv8847_init(void);
void internalTimer_init(void);
void Timer1_init(void);
#define PWM 1
// #define TIMER1_asPWM 1
int main(void){
    DDRB = 0xF0;
    C4M_DEVICE_set();
    // printf("start\n");
   	period[0]  = work_FR;
	period[P2] = work_FR;
	period[P3] = work_FR;
	period[P4] = work_FR;
	uint8_t task1_name[] = "Task1\0";
	uint8_t task2_name[] = "Task2\0";
	uint8_t task3_name[] = "Task3\0";
	uint8_t task4_name[] = "Task4\0";

	

    //============馬達硬體設定============//
    Drv8847_init();
    //============計時器硬體設定============//    
    #ifdef TIMER1_asPWM
	Timer1_init();
	#endif

	PWM1_HW_LAY();//啟用pwm1計時器硬體設定佈局
	hardware_set(&PWM1HWSet_str);//設定timer1硬體
	PWMHWINT_LAY(PWMInt_str,1,1);//計時器中斷分享(結構名稱, timer1, 一個工作)

   	//============硬體中斷============//
	// #ifdef TIMER1_asPWM
	// TIMHWINT_LAY(Tim1Int_str, 1, 1);//計時器中斷分享(結構名稱, timer1, 一個工作)
    // TR= HWInt_reg(&Tim1Int_str, &test, &apple);//
	// TE= HWInt_en(&Tim1Int_str, TR, 1);//致能PWMInt_str
	// #endif

    //============FreqRedu降頻器設定============//
	FREQREDU_LAY(FreqRedu_str, 4, N, &OCR1C, 4, period);//(結構名稱, 4個工作, N個count, 計數器top值, 登錄byte數, 週期)

    //============Pipeline設定============//
	PIPELINE_LAY(4, 4, 15);//(tasknum, fbdepth, sbdepth)
	
	
	//============ADC硬體設定============//
	ADC_HW_LAY();//ADC硬體設定佈局
	hardware_set(&ADCHWSet_str);//ADC硬體設定
	ADCHWINT_LAY(ADCInt_str, 0, 1);//ADC中斷分享(結構名稱, ininum, 一個工作)

    //============ADC後處理設定============//
	ADCPOSTPRO_LAY(ADCPPStr, 200, 1, &ADC_DATA);

	//============PWM前處理設定============//
	PWMPrePro_LAY(PWMPPStr, 200, 1, &PWM_DATA);

	//============全體結構設定============//
	ALL_Stuct_LAY(ALLStr,&ADCPPStr,&PWMPPStr);
	
	
    PWM1= HWInt_reg(&PWMInt_str, &FreqRedu_step, &FreqRedu_str);//將FreqRedu_str登錄進PWMInt_str
	HWInt_en(&PWMInt_str, PWM1, 1);//致能PWMInt_str
	//============降頻器之登錄&致能============//
	// FR1= FreqRedu_reg(&FreqRedu_str, &test, &apple, 1, 0);
	FR1= FreqRedu_reg(&FreqRedu_str, &Pipeline_step, &SysPipeline_str, 1, 0);//設定第1個工作點
	FR2= FreqRedu_reg(&FreqRedu_str, &Pipeline_step, &SysPipeline_str, 1, 2);//設定第2個工作點
	FR3= FreqRedu_reg(&FreqRedu_str, &Pipeline_step, &SysPipeline_str, 1, 4);//設定第3個工作點
	FR4= FreqRedu_reg(&FreqRedu_str, &Pipeline_step, &SysPipeline_str, 1, P4);//設定第4個工作點
	// printf("freqreg : %d %d %d %d\n",FR1,FR2,FR3,FR4);
	FreqRedu_en(&FreqRedu_str, FR1, 1);
	FreqRedu_en(&FreqRedu_str, FR2, 1);
	FreqRedu_en(&FreqRedu_str, FR3, 1);
	FreqRedu_en(&FreqRedu_str, FR4, 1);
	// printf("freqen : %d %d %d %d\n",FreqRedu_en(&FreqRedu_str, FR1, 1),FreqRedu_en(&FreqRedu_str, FR2, 1),FreqRedu_en(&FreqRedu_str, FR3, 1),FreqRedu_en(&FreqRedu_str, FR4, 1));
	//============Pipeline之登錄&致能============//
	// PR1= Pipeline_reg(&SysPipeline_str, &ADCTrigger1, NULL, task1_name);
	// PR2= Pipeline_reg(&SysPipeline_str, &ADCPostPro_step, &ADCPPStr, task2_name);
	// PR3= Pipeline_reg(&SysPipeline_str, &ADCTrigger2, NULL, task3_name);
	// PR4= Pipeline_reg(&SysPipeline_str, &ADCPostPro_step, &ADCPPStr, task4_name);
	// printf("freqreg : %d %d %d %d\n",PR1,PR2,PR3,PR4);
	PR1= Pipeline_reg(&SysPipeline_str, &test, &apple, task1_name);
	PR2= Pipeline_reg(&SysPipeline_str, &test1, &apple, task2_name);
	PR3= Pipeline_reg(&SysPipeline_str, &test2, &apple, task3_name);
	PR4= Pipeline_reg(&SysPipeline_str, &test3, &apple, task4_name);

	//============ADC轉換及致能============//
	ADC1= HWInt_reg(&ADCInt_str, &ADCgetData, &ADCPPStr);
	HWInt_en(&ADCInt_str, ADC1, 1);
    DDRD |= 0x70;
    TRIG_NEXT_TASK(TASK1);
    sei();
    while(1){
        ;
    }
}

void test(void* FBData_p){
	PORTD |= (1<<4);
	TRIG_NEXT_TASK(TASK2);
}
void test1(void* FBData_p){
	PORTD &= ~(1<<4);
	TRIG_NEXT_TASK(TASK3);
}
void test2(void* FBData_p){
	PORTD |= (1<<4);
	TRIG_NEXT_TASK(TASK4);
}
void test3(void* FBData_p){
	PORTD &= ~(1<<4);
	TRIG_NEXT_TASK(TASK1);
}
void Drv8847_init(void){
    DDRD |= (1<<6); //set Mode PIN to output PD6
    PORTD |= (1<<6); //set Mode PIN & nsleep to High
}

void internalTimer_init(void){
  	TIM1_HW_LAY();//啟用timer1計時器硬體設定佈局
	hardware_set(&TIM1HWSet_str);//設定timer2硬體
}

void Timer1_init(void){
    DDRB = 0xF0; //set PB5 6 7 to Output
    TCCR1B = 0x18; // 0b00011000, Mode 12(CTC to ICR1)
	TCCR1A = 0x54; // 0b01010100, Mode 12(CTC to ICR1), toggle when compare match	
    ICR1=16; // f = 11059200/(2*N*(1+ICR1))
    OCR1AL=9;
    OCR1BL=4;
	TCCR1B |= 2;//0b00011101, N=8
    TIMSK =0x10; //OCIE1A=1, Output compare A match interrupt
    TCNT1=0; //init counter
}

void ADCPostPro_step(void* VoidStr_p)
{
	ADCPostProStr_t* ADCPostProStr_t_new = (ADCPostProStr_t*)VoidStr_p;
	if(ADCPostProStr_t_new->DataCount >= ADCPostProStr_t_new->DataLength)//if full
	{
		cli();
		
		// HMI_snput_matrix(HMI_TYPE_UI16, 1, ADCPostProStr_t_new->DataLength, ADCPostProStr_t_new->DataList_p);
		ADCPostProStr_t_new->DataCount=0;
		ADCPostProStr_t_new->TrigCount=0;
		sei();
	}
	ADCPostProStr_t_new->TrigCount++;
	if(flag1==0)
	{
		PORTD &= ~(1<<4);
		flag1=1;
		TRIG_NEXT_TASK(TASK3);
	}
	else if(flag1==1)
	{
		PORTD |= (1<<4);
		flag1=0;
		TRIG_NEXT_TASK(TASK1);
	}
}

void ADCTrigger1(void* FBData_p)
{
	REGFPT(&ADMUX, 0x1F, 0, 0x00);//選擇輸入訊號為ADC0(Port F0)/sigleE_0_x1
	REGFPT(&ADCSRA, 0x40, 6, 1);//觸發ADC轉換
	TRIG_NEXT_TASK(TASK2);
}

void ADCTrigger2(void* FBData_p)
{
	// PORTD |= (1<<4);
	REGFPT(&ADMUX, 0x1F, 0, 0x1B);//選擇輸入訊號V+為ADC3(Port F3) V-為ADC2(Port F2)
	REGFPT(&ADCSRA, 0x40, 6, 1);//觸發ADC轉換
	TRIG_NEXT_TASK(TASK4);
}

void ADCgetData(void* FBData_p)
{
	ADCPostProStr_t* ADCPostProStr_t_new =  (ADCPostProStr_t*)FBData_p;
	REGGET(&ADCL, 2, &ADC_DATA);//讀取ADC轉換結果並記憶
	ADCPostProStr_t_new->DataList_p[ADCPostProStr_t_new->DataCount]=*(ADCPostProStr_t_new->InData_p);
	ADCPostProStr_t_new->DataCount++;
}

void updateDuty(void* VoidStr_p){
	ALL_Stuct* ALL_Stuct_new = (ALL_Stuct*)VoidStr_p;
	PWMPreProStr_t* PWMPreProStr_t_new = ALL_Stuct_new->PWMPrePro;
	uint16_t tempDuty;
	REGGET(&ICR1L,2,&tempDuty);
	// printf("icr = %d\n",tempDuty);
	// cli();
	// REGPUT(&OCR1AL,2,&duty[n]);
}
// ISR(TIMER1_COMPA_vect){
//     if(flag){
//         PORTD |= (1<<4);
//         flag = 0;
//     }
//     else
//     {
//         PORTD &= ~(1<<4);
//         flag = 1;
//     }
//     // printf("in\n");
// }