/**
 * @file adj_velocity.h
 * @author Xiang-Guan Deng
 * @brief Adjust the stepper motor velocity
 * @date 2020.xx.xx
 *
 * 電流PI回饋功能方塊
 */

#ifndef PI_CURRENT_H
#define PI_CURRENT_H
#include "i_excite_angle.h"
#include "control_config.h"

#ifdef USE_HAL /* Using Danny's Hardware abstract layer */
    typedef struct _fb_current_ {
        fb_pid_t pid;
        volatile float i_svpwm;
        float low_limit;
        float high_limit;
    }fb_current_t;

    /**
     * @brief Initialize PI current object patameters
     */
    void init_current_para(fb_current_t *fb_current, float kp, float ki, float low_limit, float high_limit);

    /**
     * @brief Calculate PI current value to excite SVPWM
     */
    void cal_current_correct(fb_exc_angle_t *fb_exc_angle, fb_current_t *fb_current);

#else
/*==========PI_CURRENT工作方塊結構體型態==========*/
/**
 * @brief PI_CURRENT工作方塊結構體型態
 * @input th_er (角度誤差)
 * @output i_svpwm 激磁電流)
 * 
 */
typedef struct{
    fb_pid_t pid;
    float i_svpwm; /*激磁電流*/
    float low_limit; /*激磁角下限*/
    float high_limit; /*激磁角上限*/
    float th_er; /*角度誤差*/
    float th_cum; /*角度積分*/

    float* uIn_p[2]; /*pointer to input and output*/
    float* yOut_p[1]; /*pointer to input and output*/
} PI_CURRENTStr_t;
/*==========PI_CURRENT工作方塊結構體定義及初始化==========*/
#define PI_CURRENTINI0 {\
    .pid.kp = I_SVPWM_KP,\
    .pid.ki = I_SVPWM_KI,\
    .i_svpwm = 0,\
    .low_limit = LOW_LIMIMT,\
    .high_limit = HIGH_LIMIT,\
    .yOut_p[0]=0}

// PI_CURRENTStr_t PI_CURRENT_str = PI_CURRENTINI0;
/*==========PI_CURRENT工作方塊佈線==========*/
#define PI_CURRENT_LAY(PI_CURRENTSTR) {\
    PI_CURRENTSTR.uIn_p[0] = &PI_CURRENTSTR.th_er;\
    PI_CURRENTSTR.uIn_p[1] = &PI_CURRENTSTR.th_cum;\
    PI_CURRENTSTR.yOut_p[0] = &PI_CURRENTSTR.i_svpwm;\
}

/*==========PI_CURRENT工作方塊步級執行函式==========*/
void PI_CURRENT_step(void* void_p);
#endif
#endif /* PI_CURRENT_H */
