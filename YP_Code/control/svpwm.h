/**
 * @file svpwm.h
 * @author Xiang-Guan Deng
 * @brief Calculate SVPWM
 * @date 2020.xx.xx
 *
 * 兩相SVPWM功能方塊，計算出控制的PWM duty值
 */

#ifndef SVPWM_H
#define SVPWM_H

#include <stdint.h>
#include "pi_current.h"

#define DEGREE_TO_RADIAN      (3.141593/180.0)

#ifdef USE_HAL /* Using Danny's Hardware abstract layer */
    typedef struct _pwmAB_ {
        int16_t pwm1;
        int16_t pwm2;
    }pwmAB_t;

    void cal_pwmAB(pwmAB_t *pwmAB, fb_exc_angle_t *fb_exc_angle, fb_current_t *fb_current);
#else
/*==========SVPWM工作方塊結構體型態==========*/
/**
 * @brief SVPWM工作方塊結構體型態
 * @input th_esvpwm (回饋激磁角), i_svpwm (電流PI回饋)
 * @output pwmA (A相PWWduty), pwmB (B相PWWduty)
 */
typedef struct{
    int8_t pwmA; /*A相PWWduty*/
    int8_t pwmB; /*B相PWWduty*/
    float th_esvpwm; /*回饋激磁角*/
    float i_svpwm; /*電流PI回饋*/

    float* uIn_p[2]; /*pointer to input and output*/
    int8_t* yOut_p[2]; /*pointer to input and output*/
} SVPWMStr_t;

/*==========SVPWM工作方塊結構體定義及初始化==========*/
#define SVPWMINI0 {.pwmA=0,.pwmB=0,.yOut_p[0]=0,.yOut_p[1]=0}

// SVPWMStr_t SVPWM_str = SVPWMINI0;

/*==========SVPWM工作方塊佈線==========*/
#define SVPWM_LAY(SVPWMSTR) {\
    SVPWMSTR.uIn_p[0] = &SVPWMSTR.th_esvpwm;\
    SVPWMSTR.uIn_p[1] = &SVPWMSTR.i_svpwm;\
    SVPWMSTR.yOut_p[0] = &SVPWMSTR.pwmA;\
    SVPWMSTR.yOut_p[1] = &SVPWMSTR.pwmB;\
}

/*==========SVPWM工作方塊步級執行函式==========*/
void SVPWM_step(void* void_p);
#endif

#endif
