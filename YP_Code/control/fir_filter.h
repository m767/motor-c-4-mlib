/**
 * @file filter.h
 * @author Xiang-Guan Deng
 * @brief Filter encoder data
 * @date 2020.xx.xx
 *
 * 將馬達編碼器濾波，濾波器只支援8項
 */
#ifndef FIR_FILTER_H
#define FIR_FILTER_H

#define FIR_MAX_BUF_SIZE      16
#define MV_AVG_WINDOW_SIZE     8
#define MV_AVG_MAX_WINDOW      32


/* FIL filter structure */
typedef struct _fir_t {
    float fir_buf[FIR_MAX_BUF_SIZE];
    const float *fir_feat;
    uint_fast8_t feat_sz;
    uint_fast8_t fir_index;
    float fir_output;
}fir_t;

void init_fir(fir_t *fir, const float *feat, uint_fast8_t feat_sz);
void set_fir(fir_t *fir, float fir_input);
float get_fir(fir_t *fir);

#ifdef USE_HAL /* Using Danny's Hardware abstract layer */
/* Moving average structure */
typedef struct _mv_avg_t {
    int_fast16_t mv_buf[MV_AVG_MAX_WINDOW];
    uint_fast8_t mv_index;
    uint_fast8_t wd_sz;       /* window size */
    uint_fast8_t shift;
    int_fast16_t mv_output;
}mv_avg_t;

void init_mv_avg(mv_avg_t *mv_avg, uint_fast8_t window_size);
void set_mv_avg(mv_avg_t *mv_avg, int_fast16_t mv_input);
int_fast16_t get_mv_avg(mv_avg_t *mv_avg);

#else  /* Using C4MOS */

/*==========mv_avg工作方塊結構體型態==========*/
/**
 * @brief mv_avg工作方塊結構體型態
 * @input 
 * @output 
 */
typedef struct {
    int_fast8_t mv_buf[MV_AVG_MAX_WINDOW];
    uint_fast8_t mv_index;
    uint_fast8_t wd_sz;       /* window size */
    uint_fast8_t shift;
    int_fast8_t mv_output;

    int_fast8_t* uIn_p[0]; /*pointer to input and output*/
    int_fast8_t* yOut_p[1]; /*pointer to input and output*/
}mv_avgStr_t;

#define MV_AVGINI_ENC {.mv_index=0,.mv_output=0,.shift=3,.wd_sz=8,.yOut_p[0]=0} /* shift 是以2為底數，wd_sz的對數*/
#define MV_AVGINI_PWMA {.mv_index=0,.mv_output=0,.shift=4,.wd_sz=16,.yOut_p[0]=0} /* shift 是以2為底數，wd_sz的對數*/
#define MV_AVGINI_PWMB {.mv_index=0,.mv_output=0,.shift=4,.wd_sz=16,.yOut_p[0]=0} /* shift 是以2為底數，wd_sz的對數*/


#define MV_AVG_LAY(MV_AVG_STR) {\
    MV_AVG_STR.uIn_p[0] = &MV_AVG_STR.mv_buf[0];\
    MV_AVG_STR.yOut_p[0] = &MV_AVG_STR.mv_output;\
}
void mv_avg_step(void* void_p);

#endif


#endif /* FILTER_H */
