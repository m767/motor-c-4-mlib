/**
 * @file adj_velocity.h
 * @author Xiang-Guan Deng
 * @brief Excited angle feedback
 * @date 2020.xx.xx
 *
 * 激磁角回饋
 */

#ifndef I_EXCITE_ANGLE_H
#define I_EXCITE_ANGLE_H
#include <stdint.h>
#include "pid.h"
#include "control_config.h"

#ifndef abs_float
#define abs_float(X) ((X < 0)? (-X) : (X))
#endif
#ifndef abs_int
#define abs_int(X) ((X < 0)? (-X) : (X))
#endif

#ifdef USE_HAL /* Using Danny's Hardware abstract layer */
    typedef struct _fb_exc_angle_ {
        fb_pid_t pid;
        float th_esvpwm;
        float th_er;
        float th_cum;
        float cum_limit;
        float last_th;
        float w;
    }fb_exc_angle_t;

    /**
     * @brief Initialize excited angle parameters
     *
     */
    void init_exc_ang_para(fb_exc_angle_t *fb_exc_angle, float ki);

    /**
     * @brief Calculate the theta svpwm to adjest step motor
     *
     */
    void cal_exc_ang_correct(fb_exc_angle_t *fb_exc_angle, float  e_sdegree, float e_cdegree);

#else
/*==========I_EXCITE工作方塊結構體型態==========*/
/**
 * @brief I_EXCITE工作方塊結構體型態
 * @input e_sdegree (感測電子角(度度量)), e_cdegree (命令電子角(度度量))
 * @output th_esvpwm (激磁角度), th_er (激磁角度誤差)
 * @parameter ki
 */
typedef struct{
    fb_pid_t pid;
    float th_esvpwm; /*激磁角度*/
    float th_er; /*激磁角度誤差*/
    float th_cum; /*角度累計誤差*/
    float cum_limit; /*激磁角累計上限*/
    float last_th; /*相位待調整角度*/
    float w; /*角速度*/
    float e_sdegree; /*感測電子角(度度量)*/
    float e_cdegree; /*命令電子角(度度量)*/

    float* uIn_p[2]; /*pointer to input and output*/
    float* yOut_p[2]; /*pointer to input and output*/
} I_EXCITEStr_t;

/*==========I_EXCITE工作方塊結構體定義及初始化==========*/
#define I_EXCITEINI0 {\
    .pid.kp = 0,\
    .pid.ki = EXC_KI,\
    .pid.kd = 0,\
    .th_cum=0,\
    .th_er=0,\
    .th_esvpwm = 0,\
    .cum_limit = 120.0/EXC_KI,\
    .yOut_p[0]=0,\
    .yOut_p[1]=0\
}

// I_EXCITEStr_t I_EXCITE_str = I_EXCITEINI0;
/*==========I_EXCITE工作方塊佈線==========*/
#define I_EXCITE_LAY(I_EXCITESTR) {\
    I_EXCITESTR.uIn_p[0] = &I_EXCITESTR.e_sdegree;\
    I_EXCITESTR.uIn_p[1] = &I_EXCITESTR.e_cdegree;\
    I_EXCITESTR.yOut_p[0] = &I_EXCITESTR.th_er;\
    I_EXCITESTR.yOut_p[1] = &I_EXCITESTR.th_cum;\
}

/*==========I_EXCITE工作方塊步級執行函式==========*/
void I_EXCITE_step(void* void_p);
#endif
#endif /* I_EXCITE_ANGLE_H */
