/**
 * @file adj_velocity.h
 * @author Xiang-Guan Deng
 * @brief Adjust the stepper motor velocity
 * @date 2020.xx.xx
 *
 * 相位調整限速器
 */

#ifndef ADJ_VELOCITY_H
#define ADJ_VELOCITY_H
#include <stdint.h>
#include "control_config.h"

/* Define the adject velocity status */
#define SM_READY              0
#define SM_RUNNING            1
#define SM_OVERLIMIT          2 //要調整的相位超過設定上限

#ifdef USE_HAL /* Using Danny's Hardware abstract layer */
    /**
     *  @brief Adjust velocity parameter, state structure
     *  @ingroup
     */
    typedef struct _adj_v_ {
        int16_t limit;           /* Limit angle velocity */
        int16_t add;             /* Add new phase angle*/
        int16_t w_back;          /* Background angle velocity */
        int16_t residual_phase;  /* Phase residual state variable */
        int16_t w_phase;         /* Phase angle velocity */
        uint8_t sm;              /* Current status */
    }adj_v_t;

    /**
     * @brief Initialize command eletrical angle increment
     */
    void init_cangle_inc(adj_v_t *adj_v);

    /**
     * @brief Set command eletrical angle increment
     */
    void set_cangle_inc(adj_v_t *adj_v, int16_t th_add);

    /**
     * @brief Set command background eletrical angle speed
     */
    void set_cangle_wback(adj_v_t *adj_v, int16_t w_back);

    /**
     * @brief Get command angle
     *
     * If angle more than max speed, it will return max spped value
     */
    int16_t get_cangle_inc(adj_v_t *adj_v);
#else
/*==========ADJ_VELOCITY工作方塊結構體型態==========*/
/**
 * @brief ADJ_VELOCITY工作方塊結構體型態
 * @input add (新增相位調整角度), w_back (背景角速度)
 * @output add_step (新增微步)
 * 
 */
typedef struct{
    int16_t limit;           /*最高相位調整速度 */
    int16_t add;             /*新度*/
    int16_t add_step;        /*新增相位調整角微步*/
    int16_t w_back;          /*背景角速度*/
    int16_t w_phase;         /*相位角速度(w) Phase angle velocity */
    int16_t residual_phase;  /*要調整的相位 Phase residual state variable  */
    uint8_t sm;              /*Current status 狀態2=要調整的相位超過設定上限 狀態1=未超過*/
    int16_t* uIn_p[2]; /*pointer to input and output*/
    int16_t* yOut_p[1]; /*pointer to input and output*/
} ADJ_VELOCITYStr_t;
/*==========ADJ_VELOCITY工作方塊結構體定義及初始化==========*/
#define ADJ_VELOCITYINI0 {\
    .add = 0,\
    .limit = ADJ_MAX,\
    .sm = SM_READY,\
    .w_back = 1,\
    .w_phase = 0,\
    .residual_phase = 0,\
    .yOut_p[0]=0}

// ADJ_VELOCITYStr_t ADJ_VELOCITY_str = ADJ_VELOCITYINI0;
/*==========ADJ_VELOCITY工作方塊佈線==========*/
#define ADJ_VELOCITY_LAY(ADJ_VELOCITYSTR) {\
    ADJ_VELOCITYSTR.uIn_p[0] = &ADJ_VELOCITYSTR.add;\
    ADJ_VELOCITYSTR.uIn_p[1] = &ADJ_VELOCITYSTR.w_back;\
    ADJ_VELOCITYSTR.yOut_p[0] =&ADJ_VELOCITYSTR.add_step;\
}

/*==========ADJ_VELOCITY工作方塊步級執行函式==========*/
void ADJ_VELOCITY_step(void* void_p);
#endif
#endif /* ADJ_VELOCITY_H */
