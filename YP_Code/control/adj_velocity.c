#include "adj_velocity.h"

#ifdef USE_HAL /* Using Danny's Hardware abstract layer */
    void init_cangle_inc(adj_v_t *adj_v) {
        adj_v->add = 0;
        adj_v->limit = ADJ_MAX;
        adj_v->sm = SM_READY;
        adj_v->w_back = 0;
        adj_v->w_phase = 0;
        adj_v->residual_phase = 0;
    }

    void set_cangle_inc(adj_v_t *adj_v, int16_t th_add) {
        adj_v->add = th_add;    /* Update the new phase adjust angle */
    }

    void set_cangle_wback(adj_v_t *adj_v, int16_t w_back) {
        adj_v->w_back = w_back;
    }

    int16_t get_cangle_inc(adj_v_t *adj_v) {
        if(adj_v->residual_phase > adj_v->limit) {
            adj_v->w_phase = adj_v->limit;
            adj_v->sm = SM_OVERLIMIT;
        }
        else {
            adj_v->w_phase = adj_v->residual_phase;
            adj_v->sm = SM_RUNNING;
        }
        adj_v->residual_phase += adj_v->add - adj_v->w_phase;
        adj_v->add = 0;

        return adj_v->w_back + adj_v->w_phase;
    }

#else
/*==========ADJ_VELOCITY工作方塊步級執行函式==========*/
void ADJ_VELOCITY_step(void* void_p){ //get_cangle_inc
    ADJ_VELOCITYStr_t* Str_p =(ADJ_VELOCITYStr_t*) void_p; /*typeset the str pointer */
    if(Str_p->residual_phase > Str_p->limit) {
        Str_p->w_phase = Str_p->limit;
        Str_p->sm = SM_OVERLIMIT;
    }
    else {
        Str_p->w_phase = Str_p->residual_phase;
        Str_p->sm = SM_RUNNING;
    }
    Str_p->residual_phase += Str_p->add - Str_p->w_phase;
    Str_p->add = 0;
    Str_p->add_step = Str_p->w_back + Str_p->w_phase;
}

#endif