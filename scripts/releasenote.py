
import sys
import os
import requests
from settings import SETTINGS



def get_release_mr():
    mes = os.environ['CI_COMMIT_MESSAGE']
    lines = mes.split('\n')

    text = ''
    for line in lines:
        if 'See merge request' in line:
            text = line
            break
    
    if text == '':
        print('ERROR: 此Commit不是Merge訊息。')
        sys.exit(1)

    mr_id = text.split('!')[1]

    return mr_id

def des_parser(text:str):
    lines = text.split('\n')

    start_line = 0
    end_line = 0
    for i, line in enumerate(lines):
        if '## 發佈訊息' in line:
            start_line = i + 1

        if '## 檢查事項' in line:
            end_line = i
    
    if start_line > end_line:
        print('ERROR: "## 發佈訊息" 應該在 "## 檢查事項" 之上。')
        sys.exit(1)
    
    res = "\n".join(lines[start_line:end_line])
    res = res.strip()

    if res == '':
        print('ERROR: 發佈訊息不能為空白。')
        sys.exit(1)
    
    return res

def get_release_note(mr_id:str):
    url = 'https://gitlab.com/api/v4/projects/{pid}/merge_requests/{mr_id}'.format(pid = SETTINGS['gitlab_pid'],mr_id = mr_id)

    r = requests.get(
        url=url,
        headers={
            'Private-Token': SETTINGS['PAT']
        }
    )
    j = r.json()

    des = j['description']
    res = des_parser(des)
    
    return res

cicd_stage = os.environ['CI_JOB_STAGE']

if cicd_stage == 'release-test':
    mr_id = os.environ['CI_MERGE_REQUEST_IID']
elif cicd_stage == 'release':
    mr_id = get_release_mr()
else:
    print('ERROR :預設stage以外狀況')
    sys.exit(1)

RELEASENOTE = get_release_note(mr_id)

def run():
    print(RELEASENOTE)

if __name__ == "__main__":
    run()
