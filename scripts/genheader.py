import os
import time

# start section text of c4mlib.h
HEADER_START_PATTERN = (
"""
// build info: 
//   version  : {version}
//   time     : {time}
//   device   : {dev_name}
//   platform : {dev_platform}
//   mcu      : {dev_mcu}
//   fcpu     : {dev_fcpu}

// developed by MVMC-lab
// you can get lastest version at https://gitlab.com/MVMC-lab/c4mlib/c4mlib/tags

#ifndef C4MLIB_H
#define C4MLIB_H

{header_inc}

{config_inc}

#define F_CPU 11059200UL

"""
)

# TODO 改成可以設定
AVR_HEADER_INC = (
"""
#include <avr/io.h>
#include <util/delay.h>
#include <stdint.h>
#include <stdio.h>
"""
)

CONFIG_INC_PATTERN = "#include \"{}\""

# end section text of c4mlib.h
HEADER_END = (
"""
#endif // C4MLIB_H
"""
)

def getConfigFiles(path):
    return os.listdir(path+'config')

def genHeaderStartSection(cfgs, ver, device):
    s = '\n'.join([CONFIG_INC_PATTERN.format(cfg) for cfg in cfgs])

    res = HEADER_START_PATTERN.format(
        version=ver,
        time=time.strftime("%m/%d/%Y %H:%M:%S"),
        dev_name=device['name'],
        dev_platform=device['platform'],
        dev_mcu=device['mcu'],
        dev_fcpu=device['fcpu'],
        header_inc=AVR_HEADER_INC,
        config_inc=s
    )

    return res

def searchModuleHeaders(path, module, device):
    searchFolders = [path + module + '/src']
    # add device folder if it exist
    # like c4mlib/hardware/src/m128
    if device['name'] in os.listdir(searchFolders[0]) :
        searchFolders += [searchFolders[0] + '/' + device['name']]
    
    # search header files in searchFolders
    files = list()
    for folder in searchFolders:
        for file in os.listdir(folder):
            if file.endswith(".h"):
                files += [folder + '/' + file]

    headers = sortHeaders(files)
    return headers

def sortHeaders(headers):
    #sort headers by priority
    tmpList = list()
    for header in headers:
        with open(header, 'r', encoding='UTF-8') as f:
            lines = f.readlines()
        priority = 99
        for i, line in enumerate(lines):
            if '@priority' in line:
                try:
                    priority = int(line.split('@priority')[1])
                except ValueError:
                    print('ERROR cannot parse priority in file {}'.format(header))
                    print('    line {}: {}'.format(i, line))
                    raise
                break
        tmpList += [{
            'file': header,
            'priority': priority
        }]
    return sorted(tmpList, key=lambda k: k['priority'])

def parseHeader(header):
    res = ''
    with open(header, 'r', encoding='UTF-8') as f:
        lines = f.readlines()
    starts = []
    ends = []
    for i, line in enumerate(lines):
        if '/* Public Section Start */' in line:
            starts += [i]
        elif '/* Public Section End */' in line:
            ends += [i]

    for s, e in zip(starts, ends):
        for line in lines[s+1:e]:
            res += line

    return res

def genC4mlibHeaders(sets, device, target):
    libpath = sets['library_path']
    modules = sets['modules']
    version = sets['version']
    
    content = str()
    for pack in modules.keys():
        for module in modules[pack]:
            modulepath = pack + '/' + module
            headers = searchModuleHeaders(libpath, modulepath, device)
            blocks = list()
            for header in headers:
                c = parseHeader(header['file'])
                if c != '':
                    blocks += [c]

            if len(blocks) != 0:
                title = (libpath + modulepath).replace('/','_').upper() + '_H'
                defineguardHeader = '#ifndef ' + title + '\n'
                defineguardHeader += '#define ' + title + '\n'

                defineguardEnd = '#endif //'+ title +'\n'

                content = defineguardHeader + '\n'.join(blocks) + defineguardEnd
                path = 'dist/' + libpath + pack
                if not os.path.isdir(path):
                    os.makedirs(path)
                with open(path + '/' + module + '.h' , 'w', encoding='UTF-8') as f:
                    f.write(content)
                    
        ## 產生[package].h
        blocks = ["#include \"{pack}/{mod}.h\"".format(pack=pack,mod = mod) for mod in modules[pack]]
        title = (libpath + pack).replace('/','_').upper() + '_H'
        defineguardHeader = '#ifndef ' + title + '\n'
        defineguardHeader += '#define ' + title + '\n'

        defineguardEnd = '#endif //'+ title +'\n'

        content = defineguardHeader + '\n'.join(blocks) +'\n' + defineguardEnd
        path = 'dist/' + libpath
        with open(path + '/' + pack + '.h' , 'w', encoding='UTF-8') as f:
            f.write(content)

    ## 產生c4mlib.h
    content = '\n'.join(["#include \"{libpath}{pack}.h\"".format(libpath=libpath,pack=pack) for pack in modules.keys()])
    
    with open(target , 'w', encoding='UTF-8') as f:
        cfgs = getConfigFiles(libpath)
        f.write(genHeaderStartSection(cfgs, version, device))
        f.write(content)
        f.write(HEADER_END)

    print('c4mlib.h is done.')


