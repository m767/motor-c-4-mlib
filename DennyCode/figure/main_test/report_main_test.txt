Data size is 1000
===== sensor ele angle =====
max: 359.253
min: -3.296
mean: 182.54553
===== command ele angle =====
max: 337.0
min: 0.0
mean: 167.87
===== theta error (degree) =====
max: 1.5830001
min: 0.61880004
mean: 1.0567132
std: 0.26418912
MSE: 1.1864386
N_STEP: 1.137081444943045, 2.9088556429161887
===== consume power (watt) =====
max: 8.609905
min: 0.12085197
mean: 4.1751137
std: 1.672283
sum: 4175.114 J
===== current RMS Square (watt) =====
rms_ia_square: 0.5196415 A
rms_ib_square: 0.13061453 A
===== current RMS (watt) =====
rms_ia: 0.7208617 A
rms_ib: 0.36140633 A
===== P_loss (watt) =====
P_loss_A: 3.3698752480745315 W
P_loss_B: 3.2035899621248247 W
===== P_in (watt) =====
P_in_A: 3.3504205 W
P_in_B: 1.6479756 W
===== Eta (H) (%) =====
P_efficient_A: -0.5806666122523795 %
P_efficient_B: 51.1377098165242 %
P_all: 16.47092552689051 %
