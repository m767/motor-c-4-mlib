import pickle
import matplotlib.pyplot as plt
from mypickle import save2pickle, load_pickle
import os
import numpy as np

FIG_DIR = "figure"
def plot_current(file_marker, DELTA_T):
    try:
        os.mkdir(FIG_DIR)
    except:
        pass
    try:
        os.mkdir(FIG_DIR+"/"+file_marker)
    except:
        pass

    save_data = load_pickle("data/data_"+file_marker+'.pickle')

    ia = save_data["ia"]
    ib = save_data["ib"]
    angle = save_data["angle"]
    pwma = save_data["pwma"]
    pwmb = save_data["pwmb"]

    t = np.linspace(0, ia.shape[0]-1, ia.shape[0]) * DELTA_T
    
    ### ==================================================================================== ###
    fig = plt.figure(figsize=(20, 12))
    # first figure
    plt.subplot(211)
    plt.plot(t, pwma)
    plt.plot(t, pwmb)
    plt.grid(True)
    plt.xlabel('t (s)', fontsize=24, position=(1,0))
    plt.ylabel('duty', fontsize=24, position=(0,1), rotation="horizontal")
    plt.title('Phase A B PWM', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20, position=(0,1), rotation="horizontal")
    plt.legend(['pwma', 'pwmb'], fontsize=20)
    # second figure
    plt.subplot(212)
    plt.plot(t, ia)
    plt.plot(t, ib)
    plt.grid(True)
    plt.xlabel('t (s)', fontsize=24, position=(1,0))
    plt.ylabel('I (A)', fontsize=24, position=(0,1), rotation="horizontal")
    plt.title('Phase A B current', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(['ia', 'ib'], fontsize=20)
    fig.align_labels()
    plt.savefig(FIG_DIR+"/"+file_marker + '/current_pwm_' + file_marker + '.png', dpi=300)
