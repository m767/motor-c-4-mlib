import os
import sys
import glob
import time
import shutil
import zipfile
import configparser
import ast
import subprocess

from settings import SETTINGS

VERSION = SETTINGS['version']
COMPILER = SETTINGS['avr_compilers'][0]

ZIPFILE = "dist/" + "c4mlib_" + VERSION + "_asam128_"+COMPILER['name']+".zip"

def run():
    print(ZIPFILE)
    with zipfile.ZipFile(ZIPFILE, 'r') as zip:
        zip.extractall(path='test/build_test')
    
    os.chdir('test/build_test')
    res = b''
    cmd = 'make COMPILER_PATH={}'.format(
        COMPILER['path']
    )
    if sys.platform =='win32':       
        with subprocess.Popen([cmd],shell=True,stdout=subprocess.PIPE, stderr=subprocess.PIPE) as proc:
            res += proc.stdout.read()
            res += proc.stderr.read()
    elif sys.platform == 'linux':
        with subprocess.Popen([cmd], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) as proc:
            res += proc.stdout.read()
            res += proc.stderr.read()

    print(res.decode('utf-8'))

if __name__ == "__main__":
    run()
