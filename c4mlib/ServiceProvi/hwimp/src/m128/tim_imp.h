/**
 * @file tim_imp.h
 * @author 
 * @brief 
 * @date 2019.09.04
 * 
 */
#ifndef C4MLIB_HARDEARE2_TIM_IMP_H
#define C4MLIB_HARDEARE2_TIM_IMP_H

#include "c4mlib/ServiceProvi/hwimp/src/hardware_interrupt.h"

/* Public Section Start */
#define TIM_HW_NUM 4
extern HWIntStr_t *TimIntStrList_p[TIM_HW_NUM];
/* Public Section End */

#endif  // C4MLIB_HARDEARE2_TIM_IMP_H
