/**
 * @file layout_macro.h
 * @author ya058764,smallplayplay
 * @brief
 * @date 2020.05.06,2021.05.21
 *
 */

#ifndef C4MLIB_HWIMP_M128_LAYOUT_H
#define C4MLIB_HWIMP_M128_LAYOUT_H

#include "c4mlib/ServiceProvi/hwimp/src/adc_imp.h"
#include "c4mlib/ServiceProvi/hwimp/src/ext_imp.h"
#include "c4mlib/ServiceProvi/hwimp/src/hardware_interrupt.h"
#include "c4mlib/ServiceProvi/hwimp/src/pwm_imp.h"
#include "c4mlib/ServiceProvi/hwimp/src/spi_imp.h"
#include "c4mlib/ServiceProvi/hwimp/src/tim_imp.h"
#include "c4mlib/ServiceProvi/hwimp/src/twi_imp.h"
#include "c4mlib/ServiceProvi/hwimp/src/uart_imp.h"

/* Public Section Start */

//=========ADC中斷=========

/**
 * @brief
 *呼叫本巨集會建立一個中斷服務分享器管理結構體HWIntStr_t以及搭配的分享工作表單，
 *並且將它掛進特定類型硬體中斷查詢表單XXXIntList_p[n]中，等待INTERNAL_ISR取用並執行
 *
 * @param INTSTR 中斷服務分享器的結構體命名
 * @param ININUM 本中斷服務分享器的結構體佔同類型中斷的編序
 * @param TASKNUM 允許被登錄至本中斷服務分享器中執行的工作最高數目
 * @param HWOPSTR 硬體操作結構體
 */
#define ADCHWINT_LAY(INTSTR, ININUM, TASKNUM, HWOPSTR)                         \
    HWIntStr_t INTSTR = {.IntEnReg_p = HWOPSTR.IntEnReg_p,                     \
                         .IntEnMask  = HWOPSTR.IntEnMask,                      \
                         .IntEnShift = HWOPSTR.IntEnShift,                     \
                         .MaxTask    = TASKNUM,                                \
                         .Total      = 0};                                     \
    {                                                                          \
        static TaskBlock_t INTSTR##_TASKLIST[TASKNUM];                         \
        INTSTR.Task_p = INTSTR##_TASKLIST;                                     \
    }                                                                          \
    AdcIntStrList_p[ININUM] = &INTSTR

/**
 * @brief
 * 本巨集協助使用者將功能方塊登錄為降頻工作執行器之其中一個工作，同時初始其禁致能
 * @param HWISTR 中斷服務分享器的結構體
 * @param FBFUNCA 使用者自訂ISR
 * @param FBPARASTR 使用者自訂ISR中需要用到的傳參結構
 * @param ENABLE 禁致能本工作在降頻執行器
 */
#define ADCHWINT_REG(HWISTR, FBFUNCA, FBPARASTR, ENABLE)                       \
    {                                                                          \
        uint8_t TaskID       = HWInt_reg(&HWISTR, FBFUNCA, &FBPARASTR);        \
        FBPARASTR.ADCSTR_ssp = &HWISTR;                                        \
        FBPARASTR.ADCTaskId  = TaskId;                                         \
    }                                                                          \
    HWInt_en(FBPARASTR.ADCSTR_ssp, FBPARASTR.ADCTaskId, ENABLE)

//=========計時中斷=========
/**
 * @brief
 *呼叫本巨集會建立一個中斷服務分享器管理結構體HWIntStr_t以及搭配的分享工作表單，
 *並且將它掛進特定類型硬體中斷查詢表單XXXIntList_p[n]中，等待INTERNAL_ISR取用並執行
 *
 * @param INTSTR 中斷服務分享器的結構體命名
 * @param ININUM 本中斷服務分享器的結構體佔同類型中斷的編序
 * @param TASKNUM 允許被登錄至本中斷服務分享器中執行的工作最高數目
 * @param HWOPSTR 硬體操作結構體
 */
#define TIMHWINT_LAY(INTSTR, ININUM, TASKNUM, HWOPSTR)                         \
    HWIntStr_t INTSTR = {.IntEnReg_p = HWOPSTR.IntEnReg_p,                     \
                         .IntEnMask  = HWOPSTR.IntEnMask,                      \
                         .IntEnShift = HWOPSTR.IntEnShift,                     \
                         .MaxTask    = TASKNUM,                                \
                         .Total      = 0};                                     \
    {                                                                          \
        static TaskBlock_t INTSTR##_TASKLIST[TASKNUM];                         \
        INTSTR.Task_p = INTSTR##_TASKLIST;                                     \
    }                                                                          \
    TimIntStrList_p[ININUM] = &INTSTR

/**
 * @brief
 * 本巨集協助使用者將功能方塊登錄為降頻工作執行器之其中一個工作，同時初始其禁致能
 * @param HWISTR 中斷服務分享器的結構體
 * @param FBFUNCA 使用者自訂ISR
 * @param FBPARASTR 使用者自訂ISR中需要用到的傳參結構
 * @param ENABLE 禁致能本工作在降頻執行器
 */
#define TIMHWINT_REG(HWISTR, FBFUNCA, FBPARASTR, ENABLE)                       \
    {                                                                          \
        uint8_t TaskID       = HWInt_reg(&HWISTR, FBFUNCA, &FBPARASTR);        \
        FBPARASTR.TIMSTR_ssp = &HWISTR;                                        \
        FBPARASTR.TIMTaskId  = TaskId;                                         \
    }                                                                          \
    HWInt_en(FBPARASTR.TIMSTR_ssp, FBPARASTR.TIMTaskId, ENABLE)

//=========PWM中斷=========
/**
 * @brief
 *呼叫本巨集會建立一個中斷服務分享器管理結構體HWIntStr_t以及搭配的分享工作表單，
 *並且將它掛進特定類型硬體中斷查詢表單XXXIntList_p[n]中，等待INTERNAL_ISR取用並執行
 *
 * @param INTSTR 中斷服務分享器的結構體命名
 * @param ININUM 本中斷服務分享器的結構體佔同類型中斷的編序
 * @param TASKNUM 允許被登錄至本中斷服務分享器中執行的工作最高數目
 * @param HWOPSTR 硬體操作結構體
 */
#define PWMHWINT_LAY(INTSTR, ININUM, TASKNUM, HWOPSTR)                         \
    HWIntStr_t INTSTR = {.IntEnReg_p = HWOPSTR.IntEnReg_p,                     \
                         .IntEnMask  = HWOPSTR.IntEnMask,                      \
                         .IntEnShift = HWOPSTR.IntEnShift,                     \
                         .MaxTask    = TASKNUM,                                \
                         .Total      = 0};                                     \
    {                                                                          \
        static TaskBlock_t INTSTR##_TASKLIST[TASKNUM];                         \
        INTSTR.Task_p = INTSTR##_TASKLIST;                                     \
    }                                                                          \
    PwmIntStrList_p[ININUM] = &INTSTR

/**
 * @brief
 * 本巨集協助使用者將功能方塊登錄為降頻工作執行器之其中一個工作，同時初始其禁致能
 * @param HWISTR 中斷服務分享器的結構體
 * @param FBFUNCA 使用者自訂ISR
 * @param FBPARASTR 使用者自訂ISR中需要用到的傳參結構
 * @param ENABLE 禁致能本工作在降頻執行器
 */
#define PWMHWINT_REG(HWISTR, FBFUNCA, FBPARASTR, ENABLE)                       \
    {                                                                          \
        uint8_t TaskID       = HWInt_reg(&HWISTR, FBFUNCA, &FBPARASTR);        \
        FBPARASTR.PWMSTR_ssp = &HWISTR;                                        \
        FBPARASTR.PWMTaskId  = TaskId;                                         \
    }                                                                          \
    HWInt_en(FBPARASTR.PWMSTR_ssp, FBPARASTR.PWMTaskId, ENABLE)

//=========外部中斷=========
/**
 * @brief
 *呼叫本巨集會建立一個中斷服務分享器管理結構體HWIntStr_t以及搭配的分享工作表單，
 *並且將它掛進特定類型硬體中斷查詢表單XXXIntList_p[n]中，等待INTERNAL_ISR取用並執行
 *
 * @param INTSTR 中斷服務分享器的結構體命名
 * @param ININUM 本中斷服務分享器的結構體佔同類型中斷的編序
 * @param TASKNUM 允許被登錄至本中斷服務分享器中執行的工作最高數目
 * @param HWOPSTR 硬體操作結構體
 */
#define EXTHWINT_LAY(INTSTR, ININUM, TASKNUM, HWOPSTR)                         \
    HWIntStr_t INTSTR = {.IntEnReg_p = HWOPSTR.IntEnReg_p,                     \
                         .IntEnMask  = HWOPSTR.IntEnMask,                      \
                         .IntEnShift = HWOPSTR.IntEnShift,                     \
                         .MaxTask    = TASKNUM,                                \
                         .Total      = 0};                                     \
    {                                                                          \
        static TaskBlock_t INTSTR##_TASKLIST[TASKNUM];                         \
        INTSTR.Task_p = INTSTR##_TASKLIST;                                     \
    }                                                                          \
    ExtIntStrList_p[ININUM] = &INTSTR

/**
 * @brief
 * 本巨集協助使用者將功能方塊登錄為降頻工作執行器之其中一個工作，同時初始其禁致能
 * @param HWISTR 中斷服務分享器的結構體
 * @param FBFUNCA 使用者自訂ISR
 * @param FBPARASTR 使用者自訂ISR中需要用到的傳參結構
 * @param ENABLE 禁致能本工作在降頻執行器
 */
#define EXTHWINT_REG(HWISTR, FBFUNCA, FBPARASTR, ENABLE)                       \
    {                                                                          \
        uint8_t TaskID       = HWInt_reg(&HWISTR, FBFUNCA, &FBPARASTR);        \
        FBPARASTR.EXTSTR_ssp = &HWISTR;                                        \
        FBPARASTR.EXTTaskId  = TaskId;                                         \
    }                                                                          \
    HWInt_en(FBPARASTR.EXTSTR_ssp, FBPARASTR.EXTTaskId, ENABLE)

//======================通訊============================

//=========UART TX中斷=========
/**
 * @brief
 *呼叫本巨集會建立一個中斷服務分享器管理結構體HWIntStr_t以及搭配的分享工作表單，
 *並且將它掛進特定類型硬體中斷查詢表單XXXIntList_p[n]中，等待INTERNAL_ISR取用並執行
 *
 * @param INTSTR 中斷服務分享器的結構體命名
 * @param ININUM 本中斷服務分享器的結構體佔同類型中斷的編序
 * @param TASKNUM 允許被登錄至本中斷服務分享器中執行的工作最高數目
 * @param HWOPSTR 硬體操作結構體
 */
#define UARTTXHWINT_LAY(INTSTR, ININUM, TASKNUM, HWOPSTR)                      \
    HWIntStr_t INTSTR = {.IntEnReg_p = HWOPSTR.TXIntEnReg_p,                   \
                         .IntEnMask  = HWOPSTR.TXIntEnMask,                    \
                         .IntEnShift = HWOPSTR.TXIntEnShift,                   \
                         .MaxTask    = TASKNUM,                                \
                         .Total      = 0};                                     \
    {                                                                          \
        static TaskBlock_t INTSTR##_TASKLIST[TASKNUM];                         \
        INTSTR.Task_p = INTSTR##_TASKLIST;                                     \
    }                                                                          \
    UartTxIntStrList_p[ININUM] = &INTSTR

/**
 * @brief
 * 本巨集協助使用者將功能方塊登錄為降頻工作執行器之其中一個工作，同時初始其禁致能
 * @param HWISTR 中斷服務分享器的結構體
 * @param FBFUNCA 使用者自訂ISR
 * @param FBPARASTR 使用者自訂ISR中需要用到的傳參結構
 * @param ENABLE 禁致能本工作在降頻執行器
 */
#define UARTTXHWINT_REG(HWISTR, FBFUNCA, FBPARASTR, ENABLE)                    \
    {                                                                          \
        uint8_t TaskID          = HWInt_reg(&HWISTR, FBFUNCA, &FBPARASTR);     \
        FBPARASTR.UARTTXSTR_ssp = &HWISTR;                                     \
        FBPARASTR.UARTTXTaskId  = TaskId;                                      \
    }                                                                          \
    HWInt_en(FBPARASTR.UARTTXSTR_ssp, FBPARASTR.UARTTXTaskId, ENABLE)

//=========UART RX中斷=========
/**
 * @brief
 *呼叫本巨集會建立一個中斷服務分享器管理結構體HWIntStr_t以及搭配的分享工作表單，
 *並且將它掛進特定類型硬體中斷查詢表單XXXIntList_p[n]中，等待INTERNAL_ISR取用並執行
 *
 * @param INTSTR 中斷服務分享器的結構體命名
 * @param ININUM 本中斷服務分享器的結構體佔同類型中斷的編序
 * @param TASKNUM 允許被登錄至本中斷服務分享器中執行的工作最高數目
 * @param HWOPSTR 硬體操作結構體
 */
#define UARTRXHWINT_LAY(INTSTR, ININUM, TASKNUM, HWOPSTR)                      \
    HWIntStr_t INTSTR = {.IntEnReg_p = HWOPSTR.RXIntEnReg_p,                   \
                         .IntEnMask  = HWOPSTR.RXIntEnMask,                    \
                         .IntEnShift = HWOPSTR.RXIntEnShift,                   \
                         .MaxTask    = TASKNUM,                                \
                         .Total      = 0};                                     \
    {                                                                          \
        static TaskBlock_t INTSTR##_TASKLIST[TASKNUM];                         \
        INTSTR.Task_p = INTSTR##_TASKLIST;                                     \
    }                                                                          \
    UartRxIntStrList_p[ININUM] = &INTSTR

/**
 * @brief
 * 本巨集協助使用者將功能方塊登錄為降頻工作執行器之其中一個工作，同時初始其禁致能
 * @param HWISTR 中斷服務分享器的結構體
 * @param FBFUNCA 使用者自訂ISR
 * @param FBPARASTR 使用者自訂ISR中需要用到的傳參結構
 * @param ENABLE 禁致能本工作在降頻執行器
 */
#define UARTRXHWINT_REG(HWISTR, FBFUNCA, FBPARASTR, ENABLE)                    \
    {                                                                          \
        uint8_t TaskID          = HWInt_reg(&HWISTR, FBFUNCA, &FBPARASTR);     \
        FBPARASTR.UARTRXSTR_ssp = &HWISTR;                                     \
        FBPARASTR.UARTRXTaskId  = TaskId;                                      \
    }                                                                          \
    HWInt_en(FBPARASTR.UARTRXSTR_ssp, FBPARASTR.UARTRXTaskId, ENABLE)

//=========SPI中斷=========
/**
 * @brief
 *呼叫本巨集會建立一個中斷服務分享器管理結構體HWIntStr_t以及搭配的分享工作表單，
 *並且將它掛進特定類型硬體中斷查詢表單XXXIntList_p[n]中，等待INTERNAL_ISR取用並執行
 *
 * @param INTSTR 中斷服務分享器的結構體命名
 * @param ININUM 本中斷服務分享器的結構體佔同類型中斷的編序
 * @param TASKNUM 允許被登錄至本中斷服務分享器中執行的工作最高數目
 * @param HWOPSTR 硬體操作結構體
 */
#define SPIHWINT_LAY(INTSTR, ININUM, TASKNUM, HWOPSTR)                         \
    HWIntStr_t INTSTR = {.IntEnReg_p = HWOPSTR.IntEnReg_p,                     \
                         .IntEnMask  = HWOPSTR.IntEnMask,                      \
                         .IntEnShift = HWOPSTR.IntEnShift,                     \
                         .MaxTask    = TASKNUM,                                \
                         .Total      = 0};                                     \
    {                                                                          \
        static TaskBlock_t INTSTR##_TASKLIST[TASKNUM];                         \
        INTSTR.Task_p = INTSTR##_TASKLIST;                                     \
    }                                                                          \
    SpiIntStrList_p[ININUM] = &INTSTR

/**
 * @brief
 * 本巨集協助使用者將功能方塊登錄為降頻工作執行器之其中一個工作，同時初始其禁致能
 * @param HWISTR 中斷服務分享器的結構體
 * @param FBFUNCA 使用者自訂ISR
 * @param FBPARASTR 使用者自訂ISR中需要用到的傳參結構
 * @param ENABLE 禁致能本工作在降頻執行器
 */
#define SPIHWINT_REG(HWISTR, FBFUNCA, FBPARASTR, ENABLE)                       \
    {                                                                          \
        uint8_t TaskID       = HWInt_reg(&HWISTR, FBFUNCA, &FBPARASTR);        \
        FBPARASTR.SPISTR_ssp = &HWISTR;                                        \
        FBPARASTR.SPITaskId  = TaskId;                                         \
    }                                                                          \
    HWInt_en(FBPARASTR.SPISTR_ssp, FBPARASTR.SPITaskId, ENABLE)

//=========TWI中斷=========
/**
 * @brief
 *呼叫本巨集會建立一個中斷服務分享器管理結構體HWIntStr_t以及搭配的分享工作表單，
 *並且將它掛進特定類型硬體中斷查詢表單XXXIntList_p[n]中，等待INTERNAL_ISR取用並執行
 *
 * @param INTSTR 中斷服務分享器的結構體命名
 * @param ININUM 本中斷服務分享器的結構體佔同類型中斷的編序
 * @param TASKNUM 允許被登錄至本中斷服務分享器中執行的工作最高數目
 * @param HWOPSTR 硬體操作結構體
 */
#define TWIHWINT_LAY(INTSTR, ININUM, TASKNUM, HWOPSTR)                         \
    HWIntStr_t INTSTR = {.IntEnReg_p = HWOPSTR.IntEnReg_p,                     \
                         .IntEnMask  = HWOPSTR.IntEnMask,                      \
                         .IntEnShift = HWOPSTR.IntEnShift,                     \
                         .MaxTask    = TASKNUM,                                \
                         .Total      = 0};                                     \
    {                                                                          \
        static TaskBlock_t INTSTR##_TASKLIST[TASKNUM];                         \
        INTSTR.Task_p = INTSTR##_TASKLIST;                                     \
    }                                                                          \
    TwiIntStrList_p[ININUM] = &INTSTR

/**
 * @brief
 * 本巨集協助使用者將功能方塊登錄為降頻工作執行器之其中一個工作，同時初始其禁致能
 * @param HWISTR 中斷服務分享器的結構體
 * @param FBFUNCA 使用者自訂ISR
 * @param FBPARASTR 使用者自訂ISR中需要用到的傳參結構
 * @param ENABLE 禁致能本工作在降頻執行器
 */
#define TWIHWINT_REG(HWISTR, FBFUNCA, FBPARASTR, ENABLE)                       \
    {                                                                          \
        uint8_t TaskID       = HWInt_reg(&HWISTR, FBFUNCA, &FBPARASTR);        \
        FBPARASTR.TWISTR_ssp = &HWISTR;                                        \
        FBPARASTR.TWITaskId  = TaskId;                                         \
    }                                                                          \
    HWInt_en(FBPARASTR.TWISTR_ssp, FBPARASTR.TWITaskId, ENABLE)
/* Public Section End */
#endif  // C4MLIB_HWIMP_M128_LAYOUT_H
