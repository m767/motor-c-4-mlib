/**
 * @file hardware_interrupt.h
 * @author ya058764, smallplayplay, cy023
 * @date 2021.5.30
 * @brief
 *
 **/

#ifndef C4MLIB_HARDWARE_INTERRUPT_H
#define C4MLIB_HARDWARE_INTERRUPT_H
#include "c4mlib/C4MBios/macro/src/std_type.h"
/* Public Section Start */

/**
 * @brief 用於定義結構體以存放工作方塊管理參數
 *
 * @param func_ret 函式回傳錯誤碼
 * @param state 禁致能及錯誤狀態
 * @param FBFunc_p 功能方塊執行函式指標
 * @param FBPara_p 功能方塊結執行函式傳參結構指標
 */
typedef struct {
    uint8_t func_ret;
    volatile uint8_t state;
    TaskFunc_t FBFunc_p;
    void *FBPara_p;
} TaskBlock_t;

/**
 * @brief
 *用於為不同硬體中斷定義出管理用結構體以備後續使用
 * @param IntEnReg_p 中斷禁致能暫存器
 * @param IntEnMask  中斷禁致能遮罩
 * @param IntEnShift 中斷禁致能平移
 * @param MaxTask 最大Task數
 * @param Total 目前登記的總Task數
 * @param Task_p Task串列
 */
typedef struct {
    volatile uint8_t *IntEnReg_p;
    uint8_t IntEnMask, IntEnShift;
    uint8_t MaxTask;
    uint8_t Total;
    volatile TaskBlock_t *Task_p;
} HWIntStr_t;

/**
 * @brief
 *被 internal ISR( )呼叫執行登錄在特定中斷中所有己登錄的工作方塊
 * @param Str_p 特定硬體中斷管理用結構體指標，結構體提供己登錄工作資料
 */
void HWInt_step(HWIntStr_t *Str_p);

/**
 * @brief
 *將工作方塊登錄分享特定硬體中斷服務常式。其工作項目包括：
 *把功能方塊函式及結構體指標，存入硬體中斷管理結構體，取得工作編號TaskId
 * @param Str_p 特定硬體中斷服務常式分享管理結構體指標
 * @param FBFunc_p 功能方塊執行函式指標
 * @param FBPara_p 功能方塊參數結構體指標
 * @return uint8_t TaskID
 */
uint8_t HWInt_reg(HWIntStr_t *Str_p, TaskFunc_t FBFunc_p, void *FBPara_p);

/**
 * @brief
 *禁致能己登錄特定硬體中斷中等待由特定中斷於中斷時執行的工作方塊
 * @param Str_p 特定硬體中斷服務常式分享管理結構體指標
 * @param TaskID 登錄進特定硬體中斷時所取得的工作識別編號
 * @param Enable 禁致能參數，1: 致能，0:禁能
 * @return uint8_t
 */
uint8_t HWInt_en(HWIntStr_t *Str_p, uint8_t TaskID, uint8_t Enable);

/**
 * @brief
 *檢查己登錄特定硬體中斷中等待由特定中斷於中斷時執行的工作方塊禁致能狀態傳值參數
 * @param Str_p 特定硬體中斷服務常式分享管理結構體指標
 * @param TaskID 登錄進特定硬體中斷時所取得的工作識別編號
 * @return uint8_t: Disable(0), Enable(1), Other Exception(2)
 */
uint8_t HWInt_chk(HWIntStr_t *Str_p, uint8_t TaskId);

/* Public Section End */
#endif
