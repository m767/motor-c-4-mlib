/**
 * @file hardware_interrupt.c
 * @author ya058764, smallplayplay, cy023
 * @date 2021.5.30
 * @brief
 *
 **/

#include "c4mlib/ServiceProvi/hwimp/src/hardware_interrupt.h"

#include "c4mlib/C4MBios/debug/src/debug.h"
#include "c4mlib/C4MBios/macro/src/bits_op.h"

#include <stdio.h>

#define OK                                      0
#define ERROR_HWINT_EN_TASKID_DOES_NOT_EXIST    255
#define ERROR_HWINT_REG_OVER_REGIST             255
#define ERROR_HW_INTERUPT_STR_DOES_NOT_EXIST    255
#define PERIPHERAL_INTERRUPT_ENABLE             255
#define ERROR_HWINT_CHK_TASKID_DOES_NOT_EXIST   2

void HWInt_step(HWIntStr_t *Str_p) {
    if (Str_p != NULL) {
        volatile TaskBlock_t *ptr = NULL;
        for (uint8_t i = 0; i < Str_p->Total; i++) {
            ptr = Str_p->Task_p + i;
            if (ptr->state) {
                ptr->func_ret = ptr->FBFunc_p(ptr->FBPara_p);
            }
        }
    }
}

uint8_t HWInt_reg(HWIntStr_t *Str_p, TaskFunc_t FbFunc_p, void *FbPara_p) {
    if (Str_p->Total < Str_p->MaxTask) {
        uint8_t TaskId = Str_p->Total;  // New Id is the current Total
        if (Str_p != NULL) {
            volatile TaskBlock_t *ptr = NULL;
            ptr                       = Str_p->Task_p + TaskId;
            ptr->state                = 0;  // Default Disable
            ptr->FBFunc_p             = FbFunc_p;
            ptr->FBPara_p             = FbPara_p;
            Str_p->Total++;
            return TaskId;
        }
        else {
            DEBUG_INFO(
                "Warning !!! HWInt_reg() HW Interrupt Str. does not exist\n");
            return ERROR_HW_INTERUPT_STR_DOES_NOT_EXIST;
        }
    }
    else {
        DEBUG_INFO("Warning !!! HWInt_reg() over regist \n");
        return ERROR_HWINT_REG_OVER_REGIST;
    }
}

uint8_t HWInt_en(HWIntStr_t *Str_p, uint8_t TaskId, uint8_t Enable) {
    if (Str_p != NULL) {
        if (TaskId == PERIPHERAL_INTERRUPT_ENABLE) {
            REGFPT(Str_p->IntEnReg_p, Str_p->IntEnMask, Str_p->IntEnShift,
                   Enable);
        }
        else if (TaskId < Str_p->Total) {
            ((Str_p->Task_p) + TaskId)->state = Enable;
        }
        else {
            DEBUG_INFO("Warning !!! HWInt_en() TaskId does not exist\n");
            return ERROR_HWINT_EN_TASKID_DOES_NOT_EXIST;
        }
    }
    else {
        DEBUG_INFO("Warning !!! HWInt_en() HW Interrupt Str. does not exist\n");
        return ERROR_HW_INTERUPT_STR_DOES_NOT_EXIST;
    }
    return OK;
}

uint8_t HWInt_chk(HWIntStr_t *Str_p, uint8_t TaskId) {
    if (Str_p != NULL) {
        if (TaskId == ERROR_HWINT_EN_TASKID_DOES_NOT_EXIST) {
            uint8_t state;
            REGFGT(Str_p->IntEnReg_p, Str_p->IntEnMask, Str_p->IntEnShift, &state);
            return state;
        }
        else if (TaskId < Str_p->Total) {
            return (Str_p->Task_p[TaskId].state);
        }
        else {
            DEBUG_INFO("Warning !!! HWInt_en() TaskId does not exist\n");
            return ERROR_HWINT_CHK_TASKID_DOES_NOT_EXIST;
        }
    }
    else {
        DEBUG_INFO("Warning !!! HWInt_en() HW Interrupt Str. does not exist\n");
        return ERROR_HWINT_CHK_TASKID_DOES_NOT_EXIST;
    }
}