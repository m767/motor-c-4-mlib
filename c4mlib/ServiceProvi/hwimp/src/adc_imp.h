/**
 * @file adc_imp.h
 * @author LiYu87
 * @brief 
 * @date 2019.09.04
 * 
 */

#ifndef C4MLIB_HARDWARE2_ADC_IMP_H
#define C4MLIB_HARDWARE2_ADC_IMP_H

#if defined(__AVR_ATmega128__)
#    include "m128/adc_imp.h"
#elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega48__) || \
    defined(__AVR_ATmega168__)
#    include "m88/adc_imp.h"
#elif defined(__AVR_ATtiny2313__)
#    include "tiny2313/adc_imp.h"
#else
#    if !defined(__COMPILING_AVR_LIBC__)
#        warning "device type not defined"
#    endif
#endif

#endif  // C4MLIB_HARDWARE2_ADC_IMP_H
