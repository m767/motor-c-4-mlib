/**
 * @file AS5047D.h
 * @author 塗塗改改 (w8indow61231111@gmail.com)
 * @date 2021.08.02
 * @brief encoder程式
 *
 */
#ifndef __AS5047D_H__
#define __AS5047D_H__

#include "c4mlib/C4MBios/device/src/device.h"
// #include "spi.h"
// #include <util\delay.h>
// AS5047D Volatile Register Table (Figure 18)

#define NOP      0x0000
#define ERRFL    0x0001
#define PROG     0x0003
#define DIAAGC   0x3FFC
#define MAG      0x3FFD
#define ANGLEUNC 0x3FFE
#define ANGLECOM 0x3FFF

// AS5047D Non-Volatile Register Table (Figure 25)

#define ZPOSM     0x0016
#define ZPOSL     0x0017
#define SETTINGS1 0x0018
#define SETTINGS2 0x0019
#define RED       0x001A

// AS5047D Use SPI Mode1
// AS5047D with M128 and ASAbus hardware pins configuration
#define SpiEncoder_DDR      DDRB
#define SpiEncoder_PORT     PORTB
#define SpiEncoder_CLK      PB1  // SCK
#define SpiEncoder_DI       PB2  // MOSI
#define SpiEncoder_DO       PB3  // MISO
#define SpiEncoder_DIO_DDR  DDRD
#define SpiEncoder_DIO_PORT PORTD
#define SpiEncoder_CS       PD5  // /CS
// SPI Encoder AS5047D Initial


typedef struct {
    void (*init)(void);
    volatile uint16_t angle;
    void (*update)(void);
    void (*update_iir)(void);
    void (*update_compensate)(void);
}as5047d_t;

/**
 * @brief encoder初始化
 * 
 */
void SpiEncoder_Init(void);
/**
 * @brief 讀回沒有動態補償的值
 *
 * @param ANGLEUNC_p 值的指標
 */
void SpiEncoder_ReadANGLEUNC_IIR(void);

/**
 * @brief 讀回沒有動態補償的值
 *
 * @param ANGLEUNC_p 值的指標
 */
void SpiEncoder_ReadANGLEUNC(void);

/**
 * @brief 讀回有動態補償的值
 *
 * @param ANGLEUNC_p 值的指標
 */
void SpiEncoder_ReadANGLECOM(void);

#endif
