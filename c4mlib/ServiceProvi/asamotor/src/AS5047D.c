/**
 * @file AS5047D.c
 * @author 塗塗改改 (w8indow61231111@gmail.com)
 * @date 2021.08.02
 * @brief encoder程式
 *
 */
#include "AS5047D.h"

#include <avr/io.h>

// #include "uart.h"

/*SETBIT Series*/
// #define SETBIT(ADDRESS,BIT)		(ADDRESS |= (1<<BIT))
// #define CLRBIT(ADDRESS,BIT)		(ADDRESS &= ~(1<<BIT))
// #define CHKBIT(ADDRESS,BIT)		((ADDRESS & (1<<BIT))==(1<<BIT))

#define SpiEncoder_ChipSelect()  CLRBIT(SpiEncoder_DIO_PORT, SpiEncoder_CS)
#define SpiEncoder_ChipRelease() SETBIT(SpiEncoder_DIO_PORT, SpiEncoder_CS)

as5047d_t encoder = {
    .angle             = 0,
    .init              = SpiEncoder_Init,
    .update            = SpiEncoder_ReadANGLEUNC,
    .update_iir        = SpiEncoder_ReadANGLEUNC_IIR,
    .update_compensate = SpiEncoder_ReadANGLECOM,
};

uint8_t get_parity(uint16_t n) {
    //若有奇數個1，回傳1
    uint8_t parity = 0;
    while (n) {
        parity = !parity;
        n      = n & (n - 1);
    }
    return parity;
}

void SpiEncoder_Init(void) {
    /* Set CS, SCK and MOSI as output when used with SPI Encoder, all others
     * input */
    SpiEncoder_DDR |=
        (1 << SpiEncoder_CLK) | (1 << SpiEncoder_DI) | (0 << SpiEncoder_DO);
    SpiEncoder_DIO_DDR |= (1 << SpiEncoder_CS);
    SpiEncoder_ChipRelease();
    SPI_MasterInst.init();  // Spi_Init();
    // SCK oscillator frequency divided 2
    SPCR &= ~((1 << SPR0) | (1 << SPR1));
    SPSR |= (1 << SPI2X);
    // Spi Encoder use SPI mode 1, so that CPOL be set to 0 and CPHA be set to 1
    SPCR |= (0 << CPOL) | (1 << CPHA);
    SpiEncoder_ReadANGLEUNC();
}

void SpiEncoder_ReadANGLEUNC(void) {
    uint16_t TransmitData =
        ANGLEUNC + 0x4000;  // SPI Command Frame//0x4000:read
    uint16_t readData = 0;
    SpiEncoder_ChipSelect();
    SPDR = (TransmitData >> 8);
    while (!(SPSR & (1 << SPIF))) {
        ;
    }
    readData = SPDR;
    SPDR     = TransmitData;
    while (!(SPSR & (1 << SPIF))) {
        ;
    }
    SpiEncoder_ChipRelease();
    readData = ((readData << 8) | SPDR);
    if (((readData >> 14) & 0x01) == 1) {
        printf("read error ");
        return;
    }
    else if (get_parity(readData)) {
        printf("read parity error ");
        return;
    }
    encoder.angle = readData & 0x3fff;
}

// SPI Encoder AS5047D Read ANGLEUNC
// 讀回沒有動態補償的值
void SpiEncoder_ReadANGLEUNC_IIR(void) {
    uint16_t TransmitData =
        ANGLEUNC + 0x4000;  // SPI Command Frame//0x4000:read
    uint16_t readData = 0;
    SpiEncoder_ChipSelect();
    SPDR = (TransmitData >> 8);
    while (!(SPSR & (1 << SPIF))) {
        ;
    }
    readData = SPDR;
    SPDR     = TransmitData;
    while (!(SPSR & (1 << SPIF))) {
        ;
    }
    SpiEncoder_ChipRelease();
    readData = ((readData << 8) | SPDR);
    if (((readData >> 14) & 0x01) == 1) {
        printf("read error ");
        return;
    }
    else if (get_parity(readData)) {
        printf("read parity error ");
        return;
    }
    int16_t encoder_temp = (uint16_t)(readData & 0x3fff);
    // printf("%d,", encoder_temp);
    // static uint8_t flag=1;
    static double y_iir[2] = {0};
    static double x[2]   = {0};
    static double y      = 0;

    if(encoder_temp-x[0]>8192){
        y_iir[0]+=16384;
        y_iir[1]+=16384;
        x[0]+=16384;
        x[1]+=16384;
    }
    else if(encoder_temp-x[0]<-8192){
        y_iir[0]-=16384;
        y_iir[1]-=16384;
        x[0]-=16384;
        x[1]-=16384;
    }
    y = 1.630102594291077 * y_iir[0]- 0.688557694406910 * y_iir[1] + 0.014613775028958 * encoder_temp + 0.029227550057916 * x[0] + 0.014613775028958 * x[1];

    x[1]          = x[0];
    x[0]          = encoder_temp;
    y_iir[1]      = y_iir[0];
    y_iir[0]      = y;
    encoder.angle=y+0.5;
    
    // printf("%d\n", encoder.angle);
}
// SPI Encoder AS5047D Read ANGLECOM
// 讀回有動態補償的值
void SpiEncoder_ReadANGLECOM(void) {
    uint16_t TransmitData;
    TransmitData =
        0x3FFF + 0x4000 +
        0x8000;  // SPI Command Frame//0x4000:read//0x8000: Parity bit (even)
    uint16_t readData = 0;
    SpiEncoder_ChipSelect();
    SPDR = (TransmitData >> 8);
    while (!(SPSR & (1 << SPIF))) {
        ;
    }
    readData = SPDR;
    SPDR     = TransmitData;
    while (!(SPSR & (1 << SPIF))) {
        ;
    }
    SpiEncoder_ChipRelease();
    readData = ((readData << 8) | SPDR);
    if (((readData >> 14) & 0x01) == 1) {
        printf("read error ");
        return;
    }
    else if (get_parity(readData)) {
        printf("read parity error ");
        return;
    }
    encoder.angle = readData & 0x3fff;
}
