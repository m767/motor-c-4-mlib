/**
 * @file test_encoder.c
 * @author 塗塗改改,sjchuang
 * @date 2021.7.29
 * @brief 測試encoder
 **/
// #include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/ServiceProvi/asamotor/src/AS5047D.h"
extern as5047d_t encoder;

int main() {
    C4M_DEVICE_set();
    encoder.init();
    printf("%x \t %x\n", SPCR, SPSR);
    while (1) {
        encoder.update();  //讀回沒有動態補償的值0~16384
        printf("%d \t", encoder.angle);
        encoder.update_compensate();  //讀回有動態補償的值0~16384
        printf("%d \n", encoder.angle);
        _delay_ms(300);
    }
}