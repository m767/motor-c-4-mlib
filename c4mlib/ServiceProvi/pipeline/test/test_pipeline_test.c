/**
 * @file test_pipline_test.c
 * @author cy023
 * @date 2021.7.19
 * @brief 測試單一程式執行時間。
 *      請注意!!! 若遇到 overflow 的情況，請修改 timer 頻率，或是換成 16 bit timer。
 *
 */
#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardwareset/src/tim_set.h"
#include "c4mlib/ServiceProvi/hwimp/src/layout_macro.h"
#include "c4mlib/ServiceProvi/hwimp/src/tim_imp.h"
#include "c4mlib/ServiceProvi/pipeline/src/pipeline_executor.h"
#include "c4mlib/ServiceProvi/pipeline/src/pipeline_imp.h"

#include <stdio.h>
#include <string.h>

#include "c4mlib/ServiceProvi/pipeline/test/test_pipeline_tim.cfg"

#define TASK1                       1
#define PERIPHERAL_INTERRUPT_ENABLE 255

typedef struct {
    int p;
} FuncParaStr_t;

uint8_t Task1(void *void_p);

uint8_t TIM2_init(void) {
    HardWareSet_t TIM2HWSet_str      = TIM2HWSETSTRINI;
    HWFlagPara_t TIM2FgGpData_str[5] = TIM2FLAGPARALISTINI;
    HWRegPara_t TIM2RegData_str      = TIM2REGPARALISTINI;
    TIM2HWSetDataStr_t TIM2HWSetData = TIM2SETDATALISTINI;
    HARDWARESET_LAY(TIM2HWSet_str, TIM2FgGpData_str[0], TIM2RegData_str,
                    TIM2HWSetData)
    return HardwareSet_step(&TIM2HWSet_str);
}

int main() {
    C4M_DEVICE_set();
    printf("========== start ==========\n");
    // hardware initial section start
    TIM2_init();
    TIMOpStr_t TIMEROPSTR = TIM2OPSTRINI;
    PIPELINE_LAY(3, 3, 10, TIMEROPSTR);
    // hardware initial section end

    // pipeline regist section start
    FuncParaStr_t Task1_para = {.p = 0};
    uint8_t task1_name[] = "Task1\0";
    Pipeline_reg(&SysPipeline_str, &Task1, &Task1_para, task1_name);
    // pipeline regist section end

    printf("time spend : %d\n", Pipeline_test(&SysPipeline_str, TASK1));
    sei();
    while (1) ;

    return 0;
}

uint8_t Task1(void *void_p) {
    for (int i = 0; i < 100; i++) {
        __asm__("nop");
    }
    return 0;
}
