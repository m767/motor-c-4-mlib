/**
 * @file pipeline_executor.c
 * @author cy023, ya058764
 * @date 2021.7.19
 * @brief
 */

#include "c4mlib/ServiceProvi/pipeline/src/pipeline_executor.h"
#include "c4mlib/C4MBios/debug/src/debug.h"
#include "c4mlib/ServiceProvi/pipeline/src/pipeline_imp.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdint.h>

#define DISABLE                 0
#define ERRORCODE               255
#define PIPELINE_TASK_RUN_OK    0

uint8_t Pipeline_reg(PipelineStr_t* Str_p, TaskFunc_t FbFunc_p, void* FbPara_p,
                     uint8_t* TaskName_p) {
    uint8_t TaskId                     = Str_p->Total + 1; //avoid number 0
    Str_p->Task_p[TaskId].state    = 0;
    Str_p->Task_p[TaskId].FBFunc_p = FbFunc_p;
    Str_p->Task_p[TaskId].FBPara_p = FbPara_p;

    // TODO: TickStart(), TickStop()
    DEBUG_INFO("TaskId:%d, Name:%s", TaskId, TaskName_p);

    if (Str_p->Total == Str_p->MaxTask) {
        DEBUG_INFO("Pipeline_reg Over Booking Error\n");
        return ERRORCODE;
    }
    else {
        DEBUG_INFO("TaskId:%d, Name:%s\n", TaskId, TaskName_p);
        Str_p->Total++;
        return TaskId;
    }
}

uint8_t Pipeline_step(void* void_p) {
    volatile PipelineStr_t* Str_p = (PipelineStr_t*)void_p;
    uint8_t ErrorCode = 0;
    uint8_t TaskId;

    if (Str_p->Busy) {
        return 0;
    } else {
        Str_p->Busy = 1;
    }
    if (FifoBF_get((FifoBFStr_t*)(Str_p->FifoBF_p), (void*)&TaskId, 1) >=
        0) {
        switch (Str_p->Task_p[TaskId].state) {
            case 0: {
                DEBUG_INFO("Pipeline_step() Error Code : NonTrig\n");
                break;
            }
            case 1: {
                if (Str_p->TaskId){
                    ScopeBF_put((ScopeBFStr_t*)(Str_p->ScopeBF_p),
                                (void*)&TaskId, 1);
                    DEBUG_INFO("Pipeline_step() Error Code : TriggerOneTime\n");
                    Str_p->Task_p[TaskId].state = 0;
                    ErrorCode = Str_p->Task_p[TaskId].FBFunc_p(
                        Str_p->Task_p[TaskId].FBPara_p);
                }
                break;
            }
            default: {
                DEBUG_INFO("Pipeline_step() Error Code : OverTrig\n");
                break;
            }
        }
        if (ErrorCode != PIPELINE_TASK_RUN_OK) {
            ScopeBF_tri(Str_p->ScopeBF_p);
            ScopeBF_full(Str_p->ScopeBF_p);
            /* Disable SysTick */
            REGFPT(Str_p->timer.IntEnReg_p, Str_p->timer.IntEnMask, Str_p->timer.IntEnShift, DISABLE);
        }
    }
    Str_p->Busy = 0;
    return 0;
}

uint16_t Pipeline_test(PipelineStr_t* Str_p, uint8_t TaskId) {
    uint16_t SubTick0 = 0;
    uint16_t SubTick1 = 0;
    uint16_t SubTickD = 0;
    uint16_t Period   = 0;
    REGGET(Str_p->timer.PeriodReg_p, Str_p->timer.PeriodBytes, &Period);
    TRIG_NEXT_TASK(TaskId);
    REGGET(Str_p->timer.CountReg_p, Str_p->timer.CountBytes, &SubTick0);
    Pipeline_step((void *)Str_p);
    REGGET(Str_p->timer.CountReg_p, Str_p->timer.CountBytes, &SubTick1);
    if (SubTick1 > SubTick0) {
        SubTickD = SubTick1 - SubTick0;
        printf("[DEBUG]: SubTickD = %d\n", SubTickD);
    } else {
        SubTickD = Period + SubTick1 - SubTick0;
        printf("[DEBUG]: SubTickD = %d (overflow)\n", SubTickD);
    }
    return SubTickD;
}
