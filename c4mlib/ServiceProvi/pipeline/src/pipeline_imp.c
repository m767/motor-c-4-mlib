/**
 * @file pipeline_imp.c
 * @author ya058764 (ya058764@gmail.com)
 * @date 2020.10.06
 * @brief 宣告pipeline結構實體
 * 
 */

#include "c4mlib/ServiceProvi/pipeline/src/pipeline_executor.h"

PipelineStr_t SysPipeline_str = {.MaxTask = 0, .Total = 0, .TaskId = 0};
