/**
 * @file asaspi_remo_master.c
 * @author smallplayplay
 * @brief
 * @date 2020.08.25
 *
 */
#include "c4mlib/ServiceProvi/remoptxrx/src/asaspi_remo_master.h"

#include "c4mlib/ServiceProvi/databuffer/src/remobf.h"
#include "c4mlib/C4MBios/debug/src/debug.h"
#include "c4mlib/ServiceProvi/hwimp/src/hardware_interrupt.h"
#include "c4mlib/C4MBios/macro/src/bits_op.h"

#include <avr/io.h>
#include <stdio.h>

#define SPI_REMO_OK 0
#define SPI_REMO_NG 1

#define SPI_REMO_NONZERO    255
#define SPI_REMO_ALLREGADDR 255

#define SPI_REMO_HEADER   0
#define SPI_REMO_BCDATA   1
#define SPI_REMO_BCCHKSUM 2
#define SPI_REMO_ULDATA   3
#define SPI_REMO_ULOK     4
#define SPI_REMO_DLDATA   5
#define SPI_REMO_DLOK     6

#define ASASPI_CMD_HEADER 0xAA
#define ASASPI_RSP_HEADER 0xAB

#define ENABLE  1
#define DISABLE 0

uint8_t MSpiRemoPTXRX_step(void* void_p) {
    static uint8_t InData   = 0, OutData;
    static uint8_t rec_trig = 0;
    uint8_t keeploop        = 0;
    MSpiRemoPStr_t* Str_p   = (MSpiRemoPStr_t*)void_p;
    DEBUG_INFO("state = %d\n", Str_p->State);
    do {
        keeploop = 0;
        switch (Str_p->State) {
            case SPI_REMO_HEADER: {
                REGFPT(&PORTB, 0x01, 0, 0);
                HWInt_en(Str_p->SpiHWIntStr_ssp, Str_p->SpiTaskId, ENABLE);
                Str_p->CardId = 0; /*CardId is the BroadCast*/
                REGFPT(Str_p->ASAReg_p, Str_p->Mask, Str_p->Shift,
                       Str_p->CardId); /*Swith ASA Address*/
                OutData = ASASPI_CMD_HEADER;
                REGPUT(Str_p->SpiReg_p, 1, &OutData);
                Str_p->ChkSum = OutData;
                if (Str_p->BCRemoBF_p->DataTotal == 0) {
                    Str_p->State = SPI_REMO_BCCHKSUM;
                }
                else {
                    Str_p->State = SPI_REMO_BCDATA;
                }
                break;
            }
            case SPI_REMO_BCDATA: {
                if (RemoBF_get(Str_p->BCRemoBF_p, SPI_REMO_ALLREGADDR,
                               &OutData) == 0) {
                    Str_p->State = SPI_REMO_BCCHKSUM;
                }
                REGPUT(Str_p->SpiReg_p, 1, &OutData);
                Str_p->ChkSum += OutData;
                break;
            }
            case SPI_REMO_BCCHKSUM: {
                REGPUT(Str_p->SpiReg_p, 1, &Str_p->ChkSum);
                Str_p->CardId = 0;

                if (Str_p->ULRemoBF_p[Str_p->CardId]->DataTotal == 0) {
                    Str_p->State = SPI_REMO_ULOK;
                }
                else {
                    Str_p->State    = SPI_REMO_ULDATA;
                    Str_p->Residual = SPI_REMO_NONZERO;
                    Str_p->FeedBack = 0;
                }
                REGFPT(Str_p->ASAReg_p, Str_p->Mask, Str_p->Shift,
                       Str_p->CardId); /*Swith ASA Address*/
                break;
            }
            case SPI_REMO_ULDATA: {
                if (rec_trig < 2) {
                    REGPUT(Str_p->SpiReg_p, 1, &Str_p->FeedBack);
                    rec_trig++;
                    break;
                }
                REGGET(Str_p->SpiReg_p, 1, &InData);
                Str_p->FeedBack = InData; /*Next FeedBack are Indata */
                Str_p->Residual = RemoBF_temp(*(Str_p->ULRemoBF_p),
                                              SPI_REMO_ALLREGADDR, InData);
                REGPUT(Str_p->SpiReg_p, 1, &Str_p->FeedBack);

                if (Str_p->Residual == 0) {
                    Str_p->State    = SPI_REMO_ULOK;
                    Str_p->FeedBack = 0;
                    rec_trig        = 0;
                }
                break;
            }
            case SPI_REMO_ULOK: {
                if (rec_trig == 0) {
                    REGPUT(Str_p->SpiReg_p, 1, &Str_p->FeedBack);
                    rec_trig = 1;
                    break;
                }
                REGGET(Str_p->SpiReg_p, 1, &(Str_p->OKNG));
                if (Str_p->OKNG == SPI_REMO_OK) {
                    RemoBF_put(Str_p->ULRemoBF_p[Str_p->CardId],
                               SPI_REMO_ALLREGADDR);
                }
                else {
                    RemoBF_clr(Str_p->ULRemoBF_p[Str_p->CardId]);
                }
                if (Str_p->DLRemoBF_p[Str_p->CardId]->DataTotal == 0) {
                    Str_p->State = SPI_REMO_DLOK;
                }
                else {
                    Str_p->State    = SPI_REMO_DLDATA;
                    Str_p->Residual = SPI_REMO_NONZERO;
                    Str_p->OKNG     = SPI_REMO_OK;
                    Str_p->OldData  = 0;
                    rec_trig        = 0;
                }
                keeploop = 1;
                break;
            }
            case SPI_REMO_DLDATA: {
                if (rec_trig != 0) {
                    REGGET(Str_p->SpiReg_p, 1, &(Str_p->FeedBack));
                    if (Str_p->FeedBack != Str_p->OldData) {
                        Str_p->OKNG++;
                    }
                    Str_p->OldData = OutData;
                }
                Str_p->Residual = RemoBF_get(Str_p->DLRemoBF_p[Str_p->CardId],
                                             SPI_REMO_ALLREGADDR, &OutData);
                REGPUT(Str_p->SpiReg_p, 1, &OutData);
                rec_trig = 1;

                if (Str_p->Residual == 0) {
                    Str_p->State = SPI_REMO_DLOK;
                }
                break;
            }
            case SPI_REMO_DLOK: {
                REGPUT(Str_p->SpiReg_p, 1, &Str_p->OKNG);
                Str_p->CardId++; /*CardId=CardId+1;*/
                REGFPT(Str_p->ASAReg_p, Str_p->Mask, Str_p->Shift,
                       Str_p->CardId); /*Swith ASA Address*/
                if (Str_p->CardId == Str_p->TotalCard) {
                    Str_p->State = SPI_REMO_HEADER;
                    DEBUG_INFO("done!\n");
                    HWInt_en(Str_p->SpiHWIntStr_ssp, Str_p->SpiTaskId, DISABLE);
                    while (!(SPSR & (1 << SPIF))) {
                    }
                    REGFPT(&PORTB, 0x01, 0, 1);
                }
                else {
                    Str_p->CardId++; /*Next CardId */
                    if (Str_p->ULRemoBF_p[Str_p->CardId]->DataTotal == 0) {
                        Str_p->State = SPI_REMO_ULOK;
                    }
                    else {
                        Str_p->State    = SPI_REMO_ULDATA;
                        Str_p->Residual = SPI_REMO_NONZERO;
                        Str_p->FeedBack = 0;
                    }
                    keeploop = 1;
                }
                break;
            }
        }
    } while (keeploop);
    return 0;
}
