/**
 * @file asaspi_remo_master.h
 * @author smallplayplay sourslemon
 * @brief 
 * @date 2020.08.25
 *
 */

#ifndef C4MLIB_ASASPI_MASTER_SPI_H
#define C4MLIB_ASASPI_MASTER_SPI_H

#include "c4mlib/ServiceProvi/databuffer/src/remobf.h"
#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/ServiceProvi/hwimp/src/hardware_interrupt.h"
#include "c4mlib/ServiceProvi/interrupt/src/frequency_set.h"
#include "c4mlib/C4MBios/macro/src/bits_op.h"

#include <stdint.h>

/**
 * @defgroup asaspi_func
 * @defgroup asaspi_macro
 * @defgroup asaspi_struct
 */
/* Public Section Start */
/**
 * @brief MSpiRemoP結構原型
 * @ingroup asaspi_struct
 */
typedef struct {
    uint8_t OldData;              ///< 前次傳送資料
    uint8_t FeedBack;             ///< 通訊回傳值
    uint8_t OKNG;                 ///<
    uint8_t ChkSum;               ///< 資料通訊檢查
    uint8_t Residual;             ///< 資料通訊剩餘筆數
    uint8_t SpiTaskId;            ///<  Spi Service Interrupt ID
    uint8_t State;                ///< spi封包通訊狀態機
    void *SpiReg_p;               ///< Spi HardWare Register Address
    HWIntStr_t *SpiHWIntStr_ssp;  ///<  Spi Service Structure pointer
    uint8_t SpiHWIntTaskId;       ///< Spi task Id issute by Spi service
    RemoBFStr_t *BCRemoBF_p;      ///< BroadCast Remo Buffer
    RemoBFStr_t **ULRemoBF_p;     ///< Up link Remo Buffer Pointer
    RemoBFStr_t **DLRemoBF_p;     ///< DownLink Remo Buffer Pointer
    void *ASAReg_p;               ///< Register of ASA Address Flags
    uint8_t Mask;                 ///< Mask of ASA Address flags
    uint8_t Shift;                ///< Shift of ASA Address flag
    uint8_t CardId;               ///< ID of Card
    uint8_t TotalCard;            ///< Total Card Number
    uint8_t MaxCard;              ///< Maximum number of cards
} MSpiRemoPStr_t;

/**
 * @brief MSPIREMOP佈線巨集
 *
 * @param REMOSTR 建構的結構體名稱
 * @param SPIREGA SPI_Data_Reg指標
 * @param BCBFSTR 廣播Remo結構體名稱
 * @param BCDDEPTH 廣播Remo結構體最大資料長度
 * @param BCVSDEPTH 廣播Remo結構體最大變數數量
 * @param MAXCARD Card最大數量
 */
#define MSPIREMOP_LAY(REMOSTR, SPIREGA, BCBFSTR, BCDDEPTH, BCVSDEPTH, MAXCARD) \
    MSpiRemoPStr_t REMOSTR = {0};                                              \
    REMOBUFF_LAY(BCBFSTR, BCDDEPTH, BCVSDEPTH);                                \
    {                                                                          \
        static RemoBFStr_t *REMOSTR##_DLRemoBF_p[MAXCARD] = {0};               \
        static RemoBFStr_t *REMOSTR##_ULRemoBF_p[MAXCARD] = {0};               \
        REMOSTR.SpiReg_p                                  = (char *)SPIREGA;   \
        REMOSTR.ASAReg_p                                  = (char *)&PORTF;    \
        REMOSTR.Mask                                      = 0xE0;              \
        REMOSTR.Shift                                     = 5;                 \
        REMOSTR.BCRemoBF_p                                = &BCBFSTR;          \
        REMOSTR.DLRemoBF_p = REMOSTR##_DLRemoBF_p;                             \
        REMOSTR.ULRemoBF_p = REMOSTR##_ULRemoBF_p;                             \
        REMOSTR.MaxCard    = MAXCARD;                                          \
    }

/**
 * @brief MSPIREMOP佈線巨集
 *
 * @param REMOSTR MSPIREMOPC結構體名稱
 * @param DLBFSTR 下行鏈結Remo結構體名稱
 * @param DLDDEPTH 下行鏈結Remo結構體最大資料長度
 * @param DLVSDEPTH 下行鏈結Remo結構體最大變數數量
 * @param ULBFSTR 上行鏈結Remo結構體名稱
 * @param ULDDEPTH 上行鏈結Remo結構體最大資料長度
 * @param ULVSDEPTH 上行鏈結Remo結構體最大變數數量
 */
#define MSPIREMOP_REG(REMOSTR, DLBFSTR, DLDDEPTH, DLVSDEPTH, ULBFSTR,          \
                      ULDDEPTH, ULVSDEPTH)                                     \
    REMOBUFF_LAY(DLBFSTR, DLDDEPTH, DLVSDEPTH);                                \
    REMOBUFF_LAY(ULBFSTR, ULDDEPTH, ULVSDEPTH);                                \
    (REMOSTR.DLRemoBF_p)[REMOSTR.TotalCard] = &DLBFSTR;                        \
    (REMOSTR.ULRemoBF_p)[REMOSTR.TotalCard] = &ULBFSTR;                        \
    REMOSTR.TotalCard++;

/**
 * @brief
 *
 * @param void_p MSpiRemoP結構指標
 */
uint8_t MSpiRemoPTXRX_step(void *void_p);

/* Public Section End */

#endif  // C4MLIB_ASASPI_MASTER_SPI_H
