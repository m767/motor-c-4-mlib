/**
 * @file flash.h
 * @author LCY (nacmvmclab@gmail.com)
 * @date 2020.07.29
 * @brief flash 記憶體相關實作定義
 *
 */

#ifndef C4MLIB_FLASH_H
#define C4MLIB_FLASH_H

#include "w25q128jv.h"
#include "c4mlib/ServiceProvi/asaspi/src/asaspi_master.h"

/**
 * @defgroup flash_func asaspi functions
 */

/* Public Section Start */

// Flash Default Setting
#define SPI_MODE_FLASH          6
#define FlASH_NAME_LENGTH       8

// Flash Info return value
#define FLASH_FORMATED_CODE     0
#define FLASH_BLANK_CODE        1
#define FLASH_SPI_FAIL_CODE     2
#define FLASH_DATA_ERROR_CODE   3   

#define FLASH_FORMAT_SUCCESS    0

#define SECTOR_END     0xFFFFFFFF


// Flash open return value
#define FLASH_SUCCESS_OPEN_FILE     0
#define FLASH_FILENUM_EXCEED        1
#define FLASH_NEW_FILE_SUCCESS      2
#define FLASH_INVALID_FILEID        3

#define FLASH_SUCCESS_OPEN_FILE_BYNAME     10
#define FLASH_FILENUM_EXCEED_BYNAME        11
#define FLASH_NEW_FILE_SUCCESS_BYNAME      12

#define FLASH_ILLEGAL_FILE_NAME     98

// Flash close return value
#define FLASH_FILE_NO_CHANGE        0
#define FLASH_FILE_CLOSE_SUCCESS    1

// Flash delete return value
#define FLASH_DELETE_SUCCESS                0
#define FLASH_DELETE_NOT_FOUND              1
#define FLASH_DELETE_SUCCESS_BY_NAME        10
#define FLASH_DELETE_NOT_FOUND_BY_NAME      11

// Flash read return value
#define FLASH_OVER_READ                 0
#define FLASH_READ_S_SECROR_FINISH      1
#define FLASH_READ_L_SECROR_FINISH      2

// Flash write return value
#define FLASH_WRITE_NO_CROSS_SUCCESS    0
#define FLASH_WRITE_CROSS_SUCCESS       1
#define FLASH_FILE_NOT_OPEN             2

// Unexpect
#define FLASH_UNEXPECT_EVENT     99

/**
 * @brief 記憶體管理結構初始化巨集。
 * 
 */
#define DiscStr_Init(Disc_str, CardID)                                           \
    Disc_str->BackUpSector  = 1;                                                 \
    Disc_str->DataSector    = 2;                                                 \
    Disc_str->DataSectorMax = 4000;                                              \
    Disc_str->FSLLHead      = SECTOR(DATA_START, 0);                             \
    Disc_str->FSLLTail      = SECTOR(DATA_END, 15);                              \
    Disc_str->CardId        = CardID;                                            \
    Disc_str->FileMax       = 250;                                               \
    Disc_str->FileTotal     = 0;                                                 \
    Disc_str->Writen        = 0;

/**
 * @brief 檔案管理結構初始化巨集。
 * 
 */
#define FileStr_Init(File_str)                                                 \
    File_str->Month             = 0;                                           \
    File_str->Date              = 0;                                           \
    File_str->Hour              = 0;                                           \
    File_str->Min               = 0;                                           \
    File_str->Year              = 0;                                           \
    File_str->FLLHead           = 0;                                           \
    File_str->FLLTail           = 0;                                           \
    File_str->FullSectors       = 0;                                           \
    File_str->RNextLinkSector   = 0;                                           \
    File_str->RAddr             = 0;                                           \
    File_str->RShiftSector      = 0;                                           \
    File_str->WNextLinkSector   = 0;                                           \
    File_str->WAddr             = 0;                                           \
    File_str->WShiftSector      = 0;                                           \
    File_str->FileSize          = 0;                                           \
    File_str->FileSize          = 0;                                           \

/**
 * @brief 記憶體管理 參數結構定義。
 * 
 * @param BackUpSector flash 內暫存記憶體位置
 * @param DataSector 資料儲存起始 flash 記憶體位置
 * @param DataSectorMax 最大支援資料儲存用 Sector數量
 * @param FSLLHead 閒置空間起始 Sector
 * @param FSLLTail 閒置空間結尾 Sector
 * @param CardId 使用介面卡ID (ASAID/CS)
 * @param FileMax 最大可容納檔案數量
 * @param FileTotal 目前檔案總數
 * @param Writen 記憶體變動旗標
 */
typedef struct {
    uint32_t BackUpSector;      
    uint32_t DataSector;        
    uint16_t DataSectorMax;     
    uint32_t FSLLHead;          
    uint32_t FSLLTail;          
    uint8_t CardId;             
    uint8_t FileMax;            
    uint8_t FileTotal;          
    uint8_t Writen;             
} DiscStr_t;

/**
 * @brief 檔案管理 參數結構定義。
 * 
 * @param FileName[8] 檔案檔名字串
 * @param Month 檔案最後修改日期(月)
 * @param Date 檔案最後修改日期(日)
 * @param Hour 檔案最後修改日期(時)
 * @param Min 檔案最後修改日期(分)
 * @param Year 檔案最後修改日期(年)
 * @param FLLHead 檔案空間起始 Sector
 * @param FLLTail 檔案空間結尾 Sector
 * @param FullSectors 檔案占用 Sector 數量
 * @param RNextLinkSector 讀檔時，該檔案連結之下一 Sector
 * @param RAddr 讀檔時，紀錄記憶體位置
 * @param RShiftSector 讀檔時，紀錄記憶體位置平移
 * @param WNextLinkSector 寫檔時，該檔案連結之下一 Sector
 * @param WAddr 寫檔時，紀錄記憶體位置   
 * @param WShiftSector 寫檔時，紀錄記憶體位置平移
 * @param FileSize 檔案大小
 * 
 * 實際記憶體操作位置 = (shift * SECTOR_SIZE) + Addr
 */
typedef struct {
    char FileName[8];  
    uint8_t Month;
    uint8_t Date;
    uint8_t Hour;
    uint8_t Min;
    uint16_t Year;
    uint32_t FLLHead;     
    uint32_t FLLTail;     
    uint16_t FullSectors; 
    uint16_t RNextLinkSector;
    uint16_t RAddr;       
    uint16_t RShiftSector;
    uint16_t WNextLinkSector;
    uint16_t WAddr;       
    uint16_t WShiftSector;
    uint32_t FileSize;
} FileStr_t;

/**
 * @brief Sector 管理參數結構定義。
 * 
 * @param Sector 資料空間之各 Sector 編號
 * @param NextLink 閒置空間/檔案空間 連結之下一 Sector 的 Flash 記憶體位置
 * 提供指向各Sector的資料，以及指向下一個Link_str的資料。
 * uint16_t Sector: The Sector relate to this link
 * uint32_t NextLink: FileName String
 */
typedef struct {
    uint16_t Sector;
    uint32_t NextLink;  
} LinkStr_t;

/**
 * @brief FlashFormat 記憶體格式化函式:
 *      (1) 清空整顆記憶體
 *      (2) 初始化一份 Disc 結構，並寫入記憶體 DISC_INFO 位置
 *      (3) 所有 DATA Sector 頭4bytes(Link 大小)，寫入Link 初始資料
 * 
 * @param DStr_p 記憶體管理結構記憶體位置。
 * @param CardID 操作介面卡編號(ASAID / CS)。
 * @return 函式執行回傳值，參考 flash.h 回傳定義。
 */
uint8_t FlashFormat(DiscStr_t* DStr_p, uint8_t CardID);

/**
 * @brief FlashInfo 記憶體探詢函式:
 *      (1) 讀取 DISC_INFO 位置
 *      (2) 讀取所有 Data sector Link 標頭位置(Sector 編號) 是否為正常順序
 *      (3) 依照(1)、(2)結果決定目前記憶體狀態
 * 
 * @param DStr_p 記憶體管理結構記憶體位置。
 * @param CardID 操作介面卡編號(ASAID / CS)。
 * @return 函式執行回傳值，參考 flash.h 回傳定義。
 */
uint8_t FlashInfo(DiscStr_t* DStr_p, uint8_t CardID);

/**
 * @brief FileOpen 開檔/建檔函式:
 *      (1) 依照檔名開啟檔案
 *          -- 成功: 讀取檔案訊息，存入 Fstr 中
 *          -- 失敗: 建立新檔案
 *      (2) 依照檔案ID開啟檔案
 *          -- 成功: 讀取檔案訊息，存入 Fstr 中
 *          -- 失敗: 依照給定ID判斷，符合條件則建立新檔案
 * ID機制更新為對號入座，純粹用以推算位置，實際並無此變數
 * 
 * @param DStr_p 記憶體管理結構記憶體位置。
 * @param FStr_p 檔案管理結構記憶體位置。
 * @param FileId 開檔/建檔檔案ID。
 * @param Name_p 開檔/建檔檔案名稱。
 * @param FileBytes_p 開檔/建檔檔案名稱回傳。
 * @return 函式執行回傳值，參考 flash.h 回傳定義。
 */
uint8_t FileOpen(DiscStr_t* DStr_p, FileStr_t* FStr_p, uint8_t FileId,
                 char* Name_p, uint16_t* FileBytes_p);

/**
 * @brief FileClose 關檔函式:
 *      (1) 執行過 FileOpen 函式，必須再執行此函式
 *      (2) 判斷是否有寫檔
 *      (3) 有更動:
 *          -- 更新file str
 *          -- 更新disc str
 *          !!!: 新資料預先寫入 Backup_block, 再取代回原 sys block (BLOCK_0)
 * 
 * @param DStr_p 記憶體管理結構記憶體位置。
 * @param FStr_p 檔案管理結構記憶體位置。
 * @return 函式執行回傳值，參考 flash.h 回傳定義。
 */
uint8_t FileClose(DiscStr_t* DStr_p, FileStr_t* FStr_p);

/**
 * @brief FileDelete 刪除檔案函式:
 *      (1) 依照檔名搜尋刪除檔案
 *          -- 成功: FSLL 變更
 *                   對應sector清空 & 重上Link
 *          -- 失敗: 跳出，回傳錯誤碼
 *      (2) 依照檔案ID搜尋刪除檔案
 * 
 * @param DStr_p 記憶體管理結構記憶體位置。
 * @param FileID 刪檔檔案ID。
 * @param Name_p 刪檔檔案名稱。
 * @return 函式執行回傳值，參考 flash.h 回傳定義。
 */
uint8_t FileDelete(DiscStr_t* DStr_p, uint8_t FileID, char* Name_p);

/**
 * @brief FileWrite 檔案續寫函式:
 *      (1) 判斷寫入是否跨sector
 *          -- 是: Link更新 / 各 Tail及 Head更動 !!!: 搜尋空間規則!!
 *          -- 否: Link無更新 / 各 Tail及 Head無更動
 *      !!!: 單次寫入大小上限: 1 sector (4096 bytes) * 
 * @param DStr_p 記憶體管理結構記憶體位置。
 * @param FStr_p 檔案管理結構記憶體位置。
 * @param Bytes 寫入資料大小。
 * @param Data_p 寫入資料記憶體位置。
 * @return 函式執行回傳值，參考 flash.h 回傳定義。
 */
uint8_t FileWrite(DiscStr_t* DStr_p, FileStr_t* FStr_p, uint8_t Bytes, void* Data_p);

/**
 * @brief FileRead 檔案讀取函式:
 *      (1) 檢查讀取資料數是否超出資料大小
 *      (2) 檢查讀取資料數是否大於一sector
 *          -- 小於或等於: 正常讀取，存入 Data_p
 *          -- 大於: 讀取 NextLink，跳轉位置，重複
 * 
 * @param DStr_p 記憶體管理結構記憶體位置。
 * @param FStr_p 檔案管理結構記憶體位置。
 * @param Bytes 讀取資料大小。
 * @param Data_p 讀取資料記憶體位置。
 * @return 函式執行回傳值，參考 flash.h 回傳定義。
 */
uint8_t FileRead(DiscStr_t* DStr_p, FileStr_t* FStr_p, uint16_t Bytes, void* Data_p);

/* Public Section End */

/**
 * @brief Flash_wait_to_complete 工作等待函式，讀寫等工作執行完畢才會跳出此函式。
 * 
 */
void Flash_wait_to_complete(void);

/**
 * @brief Flash_sector_copy Sector完全複製，僅適用於整個 Sector。
 * 
 * @param from 來源 Sector 的 Flash 記憶體位置。
 * @param to 目的 Sector 的 Flash 記憶體位置。
 */
void Flash_sector_copy(uint32_t from, uint32_t to);

/**
 * @brief Flash_page_write 資料寫入，可寫入 1 ~ 256 Bytes。
 * 
 * @param Addr 寫入資料之 Flash 記憶體位置。
 * @param Bytes 寫入資料大小。
 * @param Data 寫入資料記憶體位置。
 */
void Flash_page_write(uint32_t Addr, uint16_t Bytes, void* Data);

/**
 * @brief Flash_read_data 資料讀取函式。
 * 
 * @param Addr 讀取資料之 Flash 記憶體位置。
 * @param Bytes 讀取資料大小。
 * @param Data 讀取資料記憶體位置。
 */
void Flash_read_data(uint32_t Addr, uint16_t Bytes, void* Data);

/**
 * @brief Flash_clear_sector Sector 資料清除函式。(all to 0xFF)
 * 
 * @param Sector_num 欲抹除 Sector 之 Flash 記憶體位置。
 */
void Flash_clear_sector(uint32_t Sector_num);

/**
 * @brief Flash_clear_block Block 資料清除函式。(all to 0xFF)
 * 
 * @param Block_addr 欲抹除 Block 之 Flash 記憶體位置。
 */
void Flash_clear_block(uint32_t Block_addr);

/**
 * @brief Flash_manufacture_code 製造編號讀取函式。
 * 
 * @param Recv_code 製造編號讀取目的記憶體位置。
 */
void Flash_manufacture_code(uint8_t* Recv_code);

/**
 * @brief FileStr_blank 檔案管理結構初始化函式。
 * 
 * @param FStr_p 檔案管理結構記憶體位置。
 */
void FileStr_blank(FileStr_t* FStr_p);

#endif  // C4MLIB_FLASH_H
