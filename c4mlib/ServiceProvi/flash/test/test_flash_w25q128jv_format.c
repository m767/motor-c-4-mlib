/**
 * @file test_flash_w25q128jv_erase.c
 * @author LCY (nacmvmclab@gmail.com)
 * @date 2020.10.07
 * @brief 使用SPI Master記憶傳輸和接收驅動函式測試w25q128jv flash IC 格式化功能。
 * 步驟:
 *      (1) 打開 write enable開關。
 *      (2) 發送全記憶體抹除指令。
 *      (3) 發送讀取資料指令，檢視抹除後結果。
 * 每一操作後皆以Flash_wait_to_complete函式等待操作完成。
 */

#include "c4mlib/C4MBios/asabus/src/asabus.h"
#include "c4mlib/C4MBios/asabus/src/pin_def.h"
#include "c4mlib/ServiceProvi/asaspi/src/asaspi_master.h"
#include "c4mlib/C4MBios/hardware/src/hal_spi.h"

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardwareset/src/m128/spi_set.h"
#include "c4mlib/ServiceProvi/flash/test/spi_test.cfg"

#include "c4mlib/ServiceProvi/flash/src/flash.h"
#include "c4mlib/ServiceProvi/flash/src/w25q128jv.h"

#define SPI_MODE        6
#define ASAID           8
#define SPI_DUMMY       0x77

uint32_t mem_addr = 0;
uint8_t manu_cont[3] = {0, 0, 0};
uint8_t test_rec[4] = {0, 0, 0 ,0};
uint8_t test_id[2] = {0, 0};
uint8_t addr[3] = {0, 0, 0};
uint8_t read_status[2] = {0, 0};

uint8_t SPI_init(void){
    HardWareSet_t SPIHWSet_str = SPIHWSETSTRINI;
    HWFlagPara_t SPIFgGpData_str[11] = SPIFLAGPARALISTINI;
    HWRegPara_t SPIRegData_str = SPIREGPARALISTINI;
    SPIHWSetDataStr_t SPIHWSetData = SPI_FG_DATA_INT;
    HARDWARESET_LAY(SPIHWSet_str, SPIFgGpData_str[0], SPIRegData_str, SPIHWSetData);
    return HardwareSet_step(&SPIHWSet_str);
}

int main() {
    // Setup
    C4M_STDIO_init();
    // FIXME: 待更新..
    DDRB |= 0x07;
    SPI_init();

    Extern_CS_disable(ASAID);

    uint64_t ADDR = 0x777879;

    printf("Start test flash Entire format...\n");

    while(true) {
        printf("=================\n");

        Extern_CS_enable(ASAID);
        ASABUS_SPI_swap(SPI_DUMMY); 
        Extern_CS_disable(ASAID);

        SPIM_Mem_rec(SPI_MODE, ASAID, READ_MANU, 3, &mem_addr, 2,
                     manu_cont);
        Flash_wait_to_complete();

        for (uint8_t i = 0; i < 2; i++) {
            printf("manu_cont=>%x\n", manu_cont[i]);
        }

        printf("--WRITE_ENABLE--\n");
        SPIM_Mem_trm(SPI_MODE, ASAID, WRITE_ENABLE, 0, &mem_addr, 0,
                     manu_cont);
        Flash_wait_to_complete();

        printf("--ENTIRE_ERASE--\n");
        SPIM_Mem_trm(SPI_MODE, ASAID, ENTIRE_ERASE, 0, 0, 0, 0);
        Flash_wait_to_complete();

        printf("--READ_4BYTES_DATA--\n");
        SPIM_Mem_rec(SPI_MODE, ASAID, READ_DATA, 3, &ADDR, 4, test_rec);
        printf("read: %d, %d, %d, %d\n", test_rec[0],test_rec[1],test_rec[2],test_rec[3]);
        Flash_wait_to_complete();

        break;
        _delay_ms(2000);
    }

}
