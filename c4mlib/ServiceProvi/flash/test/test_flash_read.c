/**
 * @file test_flash_read.c
 * @author ya058764 (ya058764@gmail.com)
 * @date 2021.02.27
 * @brief 測試檔案階段讀取功能。
 * 步驟:
 *      (1) FlashInfo讀取目前記憶體狀態 (DiscStr_t)。
 *      (2) 以檔名開啟/建立檔案。
 *      (3) 寫入資料至該檔案。
 *      (4) 分別以不同大小讀檔，檢視以不同大小讀取之結果。
 */
#include <string.h>
#include <stdlib.h>

#include "c4mlib/C4MBios/asabus/src/asabus.h"
#include "c4mlib/C4MBios/asabus/src/pin_def.h"
#include "c4mlib/ServiceProvi/asaspi/src/asaspi_master.h"
#include "c4mlib/C4MBios/hardware/src/hal_spi.h"

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardwareset/src/m128/spi_set.h"
#include "c4mlib/ServiceProvi/flash/test/spi_test.cfg"

#include "c4mlib/ServiceProvi/flash/src/flash.h"
#include "c4mlib/ServiceProvi/flash/src/w25q128jv.h"

#define SPI_MODE        5
#define ASAID           8
#define SPI_DUMMY       0x77

DiscStr_t test_disc;
FileStr_t test_file;
uint8_t CardID = 8;
uint8_t mem_addr[3] = {0, 0, 0};
uint8_t manu_cont[2] = {0, 0};
uint8_t fileID = 0;
char fileNAME[20];

uint8_t SPI_init(void){
    HardWareSet_t SPIHWSet_str = SPIHWSETSTRINI;
    HWFlagPara_t SPIFgGpData_str[11] = SPIFLAGPARALISTINI;
    HWRegPara_t SPIRegData_str = SPIREGPARALISTINI;
    SPIHWSetDataStr_t SPIHWSetData = SPI_FG_DATA_INT;
    HARDWARESET_LAY(SPIHWSet_str, SPIFgGpData_str[0], SPIRegData_str, SPIHWSetData);
    return HardwareSet_step(&SPIHWSet_str);
}

int main(void) {
    // Setup
    C4M_STDIO_init();
    DDRB |= 0x07;
    SPI_init();
    // FIXME: 待更新..
    
    Extern_CS_disable(ASAID);

    fileNAME[0] = 'T';
    fileNAME[1] = 'E';
    fileNAME[2] = 's';
    fileNAME[3] = 'T';



    printf("Start test flash Manufacturer ID reading...\n");
    printf("=================\n");
    Flash_manufacture_code(manu_cont);
    for (uint8_t i = 0; i < 2; i++) {
        printf("manu_cont=>%x\n", manu_cont[i]);
    }

    // int a = FlashFormat(&test_disc, CardID);
    // printf("---Format complete!!!---, return = %d\n", a);

    int a = FlashInfo(&test_disc, CardID);
    printf("---FlashInfo---, return = %d\n", a);

    a = FileOpen(&test_disc, &test_file, fileID, fileNAME);
    Flash_wait_to_complete();
    printf("---File open--, return = %d\n", a);
    
    printf("test_disc.CardId = %d\n", test_disc.CardId);
    printf("test_disc.FileMax = %d\n", test_disc.FileMax);
    printf("test_disc.FileTotal = %d\n", test_disc.FileTotal);
    printf("test_disc.FSLLHead = %lx\n", test_disc.FSLLHead);
    printf("test_disc.FSLLTail = %lx\n", test_disc.FSLLTail);
    printf("FileName: %s\n", test_file.FileName);
    printf("test_file.FLLHead =  %lx\n", test_file.FLLHead);
    printf("test_file.FLLTail =  %lx\n", test_file.FLLTail);
    printf("DStr_p -> Writen =  %d\n", test_disc.Writen);
        
    uint8_t *Data_p = (uint8_t *)malloc(sizeof(uint8_t) * 6);
    strcpy((char *)Data_p, "abcdef");
    a = FileWrite(&test_disc, &test_file, 6, Data_p);
    printf("---File write--, return = %d\n", a);
    free(Data_p);
    
    uint8_t *read = (uint8_t *)malloc(sizeof(uint8_t) * 100);
    a = FileRead(&test_disc, &test_file, 1, read);
    printf("---File read 1Byte complete---, return = %d\n---read : %s\n",a,read);
    strcpy((char *)read, "      ");

    a = FileRead(&test_disc, &test_file, 2, read);
    printf("---File read 2Byte complete---, return = %d\n---read : %s\n",a,read);
    strcpy((char *)read, "      ");

    a = FileRead(&test_disc, &test_file, 3, read);
    printf("---File read 3Byte complete---, return = %d\n---read : %s\n",a,read);
    strcpy((char *)read, "      ");

    a = FileRead(&test_disc, &test_file, 4, read);
    printf("---File read 4Byte complete---, return = %d\n---read : %s\n",a,read);
    free(read);

    a = FileClose(&test_disc, &test_file);
    Flash_wait_to_complete();
    printf("---File close---, return = %d\n",a);
    _delay_ms(2000);

    return 0;
}
