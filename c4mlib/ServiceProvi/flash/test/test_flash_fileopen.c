/**
 * @file test_flash_fileopen.c
 * @author LCY (nacmvmclab@gmail.com)
 * @date 2020.11.12
 * @brief 測試檔案開啟功能。
 * 步驟:
 *      (1) 確認連接(製造編號)以及格式化。
 *      (2) FlashInfo讀取目前記憶體狀態 (DiscStr_t)。
 *      (3) 以ID建立2個檔案。
 *      (4) 以檔名開啟建立的第一個檔案。
 * 過程中隨時以 Filetotal, FSShead, FSStail來確認空間變化
 */

#include "c4mlib/C4MBios/asabus/src/asabus.h"
#include "c4mlib/C4MBios/asabus/src/pin_def.h"
#include "c4mlib/ServiceProvi/asaspi/src/asaspi_master.h"
#include "c4mlib/C4MBios/hardware/src/hal_spi.h"

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardwareset/src/m128/spi_set.h"
#include "c4mlib/ServiceProvi/flash/test/spi_test.cfg"

#include "c4mlib/ServiceProvi/flash/src/flash.h"
#include "c4mlib/ServiceProvi/flash/src/w25q128jv.h"

#define SPI_MODE        5
#define ASAID           8
#define SPI_DUMMY       0x77

DiscStr_t test_disc;
FileStr_t test_file;
uint8_t CardID = 8;
uint8_t mem_addr[3] = {0, 0, 0};
uint8_t manu_cont[2] = {0, 0};
uint8_t fileID = 1;
char fileNAME[20];

uint8_t SPI_init(void){
    HardWareSet_t SPIHWSet_str = SPIHWSETSTRINI;
    HWFlagPara_t SPIFgGpData_str[11] = SPIFLAGPARALISTINI;
    HWRegPara_t SPIRegData_str = SPIREGPARALISTINI;
    SPIHWSetDataStr_t SPIHWSetData = SPI_FG_DATA_INT;
    HARDWARESET_LAY(SPIHWSet_str, SPIFgGpData_str[0], SPIRegData_str, SPIHWSetData);
    return HardwareSet_step(&SPIHWSet_str);
}

int main(void) {
    // Setup
    C4M_DEVICE_set();
    DDRB |= 0x07;
    SPI_init();
    // FIXME: 待更新..
    Extern_CS_disable(ASAID);

    fileNAME[0] = 'T';
    fileNAME[1] = 'E';
    fileNAME[2] = 'S';
    fileNAME[3] = 'T';



    printf("Start test flash Manufacturer ID reading...\n");
    printf("=================\n");
    Flash_manufacture_code(manu_cont);
    for (uint8_t i = 0; i < 2; i++) {
        printf("manu_cont=>%x\n", manu_cont[i]);
    }

    int a = FlashFormat(&test_disc, CardID);
    printf("---Format complete!!!---, return = %d\n", a);

    a = FlashInfo(&test_disc, CardID);
    printf("---FlashInfo---, return = %d\n", a);

    a = FileOpen(&test_disc, &test_file, fileID, fileNAME);
    printf("---File 1 open--, return = %d\n", a);
    a = FileClose(&test_disc, &test_file);
    printf("---File 1 close--, return = %d\n", a);
    
    printf("test_disc.CardId = %d\n", test_disc.CardId);
    printf("test_disc.FileMax = %d\n", test_disc.FileMax);
    printf("test_disc.FileTotal = %d\n", test_disc.FileTotal);
    printf("test_disc.FSLLHead = %lx\n", test_disc.FSLLHead);
    printf("test_disc.FSLLTail = %lx\n", test_disc.FSLLTail);
    printf("FileName--0: %c\n", test_file.FileName[0]);
    printf("FileName--1: %c\n", test_file.FileName[1]);
    printf("FileName--2: %c\n", test_file.FileName[2]);
    printf("FileName--3: %c\n", test_file.FileName[3]);
    printf("test_file.FLLHead =  %lx\n", test_file.FLLHead);
    printf("test_file.FLLTail =  %lx\n", test_file.FLLTail);
    printf("DStr_p -> Writen =  %d\n", test_disc.Writen);
    printf("-----END-----\n");

    fileNAME[0] = 'T';
    fileNAME[1] = 'S';
    fileNAME[2] = 'E';
    fileNAME[3] = 'T';

    fileID = 2;

    a = FileOpen(&test_disc, &test_file, fileID, fileNAME);
    printf("---File 2 open--, return = %d\n", a);
    a = FileClose(&test_disc, &test_file);
    printf("---File 2 close--, return = %d\n", a);
    
    printf("test_disc.CardId = %d\n", test_disc.CardId);
    printf("test_disc.FileMax = %d\n", test_disc.FileMax);
    printf("test_disc.FileTotal = %d\n", test_disc.FileTotal);
    printf("test_disc.FSLLHead = %lx\n", test_disc.FSLLHead);
    printf("test_disc.FSLLTail = %lx\n", test_disc.FSLLTail);
    printf("FileName--0: %c\n", test_file.FileName[0]);
    printf("FileName--1: %c\n", test_file.FileName[1]);
    printf("FileName--2: %c\n", test_file.FileName[2]);
    printf("FileName--3: %c\n", test_file.FileName[3]);
    printf("test_file.FLLHead =  %lx\n", test_file.FLLHead);
    printf("test_file.FLLTail =  %lx\n", test_file.FLLTail);
    printf("DStr_p -> Writen =  %d\n", test_disc.Writen);
    printf("-----END-----\n");

    fileNAME[0] = 'T';
    fileNAME[1] = 'E';
    fileNAME[2] = 'S';
    fileNAME[3] = 'T';

    fileID = 0;

    a = FileOpen(&test_disc, &test_file, fileID, fileNAME);
    printf("---File 1 open--, return = %d\n", a);
    a = FileClose(&test_disc, &test_file);
    printf("---File 1 close--, return = %d\n", a);
    
    printf("test_disc.CardId = %d\n", test_disc.CardId);
    printf("test_disc.FileMax = %d\n", test_disc.FileMax);
    printf("test_disc.FileTotal = %d\n", test_disc.FileTotal);
    printf("test_disc.FSLLHead = %lx\n", test_disc.FSLLHead);
    printf("test_disc.FSLLTail = %lx\n", test_disc.FSLLTail);
    printf("FileName--0: %c\n", test_file.FileName[0]);
    printf("FileName--1: %c\n", test_file.FileName[1]);
    printf("FileName--2: %c\n", test_file.FileName[2]);
    printf("FileName--3: %c\n", test_file.FileName[3]);
    printf("test_file.FLLHead =  %lx\n", test_file.FLLHead);
    printf("test_file.FLLTail =  %lx\n", test_file.FLLTail);
    printf("DStr_p -> Writen =  %d\n", test_disc.Writen);
    printf("-----END-----\n");

    return 0;
}
