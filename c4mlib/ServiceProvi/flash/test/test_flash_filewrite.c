/**
 * @file test_flash_filewrite.c
 * @author LCY (nacmvmclab@gmail.com)
 * @date 2021.03.04
 * @brief 測試檔案寫入功能。
 * 步驟:
 *      (1) FlashInfo讀取目前記憶體狀態 (DiscStr_t)。
 *      (2) 以ID開啟建立的第一個檔案。
 *      (3) 寫入資料至已開啟檔案。
 *      (4) 讀取檔案內容，檢視是否正常寫入。
 * 寫入前後以寫入狀態旗標Writen值，確認函式正常執行
 */

#include "c4mlib/C4MBios/asabus/src/asabus.h"
#include "c4mlib/C4MBios/asabus/src/pin_def.h"
#include "c4mlib/ServiceProvi/asaspi/src/asaspi_master.h"
#include "c4mlib/C4MBios/hardware/src/hal_spi.h"

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardwareset/src/m128/spi_set.h"
#include "c4mlib/ServiceProvi/flash/test/spi_test.cfg"

#include "c4mlib/ServiceProvi/flash/src/flash.h"
#include "c4mlib/ServiceProvi/flash/src/w25q128jv.h"

#define SPI_MODE        5
#define ASAID           8
#define SPI_DUMMY       0x77

DiscStr_t test_disc;
FileStr_t test_file;
uint8_t CardID = 8;
uint8_t mem_addr[3] = {0, 0, 0};
uint8_t manu_cont[2] = {0, 0};
uint8_t fileID = 1;
char fileNAME[20] = {'Z', 'Y', 'X', 'V'};
uint8_t writedata[10] = {0};
uint8_t readdata[3] = {0};

uint8_t SPI_init(void){
    HardWareSet_t SPIHWSet_str = SPIHWSETSTRINI;
    HWFlagPara_t SPIFgGpData_str[11] = SPIFLAGPARALISTINI;
    HWRegPara_t SPIRegData_str = SPIREGPARALISTINI;
    SPIHWSetDataStr_t SPIHWSetData = SPI_FG_DATA_INT;
    HARDWARESET_LAY(SPIHWSet_str, SPIFgGpData_str[0], SPIRegData_str, SPIHWSetData);
    return HardwareSet_step(&SPIHWSet_str);
}

int main(void) {
    // Setup
    C4M_DEVICE_set();
    DDRB |= 0x07;
    printf("SPI initial = %d\n",SPI_init());
    // FIXME: 待更新..
    Extern_CS_disable(ASAID);

    printf("Start test flash Manufacturer ID reading...\n");
    printf("=================\n");
    Flash_manufacture_code(manu_cont);
    for (uint8_t i = 0; i < 2; i++) {
        printf("manu_cont=>%x\n", manu_cont[i]);
    }

    // int a = FlashFormat(&test_disc, CardID);
    // printf("---FlashFormat---, return = %d\n", a);

    int a = FlashInfo(&test_disc, CardID);
    printf("---FlashInfo---, return = %d\n", a);

    a = FileOpen(&test_disc, &test_file, fileID, fileNAME);
    printf("---File open--, return = %d\n", a);

    for (uint16_t i = 0; i < sizeof(writedata); i++)
        writedata[i] = i + 10;
    

    a = FileWrite(&test_disc, &test_file, sizeof(writedata), writedata);
    printf("---File Write--, return = %d\n", a);
    a = FileRead(&test_disc, &test_file, sizeof(readdata), readdata);
    printf("---File Read--, return = %d\n", a);
    printf("Data content:\n");
    for (uint8_t i = 0; i < sizeof(readdata); i++)
        printf("***data byte.%d = %d\n", i, readdata[i]);
    
    printf("DStr_p -> Writen =  %d\n", test_disc.Writen);
    a = FileClose(&test_disc, &test_file);
    printf("---File close--, return = %d\n", a);
    
    printf("FileName--0: %c\n", test_file.FileName[0]);
    printf("FileName--1: %c\n", test_file.FileName[1]);
    printf("FileName--2: %c\n", test_file.FileName[2]);
    printf("test_file.FLLHead =  %lx\n", test_file.FLLHead);
    printf("test_file.FLLTail =  %lx\n", test_file.FLLTail);
    printf("test_file.WAddr =  %x\n", test_file.WAddr);
    printf("DStr_p -> Writen =  %d\n", test_disc.Writen);
    printf("-----END-----\n");

    return 0;
}
