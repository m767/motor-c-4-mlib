/**
 * @file test_frequency.c
 * @author smallplayplay
 * @date 2021.6.5
 * @brief
 * 將降頻處理器掛到HWint底下，以中斷執行USER_FUNCRION的內容
 * 步驟:
 *      TIMER2設定
 *      使用TIMHWINT_LAY進行TIMER的登記中斷的callback設定
 *      使用FREQREDU_LAY進行降頻的初始化設定
 *      使用FreqRedu_reg設定工作內容，並使用FreqRedu_en致能
 *      使用HWInt_en將中斷分享器致能、TIMER2中斷致能
 */
#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardwareset/src/hardware_set.h"
#include "c4mlib/C4MBios/hardwareset/src/tim_set.h"
#include "c4mlib/C4MBios/macro/src/std_def.h"
#include "c4mlib/ServiceProvi/hwimp/src/hardware_interrupt.h"
#include "c4mlib/ServiceProvi/hwimp/src/layout_macro.h"
#include "c4mlib/ServiceProvi/interrupt/src/frequency_set.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <string.h>

#include "c4mlib/ServiceProvi/interrupt/test/test_interrupt_tim.cfg"

#define CYCLE                       8
#define PERIPHERAL_INTERRUPT_ENABLE 255
#define PORT_B_7                    128
#define FREQREDU_TASKNUM            4
#define FREQREDU_ADJUSTP            1

uint8_t USER_FUNCTION(void* pvData_p);

#define MY_TIM2SETDATALISTINI                                                  \
    {                                                                          \
        .WGMn0_1 = 1, .CSn0_2 = TIM123_CLK_DIV_BY1, .COMn0_1 = DISABLE,        \
        .DDx = OUTPUT, .TIM2EN = ENABLE, .FlagTotalBytes = 5, .OCRn = 255,      \
        .RegTotalBytes = 1                                                     \
    }

uint8_t TIM2_init(void) {
    HardWareSet_t TIM2HWSet_str      = TIM2HWSETSTRINI;
    HWFlagPara_t TIM2FgGpData_str[5] = TIM2FLAGPARALISTINI;
    HWRegPara_t TIM2RegData_str      = TIM2REGPARALISTINI;
    TIM2HWSetDataStr_t TIM2HWSetData = MY_TIM2SETDATALISTINI;
    HARDWARESET_LAY(TIM2HWSet_str, TIM2FgGpData_str[0], TIM2RegData_str,
                    TIM2HWSetData)
    return HardwareSet_step(&TIM2HWSet_str);
}
FreqReduStr_t freqredustr = {0};
int main() {
    C4M_DEVICE_set();
    DDRB |= PORT_B_7;
    TIM2_init();
    printf("tccr:%x\n", TCCR2);

    TIMOpStr_t HWOPSTR = TIM2OPSTRINI;
    TIMHWINT_LAY(freqredu4isr, 2, 1, HWOPSTR);

    uint8_t period[CYCLE];

    FREQREDU_LAY(freqredustr, FREQREDU_TASKNUM, CYCLE, 0, period, HWOPSTR);
    uint8_t param_1 = 1, id0 = 0;

    id0 = FreqRedu_reg(&freqredustr, &USER_FUNCTION, &param_1, 1, 1);

    FreqRedu_en(&freqredustr, id0, ENABLE);

    printf("id0 = %d\n", FreqRedu_chk(&freqredustr, id0));

    // uint8_t TaskID = HWInt_reg(&freqredu4isr, &FreqRedu_step, &freqredustr);
    // HWInt_en(&freqredu4isr, PERIPHERAL_INTERRUPT_ENABLE,
    //          ENABLE);  //中斷分享器禁致能
    // HWInt_en(&freqredu4isr, TaskID, ENABLE);
    sei();
    while (1) {
        ;
    }
}

uint8_t USER_FUNCTION(void* pvData_p) {
    PORTB ^= PORT_B_7;
    return 0;
}

volatile clear = 1;
ISR(TIMER2_COMP_vect) {
    sei();
    if (clear) {
        clear = 0;
        FreqRedu_step(&freqredustr);
        clear = 1;
    }
    cli();
}
