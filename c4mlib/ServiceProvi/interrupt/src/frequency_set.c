/**
 * @file frequency_set.c
 * @author smallplayplay
 * @date 2021.6.5
 * @brief 降頻處理器
 */

#include "c4mlib/ServiceProvi/interrupt/src/frequency_set.h"

#include "c4mlib/C4MBios/debug/src/debug.h"
#include "c4mlib/C4MBios/macro/src/bits_op.h"
#include "c4mlib/C4MBios/macro/src/std_type.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdint.h>
#include <stdio.h>

#define FREQREDU_SET_OK                 0
#define FREQREDU_DISABLE                0
#define FREQREDU_SET_Error              255
#define FREQREDU_SET_NULL_POINTER_ERROR 1
#define FREQREDU_SET_OVER_TASKID_ERROR  2

uint8_t FreqRedu_reg(FreqReduStr_t* Str_p, TaskFunc_t FbFunc_p, void* FbPara_p,
                     uint8_t divi, uint8_t phase) {
    uint8_t TaskIndex = Str_p->Total;
    Str_p->Counter    = 0;
    uint8_t TaskId    = TaskIndex + 1;
    if (Str_p->Total < Str_p->MaxTask) {
        Str_p->Divi_p[TaskIndex].Divi      = divi;
        Str_p->Divi_p[TaskIndex].DiviCount = 0;
        Str_p->Divi_p[TaskIndex].phase     = phase;
        Str_p->Task_p[TaskIndex].state     = 0;
        Str_p->Task_p[TaskIndex].FBFunc_p  = FbFunc_p;
        Str_p->Task_p[TaskIndex].FBPara_p  = FbPara_p;
        Str_p->Total++;
        return TaskId;
    }
    return FREQREDU_SET_Error;
}

uint8_t FreqRedu_en(FreqReduStr_t* FRSTR_p, uint8_t Task_Id, uint8_t Enable) {
    uint8_t TaskIndex = Task_Id - 1;
    if (FRSTR_p == NULL) {
        return (FREQREDU_SET_NULL_POINTER_ERROR);
    }
    if (FRSTR_p->Total <= TaskIndex) {
        return (FREQREDU_SET_OVER_TASKID_ERROR);
    }
    FRSTR_p->Task_p[TaskIndex].state = Enable;
    return FREQREDU_SET_OK;
}

uint8_t FreqRedu_step(void* void_p) {
    FreqReduStr_t* Str_p = (FreqReduStr_t*)void_p;
    cli();
    char count_flag = 1;
    if (Str_p->AdjustP == 1) {
        REGPUT(Str_p->PeriodReg_p, Str_p->PeriodBytes,
               ((uint8_t*)(Str_p->Period_p) + Str_p->Counter));
    }
    for (uint8_t i = 0; i < Str_p->Total; i++) {
        if ((Str_p->Counter == Str_p->Divi_p[i].phase) &&
            (Str_p->Divi_p[i].DiviCount == 0) &&
            (Str_p->Task_p[i].state == 1)) {
            if (Str_p->Task_p[i].FBFunc_p(Str_p->Task_p[i].FBPara_p)) {
                REGFPT(Str_p->IntEnReg_p, Str_p->IntEnMask, Str_p->IntEnShift,
                       FREQREDU_DISABLE);
            }
        }
        if (Str_p->Counter == (Str_p->Cycle - 1)) {
            Str_p->Divi_p[i].DiviCount++;
            if (Str_p->Divi_p[i].DiviCount == Str_p->Divi_p[i].Divi) {
                Str_p->Divi_p[i].DiviCount = 0;
            }
        }
    }
    if (Str_p->Counter == (Str_p->Cycle - 1)) {
        Str_p->Counter = 0;
        count_flag     = 0;
    }
    if (count_flag) {
        Str_p->Counter++;
    }
    sei();
    return FREQREDU_SET_OK;
}

uint8_t FreqRedu_chk(FreqReduStr_t* Str_p, uint8_t TaskId) {
    uint8_t TaskIndex = TaskId - 1;
    if (Str_p != NULL) {
        return (Str_p->Task_p[TaskIndex].state);
    }
    else {
        DEBUG_INFO("Warning !!! FreqRedu_chk() TaskId does not exist\n");
    }
    return FREQREDU_SET_OK;
}
