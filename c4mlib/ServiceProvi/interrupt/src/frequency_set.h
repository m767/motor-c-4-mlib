/**
 * @file frequency_set.h
 * @author smallplayplay
 * @date 2021.6.5
 * @brief 降頻處理器
 *
 */
#ifndef FREQ_SET_H
#define FREQ_SET_H
#include "c4mlib/C4MBios/macro/src/std_type.h"
#include "c4mlib/ServiceProvi/hwimp/src/hardware_interrupt.h"

#include <stdint.h>

#ifndef C4MLIB_INTERRUPT_FREQUENCY_H
#define C4MLIB_INTERRUPT_FREQUENCY_H

/* Public Section Start */
/**
 * @brief type Array to content the pointer the phase and divi info of registed
 * Task
 *
 * @param Divi  Frequecy Divider of the
 * @param DiviCount Execute when DiviCount = Divi[i]
 * @param phase Execute when counter value = phase[i]
 */
typedef struct {
    volatile uint8_t Divi;
    volatile uint8_t DiviCount;
    volatile uint8_t phase;
} PhaseDivi_t;

/**
 * @brief 中斷除頻管理用結構體的住址指標
 *
 * @param Cycle Counts per period
 * @param Counter   count value
 * @param TaskId    Task Identification gotten
 * @param MaxTask   Maximum capacity for tasks to be Registed
 * @param Total Total number of Registed Tasks
 * @param AdjustP Adjust period or non
 * @param Divi_p    pointer of PhaseDivi List
 * @param Task_p    pointer of Task List
 * @param Period_p pointer of Period List
 * @param HWRegByte Hard ware Register for adjust period
 * @param PeriodReg_p Register address for adjust period
 * @param PeriodBytes Bytes of the Register
 * @param IntEnReg_p Interrupt Enalbe Register
 * @param IntEnMask Interrupt Enalbe Mask
 * @param IntEnShift Interrupt Enalbe Shift
 */
typedef struct {
    volatile uint8_t Cycle;
    volatile uint8_t Counter;
    volatile uint8_t TaskId;
    uint8_t MaxTask;
    uint8_t Total;
    uint8_t AdjustP;
    volatile PhaseDivi_t* Divi_p;
    volatile TaskBlock_t* Task_p;
    void* Period_p;
    volatile uint8_t* PeriodReg_p;
    uint8_t PeriodBytes;
    volatile uint8_t* CountReg_p;
    uint8_t CountBytes;
    volatile uint8_t* IntEnReg_p;
    uint8_t IntEnMask, IntEnShift;
} FreqReduStr_t;

/**
 * @brief 降頻工作執行器登錄函式，供使用者叫用以將功能方塊排入中斷除頻的排程表中
 *
 * @param FRSTR_ssp 中斷除頻管理用結構體的住址指標
 * @param FbFunc_p 中斷除頻後觸發執行之工作函式之住址指標(使用者資料)
 * @param FbPara_p 中斷除頻後觸發執行之工作函式專用結構體之住址指標(使用者傳參)
 * @param cycle
 * 每個循環週期計數次數，必需與其他登錄同一中斷除頻器的其他工作的循環週期相吻合
 * @param divi
 * @param phase 每個循環中，執行工作時機的中斷計數值
 */
uint8_t FreqRedu_reg(FreqReduStr_t* FRSTR_ssp, TaskFunc_t FbFunc_p,
                     void* FbPara_p, uint8_t divi, uint8_t phase);

/**
 * @brief
 * 降頻工作執行器禁致能函式，供使用者呼叫禁致能己登錄進中斷除頻中待執行工作方塊，僅有致能者，會在中斷除頻後被執行
 *
 * @param FRSTR_p 中斷除頻管理用結構體的住址指標
 * @param Task_Id 中斷除頻工作編號
 * @param Enable 禁致能狀態
 */
uint8_t FreqRedu_en(FreqReduStr_t* FRSTR_p, uint8_t Task_Id, uint8_t Enable);

/**
 * @brief
 * 降頻工作執行器執行函式，可與結構體共組成功能方塊之可執行函式，組成後可登錄進中斷服務函式待執行
 *
 * @param void_p 中斷除頻管理用結構體的住址指標
 */
uint8_t FreqRedu_step(void* void_p);

/**
 * @brief
 * 供使用者呼叫檢查禁致能狀態
 *
 * @param Str_p 降頻工作執行器用結構體指標。
 * @param TaskId 登錄進降頻工作執行器的工作識別編號。
 */
uint8_t FreqRedu_chk(FreqReduStr_t* Str_p, uint8_t TaskId);

/**
 * @brief
 * 本巨集協助使用者定義降頻工作執行器結構體，緩衝列，然後鏈結兩者完成結構體的組建
 * 單一工作的頻率必須小於 15,463 Hz
 *
 * @param FRSTR 降頻執行器結構體名稱FreqReduStr_t type structure for this
 * Frequence Reduce executor
 * @param TASKNUM 降頻執行器容納登錄工作數 Maximum Number of Tasks alow to be
 * registed into this Frequence Reduce executor
 * @param CYCLE 降頻週期
 * @param ADJUSTP 可調中斷週期與否，不可調ADJUSTP=0,可調ADJUSTP=1
 * @param PERIODLISTADD 存放可調週期的陣列The address of the list of adjustable
 * @param TIMEROPSTR 存放硬體計時器操作參數結構體，提供硬體操作初始值設定參考。
 */
#define FREQREDU_LAY(FRSTR, TASKNUM, CYCLE, ADJUSTP, PERIODMATADD, TIMEROPSTR) \
        {                                                                      \
            static PhaseDivi_t FRSTR##_DIVILIST[TASKNUM];                      \
            static TaskBlock_t FRSTR##_TASKLIST[TASKNUM];                      \
            FRSTR.MaxTask     = TASKNUM;                                       \
            FRSTR.Cycle       = CYCLE;                                         \
            FRSTR.Task_p      = FRSTR##_TASKLIST;                              \
            FRSTR.Divi_p      = FRSTR##_DIVILIST;                              \
            FRSTR.AdjustP     = ADJUSTP;                                       \
            FRSTR.Period_p    = PERIODMATADD;                                  \
            FRSTR.PeriodReg_p = TIMEROPSTR.PeriodReg_p;                        \
            FRSTR.PeriodBytes = TIMEROPSTR.PeriodBytes;                        \
            FRSTR.CountReg_p  = TIMEROPSTR.CountReg_p;                         \
            FRSTR.CountBytes  = TIMEROPSTR.CountBytes;                         \
            FRSTR.IntEnMask   = TIMEROPSTR.IntEnMask;                          \
            FRSTR.IntEnShift  = TIMEROPSTR.IntEnShift;                         \
            FRSTR.IntEnReg_p  = TIMEROPSTR.IntEnReg_p;                         \
        }
/* Public Section End */
#endif
#endif  // C4MLIB_INTERRUPT_FREQUENCY_H
