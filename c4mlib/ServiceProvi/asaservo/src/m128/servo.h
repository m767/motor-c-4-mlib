/**
 * @file servo.h
 * @author 葉子哇 (w8indow61231111@gmail.com)
 * @date 2020.11.19
 * @brief
 *
 */

#ifndef C4MLIB_ASASERVO_M128_SERVO_H
#define C4MLIB_ASASERVO_M128_SERVO_H
#include "c4mlib/C4MBios/IO_Func/src/tim.h"
#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardwareset/src/tim_set.h"
#include "c4mlib/ServiceProvi/hwimp/src/hardware_interrupt.h"
#include "c4mlib/ServiceProvi/hwimp/src/layout_macro.h"

#include <stdint.h>
#include <string.h>
/* Public Section Start */

#define PWM_DIV                 75  //波寬分成N等分
#define PERIPHERAL_INTERRUPT_ENABLE 255

/**
 * @brief PWM前處理結構體
 * @param PWMTableB[PWM_DIV] 8通道PWM輸出旗標表，
 *                     每一矩陣元素對應不同時間點之Hi/Low，
 *                     因有同時八通道輸出所以uint8_t
 *
 * @param BaseSpeed[8] 基礎速度
 * @param SpeedAddCount[8] 額外增加速度時間計數
 * @param SpeedAddLimit 最高調整速度(rpm)
 */
typedef struct {
    uint8_t PWMTableB[PWM_DIV];
    int8_t BaseSpeed[8];
    uint16_t SpeedAddCount[8];
    uint8_t PWM_BASICHI;  //紀錄速度為零的腳位為0；否則為1
} PWMStr_t;

/**
 * @brief 自走車PWM波寬準備工作方塊布局巨集
 *
 */
#define ASASERVO_PWMPREPRO_LAY()                                               \
    TIM_fpt(&TCCR2, 0x48, 3, 1); /*CTC*/                                       \
    TIM_fpt(&TCCR2, 0x07, 0, 3); /*除頻64*/                                  \
    TIM_fpt(&TCCR2, 0x30, 4, 1); /*toggle*/                                    \
    TIM_fpt(&TIMSK, 0X80, 7, 1);                                               \
    TIM_fpt(&OCR2, 0xff, 0, 147);                                              \
    PWMStr_t PWM_str;                                                          \
    for (int channel = 0; channel < 8; channel++) {                            \
        AsaServo_PwmPrePro_cmd(&PWM_str, channel, (int8_t)0);                  \
        PWM_str.SpeedAddCount[channel] = 0;                                    \
    }                                                                          \
    DDRB               = 0xff;                                                 \
    PORTB              = 0xff;                                                 \
    TIMOpStr_t HWOPSTR = TIM2OPSTRINI;                                         \
    TIMHWINT_LAY(TIM2HWSet, 2, 1, HWOPSTR);                                    \
    do {                                                                       \
        uint8_t TR = 0;                                                        \
        TR         = HWInt_reg(&TIM2HWSet, &PwmStateMachine, &PWM_str);        \
        HWInt_en(&TIM2HWSet, PERIPHERAL_INTERRUPT_ENABLE, ENABLE);                 \
        HWInt_en(&TIM2HWSet, TR, 1);                                           \
    } while (0);

/**
 * @brief 伺服機速度數值輸出介面函式
 *
 * @param str_p PWM前處理結構體指標
 * @param channel 設定速度之腳位0(PB0)~7(PB7)
 * @param speed 設定之速度(單位：rpm)
 * @return uint8_t 錯誤代碼：
 *   - 0：成功無誤。
 *   - 1：參數 speedchannel 錯誤。
 *   - 2：參數 channel 錯誤。
 */
uint8_t AsaServo_PwmPrePro_cmd(PWMStr_t *str_p, uint8_t channel, int8_t speed);

/**
 * @brief Speed(rpm)對PWM波寬數值轉換函式
 *
 * @param rpm 速度(單位：rpm)
 * @return uint8_t 錯誤代碼：
 *   - 其他：成功無誤。
 *   - 100：參數 rpm 錯誤。
 */
uint8_t SpeedToPwm(int8_t rpm);

/**
 * @brief 輸出旗標表更新函式
 *
 * @param str_p PWM前處理結構體指標
 * @param port 設定速度之PORT 0:portB
 * @param Channel 設定速度之腳位0(PB0)~7(PB7)
 * @param PulseWidth 指定通道的新近波寬值，0~N(0>全為0 N>全為1)
 * @return uint8_t 錯誤代碼：
 *   - 0：成功無誤。
 *   - 1：參數 port 設定錯誤。
 *   - 2：參數 Channel 設定錯誤。
 *   - 3：參數 PulseWidth 設定錯誤。
 */
uint8_t WidthAdjust(PWMStr_t *str_p, uint8_t port, uint8_t Channel,
                    uint8_t PulseWidth);

/**
 * @brief PWM狀態機函式
 *
 * @param p PWM前處理結構體指標
 */
uint8_t PwmStateMachine(void *p);

/**
 * @brief 8通道PWM輸出旗標表Print函式(除錯用)
 *
 * @param str_p PWM前處理結構體指標
 */
void PwmTable_Print(PWMStr_t *str_p);

/**
 * @brief 伺服機速度數值暫變介面函式
 *
 * @param str_p PWM前處理結構體指標
 * @param channel 設定速度之腳位0(PB0)~7(PB7)
 * @param AddSpeed 暫時改變的速度
 * @return uint8_t 錯誤代碼：
 *   - 0：成功無誤。
 *   - 1：參數 AdjSpeed 設定後超過變數範圍。
 */
uint8_t AsaServo_PwmPrePro_SpeedAdj(PWMStr_t *str_p, uint8_t channel,
                                    int8_t AdjSpeed);

/* Public Section End */

#endif  // C4MLIB_ASASERVO_M128_SERVO_H
