/**
 * @file servo.c
 * @author 葉子哇 (w8indow61231111@gmail.com)
 * @date 2020.11.19
 * @brief
 *
 */

#include "c4mlib/ServiceProvi/asaservo/src/servo.h"
#include "c4mlib/C4MBios/debug/src/debug.h"

#define SPEEDADDLIMIT     20  //設定最高調整速度 For AsaServo_PwmPrePro_SpeedAdj
#define SPEEDADDCOUNT_COE 50  // 50:表SpeedAdd持續1秒(因PWM輸出50HZ)
#define MAXINDEX          PWM_DIV - 1
#define BASICHI           1
#define HI2LOW            2
#define LOW2END           3
#define BIT(m)            (0x01 << (m))
#define BIT_CLEAR(p, m)   ((p) &= ~(BIT(m)))
#define BIT_SET(p, m)     ((p) |= (BIT(m)))
#define BIT_PUT(c, p, m)                                                       \
    (c ? BIT_SET(p, m) : BIT_CLEAR(p, m))  //將c蓋寫變數p的第m位元。
#define BIT_GET(p, m) (((p) & (BIT(m))) >> m)  //取得變數p的第m位元的值。

uint8_t AsaServo_PwmPrePro_cmd(PWMStr_t *str_p, uint8_t channel, int8_t speed) {
    uint8_t PulseWidth = SpeedToPwm(speed);
    if (PulseWidth == 100) {
        return 1;  // Error: speed outpace
    }
    if (channel < 8) {
        if (speed == 0) {  //
            PulseWidth = 0;
            BIT_PUT(0, str_p->PWM_BASICHI, channel);
        }
        else {
            BIT_PUT(1, str_p->PWM_BASICHI, channel);
        }
        str_p->BaseSpeed[channel] = speed;
        WidthAdjust(str_p, 0, channel, PulseWidth);
        return 0;  // Set successfully
    }
    return 2;  // Error:Channel outpace
}

uint8_t SpeedToPwm(int8_t rpm) {
    uint8_t pwm = 0;
    if (rpm <= 58 && rpm >= 48) {
        pwm = 17.745 * rpm - 801 + 0.5;
        if (pwm >= 69) {
            pwm = 75;
        }
        return (pwm);
    }
    if (rpm <= 47 && rpm >= 0) {
        pwm = 0.26 * rpm + 38 + 0.5;
        return (pwm);
    }
    if (rpm <= -1 && rpm >= -47) {
        pwm = 0.26 * rpm + 38 + 0.5;
        return (pwm);
    }
    if (rpm <= -48 && rpm >= -58) {
        int ans = 17.745 * rpm + 845;
        if (ans < 0) {
            rpm = 0;
        }
        else {
            rpm = ans;
        }
        return (pwm);
    }
    return 100;
    // Error: rpm outpace
}

uint8_t WidthAdjust(
    PWMStr_t *str_p, uint8_t port, uint8_t Channel,
    uint8_t PulseWidth)  // Channel 0~7 //PulseWidth 0~N(0>全為0 N>全為1)
{
    // PortB
    if (Channel > 7) {
        return 1;  // Error: channel outpace
    }
    if (PulseWidth > PWM_DIV) {
        return 2;  // Error: PulseWidth outpace
    }
    if (port != 0) {
        return 3;  // Error: port outpace
    }
    for (int i = 0; i < PulseWidth; i++) {
        BIT_PUT(1, str_p->PWMTableB[i], Channel);
    }
    for (int j = PulseWidth; j < PWM_DIV; j++) {
        BIT_PUT(0, str_p->PWMTableB[j], Channel);
    }
    return 0;
}

uint8_t PwmStateMachine(void *p) {
    PWMStr_t *str_p      = (PWMStr_t *)p;
    static uint8_t Index = 0, state = 1;
    switch (state) {
        case BASICHI:
            TCCR2 = (TCCR2 & ~(0x07)) | 0x03;
            OCR2  = 146;
            PORTB = str_p->PWM_BASICHI;
            state = 2;
            sei();
            break;

        case HI2LOW:
            OCR2  = 2;  // 1->PWM_DIV=112.32(112)
            PORTB = str_p->PWMTableB[Index];
            if (Index == MAXINDEX) {
                Index = 0;
                state = 3;
            }
            Index++;
            sei();
            break;

        case LOW2END:
            TCCR2 = (TCCR2 & ~(0x07)) | 0x05;
            OCR2  = 192;
            PORTB = 0x00;
            state = 1;
            sei();
            for (uint8_t channel = 0; channel < 8; channel++) {
                if (str_p->SpeedAddCount[channel] > 1) {
                    (str_p->SpeedAddCount[channel])--;
                }
                else if (str_p->SpeedAddCount[channel] == 1) {
                    AsaServo_PwmPrePro_cmd(str_p, channel,
                                           str_p->BaseSpeed[channel]);

                    str_p->SpeedAddCount[channel] = 0;  //將SpeedAdd方法禁能
                }
            }
            break;
    }
    return 0;
}

uint8_t AsaServo_PwmPrePro_SpeedAdj(PWMStr_t *str_p, uint8_t channel,
                                    int8_t AdjSpeed) {
    //調整速度
    static int8_t AddSpeed[8];
    if (str_p->SpeedAddCount[channel] == 0) {
        AddSpeed[channel] = 0;
    }
    //計算調整總量
    int32_t TotalPlus = (int32_t)AdjSpeed * (int32_t)SPEEDADDCOUNT_COE +
                        (AddSpeed[channel] * str_p->SpeedAddCount[channel]);
    if (TotalPlus >= 0) {
        DEBUG_INFO("TotalPlus:%ld>=0\n", TotalPlus);
        if ((str_p->BaseSpeed[channel] + SPEEDADDLIMIT) >= 58) {
            AddSpeed[channel] = 58 - str_p->BaseSpeed[channel];
        }
        else {
            AddSpeed[channel] = SPEEDADDLIMIT;
        }

        if (TotalPlus == 0) {
            AddSpeed[channel]             = 0;
            str_p->SpeedAddCount[channel] = 0;
        }
        else if (TotalPlus <= (SPEEDADDCOUNT_COE * AddSpeed[channel]) &&
                 TotalPlus >= (-SPEEDADDCOUNT_COE * AddSpeed[channel])) {
            AddSpeed[channel]             = TotalPlus / SPEEDADDCOUNT_COE;
            str_p->SpeedAddCount[channel] = SPEEDADDCOUNT_COE;
        }
        else {
            int8_t abs_AddSpeed =
                ((AddSpeed[channel] >> 7) ? (~AddSpeed[channel] + 1)
                                          : AddSpeed[channel]);
            TotalPlus = TotalPlus / abs_AddSpeed;
            //若plus轉成uint16_t會溢位
            if (TotalPlus > 65535 || TotalPlus < 0) {
                return 1;  //參數 AdjSpeed 設定後超過變數範圍
            }
            str_p->SpeedAddCount[channel] = (uint16_t)TotalPlus;
        }
    }
    else {
        DEBUG_INFO("TotalPlus:%ld<0\n", TotalPlus);
        if (str_p->BaseSpeed[channel] + SPEEDADDLIMIT <= -58) {
            AddSpeed[channel] = 58 - str_p->BaseSpeed[channel];
        }
        else {
            AddSpeed[channel] = -SPEEDADDLIMIT;
        }

        if (TotalPlus >= (SPEEDADDCOUNT_COE * AddSpeed[channel]) &&
            TotalPlus <= (-SPEEDADDCOUNT_COE * (int32_t)AddSpeed[channel])) {
            AddSpeed[channel]             = TotalPlus / SPEEDADDCOUNT_COE;
            str_p->SpeedAddCount[channel] = SPEEDADDCOUNT_COE;
        }
        else {
            int8_t abs_AddSpeed =
                ((AddSpeed[channel] >> 7) ? (~AddSpeed[channel] + 1)
                                          : AddSpeed[channel]);
            TotalPlus = (~TotalPlus + 1) / abs_AddSpeed;
            //若plus轉成uint16_t會溢位
            if (TotalPlus > 65535 || TotalPlus < 0) {
                return 1;  //參數 AdjSpeed 設定後超過變數範圍
            }
            str_p->SpeedAddCount[channel] = (uint16_t)TotalPlus;
        }
    }
    AsaServo_PwmPrePro_cmd(str_p, channel,
                           str_p->BaseSpeed[channel] + AddSpeed[channel]);
    str_p->BaseSpeed[channel] = str_p->BaseSpeed[channel] - AddSpeed[channel];
    DEBUG_INFO("%d %d(AddSpeed-SpeedAddCount)\n", AddSpeed[channel],
               str_p->SpeedAddCount[channel]);
    return 0;
}

void PwmTable_Print(PWMStr_t *str_p) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < PWM_DIV; j++) {
            printf("%d ", BIT_GET(str_p->PWMTableB[j], i));
        }
        printf("\n");
    }
}
