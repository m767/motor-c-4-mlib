/**
 * @file test_servo.c
 * @author 葉子哇 (w8indow61231111@gmail.com)
 * @date 2020.11.19
 * @brief
 * AsaServo_PwmPrePro_SpeedAdj 測試程式
 */

#include "c4mlib/ServiceProvi/asaservo/src/servo.h"

int main() {
    C4M_DEVICE_set();
    printf("Start\n");
    ASASERVO_PWMPREPRO_LAY();
    PwmTable_Print(&PWM_str);
    _delay_ms(100);
    sei();
    AsaServo_PwmPrePro_cmd(&PWM_str, 1, 20);
    AsaServo_PwmPrePro_cmd(&PWM_str, 2, 20);
    int speed;
    while (1) {
        printf("Enter SpeedAdj\n");
        scanf("%d", &speed);
        printf("%d\n", speed);
        AsaServo_PwmPrePro_SpeedAdj(&PWM_str, 1, (int8_t)speed);
        AsaServo_PwmPrePro_SpeedAdj(&PWM_str, 2, (int8_t)speed);
        _delay_ms(100);
    }
}
