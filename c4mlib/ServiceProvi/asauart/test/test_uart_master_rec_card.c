/**
 * @file test_uart_master_rec_card.c
 * @author stu1000264
 * @date 2021.05.25
 * @brief 測試 ASA單板電腦 作為Master，透過Uart通訊，傳送接收封包。
 *
 * 需與 test_uart_slave_rec_card.c 一起做測試，
 * 硬體配置:
 * >>   將Master與Slave的TXRX對接，並且雙方共地
 * 程式執行步驟:
 * >>   宣告UARTMStr_t及UARTOpStr_t結構體
 * >>   設定UART相關硬體暫存器
 * >>   使用UARTM0STR_LAY將UARTOpStr_t結構體登錄進UARTMStr_t
 * 執行成功結果:
 *  Master trm 1 bytes data to Slave
 * >>  [Test 1] UARTM_rec : data[0]=6
 * >>   Master receive 3 bytes data to Slave
 * >>  [Test 2] UARTM_rec : data[0]=5
 * >>  [Test 2] UARTM_rec : data[1]=6
 * >>  [Test 2] UARTM_rec : data[2]=7
 * >>  [Test 2] UARTM_rec : data[3]=0
 * >>  [Test 2] UARTM_rec : data[4]=0
 */

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardware/src/isr.h"
#include "c4mlib/ServiceProvi/asauart/src/asauart_master_card.h"
#include "c4mlib/ServiceProvi/time/src/hal_time.h"

#include <util/delay.h>

int main() {
    C4M_DEVICE_set();
    UARTOpStr_t UARTOpStr_p = UART1REGPARA;
    UARTMStr_t uart_str_p   = UARTM_INI;
    UARTM_LAY(uart_str_p, UARTOpStr_p);

    uint16_t baud = F_CPU / 16 / DEFAULTUARTBAUD - 1;
    UBRR1H        = (unsigned char)(baud >> 8);
    UBRR1L        = (unsigned char)baud;

    UCSR1B |= (1 << RXEN1) | (1 << TXEN1);
    UCSR1C |= (3 << UCSZ10);
    // Enable RX & TX interrupt
    UCSR1B |= (1 << RXCIE1) | (1 << TXCIE1);

    sei();
    uint8_t data_buffer0     = 0;
    uint8_t data_buffer1[3]  = {0};
    uint8_t data_buffer2[10] = {5, 6, 7, 5, 6, 7, 5, 6, 7, 8};
    uint8_t result           = 0;

    /***** UARTM Mode0 Transmit test *****/
    uart_str_p.CardId=0x80;
    printf("CardId=%d\n\n",uart_str_p.CardId);
    UARTM0_trm(&uart_str_p, 5, 10, data_buffer2);
    printf(" Master trm 1 bytes data to Slave \n");

    result = UARTM0_rec(&uart_str_p, 1, 1, &data_buffer0);
    result = UARTM0_rec(&uart_str_p, 1, 1, &data_buffer0);
    if (result) {
        printf("[Test 1]UARTM_rec Fail [%d]\n", result);
    }
    printf("[Test 1] UARTM_rec : data=%u\n", data_buffer0);

    printf(" Master receive 3 bytes data to Slave \n");
    result = UARTM0_rec(&uart_str_p, 3, 3, data_buffer1);

    if (result) {
        printf("[Test 2]UARTM_rec Fail [%d]\n", result);
    }
    for (int i = 0; i < 3; i++) {
        printf("[Test 2] UARTM_rec : data[%u]=%u\n", i, data_buffer1[i]);
    }

    while (1) {
    }
}
