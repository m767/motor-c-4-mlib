/**
 * @file test_remo_slave.c
 * @author sjchuang, kung
 * @date 2021.06.28
 * @brief 測試 ASA單板電腦 作為Slave，透過Uart通訊，定週期傳送接收封包。
 *
 * 測試注意事項：
 * 需與 test_remo_master.c 一起做測試
 * 需使用三張ASA單板電腦，並依編號設定
 * UIDD=1(Slave1) UIDD=2(Slave2) UIDD=3(Slave3)
 * 需先reset Slave1 Slave2 Slave3再reset Master
 *
 * 硬體配置:
 * >>將Master與Slave1 Slave2 Slave3的TXRX對接，並且雙方共地
 *
 * 程式執行步驟:
 * >>UART1      init+LAY
 * >>PWM中斷    init+LAY
 * >>降頻器     LAY+reg/en
 * >>PIPELINE   LAY+reg/en
 * >>REMOBUFF   LAY+reg
 * >>UARTSRemoP LAY
 * >>將測試函式掛進降頻器
 * >>sei
 *
 * 執行成功結果:
 * ==========SLAVE1==========
 * Slave Card
 * UART1_init report 0
 * PWM2_init report 0
 * SysTick(Timer)>>SysClock(FreqRedu) HWInt_reg re: 0
 * SysTick(Timer)>>SysClock(FreqRedu) HWInt_en  re: 0
 * SysTick(Timer)>>SysPipeline HWInt_reg re: 1
 * SysTick(Timer)>>SysPipeline HWInt_en  re: 0
 * BCRemoBF VariTotal:0
 * ULRemoBF VariTotal:0
 * DLRemoBF VariTotal:0
 * test FreqRedu_reg re: 0
 * UID=1
 * ==========SLAVE2==========
 * Slave Card
 * UART1_init report 0
 * PWM2_init report 0
 * SysTick(Timer)>>SysClock(FreqRedu) HWInt_reg re: 0
 * SysTick(Timer)>>SysClock(FreqRedu) HWInt_en  re: 0
 * SysTick(Timer)>>SysPipeline HWInt_reg re: 1
 * SysTick(Timer)>>SysPipeline HWInt_en  re: 0
 * BCRemoBF VariTotal:0
 * ULRemoBF VariTotal:0
 * DLRemoBF VariTotal:0
 * test FreqRedu_reg re: 0
 * UID=2
 * ==========SLAVE3==========
 * Slave Card
 * UART1_init report 0
 * PWM2_init report 0
 * SysTick(Timer)>>SysClock(FreqRedu) HWInt_reg re: 0
 * SysTick(Timer)>>SysClock(FreqRedu) HWInt_en  re: 0
 * SysTick(Timer)>>SysPipeline HWInt_reg re: 1
 * SysTick(Timer)>>SysPipeline HWInt_en  re: 0
 * BCRemoBF VariTotal:0
 * ULRemoBF VariTotal:0
 * DLRemoBF VariTotal:0
 * test FreqRedu_reg re: 0
 * UID=3
 **/

#include "c4mlib/ServiceProvi/asauart/test/test_remo_slave.cfg"

#include "c4mlib/C4MBios/hardwareset/src/m128/pwm_set.h"
#include "c4mlib/C4MBios/hardwareset/src/tim_set.h"
#include "c4mlib/ServiceProvi/asauart/src/asauart_remo_slave.h"
#include "c4mlib/ServiceProvi/hwimp/src/m128/layout_macro.h"

#define PERIPHERAL_INTERRUPT_ENABLE 255

#define SYS_STR_UART_ID 1UL  //"1"為SLAVE的裝置編號(UID)，最小為1
#define CYCLE           72   // FreqRedu的週期(數CYCLE次一輪)
#define TIMMAX          10   // Time Out倒數值(數到0 Time Out)
uint32_t BC_data_buffer = 0x00000000;  //當作可遙控的暫存器(MASTER->SLAVE)
uint32_t DL_data_buffer = 0x00000000;  //當作可遙控的暫存器(MASTER->SLAVE)
uint32_t UL_data_buffer = 0x00000000;  //當作可遙控的暫存器(SLAVE ->MASTER)

//測試函式
uint8_t test(void* a) {
    UL_data_buffer = BC_data_buffer + DL_data_buffer;
    return 0;
}

uint8_t UART1_init(void) {
    HardWareSet_t UART1HWSet_str       = UART1HWSETSTRINI;
    HWFlagPara_t UART1FgGpData_str[10] = UART1FLAGPARALISTINI;
    HWRegPara_t UART1RegData_str[2]    = UART1REGPARALISTINI;
    UART1HWSetDataStr_t UART1HWSetData = RM_UART1SETDATALISTINI;
    HARDWARESET_LAY(UART1HWSet_str, UART1FgGpData_str[0], UART1RegData_str[0],
                    UART1HWSetData);
    return HardwareSet_step(&UART1HWSet_str);
}

uint8_t PWM2_init(void) {
    HardWareSet_t PWM2_20K_str          = PWM2HWSETSTRINI;
    HWFlagPara_t PWM2FlagParaList[5]    = PWM2FLAGPARALISTINI;
    HWRegPara_t PWM2RegParaList         = PWM2REGPARALISTINI;
    PWM2HWSetDataStr_t PWM2DataList_str = RM_PWM2SETDATALISTINI;
    HARDWARESET_LAY(PWM2_20K_str, PWM2FlagParaList[0], PWM2RegParaList,
                    PWM2DataList_str);
    return HardwareSet_step(&PWM2_20K_str);
}

int main() {
    C4M_DEVICE_set();
    printf("Slave Card\n");
    uint8_t re = 77;  //宣告變數存回傳值

    //============UART1 init+LAY============
    re = UART1_init();
    printf("UART1_init report %d \n", re);      // re=0 設定正確
    UARTOpStr_t UARTOp_str = RM_UART1OPSTRINI;  //宣告結構體供模組使用
    //建立UartRemoTXInts_str與計時中斷UART鏈結
    UARTTXHWINT_LAY(UartRemoTXInts_str, 1, 1, UARTOp_str);
    //建立UartRemoRXInts_str與計時中斷UART鏈結
    UARTRXHWINT_LAY(UartRemoRXInts_str, 1, 1, UARTOp_str);

    //============PWM中斷 init+LAY============
    re = PWM2_init();
    printf("PWM2_init report %d \n", re);          // a=0 設定正確
    PWMOpStr_t PWM2Op_str = RM_PWM_OVF_2OPSTRINI;  //宣告結構體供模組使用
    TIMOpStr_t Tim2Op_str = TIM2OPSTRINI;
    /*SysTick use PWM1 IntS to construct SysTick, it allows 4 task to be
     * registered*/
    PWMHWINT_LAY(SysTick_str, 2, 4,
                 PWM2Op_str);  //建立SysTick_str與PWM2Op_str鏈結

    //============降頻器 LAY+reg/en============
    FreqReduStr_t SysClock_str = {0};
    FREQREDU_LAY(SysClock_str, 1, CYCLE, 0, NULL, Tim2Op_str);
    //降頻器掛進SysTick_str(此為TIMER2_OVF_vect)並ENABLE============
    SysClock_str.TaskId =
        HWInt_reg(&SysTick_str, &FreqRedu_step, &SysClock_str);
    HWInt_en(&SysTick_str, PERIPHERAL_INTERRUPT_ENABLE, ENABLE);
    re = HWInt_en(&SysTick_str, SysClock_str.TaskId, ENABLE);
    printf("SysTick(Timer)>>SysClock(FreqRedu) HWInt_reg re: %d \n",
           SysClock_str.TaskId);
    printf("SysTick(Timer)>>SysClock(FreqRedu) HWInt_en  re: %d \n",
           re);  // 0:設定正確

    //============PIPELINE LAY+reg/en============
    //佈局建立可以容納TASKNUM個工作方塊的管道工作執行器，並自動產生Pipeline_str結構體
    PIPELINE_LAY(1, 5, 10, Tim2Op_str);
    //將管道工作執行器SysPipeline_str登錄到計時中斷分享器SysTick_str中
    SysPipeline_str.TaskId =
        HWInt_reg(&SysTick_str, &Pipeline_step, &SysPipeline_str);
    //致能SysTick_str(致能結構體, 工作編號, 禁致能)
    re = HWInt_en(&SysTick_str, SysPipeline_str.TaskId, ENABLE);
    printf("SysTick(Timer)>>SysPipeline HWInt_reg re: %d \n",
           SysPipeline_str.TaskId);
    printf("SysTick(Timer)>>SysPipeline HWInt_en  re: %d \n",
           re);  // 0:設定正確

    //============REMOBUFF LAY+reg============
    //介面卡工作方塊變數登錄為暫存器開放遠端讀寫
    // REMOBUFF_LAY(結構體名稱, 變數的bytes數, 有幾個變數)
    //廣播
    REMOBUFF_LAY(BCRemoBF_str, 4, 1);  //建立廣播緩衝區BCRemoBF
    //將所有介面卡接收廣播變數，依序登錄廣播緩衝區ULRemoBF
    re = RemoBF_reg(&BCRemoBF_str, &BC_data_buffer, 4);
    printf("BCRemoBF VariTotal:%d\n", re);
    //上鍊
    REMOBUFF_LAY(ULRemoBF_str, 4, 1);
    re = RemoBF_reg(&ULRemoBF_str, &UL_data_buffer, 4);
    printf("ULRemoBF VariTotal:%d\n", re);
    //下鍊
    REMOBUFF_LAY(DLRemoBF_str, 4, 1);
    re = RemoBF_reg(&DLRemoBF_str, &DL_data_buffer, 4);
    printf("DLRemoBF VariTotal:%d\n", re);

    //============組織建構主控電腦固定週期以UART工作方塊============
    uint8_t Sys_str_UartId         = SYS_STR_UART_ID;
    UARTSRemoPStr_t UARTSRemoP_str = UARTSREMOPINI;
    UARTSRemoP_LAY(UARTSRemoP_str, BCRemoBF_str, ULRemoBF_str, DLRemoBF_str,
                   UARTOp_str, Tim2Op_str, &Sys_str_UartId);

    //(測試)============將測試函式掛進降頻器============
    uint8_t test_ID = FreqRedu_reg(&SysClock_str, &test, NULL, 1, 50);
    printf("test FreqRedu_reg re: %d \n", test_ID);
    FreqRedu_en(&SysClock_str, test_ID, ENABLE);

    UARTSRemoP_str.TimMax = TIMMAX;
    printf("UID=%d\n", *UARTSRemoP_str.UID_p);
    sei();
    while (1) {
        ;
    }
}
