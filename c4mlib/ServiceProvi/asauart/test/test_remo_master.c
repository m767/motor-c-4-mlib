/**
 * @file test_remo_master.c
 * @author sjchuang, kung
 * @date 2021.06.28
 * @brief 測試 ASA單板電腦 作為Master，透過Uart通訊，定週期傳送接收封包。
 *
 * 測試環境：
 * 1Master 3Slave
 * BroadCast  Remo Buffer/DownLink  Remo Buffer/UpLink  Remo Buffer皆登入1個變數
 * 登入進RemoBF的變數皆為4Bytes之資料
 * Uart Baud=38400 bits/s
 * Uart定周期通訊之頻率=
 * 11059200/64(預除頻)/510(Phase Correct
 *PWM)(TIMER2_OVF_vect)/9(降頻器CYCLE)=37.6Hz(T=26ms)
 * timeout時間=(1/(11059200/64/510))*TimMax
 *
 * 測試注意事項：
 * 需與 test_remo_slave.c 一起做測試
 * 需使用三張ASA單板電腦，並依編號設定3張Slave
 * UIDD=1(Slave1) UIDD=2(Slave2) UIDD=3(Slave3)
 * 測試時需先reset Slave1 Slave2 Slave3再reset Master
 *
 * 硬體配置:
 * >>將Master與Slave1 Slave2 Slave3的TXRX對接，並且雙方共地
 *
 * 程式執行步驟:
 * >>UART1      init+LAY
 * >>PWM中斷    init+LAY
 * >>降頻器     LAY+reg/en
 * >>PIPELINE   LAY+reg/en
 * >>REMOBUFF   LAY+reg
 * >>UARTSRemoP LAY
 * >>將測試函式掛進降頻器
 * >>將DLRemoBF1~3,ULRemoBF1~3登錄進入UARTMRemoP_str
 * >>sei
 *
 * 執行成功結果:
 * Master Card
 * UART1_init report 0
 * PWM2_init report 0
 * SysTick(Timer)>>SysClock(FreqRedu) HWInt_reg report 0
 * SysTick(Timer)>>SysClock(FreqRedu) HWInt_en report 0
 * SysTick(Timer)>>SysPipeline HWInt_reg re: 1
 * SysTick(Timer)>>SysPipeline HWInt_en  re: 0
 * BCRemoBF VariTotal:0
 * DLRemoBF1 VariTotal:0
 * ULRemoBF1 VariTotal:0
 * DLRemoBF2 VariTotal:0
 * ULRemoBF2 VariTotal:0
 * DLRemoBF3 VariTotal:0
 * ULRemoBF3 VariTotal:0
 * test FreqRedu_reg re:1
 * Card1 Reg Number 0
 * Card2 Reg Number 1
 * Card3 Reg Number 2
 *        0       0       0-->可能會有其他的值
 * 111111111111112211111133
 * 222222112222222222222233
 * 333333113333332233333333
 * 444444114444442244444433
 * 555555115555552255555533
 * 666666116666662266666633
 * 777777117777772277777733
 * 888888118888882288888833
 * 999999119999992299999933
 * aaaaaa11aaaaaa22aaaaaa33
 * bbbbbb11bbbbbb22bbbbbb33
 * cccccc11cccccc22cccccc33
 * dddddd11dddddd22dddddd33
 * eeeeee11eeeeee22eeeeee33
 * ffffff11ffffff22ffffff33
 * 111110111111102211111033
 *
 * 執行結果說明:
 * 此測試檔每次Uart定周期更新後都會將
 * BC_data_buffer += 0x10101000;
 * DL_data_buffer += 0x01010100;
 * 而slave端會將收到的值相加並在「下一次(非即時)」定周期通訊時送出
 * (UL_data_buffer = BC_data_buffer + DL_data_buffer;)
 * 再由Master印出從3張Slave收到的回傳值
 * 回傳值為8個Hex碼，最低的兩位為Slave卡的編號(UID)
 * 例：
 *  data1  UID1    data2   UID2   data3   UID3
 * 111111   11    111111    22   111111    33
 **/
#include "c4mlib/ServiceProvi/asauart/test/test_remo_master.cfg"

#include "c4mlib/C4MBios/hardwareset/src/m128/pwm_set.h"
#include "c4mlib/C4MBios/hardwareset/src/tim_set.h"
#include "c4mlib/ServiceProvi/asauart/src/asauart_remo_master.h"
#include "c4mlib/ServiceProvi/hwimp/src/m128/layout_macro.h"
#include "c4mlib/ServiceProvi/interrupt/src/frequency_set.h"
#include "c4mlib/ServiceProvi/pipeline/src/pipeline_executor.h"

#define PERIPHERAL_INTERRUPT_ENABLE 255

#define CYCLE  9                       // FreqRedu的週期(數CYCLE次一輪)
#define TIMMAX 2                       // Time Out倒數值(數到0 Time Out)
uint32_t BC_data_buffer = 0x10101000;  //當作可遙控的暫存器(MASTER->SLAVE)
uint32_t DL_data1_buffer = 0x01010111;  //當作可遙控的暫存器(MASTER->SLAVE)
uint32_t UL_data1_buffer = 0x00000000;  //當作可遙控的暫存器(SLAVE ->MASTER)
uint32_t DL_data2_buffer = 0x01010122;  //當作可遙控的暫存器
uint32_t UL_data2_buffer = 0x00000000;  //當作可遙控的暫存器
uint32_t DL_data3_buffer = 0x01010133;  //當作可遙控的暫存器
uint32_t UL_data3_buffer = 0x00000000;  //當作可遙控的暫存器

//測試函式
uint8_t test(void* a) {
    printf("%8lx%8lx%8lx\n", UL_data1_buffer, UL_data2_buffer, UL_data3_buffer);
    BC_data_buffer += 0x10101000;
    DL_data1_buffer += 0x01010100;
    DL_data2_buffer += 0x01010100;
    DL_data3_buffer += 0x01010100;
    return 0;
}

uint8_t UART1_init(void) {
    HardWareSet_t UART1HWSet_str       = UART1HWSETSTRINI;
    HWFlagPara_t UART1FgGpData_str[10] = UART1FLAGPARALISTINI;
    HWRegPara_t UART1RegData_str[2]    = UART1REGPARALISTINI;
    UART1HWSetDataStr_t UART1HWSetData = RM_UART1SETDATALISTINI;
    HARDWARESET_LAY(UART1HWSet_str, UART1FgGpData_str[0], UART1RegData_str[0],
                    UART1HWSetData);
    return HardwareSet_step(&UART1HWSet_str);
}

uint8_t PWM2_init(void) {
    HardWareSet_t PWM2HWSet_str      = PWM2HWSETSTRINI;
    HWFlagPara_t PWM2FgGpData_str[5] = PWM2FLAGPARALISTINI;
    HWRegPara_t PWM2RegData_str      = PWM2REGPARALISTINI;
    PWM2HWSetDataStr_t PWM2HWSetData = RM_PWM2SETDATALISTINI;
    HARDWARESET_LAY(PWM2HWSet_str, PWM2FgGpData_str[0], PWM2RegData_str,
                    PWM2HWSetData);
    return HardwareSet_step(&PWM2HWSet_str);
}

int main() {
    C4M_DEVICE_set();
    printf("Master Card\n");
    uint8_t re = 11;

    //============UART1 init+LAY============
    re = UART1_init();
    printf("UART1_init report %d \n", re);       // 0:設定正確
    UARTOpStr_t UART1Op_str = RM_UART1OPSTRINI;  //宣告結構體供模組使用
    //建立UartRemoIntsTX_str與UART1Op_str鏈結
    UARTTXHWINT_LAY(UartRemoIntsTX_str, 1, 1, UART1Op_str);
    //建立UartRemoIntsRX_str與UART1Op_str鏈結
    UARTRXHWINT_LAY(UartRemoIntsRX_str, 1, 1, UART1Op_str);

    //============PWM中斷 init+LAY============
    re = PWM2_init();
    printf("PWM2_init report %d \n", re);          // 0:設定正確
    PWMOpStr_t PWM2Op_str = RM_PWM_OVF_2OPSTRINI;  //宣告結構體供模組使用
    TIMOpStr_t HWOPSTR    = TIM2OPSTRINI;
    /*SysTick use PWM1 IntS to construct SysTick, it allows 4 task to be
     * registered*/
    PWMHWINT_LAY(SysTick_str, 2, 4,
                 PWM2Op_str);  //建立SysTick_str與PWM2Op_str鏈結

    //============降頻器 LAY+reg/en============
    FreqReduStr_t SysClock_str = {0};
    FREQREDU_LAY(SysClock_str, 4, CYCLE, 0, NULL, HWOPSTR);
    //降頻器掛進SysTick_str(此為TIMER2_OVF_vect)並ENABLE============
    SysClock_str.TaskId =
        HWInt_reg(&SysTick_str, &FreqRedu_step, &SysClock_str);
    HWInt_en(&SysTick_str, PERIPHERAL_INTERRUPT_ENABLE,
             ENABLE);  //中斷分享器禁致能
    re = HWInt_en(&SysTick_str, SysClock_str.TaskId, ENABLE);
    printf("SysTick(Timer)>>SysClock(FreqRedu) HWInt_reg re: %d \n",
           SysClock_str.TaskId);
    printf("SysTick(Timer)>>SysClock(FreqRedu) HWInt_en  re: %d \n",
           re);  // 0:設定正確

    //============PIPELINE LAY+reg/en============
    //佈局建立可以容納TASKNUM個工作方塊的管道工作執行器，並自動產生Pipeline_str結構體
    PIPELINE_LAY(1, 5, 10, HWOPSTR);
    //將管道工作執行器SysPipeline_str登錄到計時中斷結構體SysTick_str中，取得工作編號TaskId
    SysPipeline_str.TaskId =
        HWInt_reg(&SysTick_str, &Pipeline_step, &SysPipeline_str);
    //致能SysTick_str(致能結構體, 工作編號, 禁致能)
    re = HWInt_en(&SysTick_str, SysPipeline_str.TaskId, ENABLE);
    printf("SysTick(Timer)>>SysPipeline HWInt_reg re: %d \n",
           SysPipeline_str.TaskId);
    printf("SysTick(Timer)>>SysPipeline HWInt_en  re: %d \n",
           re);  // 0:設定正確

    //============REMOBUFF LAY+reg============
    //主控電腦工作方塊結構體佈局及登錄
    //呼叫REMOBUFF_LAY()建立廣播緩衝區BCRemoBF
    REMOBUFF_LAY(BCRemoBF_str, 4, 1);  //建立BC緩衝區(結構體,位元組,變數)
    //呼叫RemoBF_reg()將所有介面卡允許遠端讀寫的變數，依序登錄並取得其暫存器編號
    re = RemoBF_reg(&BCRemoBF_str, &BC_data_buffer, 4);
    printf("BCRemoBF VariTotal:%d\n", re);

    REMOBUFF_LAY(DLRemoBF1_str, 4, 1);  //建立下鏈緩衝區1
    //將所有下鏈變數登錄進DLRemoBF1(UARTMRemoP_str.DLRemoBF_p)
    re = RemoBF_reg(&DLRemoBF1_str, &DL_data1_buffer, 4);
    printf("DLRemoBF1 VariTotal:%d\n", re);

    REMOBUFF_LAY(ULRemoBF1_str, 4, 1);  //建立上鏈緩衝區1
    //將所有上鏈變數登錄進ULRemoBF1(UARTMRemoP_str.ULRemoBF_p)
    re = RemoBF_reg(&ULRemoBF1_str, &UL_data1_buffer, 4);
    printf("ULRemoBF1 VariTotal:%d\n", re);

    REMOBUFF_LAY(DLRemoBF2_str, 4, 1);  //建立下鏈緩衝區2
    //將所有下鏈變數登錄進DLRemoF2(UARTMRemoP_str.DLRemoBF_p)
    re = RemoBF_reg(&DLRemoBF2_str, &DL_data2_buffer, 4);
    printf("DLRemoBF2 VariTotal:%d\n", re);

    REMOBUFF_LAY(ULRemoBF2_str, 4, 1);  //建立上鏈緩衝區2
    //將所有上鏈變數登錄進ULRemoF2(UARTMRemoP_str.ULRemoBF_p)
    re = RemoBF_reg(&ULRemoBF2_str, &UL_data2_buffer, 4);
    printf("ULRemoBF2 VariTotal:%d\n", re);

    REMOBUFF_LAY(DLRemoBF3_str, 4, 1);  //建立下鏈緩衝區3
    //將所有下鏈變數登錄進DLRemoF3(UARTMRemoP_str.DLRemoBF_p)
    re = RemoBF_reg(&DLRemoBF3_str, &DL_data3_buffer, 4);
    printf("DLRemoBF3 VariTotal:%d\n", re);

    REMOBUFF_LAY(ULRemoBF3_str, 4, 1);  //建立上鏈緩衝區3
    //將所有上鏈變數登錄進ULRemoF3(UARTMRemoP_str.ULRemoBF_p)
    re = RemoBF_reg(&ULRemoBF3_str, &UL_data3_buffer, 4);
    printf("ULRemoBF3 VariTotal:%d\n", re);

    //============組織建構主控電腦固定週期以UART工作方塊============
    UARTMRemoPStr_t UARTMRemoP_str = UARTM1REMOPINI;
    UARTMRemoP_LAY(UARTMRemoP_str, UART1Op_str);

    //(測試)============將測試函式掛進降頻器============
    uint8_t test_ID      = FreqRedu_reg(&SysClock_str, &test, NULL, 1, 3);
    SysClock_str.Counter = 4;
    printf("test FreqRedu_reg re: %d \n", test_ID);
    FreqRedu_en(&SysClock_str, test_ID, ENABLE);
    UARTMRemoP_str.TimMax = TIMMAX;

    //將DLRemoBF1~3,ULRemoBF1~3登錄進入UARTMRemoP_str
    re = UARTMRemoP_reg(&UARTMRemoP_str, &ULRemoBF1_str, &DLRemoBF1_str,
                        &BCRemoBF_str);
    printf("Card1 Reg Number %d \n", re);  // 0:卡號正確
    re = UARTMRemoP_reg(&UARTMRemoP_str, &ULRemoBF2_str, &DLRemoBF2_str,
                        &BCRemoBF_str);
    printf("Card2 Reg Number %d \n", re);  // 1:卡號正確
    re = UARTMRemoP_reg(&UARTMRemoP_str, &ULRemoBF3_str, &DLRemoBF3_str,
                        &BCRemoBF_str);
    printf("Card3 Reg Number %d \n", re);  // 2:卡號正確

    sei();
    while (1) {
        ;
    }
}
