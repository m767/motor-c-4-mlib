/**
 * @file test_remo_slave_sop.c
 * @author kung, sjchuang
 * @date 2021.08.03
 * @brief
 *C4MOS-UartServiceProvider規格書-標準使用步驟SOP，介面卡規畫變數可接受主控電腦定週期廣播，下鏈或上鏈
 * 測試 ASA單板電腦 作為Slave，透過Uart通訊，定週期傳送接收封包。
 *
 *
 * 測試注意事項：
 * 需與 test_remo_master.c 一起做測試
 * 需使用三張ASA單板電腦，並依編號設定
 * UIDD=1(Slave1) UIDD=2(Slave2) UIDD=3(Slave3)
 * 需先reset Slave1 Slave2 Slave3再reset Master
 *
 * 硬體配置:
 * >>將Master與Slave1 Slave2 Slave3的TXRX對接，並且雙方共地
 *
 * 程式執行步驟:
 * >>REMOBUFF   LAY
 * >>UART1      init+LAY
 * >>PWM中斷    init+LAY
 * >>降頻器     LAY+reg/en
 * >>PIPELINE   LAY+reg/en
 * >>UARTSRemoP LAY
 * >>REMOBUFF   reg
 * >>將測試函式掛進降頻器
 * >>sei
 *
 * 執行成功結果:
 * ==========SLAVE1==========
 * Slave Card
 * UART1_init report 0
 * PWM2_init report 0
 * SysTick(Timer)>>SysClock(FreqRedu) HWInt_reg re: 0
 * SysTick(Timer)>>SysClock(FreqRedu) HWInt_en  re: 0
 * SysTick(Timer)>>SysPipeline HWInt_reg re: 1
 * SysTick(Timer)>>SysPipeline HWInt_en  re: 0
 * FB0 FreqRedu_reg re: 0
 * UID=1
 * ==========SLAVE2==========
 * Slave Card
 * UART1_init report 0
 * PWM2_init report 0
 * SysTick(Timer)>>SysClock(FreqRedu) HWInt_reg re: 0
 * SysTick(Timer)>>SysClock(FreqRedu) HWInt_en  re: 0
 * SysTick(Timer)>>SysPipeline HWInt_reg re: 1
 * SysTick(Timer)>>SysPipeline HWInt_en  re: 0
 * FB0 FreqRedu_reg re: 0
 * UID=2
 * ==========SLAVE3==========
 * Slave Card
 * UART1_init report 0
 * PWM2_init report 0
 * SysTick(Timer)>>SysClock(FreqRedu) HWInt_reg re: 0
 * SysTick(Timer)>>SysClock(FreqRedu) HWInt_en  re: 0
 * SysTick(Timer)>>SysPipeline HWInt_reg re: 1
 * SysTick(Timer)>>SysPipeline HWInt_en  re: 0
 * FB0 FreqRedu_reg re: 0
 * UID=3
 **/

#include "c4mlib/C4MBios/hardwareset/src/m128/pwm_set.h"
#include "c4mlib/C4MBios/hardwareset/src/tim_set.h"
#include "c4mlib/ServiceProvi/asauart/src/asauart_remo_slave.h"
#include "c4mlib/ServiceProvi/hwimp/src/m128/layout_macro.h"

#include "c4mlib/ServiceProvi/asauart/test/test_remo_slave.cfg"

#define PERIPHERAL_INTERRUPT_ENABLE 255

#define SYS_STR_UART_ID 1UL  //"1"為SLAVE的裝置編號(UID)，最小為1
#define FREQ_CYCLE      72   // FreqRedu的週期(數CYCLE次一輪)
#define TIMMAX          10   // Time Out倒數值(數到0 Time Out)

typedef struct {
    uint32_t BC_data_buffer;  //當作可遙控的暫存器(MASTER->SLAVE)
    uint32_t DL_data_buffer;  //當作可遙控的暫存器(MASTER->SLAVE)
    uint32_t UL_data_buffer;  //當作可遙控的暫存器(SLAVE ->MASTER)
} Fb0Str_t;

#define FB0STRINI                                                              \
    {                                                                          \
        .BC_data_buffer = 0x10101000, .DL_data_buffer = 0x01010111,            \
        .UL_data_buffer = 0x00000000                                           \
    }

#define MY_UARTSREMOPINI                                                       \
    {                                                                          \
        .TimMax = TIMMAX, .Header = 0xAA, .ULHeader = 0xBB, .SyncState = 0,    \
        .TXState = 0, .RXState = 0, .TOState = 0                               \
    }

uint8_t FB0(void* a);  //測試函式
uint8_t UART1_init(void);
uint8_t PWM2_init(void);

/**
 * @param TASKNUM 降頻執行器容納登錄工作數 Maximum Number of Tasks alow to be
 * registed into this Frequence Reduce executor
 * @param CYCLE 降頻週期
 * @param ADJUSTP 可調中斷週期與否，不可調ADJUSTP=0,可調ADJUSTP=1
 * @param PERIODLISTADD 存放可調週期的陣列The address of the list of adjustable
 */
#define SYSTEM_LAY(TASKNUM, CYCLE, ADJUSTP, PERIODMATADD)                 \
    uint8_t re = 77; /*宣告變數存回傳值*/                                   \
    /*============UART1 init+LAY============*/                            \
    re = UART1_init();                                                    \
    printf("UART1_init report %d \n", re);                                \
    /* re=0 設定正確*/                                                     \
    UARTOpStr_t UARTOp_str = RM_UART1OPSTRINI;                            \
    /*宣告結構體供模組使用*/                                                \
    /*建立UartRemoTXInts_str與計時中斷UART鏈結*/                            \
    UARTTXHWINT_LAY(UartRemoTXInts_str, 1, 1, UARTOp_str);                \
    /*建立UartRemoRXInts_str與計時中斷UART鏈結*/                            \
    UARTRXHWINT_LAY(UartRemoRXInts_str, 1, 1, UARTOp_str);                \
    /*============PWM中斷 init+LAY============*/                          \
    re = PWM2_init();                                                     \
    printf("PWM2_init report %d \n", re);                                 \
    /* a=0 設定正確*/                                                      \
    PWMOpStr_t PWM2Op_str = RM_PWM_OVF_2OPSTRINI;                         \
    /*宣告結構體供模組使用*/                                                \
    TIMOpStr_t Tim2Op_str = TIM2OPSTRINI;                                 \
    /*SysTick use PWM1 IntS to construct SysTick,                         \
    it allows 4 task to be registered*/                                   \
    PWMHWINT_LAY(SysTick_str, 2, 4, PWM2Op_str);                          \
    /*建立SysTick_str與PWM2Op_str鏈結*/                                    \
    /*============降頻器 LAY+reg/en============*/                          \
    FreqReduStr_t SysClock_str = {0};                                     \
    FREQREDU_LAY(SysClock_str, TASKNUM, CYCLE, ADJUSTP, PERIODMATADD,     \
                 Tim2Op_str);                                             \
    /*降頻器掛進SysTick_str(此為TIMER2_OVF_vect)並ENABLE============*/      \
    SysClock_str.TaskId =                                                 \
        HWInt_reg(&SysTick_str, &FreqRedu_step, &SysClock_str);           \
    HWInt_en(&SysTick_str, PERIPHERAL_INTERRUPT_ENABLE, ENABLE);          \
    re = HWInt_en(&SysTick_str, SysClock_str.TaskId, ENABLE);             \
    printf("SysTick(Timer)>>SysClock(FreqRedu) HWInt_reg re: %d \n",      \
           SysClock_str.TaskId);                                          \
    printf("SysTick(Timer)>>SysClock(FreqRedu) HWInt_en  re: %d \n", re); \
    /* 0:設定正確*/                                                        \
    /*============PIPELINE LAY+reg/en============*/                       \
    /*佈局建立可以容納TASKNUM個工作方塊的管道工作執行器，                      \
    並自動產生Pipeline_str結構體*/                                          \
    PIPELINE_LAY(1, 5, 10, Tim2Op_str);                                   \
    /*將管道工作執行器SysPipeline_str                                       \
    登錄到計時中斷分享器SysTick_str中*/                                     \
    SysPipeline_str.TaskId =                                              \
        HWInt_reg(&SysTick_str, &Pipeline_step, &SysPipeline_str);        \
    /*致能SysTick_str(致能結構體, 工作編號, 禁致能)*/                        \
    re = HWInt_en(&SysTick_str, SysPipeline_str.TaskId, ENABLE);          \
    printf("SysTick(Timer)>>SysPipeline HWInt_reg re: %d \n",             \
           SysPipeline_str.TaskId);                                       \
    printf("SysTick(Timer)>>SysPipeline HWInt_en  re: %d \n", re);        \
    /* 0:設定正確*/                                                        \

int main() {
    C4M_DEVICE_set();
    printf("Slave Card\n");
    UARTSRemoPStr_t UARTSRemoP_str = MY_UARTSREMOPINI;
    Fb0Str_t FB0_str               = FB0STRINI;

    // REMOBUFF_LAY(結構體名稱, 變數的bytes數, 有幾個變數)
    REMOBUFF_LAY(BCRemoBF_str, 4, 1);  //建立廣播緩衝區BCRemoBF
    REMOBUFF_LAY(ULRemoBF_str, 4, 1);  //上鍊
    REMOBUFF_LAY(DLRemoBF_str, 4, 1);  //下鍊

    SYSTEM_LAY(1, FREQ_CYCLE, 0, NULL);

    uint8_t Sys_str_UartId = SYS_STR_UART_ID;
    UARTSRemoP_LAY(UARTSRemoP_str, BCRemoBF_str, ULRemoBF_str, DLRemoBF_str,
                   UARTOp_str, Tim2Op_str, &Sys_str_UartId);

    RemoBF_reg(&BCRemoBF_str, &(FB0_str.BC_data_buffer), 4);  //廣播
    RemoBF_reg(&ULRemoBF_str, &(FB0_str.UL_data_buffer), 4);  //上鍊
    RemoBF_reg(&DLRemoBF_str, &(FB0_str.DL_data_buffer), 4);  //下鍊

    //(測試)============將測試函式掛進降頻器============
    uint8_t FB0_ID = FreqRedu_reg(&SysClock_str, &FB0, &FB0_str, 1, 50);
    printf("FB0 FreqRedu_reg re: %d \n", FB0_ID);
    FreqRedu_en(&SysClock_str, FB0_ID, ENABLE);

    printf("UID=%d\n", *UARTSRemoP_str.UID_p);
    sei();
    while (1) {
        ;
    }
}

//測試函式
uint8_t FB0(void* a) {
    Fb0Str_t* str       = (Fb0Str_t*)a;
    str->UL_data_buffer = str->BC_data_buffer + str->DL_data_buffer;
    return 0;
}

uint8_t UART1_init(void) {
    HardWareSet_t UART1HWSet_str       = UART1HWSETSTRINI;
    HWFlagPara_t UART1FgGpData_str[10] = UART1FLAGPARALISTINI;
    HWRegPara_t UART1RegData_str[2]    = UART1REGPARALISTINI;
    UART1HWSetDataStr_t UART1HWSetData = RM_UART1SETDATALISTINI;
    HARDWARESET_LAY(UART1HWSet_str, UART1FgGpData_str[0], UART1RegData_str[0],
                    UART1HWSetData);
    return HardwareSet_step(&UART1HWSet_str);
}

uint8_t PWM2_init(void) {
    HardWareSet_t PWM2_20K_str          = PWM2HWSETSTRINI;
    HWFlagPara_t PWM2FlagParaList[5]    = PWM2FLAGPARALISTINI;
    HWRegPara_t PWM2RegParaList         = PWM2REGPARALISTINI;
    PWM2HWSetDataStr_t PWM2DataList_str = RM_PWM2SETDATALISTINI;
    HARDWARESET_LAY(PWM2_20K_str, PWM2FlagParaList[0], PWM2RegParaList,
                    PWM2DataList_str);
    return HardwareSet_step(&PWM2_20K_str);
}