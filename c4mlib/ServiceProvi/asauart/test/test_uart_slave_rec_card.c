/**
 * @file test_uart_slave_rec_card.c
 * @author stu1000264
 * @date 2021.05.25
 * @brief 測試 ASA單板電腦 作為Slave，透過Uart通訊，傳送接收封包。
 *
 * 需與 test_uart_master_rec_card.c 一起做測試，
 * 硬體配置:
 * >>   將Master與Slave的TXRX對接，並且雙方共地
 * 程式執行步驟:
 * >>   宣告UARTMStr_t及UARTOpStr_t結構體
 * >>   使用UART1_HW_LAY產生硬體設定結構體
 * >>   使用HWINT_LAY產生TX,RX以及TIMER中斷服務分享器結構體
 * >>   完成pipeline設定布局
 * >>   產生RemoBuffer所需結構體
 * >>   使用UARTSRemoSTR_LAY將對應功能方塊登錄至中斷服務分享器，以及Pipeline
 * >>   將pipeline登錄至timer服務分享器
 * >>   將對應變數作為暫存器登錄進RemoBuffer
 * >>   致能TX,RX以及TIMER中斷服務分享器
 * 測試成功結果:
 * >>  taskid=1
 * >>  regid=0
 * >>  regid=1
 * >>  regid=2
 * >>  regid=3
 * >>  regid=4
 * >>  regid=5
 * >>  regid=6
 * >>  start
 * >>  data3=0,0,0
 * >>  data3=0,0,0
 * >>  data3=0,0,0
 * >>  data3=5,6,7
 * >>  data3=5,6,7
 * >>  data3=5,6,7
 */
#include "c4mlib/C4MBios/asabus/src/remo_reg.h"
#include "c4mlib/C4MBios/hardwareset/src/m128/uart_set.h"
#include "c4mlib/C4MBios/hardwareset/src/tim_set.h"
#include "c4mlib/ServiceProvi/asauart/src/asauart_slave_card.h"
#include "c4mlib/ServiceProvi/hwimp/src/hardware_interrupt.h"
#include "c4mlib/ServiceProvi/hwimp/src/layout_macro.h"
#include "c4mlib/ServiceProvi/hwimp/src/m128/layout_macro.h"
#include "c4mlib/ServiceProvi/hwimp/src/tim_imp.h"
#include "c4mlib/ServiceProvi/pipeline/src/pipeline_executor.h"
#include "c4mlib/ServiceProvi/pipeline/src/pipeline_imp.h"

#include "c4mlib/ServiceProvi/asauart/test/uart.cfg"

void init_timer();

int main() {
    C4M_STDIO_init();
    init_timer();
    UARTOpStr_t UARTOpStr    = UART1SREGPARA;
    UARTSRemoStr_t UARTS_STR = UARTS1REMOINI;
    uint8_t uartuid          = 0x80;
    TIMOpStr_t TIMOPSTR      = TIM2OPSTRINI;
    /*UART1設定*/
    uint16_t baud = F_CPU / 16 / 38400 - 1;
    UBRR1H        = (unsigned char)(baud >> 8);
    UBRR1L        = (unsigned char)baud;

    UCSR1B |= (1 << RXEN1) | (1 << TXEN1);
    UCSR1C |= (3 << UCSZ10);
    // Enable RX & TX interrupt
    UCSR1B |= (1 << RXCIE1) | (1 << TXCIE1);

    uint8_t data_buffer = 5, data_buffer1 = 6, data_buffer2 = 7,
            data_buffer3[3] = {5, 6, 7};
    uint8_t data_trm1 = 0, data_trm3[10] = {0};
    REGPUT(UARTOpStr.DataReg_p, 1, &data_buffer);
    UARTTXHWINT_LAY(TXINTSTR, 1, 1, UARTOpStr);
    UARTRXHWINT_LAY(RXINTSTR, 1, 1, UARTOpStr);
    TIMHWINT_LAY(TOINTSTR, 2, 2, TIMOPSTR);

    PIPELINE_LAY(1, 5, 10, TIMOPSTR);
    REMOBUFF_LAY(UARTRemo_str, 20, 10);

    UARTSRemoSTR_LAY(UARTS_STR, TXINTSTR, RXINTSTR, TOINTSTR, SysPipeline_str,
                     UARTRemo_str, UARTOpStr, &uartuid);

    uint8_t taskID = HWInt_reg(&TOINTSTR, &Pipeline_step, &SysPipeline_str);
    printf("taskid=%d\n", taskID);
    uint8_t res = RemoBF_reg(UARTS_STR.RemoBF_p, &data_buffer, 1);
    printf("regid=%d\n", res);
    res = RemoBF_reg(UARTS_STR.RemoBF_p, &data_buffer1, 1);
    printf("regid=%d\n", res);
    res = RemoBF_reg(UARTS_STR.RemoBF_p, &data_buffer2, 1);
    printf("regid=%d\n", res);
    res = RemoBF_reg(UARTS_STR.RemoBF_p, &data_buffer3[0], 3);
    printf("regid=%d\n", res);
    res = RemoBF_reg(UARTS_STR.RemoBF_p, &data_trm1, 1);
    printf("regid=%d\n", res);
    res = RemoBF_reg(UARTS_STR.RemoBF_p, data_trm3, 10);
    printf("regid=%d\n", res);
    HWInt_en(&TOINTSTR, taskID, ENABLE);
    HWInt_en(&TOINTSTR, UARTS_STR.TOTaskID, ENABLE);
    HWInt_en(&RXINTSTR, UARTS_STR.RxTaskID, ENABLE);
    HWInt_en(&TXINTSTR, UARTS_STR.TxTaskID, ENABLE);

    printf("start\n");
    sei();
    while (1) {
        printf("data3=%d,%d,%d\n", data_trm3[0], data_trm3[1], data_trm3[2]);
        _delay_ms(1000);
    }
}

void init_timer() {
    DDRD |= 0xF0;
    TCCR2 = 0b00011101;
    OCR2  = 255;
    TIMSK |= (1 << OCIE2);
}
