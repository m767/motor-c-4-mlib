/**
 * @file asauart_master_card.c
 * @author s915888
 * @date 2021.05.25
 * @brief 實現 ASA UART Master 0 通訊函式
 */

#include "c4mlib/ServiceProvi/asauart/src/asauart_master_card.h"

#include "c4mlib/C4MBios/asabus/src/asabus.h"
#include "c4mlib/C4MBios/debug/src/debug.h"
#include "c4mlib/C4MBios/hardwareset/src/m128/uart_set.h"
#include "c4mlib/C4MBios/macro/src/bits_op.h"
#include "c4mlib/C4MBios/macro/src/std_res.h"
#include "c4mlib/ServiceProvi/time/src/hal_time.h"

#include <stdint.h>
#include <stdio.h>

#define DEFAULTUARTBAUD    38400
#define ASAUART_CMD_HEADER 0xAA  ///< @ingroup asauart_macro
#define ASAUART_RSP_HEADER 0xAB  ///< @ingroup asauart_macro
#define ERR_TIMEOUT        255
#define ERR_IN_STATE5      5
#define ERR_IN_STATE6      6
#define ERR_IN_STATE7      7
#define ERR_IN_STATE8      8
#define UART_TRM_COMPLETE  0
#define UART_FTM_COMPLETE  0
#define UART_REC_COMPLETE  0
#define UART_FRC_COMPLETE  0
#define UART_REC_DATA      1
#define ERR_RX             1
#define ERR_TX             2
#define UART_TXHEADER      0
#define UART_CARDID        1
#define UART_W_ADDR        2
#define UART_R_ADDR        2
#define UART_BYTES         3
#define UART_DATA          4
#define UART_REC_TXCHKSUM  4
#define UART_TXCHKSUM      5
#define UART_REC_RXHEADER  5
#define UART_RXHEADER      6
#define UART_REC_RESP      6
#define UART_RESP          7
#define UART_RXCHKSUM      8
#define UART_RESP_OK       0
#define UART_WRITE_ADDR    0x80
#define UART_REC_NULL      0
#define UART_TRM_NULL      0
#define UART_TRM_DATA      0
#define UART_RECREC_DATA   7

#define WAIT4UDRE_PUTTXDATA()                                                  \
    {                                                                          \
        int32_t i = 0;                                                         \
        for (i = Str_p->TimMax; i >= 0; i--) {                                 \
            if ((*(UOpStr_p->TXIntFReg_p) & UOpStr_p->TXIntFMask)) {           \
                REGPUT(UOpStr_p->DataReg_p, 1, &TXData);                       \
                break;                                                         \
            }                                                                  \
        }                                                                      \
        if (i == 0) {                                                          \
            return ERR_TIMEOUT;                                                \
        }                                                                      \
    }
#define WAIT4RXC_GETRXDATA()                                                   \
    {                                                                          \
        int32_t i = 0;                                                         \
        for (i = Str_p->TimMax; i >= 0; i--) {                                 \
            if (*(UOpStr_p->RXIntFReg_p) & UOpStr_p->RXIntFMask) {             \
                REGGET(UOpStr_p->DataReg_p, 1, &RXData);                       \
                break;                                                         \
            }                                                                  \
        }                                                                      \
        if (i == 0) {                                                          \
            return ERR_TIMEOUT;                                                \
        }                                                                      \
    }

uint8_t UARTM0_trm(void* void_p, uint8_t RegAdd, uint8_t Bytes, void* Data_p) {
    uint8_t State = 0, TXData = 0, ChkSum = 0, Index = 0, RXData = 0;
    UARTMStr_t* Str_p     = (UARTMStr_t*)void_p;
    UARTOpStr_t* UOpStr_p = Str_p->UARTOpStr_p;
    while (1) {
        switch (Str_p->Mode) {
            case 0:
                switch (State) {
                    case UART_TXHEADER:
                        TXData = Str_p->Header;
                        ChkSum = TXData;
                        State  = UART_CARDID;
                        WAIT4UDRE_PUTTXDATA();
                        break;
                    case UART_CARDID:
                        TXData = Str_p->CardId;
                        ChkSum = ChkSum + TXData;
                        State  = UART_W_ADDR;
                        WAIT4UDRE_PUTTXDATA();
                        break;
                    case UART_W_ADDR:
                        RegAdd = RegAdd | UART_WRITE_ADDR;
                        TXData = RegAdd;
                        ChkSum = ChkSum + TXData;
                        State  = UART_BYTES;
                        WAIT4UDRE_PUTTXDATA();
                        break;
                    case UART_BYTES:
                        TXData = Bytes;
                        ChkSum = ChkSum + TXData;
                        State  = UART_DATA;
                        Index  = 0;
                        WAIT4UDRE_PUTTXDATA();
                        break;
                    case UART_DATA:
                        TXData = *(uint8_t*)(Data_p + Index);
                        Index++;
                        ChkSum = ChkSum + TXData;
                        if (Index == Bytes) {
                            State = UART_TXCHKSUM;
                        }

                        WAIT4UDRE_PUTTXDATA();
                        break;
                    case UART_TXCHKSUM:
                        TXData = ChkSum;
                        WAIT4UDRE_PUTTXDATA();
                        State = UART_RXHEADER;
                        break;
                    case UART_RXHEADER:
                        WAIT4RXC_GETRXDATA();
                        ChkSum = RXData;
                        if (RXData == Str_p->Header) {
                            State = UART_RESP;
                        }
                        else {
                            return ERR_IN_STATE6;
                        }
                        break;
                    case UART_RESP:
                        WAIT4RXC_GETRXDATA();
                        ChkSum = ChkSum + RXData;
                        if (RXData == UART_RESP_OK) {
                            State = UART_RXCHKSUM;
                        }
                        else {
                            return ERR_IN_STATE7;
                        }
                        break;
                    case UART_RXCHKSUM:
                        WAIT4RXC_GETRXDATA();
                        if (RXData == ChkSum) {
                            return UART_TRM_COMPLETE;
                        }
                        else {
                            return ERR_IN_STATE8;
                        }
                        break;
                }
                break;
            case 3:
                switch (State) {
                    case UART_TRM_DATA:
                        TXData = *(uint8_t*)(Data_p + Index);
                        Index++;
                        if (Bytes == 0) {
                            if (TXData != 0) {
                                WAIT4UDRE_PUTTXDATA();
                                break;
                            }
                            else {
                                State = UART_REC_DATA;
                                Index = 0;
                            }
                        }
                        else {
                            WAIT4UDRE_PUTTXDATA();
                            if (Index == Bytes) {
                                return UART_TRM_COMPLETE;
                            }
                            break;
                        }

                    case UART_REC_DATA:
                        WAIT4RXC_GETRXDATA();
                        *(uint8_t*)(Data_p + Index) = RXData;
                        Index++;
                        if (RXData == UART_REC_NULL) {
                            return (Index);
                        }
                        break;
                }
                break;
        }
    }
}

uint8_t UARTM0_rec(void* void_p, uint8_t RegAdd, uint8_t Bytes, void* Data_p) {
    uint8_t TXData = 0, ChkSum = 0, State = 0, Index = 0, RXData = 0;
    UARTMStr_t* Str_p     = (UARTMStr_t*)void_p;
    UARTOpStr_t* UOpStr_p = Str_p->UARTOpStr_p;
    while (1) {
        switch (Str_p->Mode) {
            case 0:
                switch (State) {
                    case UART_TXHEADER:
                        TXData = Str_p->Header;
                        ChkSum = TXData;
                        State  = UART_CARDID;
                        WAIT4UDRE_PUTTXDATA();
                        break;
                    case UART_CARDID:
                        TXData = Str_p->CardId;
                        ChkSum = ChkSum + TXData;
                        State  = UART_R_ADDR;
                        WAIT4UDRE_PUTTXDATA();
                        break;
                    case UART_R_ADDR:
                        TXData = RegAdd;
                        ChkSum = ChkSum + TXData;
                        State  = UART_BYTES;
                        WAIT4UDRE_PUTTXDATA();
                        break;
                    case UART_BYTES:
                        TXData = Bytes;
                        ChkSum = ChkSum + TXData;
                        State  = UART_REC_TXCHKSUM;
                        Index  = 0;
                        WAIT4UDRE_PUTTXDATA();
                        break;
                    case UART_REC_TXCHKSUM:
                        TXData = ChkSum;
                        WAIT4UDRE_PUTTXDATA();
                        State = UART_REC_RXHEADER;
                        break;
                    case UART_REC_RXHEADER:
                        WAIT4RXC_GETRXDATA();
                        ChkSum = RXData;
                        if (Str_p->Header == RXData) {
                            State = UART_REC_RESP;
                        }
                        else {
                            State = ERR_IN_STATE5;
                        }
                        break;
                    case UART_REC_RESP:
                        WAIT4RXC_GETRXDATA();
                        ChkSum = ChkSum + RXData;
                        if (RXData == UART_RESP_OK) {
                            State = UART_RECREC_DATA;
                        }
                        else {
                            State = ERR_IN_STATE6;
                        }
                        break;
                    case UART_RECREC_DATA:
                        WAIT4RXC_GETRXDATA();
                        *((uint8_t*)(Data_p + Index)) = RXData;
                        ChkSum                        = ChkSum + RXData;
                        Index++;
                        if (Index == Bytes) {
                            State = UART_RXCHKSUM;
                        }
                        break;
                    case UART_RXCHKSUM:
                        WAIT4RXC_GETRXDATA();
                        if (RXData == ChkSum) {
                            return UART_REC_COMPLETE;
                        }
                        else {
                            return ERR_IN_STATE8;
                        }
                        break;
                }
                break;
            case 3:
                switch (State) {
                    case UART_REC_NULL:
                        WAIT4RXC_GETRXDATA();
                        *(uint8_t*)(Data_p + Index) = RXData;
                        Index++;
                        if (RXData == Bytes)
                            return UART_REC_COMPLETE;
                        break;
                }
                break;
        }
    }
}

uint8_t UARTM0_ftm(void* void_p, uint8_t RegAdd, uint8_t Mask, uint8_t shift,
                   uint8_t Data) {
    UARTMStr_t* Str_p = (UARTMStr_t*)void_p;
    uint8_t RegValue;
    if (UARTM0_rec(Str_p, RegAdd, 1, &RegValue)) {
        return ERR_RX;
    }

    RegValue = (RegValue & ~Mask) | ((Data << shift) & Mask);
    if (UARTM0_trm(Str_p, RegAdd, 1, &RegValue)) {
        return ERR_TX;
    }

    return UART_FTM_COMPLETE;
}

uint8_t UARTM0_frc(void* void_p, uint8_t RegAdd, uint8_t Mask, uint8_t shift,
                   uint8_t* Data_p) {
    UARTMStr_t* Str_p = (UARTMStr_t*)void_p;
    uint8_t RegValue;
    if (UARTM0_rec(Str_p, RegAdd, 1, &RegValue)) {
        return ERR_RX;
    }
    *Data_p = ((RegValue & Mask) >> shift);
    return UART_FRC_COMPLETE;
}
