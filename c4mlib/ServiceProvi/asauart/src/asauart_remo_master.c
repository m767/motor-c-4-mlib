/**
 * @file asauart_remo_master.c
 * @author kung, sjchuang
 * @date 2021.06.28
 * @brief 實現 ASA UART Master 定週期通訊函式
 *
 */

#include "c4mlib/ServiceProvi/asauart/src/asauart_remo_master.h"

#include "c4mlib/C4MBios/debug/src/debug.h"
#define BCHEADER  0
#define BCUID     1
#define SYNC      2
#define BCDATA    3
#define BCCHKSUM  4
#define DLHEADER  5
#define DLUID     6
#define DLDATA    7
#define DLCHKSUM  8
#define SWITCH2RX 9

#define ULHEADER 0
#define ULDATA   1
#define ULCHKSUM 2

#define IDLE     0
#define ONTIME   1
#define OVERTIME 2

#define OVER_BOOK_CARD_NUMBER 255
#define OK                    0
#define ERR_OVERTIME          1

uint8_t UARTMRemoP_reg(UARTMRemoPStr_t* Str_p, RemoBFStr_t* ULBFStr_p,
                       RemoBFStr_t* DLBFStr_p, RemoBFStr_t* BCBFStr_p) {
    if (Str_p->CardNum < Str_p->CardMAX) {
        Str_p->DLRemoBF_p[Str_p->CardNum] = DLBFStr_p;
        Str_p->ULRemoBF_p[Str_p->CardNum] = ULBFStr_p;
        if (Str_p->BCRemoBF_p == 0) {
            Str_p->BCRemoBF_p = BCBFStr_p;
        }
        Str_p->CardNum++;
        return (Str_p->CardNum - 1);
    }
    else {
        return (OVER_BOOK_CARD_NUMBER);
    }
}

uint8_t UARTMRemoPTX_step(void* void_p) {
    UARTMRemoPStr_t* Str_p = (UARTMRemoPStr_t*)void_p;
    UARTOpStr_t* UOpStr_p  = Str_p->UartOp_p;
    uint8_t TXData;
    int8_t Resid = 0;
    switch (Str_p->TXState) {
        case BCHEADER:
            REGFPT(UOpStr_p->TXIntEnReg_p, UOpStr_p->TXIntEnMask,
                   UOpStr_p->TXIntEnShift, 1);
            REGFPT(UOpStr_p->TXRXSwiReg_p, UOpStr_p->TXRXSwiMask,
                   UOpStr_p->TXRXSwiShift, 1);
            TXData        = Str_p->Header;
            Str_p->ChkSum = TXData;
            REGPUT(UOpStr_p->DataReg_p, 1, &TXData);
            Str_p->TXState = BCUID;
            break;
        case BCUID:
            TXData = 0;
            REGPUT(UOpStr_p->DataReg_p, 1, &TXData);
            Str_p->TXState = SYNC;
            Str_p->ChkSum  = Str_p->ChkSum + TXData;
            break;
        case SYNC:
            TXData = Str_p->SysClockStr_p->Counter;
            REGPUT(UOpStr_p->DataReg_p, 1, &TXData);
            Str_p->ChkSum = Str_p->ChkSum + TXData;
            if (Str_p->BCRemoBF_p == 0) {
                Str_p->TXState = BCCHKSUM;
            }
            else {
                Str_p->TXState = BCDATA;
            }
            break;
        case BCDATA:
            Resid         = RemoBF_get(Str_p->BCRemoBF_p, 255, &TXData);
            Str_p->ChkSum = Str_p->ChkSum + TXData;
            REGPUT(UOpStr_p->DataReg_p, 1, &TXData);
            if (Resid == 0) {
                Str_p->TXState = BCCHKSUM;
            }
            break;
        case BCCHKSUM:
            TXData = Str_p->ChkSum;
            REGPUT(UOpStr_p->DataReg_p, 1, &TXData);
            Str_p->TXState   = DLHEADER;
            Str_p->CardIndex = 0;
            break;
        case DLHEADER:
            TXData        = Str_p->Header;
            Str_p->ChkSum = TXData;
            REGPUT(UOpStr_p->DataReg_p, 1, &TXData);
            Str_p->TXState = DLUID;
            Str_p->UID     = Str_p->CardIndex + 1;
            break;
        case DLUID:
            TXData = Str_p->UID;
            REGPUT(UOpStr_p->DataReg_p, 1, &TXData);
            Str_p->ChkSum = Str_p->ChkSum + TXData;
            if (Str_p->DLRemoBF_p[Str_p->CardIndex] == 0) {
                Str_p->TXState = DLCHKSUM;
            }
            else {
                Str_p->TXState = DLDATA;
            }
            break;
        case DLDATA:
            Resid =
                RemoBF_get(Str_p->DLRemoBF_p[Str_p->CardIndex], 255, &TXData);
            REGPUT(UOpStr_p->DataReg_p, 1, &TXData);
            Str_p->ChkSum = Str_p->ChkSum + TXData;
            if (Resid == 0)
                Str_p->TXState = DLCHKSUM;
            break;
        case DLCHKSUM:
            TXData = Str_p->ChkSum;
            REGPUT(UOpStr_p->DataReg_p, 1, &TXData);
            Str_p->TXState = SWITCH2RX;
            break;
        case SWITCH2RX:
            if (Str_p->CardIndex == Str_p->CardNum - 1) {
                Str_p->TXState = BCHEADER;
            }
            else {
                Str_p->TXState = DLHEADER;
            }
            REGFPT(UOpStr_p->TXIntEnReg_p, UOpStr_p->TXIntEnMask,
                   UOpStr_p->TXIntEnShift, 0);
            REGFPT(UOpStr_p->TXRXSwiReg_p, UOpStr_p->TXRXSwiMask,
                   UOpStr_p->TXRXSwiShift, 0);
            REGFPT(UOpStr_p->RXIntEnReg_p, UOpStr_p->RXIntEnMask,
                   UOpStr_p->RXIntEnShift, 1);
            REGFPT(UOpStr_p->RXEnReg_p, UOpStr_p->RXEnMask, UOpStr_p->RXEnShift,
                   1);
            UARTMRemoPTO_step(Str_p);
            break;
    }
    return OK;
}

uint8_t UARTMRemoPRX_step(void* void_p) {
    UARTMRemoPStr_t* Str_p = (UARTMRemoPStr_t*)void_p;
    UARTOpStr_t* UOpStr_p  = Str_p->UartOp_p;
    uint8_t RXData;
    if (Str_p->TOState != OVERTIME) {
        REGGET(UOpStr_p->DataReg_p, 1, &RXData);
        Str_p->TimCount = Str_p->TimMax;
    }
    else {
        Str_p->RXState = ULHEADER;
        RemoBF_clr(Str_p->ULRemoBF_p[Str_p->CardIndex]);
        UARTMRemoPTO_step(Str_p);
        REGFPT(UOpStr_p->TXIntEnReg_p, UOpStr_p->TXIntEnMask,
               UOpStr_p->TXIntEnShift, 1);
        REGFPT(UOpStr_p->RXEnReg_p, UOpStr_p->RXEnMask, UOpStr_p->RXEnShift, 0);
        REGFPT(UOpStr_p->RXIntEnReg_p, UOpStr_p->RXIntEnMask,
               UOpStr_p->RXIntEnShift, 0);
        REGFPT(UOpStr_p->TXRXSwiReg_p, UOpStr_p->TXRXSwiMask,
               UOpStr_p->TXRXSwiShift, 1);
        if (Str_p->CardIndex < Str_p->CardNum - 1) {
            Str_p->CardIndex++;
            UARTMRemoPTX_step(Str_p);
        }
        return ERR_OVERTIME;
    }
    switch (Str_p->RXState) {
        case ULHEADER:
            Str_p->ChkSum = RXData;
            if (RXData == Str_p->ULHeader) {
                if (Str_p->ULRemoBF_p[Str_p->CardIndex] == 0) {
                    Str_p->RXState = ULCHKSUM;
                }
                else {
                    Str_p->RXState = ULDATA;
                }
            }
            break;
        case ULDATA:
            Str_p->ChkSum = Str_p->ChkSum + RXData;
            int8_t Resid =
                RemoBF_temp(Str_p->ULRemoBF_p[Str_p->CardIndex], 255, RXData);
            if (Resid == 0) {
                Str_p->RXState = ULCHKSUM;
            }
            break;
        case ULCHKSUM:
            UARTMRemoPTO_step(Str_p);
            if (RXData == Str_p->ChkSum) {
                TRIG_NEXT_TASK(Str_p->DDTaskID);
                Str_p->DDCardIndex = Str_p->CardIndex;
            }
            else {
                RemoBF_clr(Str_p->ULRemoBF_p[Str_p->CardIndex]);
            }
            Str_p->RXState = ULHEADER;
            Str_p->CardIndex++;
            REGFPT(UOpStr_p->RXEnReg_p, UOpStr_p->RXEnMask, UOpStr_p->RXEnShift,
                   0);
            REGFPT(UOpStr_p->RXIntEnReg_p, UOpStr_p->RXIntEnMask,
                   UOpStr_p->RXIntEnShift, 0);
            if (Str_p->CardIndex < Str_p->CardNum) {
                REGFPT(UOpStr_p->TXIntEnReg_p, UOpStr_p->TXIntEnMask,
                       UOpStr_p->TXIntEnShift, 1);
                REGFPT(UOpStr_p->TXRXSwiReg_p, UOpStr_p->TXRXSwiMask,
                       UOpStr_p->TXRXSwiShift, 1);
                UARTMRemoPTX_step(Str_p);
            }
            break;
    }
    return OK;
}

uint8_t UARTMRemoPDD_step(void* void_p) {
    UARTMRemoPStr_t* Str_p = (UARTMRemoPStr_t*)void_p;
    RemoBF_put(Str_p->ULRemoBF_p[Str_p->DDCardIndex], 255);
    return OK;
}

uint8_t UARTMRemoPTO_step(void* void_p) {
    UARTMRemoPStr_t* Str_p = (UARTMRemoPStr_t*)void_p;
    switch (Str_p->TOState) {
        case IDLE:
            Str_p->TOState  = ONTIME;
            Str_p->TimCount = Str_p->TimMax;
            HWInt_en(Str_p->TOIntStr_p, Str_p->TOTaskID, 1);
            break;
        case ONTIME:
            Str_p->TimCount = Str_p->TimCount - 1;
            if (Str_p->RXState == ULCHKSUM) {
                Str_p->TOState = IDLE;
                HWInt_en(Str_p->TOIntStr_p, Str_p->TOTaskID, 0);
            }
            if (Str_p->TimCount == 0) {
                Str_p->TOState = OVERTIME;
                UARTMRemoPRX_step(Str_p);
                HWInt_en(Str_p->TOIntStr_p, Str_p->TOTaskID, 0);
            }
            break;
        case OVERTIME:
            if (Str_p->RXState == ULHEADER) {
                Str_p->TOState = IDLE;
            }
            break;
    }
    return OK;
}
