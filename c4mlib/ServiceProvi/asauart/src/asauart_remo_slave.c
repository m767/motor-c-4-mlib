/**
 * @file asauart_remo_master.c
 * @author kung, sjchuang
 * @date 2021.06.20
 * @brief 實現 ASA UART Slave 定週期通訊函式
 *
 */

#include "c4mlib/ServiceProvi/asauart/src/asauart_remo_slave.h"

#include "c4mlib/C4MBios/debug/src/debug.h"

#define DLHEADER 0
#define DLUID    1
#define SYNC     2
#define BCDATA   3
#define BCCHKSUM 4
#define DLDATA   5
#define DLCHKSUM 6

#define ULHEADER  0
#define ULDATA    1
#define ULCHKSUM  2
#define SWITCH2RX 3

#define IDLE     0
#define ONTIME   1
#define OVERTIME 2

#define OK           0
#define ERR_OVERTIME 1

uint8_t UARTSRemoPRX_step(void* void_p) {
    UARTSRemoPStr_t* Str_p = (UARTSRemoPStr_t*)void_p;
    UARTOpStr_t* UOpStr_p  = Str_p->UartOp_p;
    TIMOpStr_t* TOpStr_p   = Str_p->TimeOp_p;
    uint8_t Resid          = 0;
    uint8_t RXData;

    if (Str_p->TOState != OVERTIME) {
        REGGET(UOpStr_p->DataReg_p, 1, &RXData);
        Str_p->TimCount = Str_p->TimMax;
    }
    else {
        Str_p->RXState = DLHEADER;
        UARTSRemoPTO_step(void_p);
        if (Str_p->RXState == BCDATA) {
            RemoBF_clr(Str_p->BCRemoBF_p);
        }
        if (Str_p->RXState == DLDATA) {
            RemoBF_clr(Str_p->DLRemoBF_p);
        }
        return ERR_OVERTIME;
    }
    switch (Str_p->RXState) {
        case DLHEADER:
            if (RXData == Str_p->Header) {
                Str_p->ChkSum  = RXData;
                Str_p->RXState = DLUID;
                UARTSRemoPTO_step(void_p);
            }
            break;
        case DLUID:
            Str_p->ChkSum = Str_p->ChkSum + RXData;
            if (RXData == 0) {
                Str_p->RXState = SYNC;
            }
            else if (RXData == *(Str_p->UID_p)) {
                if (Str_p->DLRemoBF_p == 0) {
                    Str_p->RXState = DLCHKSUM;
                }
                else {
                    Str_p->RXState = DLDATA;
                }
            }
            else {
                Str_p->RXState = DLHEADER;
            }
            break;
        case SYNC:
            Str_p->ChkSum = Str_p->ChkSum + RXData;
            if (Str_p->SyncState == 0) {
                Str_p->SyncTemp = RXData;
            }
            else {
                if (RXData == Str_p->SyncPhase) {
                    REGPUT(TOpStr_p->CountReg_p, TOpStr_p->CountBytes, 0);
                    Str_p->SysClockStr_p->Counter = RXData;
                }
            }
            if (Str_p->BCRemoBF_p == 0) {
                Str_p->RXState = BCCHKSUM;
            }
            else {
                Str_p->RXState = BCDATA;
            }
            break;
        case BCDATA:
            Str_p->ChkSum = Str_p->ChkSum + RXData;
            Resid         = RemoBF_temp(Str_p->BCRemoBF_p, 255, RXData);
            if (Resid == 0)
                Str_p->RXState = BCCHKSUM;
            break;
        case BCCHKSUM:
            if (RXData == Str_p->ChkSum) {
                Str_p->BCDDComplete = 0;
                if (Str_p->BCRemoBF_p != 0) {
                    TRIG_NEXT_TASK(Str_p->DDTaskID);
                }
                if (Str_p->SyncState == 0) {
                    Str_p->SyncPhase = Str_p->SyncTemp;
                    Str_p->SyncState = 1;
                }
            }
            else {
                RemoBF_clr(Str_p->BCRemoBF_p);
            }
            Str_p->RXState = DLHEADER;
            break;
        case DLDATA:
            Str_p->ChkSum = Str_p->ChkSum + RXData;
            Resid         = RemoBF_temp(Str_p->DLRemoBF_p, 255, RXData);
            if (Resid == 0)
                Str_p->RXState = DLCHKSUM;
            break;
        case DLCHKSUM:
            if (RXData == Str_p->ChkSum) {
                Str_p->BCDDComplete = 1;
                if (Str_p->DLRemoBF_p != 0) {
                    TRIG_NEXT_TASK(Str_p->DDTaskID);
                }
            }
            else {
                RemoBF_clr(Str_p->DLRemoBF_p);
            }
            Str_p->ChkSum = 0;
            UARTSRemoPTO_step(void_p);
            Str_p->RXState = DLHEADER;
            REGFPT(UOpStr_p->TXEnReg_p, UOpStr_p->TXEnMask, UOpStr_p->TXEnShift,
                   1);
            REGFPT(UOpStr_p->TXIntEnReg_p, UOpStr_p->TXIntEnMask,
                   UOpStr_p->TXIntEnShift, 1);
            REGFPT(UOpStr_p->RXIntEnReg_p, UOpStr_p->RXIntEnMask,
                   UOpStr_p->RXIntEnShift, 0);
            REGFPT(UOpStr_p->TXRXSwiReg_p, UOpStr_p->TXRXSwiMask,
                   UOpStr_p->TXRXSwiShift, 1);
            UARTSRemoPTX_step(void_p);
            break;
    }
    return OK;
}

uint8_t UARTSRemoPTX_step(void* void_p) {
    UARTSRemoPStr_t* Str_p = (UARTSRemoPStr_t*)void_p;
    UARTOpStr_t* UOpStr_p  = Str_p->UartOp_p;
    uint8_t TXData         = 0;
    switch (Str_p->TXState) {
        case ULHEADER:
            TXData        = Str_p->ULHeader;
            Str_p->ChkSum = TXData;
            REGPUT(UOpStr_p->DataReg_p, 1, &TXData);
            if (Str_p->ULRemoBF_p == 0) {
                Str_p->TXState = ULCHKSUM;
            }
            else {
                Str_p->TXState = ULDATA;
            }
            break;
        case ULDATA: {
            uint8_t Resid = RemoBF_get(Str_p->ULRemoBF_p, 255, &TXData);
            Str_p->ChkSum = Str_p->ChkSum + TXData;
            REGPUT(UOpStr_p->DataReg_p, 1, &TXData);
            if (Resid == 0) {
                Str_p->TXState = ULCHKSUM;
            }
            break;
        }
        case ULCHKSUM:
            TXData = Str_p->ChkSum;
            REGPUT(UOpStr_p->DataReg_p, 1, &TXData);
            Str_p->TXState = SWITCH2RX;
            break;
        case SWITCH2RX:
            Str_p->TXState = ULHEADER;
            REGFPT(UOpStr_p->TXIntEnReg_p, UOpStr_p->TXIntEnMask,
                   UOpStr_p->TXIntEnShift, 0);
            REGFPT(UOpStr_p->TXEnReg_p, UOpStr_p->TXEnMask, UOpStr_p->TXEnShift,
                   0);
            REGFPT(UOpStr_p->RXIntEnReg_p, UOpStr_p->RXIntEnMask,
                   UOpStr_p->RXIntEnShift, 1);
            REGFPT(UOpStr_p->TXRXSwiReg_p, UOpStr_p->TXRXSwiMask,
                   UOpStr_p->TXRXSwiShift, 0);
            break;
    }
    return OK;
}

uint8_t UARTSRemoPDD_step(void* void_p) {
    UARTSRemoPStr_t* Str_p = (UARTSRemoPStr_t*)void_p;
    if (Str_p->BCDDComplete != 1) {
        if (Str_p->BCRemoBF_p != 0) {
            RemoBF_put(Str_p->BCRemoBF_p, 255);
        }
        Str_p->BCDDComplete = 1;
    }
    else {
        if (Str_p->DLRemoBF_p != NULL) {
            RemoBF_put(Str_p->DLRemoBF_p, 255);
        }
        Str_p->BCDDComplete = 0;
    }
    return OK;
}

uint8_t UARTSRemoPTO_step(void* void_p) {
    UARTSRemoPStr_t* Str_p = (UARTSRemoPStr_t*)void_p;
    switch (Str_p->TOState) {
        case IDLE:
            if (Str_p->RXState != DLHEADER) {
                Str_p->TOState  = ONTIME;
                Str_p->TimCount = Str_p->TimMax;
                HWInt_en(Str_p->TOIntStr_p, Str_p->TOTaskID, 1);
            }
            break;
        case ONTIME:
            Str_p->TimCount = Str_p->TimCount - 1;
            if (Str_p->RXState == DLCHKSUM) {
                Str_p->TOState = IDLE;
                HWInt_en(Str_p->TOIntStr_p, Str_p->TOTaskID, 0);
            }
            if (Str_p->TimCount == 0) {
                Str_p->TOState = OVERTIME;
                UARTSRemoPRX_step(Str_p);
                HWInt_en(Str_p->TOIntStr_p, Str_p->TOTaskID, 0);
            }
            break;
        case OVERTIME:
            if (Str_p->RXState == DLHEADER) {
                Str_p->TOState = IDLE;
            }
            break;
    }
    return OK;
}
