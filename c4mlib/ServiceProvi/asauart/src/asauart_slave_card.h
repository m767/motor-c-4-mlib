/**
 * @file asauart_slave.h
 * @author s915888
 * @date 2021.05.25
 * @brief 放置UART0通訊函式以及相關marco
 */

#ifndef C4MLIB_ASAUART_ASAUART_SLAVE_H
#define C4MLIB_ASAUART_ASAUART_SLAVE_H

#include "c4mlib/C4MBios/hardwareset/src/m128/uart_set.h"
#include "c4mlib/ServiceProvi/databuffer/src/remobf.h"
#include "c4mlib/ServiceProvi/hwimp/src/hardware_interrupt.h"

/**
 * @defgroup asauart0_func
 * @defgroup asauart0_struct
 * @defgroup asauart0_macro
 */
#define ASAUART_CMD_HEADER 0xAA  ///< @ingroup asauart_macro
#define ASAUART_RSP_HEADER 0xAB  ///< @ingroup asauart_macro

/**
 * @brief 介面卡送收訊結構體
 * @param UARTOpStr_p The UART Operation Structure
 * @param Header Packet Header
 * @param UID_p UART ID的指標
 * @param RemoBF_p Pointer to Table of Remote read/writ variables
 * @param TOIntStr_p pointer 2 the TimeOut Timer sharer structure
 * @param DDTaskId： Taskid of Data distributor
 * @param TOTaskID： Taskid of Data TimeOut
 * @param TXTaskId： Taskid of TX
 * @param RXTaskID： Taskid of RX
 * @param TrmORec Transmit or Receive
 * @param RegAdd    遠端讀寫暫存器(Register)的位址或控制旗標(control flag)。
 * @param Bytes     待送資料位元組數。
 * @param ChkSum packet values
 * @param Resp packet values
 * @param TXState： state machine state for TX
 * @param RXState： state machine state for RX
 * @param TOState： state machine state for TimeOut
 * @param TimCount Time Out Timer Count, Transmit or Receive
 * @param TimMax Time Out Maximum
 */
typedef struct {
    UARTOpStr_t* UARTOpStr_p;
    uint8_t Header;
    uint8_t* UID_p;
    RemoBFStr_t* RemoBF_p;
    HWIntStr_t* TOIntStr_p;
    uint8_t DDTaskID, TOTaskID, TxTaskID, RxTaskID;
    uint8_t TrmORec;
    uint8_t RegAdd, Byte, ChkSum, Resp;
    uint8_t TXState, RXState, TOState;
    uint32_t TimCount;
    uint32_t TimMax;
} UARTSRemoStr_t;

/**
 * @brief ASA UART Slave0 結構初始化巨集
 * @ingroup asauart0_macro
 */
#define UART1SREGPARA                                                          \
    {                                                                          \
        .DataReg_p = &UDR1, .UARTDataBytes = 1, .TXRXSwiReg_p = &PORTF,        \
        .TXRXSwiMask = 0x01, .TXRXSwiShift = 4, .TXEnReg_p = &UCSR1B,          \
        .TXEnMask = 0x10, .TXEnShift = 3, .RXEnReg_p = &UCSR1B,                \
        .RXEnMask = 0x40, .RXEnShift = 4, .TXIntEnReg_p = &UCSR1B,             \
        .TXIntEnMask = 0x08, .TXIntEnShift = 3, .RXIntEnReg_p = &UCSR1B,       \
        .RXIntEnMask = 0x80, .RXIntEnShift = 7, .TXIntClrReg_p = &UCSR1A,      \
        .TXIntClrMask = 0x40, .TXIntClrShift = 6, .RXIntClrReg_p = &UCSR1A,    \
        .RXIntClrMask = 0x80, .RXIntClrShift = 7, .TXIntFReg_p = &UCSR1A,      \
        .TXIntFMask = 0X20, .TXIntFShift = 5, .RXIntFReg_p = &UCSR1A,          \
        .RXIntFMask = 0x80, .RXIntFShift = 7,                                  \
    }
#define UARTS1REMOINI                                                          \
    { .Header = ASAUART_CMD_HEADER, .TimMax = 25400 }
/**
 * @brief
 * 供使用者將函式登錄硬體中斷並取得編號，並且將函式登錄進排程以及將結構體連結至實體空間
 * @ingroup asauart0_macro
 */
#define UARTSRemoSTR_LAY(UARTREMOSTR, TXINTSTR, RXINTSTR, TOINTSTR,            \
                         PIPELINESTR, REMOBF, UARTOpStr, UARTIDADD)            \
    {                                                                          \
        UARTREMOSTR.TxTaskID =                                                 \
            HWInt_reg(&TXINTSTR, &UARTSRemoTX_step, &UARTREMOSTR);             \
        UARTREMOSTR.RxTaskID =                                                 \
            HWInt_reg(&RXINTSTR, &UARTSRemoRX_step, &UARTREMOSTR);             \
        UARTREMOSTR.TOTaskID =                                                 \
            HWInt_reg(&TOINTSTR, &UARTSRemoTO_step, &UARTREMOSTR);             \
        UARTREMOSTR.DDTaskID =                                                 \
            Pipeline_reg(&PIPELINESTR, &UARTSRemoDD_step, &UARTREMOSTR, 0);    \
        UARTREMOSTR.TOIntStr_p  = &TOINTSTR;                                   \
        UARTREMOSTR.RemoBF_p    = &REMOBF;                                     \
        UARTREMOSTR.UARTOpStr_p = &UARTOpStr;                                  \
        UARTREMOSTR.UID_p       = UARTIDADD;                                   \
    }

/**
 * @brief ASA UART REMO 多位元組接收函式。
 *
 * @ingroup asauart_func
 * @param void_p    Uart Remo結構指標
 * @return          無
 */
uint8_t UARTSRemoRX_step(void* void_p);
/**
 * @brief ASA UART REMO 資料更新函式。
 *
 * @ingroup asauart_func
 * @param void_p    Uart Remo結構指標
 * @return          無
 */
uint8_t UARTSRemoDD_step(void* void_p);
/**
 * @brief ASA UART REMO 多位元組傳送函式。
 *
 * @ingroup asauart_func
 * @param void_p    Uart Remo結構指標
 * @return          無
 */
uint8_t UARTSRemoTX_step(void* void_p);
/**
 * @brief ASA UART REMO 逾時計數函式。
 *
 * @ingroup asauart_func
 * @param void_p    Uart Remo結構指標
 * @return          無
 */
uint8_t UARTSRemoTO_step(void* void_p);
/* Public Section End */
#endif  // C4MLIB_ASAUART_ASAUART_SLAVE_H
