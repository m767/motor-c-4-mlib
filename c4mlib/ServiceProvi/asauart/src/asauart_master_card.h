/**
 * @file asauart_master_card.h
 * @author s915888
 * @date 2021.05.25
 * @brief 放置 ASA UART Master 0 通訊函式及Marco
 */

#ifndef C4MLIB_ASAUART_MASTER_CARD_H
#define C4MLIB_ASAUART_MASTER_CARD_H
#include "c4mlib/C4MBios/hardwareset/src/m128/uart_set.h"
#include "c4mlib/C4MBios/macro/src/std_res.h"

#include <stdint.h>

/**
 * @defgroup asauartS0_func
 * @defgroup asauartS0_struct
 * @defgroup asauartS0_macro
 */

#define ASAUART_CMD_HEADER 0xAA  ///< @ingroup asauart_macro
#define ASAUART_RSP_HEADER 0xAB  ///< @ingroup asauart_macro
/* Public Section Start */
#define UART1REGPARA                                                           \
    {                                                                          \
        .DataReg_p = &UDR1, .UARTDataBytes = 1, .TXRXSwiReg_p = &PORTF,        \
        .TXRXSwiMask = 0x01, .TXRXSwiShift = 4, .TXEnReg_p = &UCSR1B,          \
        .TXEnMask = 0x10, .TXEnShift = 3, .RXEnReg_p = &UCSR1B,                \
        .RXEnMask = 0x40, .RXEnShift = 4, .TXIntEnReg_p = &UCSR1B,             \
        .TXIntEnMask = 0x40, .TXIntEnShift = 6, .RXIntEnReg_p = &UCSR1B,       \
        .RXIntEnMask = 0x80, .RXIntEnShift = 7, .TXIntClrReg_p = &UCSR1A,      \
        .TXIntClrMask = 0x40, .TXIntClrShift = 6, .RXIntClrReg_p = &UCSR1A,    \
        .RXIntClrMask = 0x80, .RXIntClrShift = 7, .TXIntFReg_p = &UCSR1A,      \
        .TXIntFMask = 0X20, .TXIntFShift = 5, .RXIntFReg_p = &UCSR1A,          \
        .RXIntFMask = 0x80, .RXIntFShift = 7,                                  \
    }
#define UARTM_INI                                                              \
    { .TimMax = 500000, .Header = ASAUART_CMD_HEADER }
#define UARTM_LAY(UARTMSTR, UARTOPSTR)                                         \
    { UARTMSTR.UARTOpStr_p = &UARTOPSTR; }
/**
 * @brief UART監控參數結構體
 * @param UARTOpStr_p The UART Operation Structure
 * @param Header Time Out Maximum
 * @param TimMax Packet Header
 */
typedef struct {
    UARTOpStr_t* UARTOpStr_p;
    uint8_t Header;
    uint32_t TimMax;
    uint8_t CardId;
    uint8_t Mode;
} UARTMStr_t;

#define UARTM0STR_LAY(UARTREMOSTR, UARTOPSTR)                                  \
    {                                                                          \
        UARTREMOSTR.DataReg_p   = UARTOPSTR.UARTDataReg_p;                     \
        UARTREMOSTR.TXIntFReg_p = UARTOPSTR.TXIntFReg_p;                       \
        UARTREMOSTR.TXIntFMask  = UARTOPSTR.TXIntFMask;                        \
        UARTREMOSTR.TXIntFShift = UARTOPSTR.TXIntFShift;                       \
        UARTREMOSTR.RXIntFReg_p = UARTOPSTR.RXIntFReg_p;                       \
        UARTREMOSTR.RXIntFMask  = UARTOPSTR.RXIntFMask;                        \
        UARTREMOSTR.RXIntFShift = UARTOPSTR.RXIntFShift;                       \
    }
/**
 * @brief
 *
 * @param void_p 預先設定的UARTMStr_t型態變數
 * @param RegAdd 要設定的暫存器
 * @param Bytes  送入資料的大小
 * @param Data_p 送入資料指標
 * @return  char    錯誤代碼：
 *                   - 0： 通訊成功。
 *                   - 255： Timeout。
 *                   - 1~8： 對應State錯誤。
 * ASA 作為 Master端 透過UART通訊傳送資料。
 * Uart Master端 傳送給 Slave端，依照封包不同，分作2種 Mode ，如以下所示：
 *  - Mode 0：
 *      封包組成 為
 *      [Header]、[CardId]、[RegAdd]、[Bytes]、[Data]、[ChkSum]、[Rec(Header)]、[Rec(Resp)]、[Rec(ChkSum)]。
 *      先後傳送資料 [HEADER
 *      (0xAA)]、[CardId]、[RegAdd]、[Bytes]，隨後再根據
 *      資料筆數[Bytes]由低到高丟出資料，傳完資料後會傳送所有已傳送資料加總
 *      [checksum] 給Slave端驗證，Slave會回傳當作 [HEADER(0xAB)]
 *      成功資訊或錯誤資訊。
 *  - Mode 3：
 *      封包組成 為 [Data]，單純送收資料。
 *      若Bytes=0則為ATcommand模式，並且先送資料再從Slave接收資料
 */
uint8_t UARTM0_trm(void* void_p, uint8_t RegAdd, uint8_t Bytes, void* Data_p);
/**
 * @brief
 *
 * @param void_p 預先設定的UARTMStr_t型態變數
 * @param RegAdd 要設定的暫存器
 * @param Bytes  讀取資料的大小
 * @param Data_p 讀取資料指標
 * @return  char    錯誤代碼：
 *                   - 0： 通訊成功。
 *                   - 255： Timeout。
 *                   - 1~8： 對應State錯誤。
 * ASA 作為 Master端 透過UART通訊傳送資料。
 * Uart Master端 傳送給 Slave端，依照封包不同，分作2種 Mode ，如以下所示：
 *  - Mode 0：
 *      封包組成 為
 *      [Header]、[CardId]、[RegAdd]、[Bytes]、[Data]、[ChkSum]、[Rec(Header)]、[Rec(Resp)]、[Rec(ChkSum)]。
 *      先後傳送資料 [HEADER(0xAA)]、[CardId]、[RegAdd]、[Bytes]，隨後再根據
 *      資料筆數[Bytes]由低到高接收資料，接收完資料後會傳送所有已傳送資料加總
 *      [checksum] 給Slave端驗證，Slave會回傳當作 [HEADER(0xAB)]
 *      成功資訊或錯誤資訊。
 *  - Mode 3：
 *      封包組成 為 [Data]，單純送收資料。
 *      若Bytes=0則為ATcommand模式，並且先送資料再從Slave接收資料
 */
uint8_t UARTM0_rec(void* void_p, uint8_t RegAdd, uint8_t Bytes, void* Data_p);
/**
 * @brief
 *
 * @param void_p 預先設定的UARTMStr_t型態變數
 * @param RegAdd 要設定的暫存器
 * @param Mask   送入資料的Mask
 * @param shift  送入資料的平移
 * @param Data   送入資料
 * @return  char    錯誤代碼：
 *                   - 0： 通訊成功。
 *                   - 255： Timeout。
 * 做的事跟UARTM0_trm一樣，只是送出的資料經過MASK與SHIFT
 */
uint8_t UARTM0_ftm(void* void_p, uint8_t RegAdd, uint8_t Mask, uint8_t shift,
                   uint8_t Data);
/**
 * @brief
 *
 * @param void_p 預先設定的UARTMStr_t型態變數
 * @param RegAdd 要設定的暫存器
 * @param Mask   接收資料的Mask
 * @param shift  接收資料的平移
 * @param Data_p   接收資料指標
 * @return  char    錯誤代碼：
 *                   - 0： 通訊成功。
 *                   - 255： Timeout。
 *                   - 1~8： 對應State錯誤。
 * 做的事跟UARTM0_rec一樣，只是接收的資料經過MASK與SHIFT
 */
uint8_t UARTM0_frc(void* void_p, uint8_t RegAdd, uint8_t Mask, uint8_t shift,
                   uint8_t* Data_p);
/* Public Section End */
#endif  // C4MLIB_ASAUART_MASTER_UART0_H
