/**
 * @file asauart_remo_master.h
 * @author kung, sjchuang
 * @date 2021.06.12
 * @brief 放置 ASA UART Slave 定週期通訊結構體、函式及Marco
 *
 */

#ifndef C4MLIB_ASAUART_REMO_SLAVE_H
#define C4MLIB_ASAUART_REMO_SLAVE_H
#include "c4mlib/C4MBios/hardwareset/src/m128/uart_set.h"
#include "c4mlib/C4MBios/macro/src/bits_op.h"
#include "c4mlib/ServiceProvi/databuffer/src/remobf.h"
#include "c4mlib/ServiceProvi/hwimp/src/hardware_interrupt.h"
#include "c4mlib/ServiceProvi/interrupt/src/frequency_set.h"
#include "c4mlib/ServiceProvi/pipeline/src/pipeline_executor.h"
#include "c4mlib/ServiceProvi/pipeline/src/pipeline_imp.h"

#include <stdint.h>

/* Public Section Start */

/**
 * @brief 介面卡送收訊結構體 UART Slave Remo Periodic Structure
 * @param UartOp_p：Uart HardWare operation structure pointer
 * @param TimeOp_p：Timer Counter operation structure pointer
 * @param TimMax： Time Out Maximum
 * @param Header： Packet Header
 * @param ULHeader：UpLink Packet Header
 * @param UID_p：Pointer to Uart ID of this Card(1,2,3...)
 * @param BCRemoBF_p： Pointer BroadCast Remo Buffer
 * @param DLRemoBF_p： Pointer DownLink Remo Buffer
 * @param ULRemoBF_p： Pointer UpLink Remo Buffer
 * @param TOIntStr_p： Pointer 2 the TimeOut Timer sharer structure
 * @param DDTaskId： Taskid of Data distributor
 * @param TOTaskID： Taskid of TimeOut
 * @param TimCount： Time Out Timer Count, Transmit or Receive
 * @param ChkSum： packet values
 * @param TXState： state machine state for TX
 * @param RXState： state machine state for RX
 * @param TOState： state machine state for TimeOut
 * @param BCDDComplete：判斷要處理BCRemoBF或DLRemoBF
 * @param SyncState：
 * @param SyncTemp：
 * @param SyncStateOut_p：
 * @param SyncPhase：
 * @param SysClockStr_p：
 * @param TXTaskID：Taskid of TX
 * @param RXTaskID：Taskid of RX
 */
typedef struct {
    UARTOpStr_t* UartOp_p;
    TIMOpStr_t* TimeOp_p;
    uint8_t TimMax;
    uint8_t Header;
    uint8_t ULHeader;
    uint8_t* UID_p;
    RemoBFStr_t* BCRemoBF_p;
    RemoBFStr_t* DLRemoBF_p;
    RemoBFStr_t* ULRemoBF_p;
    HWIntStr_t* TOIntStr_p;
    uint8_t DDTaskID;
    uint8_t TOTaskID;
    uint8_t TimCount;
    uint8_t ChkSum;
    uint8_t TXState;
    uint8_t RXState;
    uint8_t TOState;
    uint8_t BCDDComplete;
    uint8_t SyncState;
    uint8_t SyncTemp;
    uint8_t* SyncStateOut_p;
    uint8_t SyncPhase;
    FreqReduStr_t* SysClockStr_p;
    uint8_t TXTaskID;
    uint8_t RXTaskID;
} UARTSRemoPStr_t;

/**
 * @brief 介面卡定週期送收結構體硬體相關欄初值巨集
 *
 */
#define UARTSREMOPINI                                                          \
    {                                                                          \
        .TimMax = 10, .Header = 0xAA, .ULHeader = 0xBB, .SyncState = 0,        \
        .TXState = 0, .RXState = 0, .TOState = 0                               \
    }

/**
 * @brief 介面卡定週期送收佈局巨集
 * @param UARTSREMOPSTR：Structure of this UART Remo RW service task
 * @param BCREMOBF： The BroadCast Remo buffer Structure.
 * @param ULREMOBF： The Up link Remo buffer Structure.
 * @param DLREMOBF： The Down link Remo buffer Structure.
 * @param UARTOPSTR： Operation Structure of Uart Hardware
 * @param TIMEOPSTR： Operation Structure of Timer Hardware
 * @param UARTIDADD： Pointer to the varialbe for Uart Id
 * 最後幾行REGFPT是用來關掉TX，並將TX的腳位設定為In+Pull-up
 * 此設計是希望通道上的多個slave只有自己要送資料時才將TX致能傳資料，其餘時間TX皆為關閉狀態並Pull-up
 */
#define UARTSRemoP_LAY(UARTSREMOPSTR, BCREMOBF, ULREMOBF, DLREMOBF, UARTOPSTR, \
                       TIMEOPSTR, UARTIDADD)                                   \
    UARTSRemoP_str.TXTaskID = HWInt_reg(                                       \
        &UartRemoTXInts_str, &UARTSRemoPTX_step, (void*)&UARTSRemoP_str);      \
    HWInt_en(&UartRemoTXInts_str, UARTSRemoP_str.TXTaskID, 1);                 \
    UARTSRemoP_str.RXTaskID = HWInt_reg(                                       \
        &UartRemoRXInts_str, &UARTSRemoPRX_step, (void*)&UARTSRemoP_str);      \
    HWInt_en(&UartRemoRXInts_str, UARTSRemoP_str.RXTaskID, 1);                 \
    UARTSRemoP_str.TOTaskID =                                                  \
        HWInt_reg(&SysTick_str, &UARTSRemoPTO_step, (void*)&UARTSRemoP_str);   \
    HWInt_en(&SysTick_str, UARTSRemoP_str.TOTaskID, 1);                        \
    UARTSRemoP_str.DDTaskID = Pipeline_reg(                                    \
        &SysPipeline_str, &UARTSRemoPDD_step, (void*)&UARTSRemoP_str, 0);      \
    UARTSREMOPSTR.UartOp_p        = &UARTOPSTR;                                \
    UARTSREMOPSTR.TimeOp_p        = &TIMEOPSTR;                                \
    UARTSRemoP_str.ULRemoBF_p     = (RemoBFStr_t*)(&ULREMOBF);                 \
    UARTSRemoP_str.DLRemoBF_p     = (RemoBFStr_t*)(&DLREMOBF);                 \
    UARTSRemoP_str.BCRemoBF_p     = (RemoBFStr_t*)(&BCREMOBF);                 \
    UARTSREMOPSTR.UID_p           = UARTIDADD;                                 \
    UARTSRemoP_str.SyncStateOut_p = &UARTSRemoP_str.SyncState;                 \
    UARTSRemoP_str.SysClockStr_p  = &SysClock_str;                             \
    UARTSRemoP_str.TOIntStr_p     = &SysTick_str;                              \
    REGFPT(UARTOPSTR.TXEnReg_p, UARTOPSTR.TXEnMask, UARTOPSTR.TXEnShift, 0);   \
    REGFPT(UARTOPSTR.TXIntEnReg_p, UARTOPSTR.TXIntEnMask,                      \
           UARTOPSTR.TXIntEnShift, 0);                                         \
    REGFPT(UARTOPSTR.TXRXSwiReg_p, UARTOPSTR.TXRXSwiMask,                      \
           UARTOPSTR.TXRXSwiShift, 0);

/**
 * @brief 介面卡定週期收訊執行函式
 *
 * @param void_p 原為UARTSRemoPStr_t形態之結構體指標，經無形化之無形指標
 * @return uint8_t 執行如有錯誤會傳回錯誤碼，如無錯誤回傳0。
 *                  -ERR_OVERTIME:1
 */
uint8_t UARTSRemoPRX_step(void* void_p);

/**
 * @brief 介面卡定週期送訊執行函式
 *
 * @param void_p 原為UARTS0RemoPStr_t形態之結構體指標，經無形化之無形指標
 * @return uint8_t 執行如有錯誤會傳回錯誤碼，如無錯誤回傳0。
 */
uint8_t UARTSRemoPTX_step(void* void_p);

/** \
 * @brief 介面卡定週期收訊配送函式 \
 *                                                                                          \
 * @param void_p 原為UARTSRemoStr_t形態之結構體指標，經無形化之無形指標 \
 * @return uint8_t 執行如有錯誤會傳回錯誤碼，如無錯誤回傳0。            \
 */
uint8_t UARTSRemoPDD_step(void* void_p);

/**
 * @brief 介面卡定週期收訊逾時計數函式
 *
 * @param void_p 原為UARTSRemoStr_t形態之結構體指標，經無形化之無形指標
 * @return uint8_t 執行如有錯誤會傳回錯誤碼，如無錯誤回傳0。
 */
uint8_t UARTSRemoPTO_step(void* void_p);

/* Public Section End */

#endif  // C4MLIB_ASAUART_REMO_MASTER_H
