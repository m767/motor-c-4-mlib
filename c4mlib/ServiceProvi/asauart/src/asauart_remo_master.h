/**
 * @file asauart_remo_master.h
 * @author kung, sjchuang
 * @date 2021.06.28
 * @brief 放置 ASA UART Master 定週期通訊結構體、函式及Marco
 *
 */

#ifndef C4MLIB_ASAUART_REMO_MASTER_H
#define C4MLIB_ASAUART_REMO_MASTER_H

#include "c4mlib/C4MBios/hardwareset/src/m128/uart_set.h"
#include "c4mlib/C4MBios/macro/src/bits_op.h"
#include "c4mlib/ServiceProvi/databuffer/src/remobf.h"
#include "c4mlib/ServiceProvi/hwimp/src/hardware_interrupt.h"
#include "c4mlib/ServiceProvi/interrupt/src/frequency_set.h"
#include "c4mlib/ServiceProvi/pipeline/src/pipeline_executor.h"
#include "c4mlib/ServiceProvi/pipeline/src/pipeline_imp.h"

#include <avr/io.h>
#include <stdint.h>

#include "c4mlib/config/uartP.cfg"
/* Public Section Start */

/**
 * @brief 主控電腦定週期遠端讀寫結構體 UART Master Remo Periodic Structure
 * @param TimMax： Time Out Maximum
 * @param Header： Packet Header
 * @param ULHeader：UpLink Packet Header
 * @param CardNum： Card Number
 * @param CardMAX： Maximum number of cards
 * @param BCRemoBF_p： Pointer BroadCast Remo Buffer
 * @param DLRemoBF_p： Pointer DownLink Remo Buffer
 * @param ULRemoBF_p： Pointer UpLink Remo Buffer
 * @param TOIntStr_p： pointer 2 the TimeOut Timer sharer structure
 * @param DDTaskId： Taskid of Data distributor
 * @param TOTaskID： Taskid of Data TimeOut
 * @param TXTaskId： Taskid of TX
 * @param RXTaskID： Taskid of RX
 * @param UID： UART ID of this card
 * @param TimCount： Time Out Timer Count, Transmit or Receive
 * @param CardIndex： packet values
 * @param ChkSum： packet values
 * @param TXState： state machine state for TX
 * @param RXState： state machine state for RX
 * @param TOState： state machine state for TimeOut
 * @param DDCardIndex：data distribution Card Index
 */
typedef struct {
    UARTOpStr_t* UartOp_p;
    uint8_t TimMax;
    uint8_t Header;
    uint8_t ULHeader;
    uint8_t CardNum;
    uint8_t CardMAX;
    RemoBFStr_t* BCRemoBF_p;
    RemoBFStr_t* DLRemoBF_p[CARDMAX];
    RemoBFStr_t* ULRemoBF_p[CARDMAX];
    HWIntStr_t* TOIntStr_p;
    uint8_t DDTaskID;
    uint8_t TOTaskID;
    uint8_t TXTaskID[2];
    uint8_t RXTaskID;
    uint8_t UID;
    uint8_t TimCount;
    uint8_t CardIndex;
    uint8_t ChkSum;
    uint8_t TXState;
    uint8_t RXState;
    uint8_t TOState;
    uint8_t DDCardIndex;
    FreqReduStr_t* SysClockStr_p;
} UARTMRemoPStr_t;

/**
 * @brief 主控電腦定週期遠端讀寫結構體硬體相關欄初值巨集
 *
 */
#define UARTM1REMOPINI                                                         \
    {                                                                          \
        .TimMax = 2, .Header = 0xAA, .ULHeader = 0xBB, .CardNum = 0,           \
        .CardMAX = CARDMAX                                                     \
    }

/**
 * @brief 主控電腦定週期遠端讀寫佈局巨集
 * @param UARTMREMOPSTR: Structure of this UART Remo RW service task
 * @param CARDNUM: The number of UART interface Cards
 * @param TXINTSTR：The UART TX  Interrupt sharor Structure
 * @param RXINTSTR：The UART RX  Interrupt sharor Structure
 * @param TOINTSTR：The time out Timer Interrupt sharor Structure
 * @param FREQREDUSTR：The Frequency Redu Executor Structure.
 * @param PIPELINESTR：The Pipeline Executor Structure
 * @param INTERTABLE ： The Table of Periods of Interval,
 * @param BCRemoBF The Remo buffer Structure.
 */
#define UARTMRemoP_LAY(UARTMREMOSTR, UARTOPSTR)                                \
    UARTMREMOSTR.UartOp_p    = &UARTOPSTR;                                     \
    UARTMREMOSTR.TOIntStr_p  = &SysTick_str;                                   \
    UARTMREMOSTR.TXTaskID[0] = FreqRedu_reg(&SysClock_str, &UARTMRemoPTX_step, \
                                            &UARTMREMOSTR, 1, UARTMEMOPHASE);  \
    UARTMREMOSTR.TXTaskID[1] =                                                 \
        HWInt_reg(&UartRemoIntsTX_str, &UARTMRemoPTX_step, &UARTMREMOSTR);     \
    UARTMREMOSTR.RXTaskID =                                                    \
        HWInt_reg(&UartRemoIntsRX_str, &UARTMRemoPRX_step, &UARTMREMOSTR);     \
    UARTMREMOSTR.TOTaskID =                                                    \
        HWInt_reg(&SysTick_str, &UARTMRemoPTO_step, &UARTMREMOSTR);            \
    UARTMREMOSTR.DDTaskID =                                                    \
        Pipeline_reg(&SysPipeline_str, &UARTMRemoPDD_step, &UARTMREMOSTR, 0);  \
    FreqRedu_en(&SysClock_str, UARTMREMOSTR.TXTaskID[0], ENABLE);              \
    HWInt_en(&UartRemoIntsTX_str, UARTMREMOSTR.TXTaskID[1], ENABLE);           \
    HWInt_en(&UartRemoIntsRX_str, UARTMREMOSTR.RXTaskID, ENABLE);              \
    HWInt_en(&SysTick_str, UARTMREMOSTR.TOTaskID, DISABLE);

/**
 * @brief 主控電腦定週期遠端讀寫緩衝區登錄函式
 *
 * @param Str_p     主控電腦定週期遠端讀寫結構體指標。
 * @param ULBFStr_p 上鏈緩衝區結構體指標，若無上鏈資料請填0。
 * @param DLBFStr_p 下鏈緩衝區結構體指標，若無下鏈資料請填0。
 * @param BCBFStr_p 廣播緩衝區結構體指標，若無廣播資料請填0。
 * @return uint8_t  此對上下鏈緩衝區取得之緩衝區編號。
 *                  -OVER_BOOK_CARD_NUMBER:255

 */
uint8_t UARTMRemoP_reg(UARTMRemoPStr_t* Str_p, RemoBFStr_t* ULBFStr_p,
                       RemoBFStr_t* DLBFStr_p, RemoBFStr_t* BCBFStr_p);

/**
 * @brief 主控電腦定週期遠端寫執行函式
 *
 * @param void_p
 * @return uint8_t
 */
uint8_t UARTMRemoPTX_step(void* void_p);

/**
 * @brief 主控電腦定週期遠端讀執行函式
 *
 * @param void_p 原為UARTMRemoPStr_t形態之結構體指標，經無形化之無形指標
 * @return uint8_t 執行如有錯誤會傳回錯誤碼，如無錯誤回傳0。
 *                  -ERR_OVERTIME:1
 */
uint8_t UARTMRemoPRX_step(void* void_p);

/**
 * @brief 主控電腦定週期收訊配送函式
 *
 * @param void_p 原為UARTSRemoStr_t形態之結構體指標，經無形化之無形指標
 * @return uint8_t 執行如有錯誤會傳回錯誤碼，如無錯誤回傳0。
 */
uint8_t UARTMRemoPDD_step(void* void_p);

/**
 * @brief 主控電腦定週期收訊逾時計數函式
 *
 * @param void_p 原為UARTSRemoStr_t形態之結構體指標，經無形化之無形指標
 * @return uint8_t 執行如有錯誤會傳回錯誤碼，如無錯誤回傳0。
 */
uint8_t UARTMRemoPTO_step(void* void_p);
/* Public Section End */

#endif  // C4MLIB_ASAUART_REMO_MASTER_H
