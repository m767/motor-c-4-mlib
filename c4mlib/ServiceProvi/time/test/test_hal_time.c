#define F_CPU 11059200UL

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/ServiceProvi/time/src/hal_time.h"

#include <avr/interrupt.h>

ISR(TIMER3_COMPA_vect) {
    HAL_tick();
}

// Initialize the TIME3 with CTC mode, interrupt at 1000 Hz
void init_timer() {
    // Pre-scale 1
    // COMA COMB non-inverting mode
    // 1    0
    // CTC with TOP ICRn
    // WGM3 WGM2 WGM1 WGM0
    // 1    1    0    0
    // TCCR3A  [COM3A1 COM3A0 COM3B1 COM3B0 COM3C1 COM3C0 WGM31 WGM30]
    // TCCR3B  [ICNC3 ICES3 �V WGM33 WGM32 CS32 CS31 CS30]

    ICR3 = 11058 * 2;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}

int main() {
    C4M_STDIO_init();
    ASABUS_ID_init();
    HAL_time_init();
    init_timer();

    sei();

    /***** Test the  HAL time component *****/
    printf("======= Test HAL time component =======\n");
    HAL_Time expire_time;
    printf("Setup expired time with 1 sec\n");
    HAL_get_expired_time_ms(1000UL,
                            &expire_time);  // Get current time tick (Unit: 1ms)
    printf("Wait for user input: \n");
    while (
        !(UCSR0A & (1 << RXC0)))  // If there is no RX complete flag, Block here
    {
        if (HAL_timeout_test(expire_time)) {  // Check timeout
            printf("Timeout !\n");
        }
    }
    printf("Without Timeout!\n");

    while (1)
        ;  // Block here
}
