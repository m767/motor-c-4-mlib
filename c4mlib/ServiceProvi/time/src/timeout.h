#ifndef C4MLIB_TIME_TIMEOUT_H
#define C4MLIB_TIME_TIMEOUT_H

#include <stdint.h>

/* Public Section Start */
#define MAX_TIMEOUT_ISR 20

typedef void (*ISRFunc)(void);
/* Public Section End */

// 逾時中斷資料結構

//  逾時中斷資料結構紀錄了:
//  時間限制、計數器、POST位置、逾時中斷函式
/* Public Section Start */
typedef struct {
    volatile uint16_t
        time_limit;  // 時間限制        當計數器上數到該數值時觸發逾時中斷
    volatile uint16_t counter;  // 計數器          計數器固定頻率上數
    volatile uint8_t* p_postSlot;  // POST欄住址 一指標指向POST欄住址，為POST &
                                   // SLOT機制的子元件
    volatile uint8_t enable;  // 禁致能控制      決定本逾時ISR是否致能
    ISRFunc p_ISRFunc;        // 逾時中斷函式    逾時中斷函式指標
} TimeoutISR_t;

typedef struct {
    uint8_t total;  // 紀錄已有多少逾時中斷已註冊
    volatile TimeoutISR_t
        timeoutISR_inst[MAX_TIMEOUT_ISR];  // 紀錄所有已註冊的逾時中斷資料結構
} TimerCntStrType;
/* Public Section End */

// Initialize the TimerCntStrType struct
void TimerCntStr_init(TimerCntStrType* p_TimerCntStr);

// Register TimeoutISR instance
uint8_t Timeout_reg(ISRFunc p_ISRFunc, uint8_t* p_postSlot,
                    uint16_t time_limit);

// Timeout tick, need to called by fixed period
void Timeout_tick();

// Reset timeout
void Timeout_reset(uint8_t timeout_Id);

// Control timeout
void Timeout_ctl(uint8_t timeout_Id, uint8_t enable);

/* Public Section Start */
volatile TimerCntStrType TimerCntStr_inst;
/* Public Section End */

#endif  // C4MLIB_TIME_TIMEOUT_H
