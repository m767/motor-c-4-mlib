/**
 * @file test_scope_untri.c
 * @author LCY
 * @brief 不進行trigger讀寫
 * @version 0.1
 * @date 2020-04-01
 *
 * @copyright Copyright (c) 2020
 *
 */

#include "c4mlib/ServiceProvi/databuffer/src/scope.h"
#include "c4mlib/C4MBios/debug/src/debug.h"

int main() {
    C4M_DEVICE_set();

    SCOPEBUFF_LAY(test_scope, 10, 0);

    uint8_t put_num    = 2;
    uint8_t get_num    = 1;
    uint8_t put_mat[5] = {11, 22, 33, 44, 55};
    uint8_t get_mat[5] = {0};

    // Put test
    printf("-----Put test-----\n");

    for (int i = 0; i < put_num; i++) {
        printf("%d", put_mat[i]);
    }
    printf("\n");

    printf("Pindex = %d\n", test_scope.Pindex);
    printf("Gindex = %d\n", test_scope.Gindex);
    printf("TOTAL = %d\n", test_scope.Total);
    ScopeBF_put(&test_scope, put_mat, put_num);
    printf("af_Pindex = %d\n", test_scope.Pindex);
    printf("af_Gindex = %d\n", test_scope.Gindex);
    printf("af_TOTAL = %d\n", test_scope.Total);

    for (int i = 0; i < put_num; i++) {
        printf("%d", put_mat[i]);
    }

    ScopeBF_tri(&test_scope);
    // Get test
    printf("\n-----Get test-----\n");

    for (int i = 0; i < get_num; i++) {
        printf("%d", get_mat[i]);
    }
    printf("\n");

    printf("Pindex = %d\n", test_scope.Pindex);
    printf("gindex = %d\n", test_scope.Gindex);
    printf("TOTAL = %d\n", test_scope.Total);
    ScopeBF_full(&test_scope);
    ScopeBF_get(&test_scope, get_mat, get_num);
    printf("af_Pindex = %d\n", test_scope.Pindex);
    printf("af_gindex = %d\n", test_scope.Gindex);

    // FIXME: 不進行trigger時，Total會出事
    printf("af_TOTAL = %d\n", test_scope.Total);

    for (int i = 0; i < get_num; i++) {
        printf("%d", get_mat[i]);
    }
    printf("\n");

    return 0;
}
