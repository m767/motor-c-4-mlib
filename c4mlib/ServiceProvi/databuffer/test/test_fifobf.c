/**
 * @file test_fifobf_float.c
 * @author YP-Shen
 * @date 2020-03-17
 * @brief  測試FifoBF_put及FifoBF_get緩衝uint8_t型態的資料
 */

#include "c4mlib/ServiceProvi/databuffer/src/fifobf.h"
#include "c4mlib/C4MBios/device/src/device.h"

#define FIFO_BUF_DATA_DEPTH_FTASK 10

int main(void) {
    C4M_STDIO_init();
    FIFOBUFF_LAY(FBF_p, FIFO_BUF_DATA_DEPTH_FTASK);

    uint8_t pdata[7] = {1, 2, 3, 4, 5, 6, 7};
    uint8_t gdata[3] = {0};

    printf("Sizeof put_data is %d\nSizeof gate_data is %d\n", sizeof(pdata),
           sizeof(gdata));

    printf("put_data = [");
    for (int i = 0; i < sizeof(pdata); i++) {
        printf("%d, ", pdata[i]);
    }
    printf("]\n");

    printf("Return of FifoBF_put : %d\n",
           FifoBF_put(&FBF_p, pdata, sizeof(pdata)));

    printf("get_data = [");
    for (int i = 0; i < sizeof(gdata); i++) {
        printf("%d, ", gdata[i]);
    }
    printf("]\n");

    printf("Return of FifoBF_get : %d\n",
           FifoBF_get(&FBF_p, gdata, sizeof(gdata)));

    printf("Get again:\n");
    printf("Return of FifoBF_get : %d\n",
           FifoBF_get(&FBF_p, gdata, sizeof(gdata)));
    printf("get_data = [");
    for (int i = 0; i < sizeof(gdata); i++) {
        printf("%d, ", gdata[i]);
    }
    printf("]\n");

    printf("Clear data in buffer:\n");
    printf("Return of FifoBF_clr : %d\n", FifoBF_clr(&FBF_p));

    printf("Get test after clear data:\n");
    printf("Return of FifoBF_get : %d\n",
           FifoBF_get(&FBF_p, gdata, sizeof(gdata)));
    printf("END\n");
}
