/**
 * @file test_scope_tri.c
 * @author LCY
 * @brief 超讀超寫測試程式
 * @version 0.1
 * @date 2020-04-01
 *
 * @copyright Copyright (c) 2020
 *
 */

#include "c4mlib/ServiceProvi/databuffer/src/scope.h"
#include "c4mlib/C4MBios/debug/src/debug.h"

int main() {
    C4M_DEVICE_set();

    SCOPEBUFF_LAY(test_scope, 2, 0);
    ScopeBF_tri(&test_scope);

    uint8_t put_num    = 5;
    uint8_t get_num    = 5;
    uint8_t put_mat[5] = {11, 22, 33, 44, 55};
    uint8_t get_mat[5] = {0};

    // Put test
    printf("-----OverPut test-----\n");

    for (int i = 0; i < sizeof(put_mat); i++) {
        printf("%d", put_mat[i]);
    }
    printf("\n");
    printf("Return = %d\n", ScopeBF_put(&test_scope, put_mat, put_num));
    printf("state = %d\n", test_scope.State);
    printf("total = %d\n", test_scope.Total);
    printf("pindex = %d\n", test_scope.Pindex);

    for (int i = 0; i < sizeof(put_mat); i++) {
        printf("%d", put_mat[i]);
    }

    // Get test
    printf("\n-----OverGet test-----\n");

    for (int i = 0; i < sizeof(get_mat); i++) {
        printf("%d", get_mat[i]);
    }
    printf("\n");

    printf("state = %d\n", test_scope.State);
    printf("gindex = %d\n", test_scope.Gindex);
    printf("TOTAL = %d\n", test_scope.Total);
    printf("Return = %d\n", ScopeBF_get(&test_scope, get_mat, get_num));
    printf("state = %d\n", test_scope.State);
    printf("gindex = %d\n", test_scope.Gindex);

    for (int i = 0; i < sizeof(get_mat); i++) {
        printf("%d", get_mat[i]);
    }
    printf("\n");

    return 0;
}
