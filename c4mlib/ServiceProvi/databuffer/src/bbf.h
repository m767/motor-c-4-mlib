/**
 * @file bbf.h
 * @author jerryhappyball
 * @date 2020.04.22
 * @brief 批次緩衝暫存器定義
 */

#ifndef C4MLIB_DATABUFFER_BBF_H
#define C4MLIB_DATABUFFER_BBF_H

#include <avr/interrupt.h>
#include <stdint.h>

/**
 * @defgroup databuffer_macro BatchBuffer macros
 * @defgroup databuffer_struct BatchBuffer structs
 * @defgroup databuffer_func BatchBuffer functions
 */

/* Public Section Start */
/**
 * @brief 供管理結構體鏈結到前後級工作之緩衝區欄。
 * @ingroup databuffer_macro
 */
#define NETBBF(task_p, num, buffer_p) task_p→BBF[num] = buffer_p

/**
 * @brief BATCHBUFF_LAY 資料連結初始化
 *
 * @param BBSTR 定義批次緩衝暫存器建構
 * @param List[MAXNUM] 定義資料陣列用矩陣
 * @ingroup databuffer_macro
 */
#define BATCHBUFF_LAY(BBSTR, MAXNUM)                                           \
    static BatchBFStr_t BBSTR = {0};                                                  \
    BBSTR.Depth        = MAXNUM;                                               \
    {                                                                          \
        static uint8_t List[MAXNUM] = {0};                                     \
        BBSTR.List_p                = List;                                    \
    }

/* Public Section Start */
/**
 * @brief 定義BBF工作結構體以管理暫存器狀態之結構
 * @ingroup databuffer_struct
 */
typedef struct {
    uint8_t State;  //<批次緩衝暫存器狀態: 0 = 可寫入 ; 1 = 可讀取.
    uint8_t Index;  //<讀寫索引.
    uint8_t Total;  //<總資料位元組數.
    uint8_t Depth;  //<暫存器總長度.
    void* List_p;   //<暫存器矩陣指標.
} BatchBFStr_t;

/**
 * @ingroup databuffer_func
 *
 * @brief BatchBF_put 資料寫入函式，供前級工作呼叫以存入新資料。
 *
 * @param *BBF_p 欲存入資料的批次緩衝區指標。
 * @param *Data_p 提供存入資料的變數或矩陣指標。
 * @param Bytes 資料位元組數。
 * @return 批之緩衝區執行完畢後剩餘空間/執行失敗時不足的空間。
 */
int8_t BatchBF_put(BatchBFStr_t* BBF_p, void* Data_p, uint8_t Bytes);

/**
 * @ingroup databuffer_func
 *
 * @brief BatchBF_get 資料讀出函式:供後級工作呼叫以讀取緩衝區內資料。
 *
 * @param *BBF_p 提供存入資料的批次緩衝區指標。
 * @param *Data_p 欲存入資料的變數或矩陣指標。
 * @param uint8_t Bytes 資料位元組數。
 * @return 批之緩衝區執行完畢後剩餘資料/執行失敗時不足的資料量。
 */
int8_t BatchBF_get(BatchBFStr_t* BBF_p, void* Data_p, uint8_t Bytes);

/**
 * @ingroup databuffer_func
 *
 * @brief BatchBF_full 設定批次緩衝區為滿。
 *
 * @param *BBF_p 欲清空批次緩衝區指標。
 * @return 批之緩衝區執行完畢後狀態。
 */
uint8_t BatchBF_full(BatchBFStr_t* BBF_p);

/**
 * @ingroup databuffer_func
 *
 * @brief BatchBF_clr 供前級工作呼叫以清空緩衝區。
 *
 * @param *BBF_p 欲清空批次緩衝區指標。
 * @return 批之緩衝區執行完畢後狀態。
 */
uint8_t BatchBF_clr(BatchBFStr_t* BBF_p);
/* Public Section End */
#endif  // C4MLIB_DATABUFFER_BBF_H
