/**
 * @file asaspi_mem_rec.c
 * @author Deng Xiang-Guan
 * @author LCY
 * @date 2019.10.02
 * @brief 提供master SPI memory transmit 的通訊方式，將三部分 command、memory
 *        address、data 送出去，將data存入falsh IC內。
 *
 * 2020/10/05:
 *      1. 未定 ASA ID 操作方式。
 *      2. 原先電路設計是ID -> 洞板 -> 拉動CS: 才有 ID 牽動 CS 事件發生，
 *      但目前已移除。
 *
 */

#include "c4mlib/C4MBios/asabus/src/pin_def.h"
#include "c4mlib/C4MBios/debug/src/debug.h"
#include "c4mlib/C4MBios/hardware/src/hal_spi.h"
#include "c4mlib/ServiceProvi/asaspi/src/asaspi_master.h"

uint8_t SPIM_Mem_trm(uint8_t mode, uint8_t ASAID, uint8_t RegAdd,
                     uint8_t AddBytes, void *MemAdd_p, uint8_t DataBytes,
                     void *Data_p) {
    Extern_CS_enable(ASAID);
    switch (mode) {
        // Ascending
        case 5:
            SPIM_Inst.spi_swap(RegAdd);
            for (int i = 0; i < AddBytes; i++) {
                SPIM_Inst.spi_swap(*((uint8_t *)MemAdd_p + i));
            }
            for (int i = 0; i < DataBytes; i++) {
                SPIM_Inst.spi_swap(*((uint8_t *)Data_p + i));
            }
            Extern_CS_disable(ASAID);
            break;
        // descending
        case 6:
            SPIM_Inst.spi_swap(RegAdd);
            for (int i = AddBytes - 1; i >= 0; i--) {
                SPIM_Inst.spi_swap(*((uint8_t *)MemAdd_p + i));
            }
            for (int i = 0; i < DataBytes; i++) {
                SPIM_Inst.spi_swap(*((uint8_t *)Data_p + i));
            }
            Extern_CS_disable(ASAID);
            break;

        default:
            Extern_CS_disable(ASAID);
            return HAL_ERROR_MODE_SELECT;
            break;
    }
    return 0;
}
