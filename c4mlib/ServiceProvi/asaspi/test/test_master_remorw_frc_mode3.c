/**
 * @file test_master_remorw_frc_mode3.c
 * @author Deng Xiang-Guan
 * @date 2019.10.03
 * @brief ASA_SPIM_frc mode 3函式。
 * 
 * 需搭配test_slave_remorw_ftm_frc_mode3.c，測試SPI Master(主)
 * mode 0旗標接收與旗標傳輸資料，具檢查機制，和Slave端交換資料，
 * 測試方式如下條列表示：
 *   1. 使用ASA_SPIM_frc函式，旗標式讀取暫存器編號2的測試資料。
 *   2. 使用ASA_SPIM_frc函式，旗標式讀取暫存器編號3的測試資料。
 *   3. 檢查接收回來的Slave端測試資料是否正確。
 *   4. 正確的話將成功計數器增加並於終端機顯示。
 *   5. 更新測試程式，回到流程1。
 */

#include "c4mlib/C4MBios/asabus/src/asabus.h"
#include "c4mlib/C4MBios/asabus/src/pin_def.h"
#include "c4mlib/C4MBios/asabus/src/remo_reg.h"
#include "c4mlib/ServiceProvi/asaspi/src/asaspi_master.h"
#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardware/src/hal_spi.h"

#include <stdlib.h>

#define SPI_MODE 3
#define ASAID 4
#define DELAY 10

#define ans1 87
#define ans2 255

int main() {
    // Setup
    C4M_STDIO_init();
    printf("Start master mode 3\n");
    SPIM_Inst.init();
    while (true) {
        uint8_t rec1, rec2;
        ASA_SPIM_frc(SPI_MODE, ASAID, 2, 0xff, 0, &rec1, DELAY);
        printf("->%d\n", rec1);
        ASA_SPIM_frc(SPI_MODE, ASAID, 3, 0xff, 0, &rec2, DELAY);
        printf("->%d\n", rec2);

        printf("==================\n");
        if ((rec1 != 0xff) || (rec2 != 0xff)) {
            printf("Test error !!!\n");
            while (true)
                ;
        }
        else {
            printf("Test Success !!!\n");
        }

        _delay_ms(3000);
    }
}
