/**
 * @file test_flash_w25q128jv.c
 * @author Deng Xiang-Guan
 * @date 2019.10.02
 * @brief 使用SPI Master記憶傳輸和接收驅動函式測試w25q128jv flash IC。
 * 
 * 測試w25q128jv flash IC，傳送的封包為[command + memory address + data]，
 * 測試方式如下條列表示：
 *  1. 清除flash的資料。
 *  2. 寫入測試資料。
 *  3. 讀取測試資料是否正確。
 *  4. 讀取flash IC的ID是否和datasheet描述的一模一樣。
 *  5. 將測試資料更新。
 *  6. 如果測試都正確，成功計數增加並於終端機顯示成功訊息。
 */

#include "c4mlib/C4MBios/asabus/src/asabus.h"
#include "c4mlib/C4MBios/asabus/src/pin_def.h"
#include "c4mlib/ServiceProvi/asaspi/src/asaspi_master.h"
#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardware/src/hal_spi.h"

#define SPI_MODE 5
#define ASAID 8

#define write_command 0x90
#define read_command 0x03
#define read_id_command 0x90

uint8_t mem_addr[3] = {0, 0, 0};
uint8_t manu_cont[2] = {0, 0};
uint8_t test_rec[4] = {0, 0, 0 ,0};
uint8_t test_id[2] = {0, 0};

int main() {
    // Setup
    // uint16_t success_cnt = 0;
    C4M_STDIO_init();
    SPIM_Inst.init();
    printf("Start test flash Manufacturer ID reading...\n");
    ASABUS_ID_set(ASAID);
    while(true) {
        printf("=================\n");
        // SPIM_Inst.enable_cs(ASAID);
        // ASABUS_SPI_swap(0x06);  // ???
        // SPIM_Inst.disable_cs(ASAID);

        // SPIM_Mem_trm(SPI_MODE, ASAID, 0x20, 3, &mem_addr, 0, manu_cont);
        // _delay_ms(1);

        // SPIM_Inst.enable_cs(ASAID);
        // ASABUS_SPI_swap(0x06);  // ???
        // SPIM_Inst.disable_cs(ASAID);

        SPIM_Mem_rec(SPI_MODE, ASAID, write_command, 3, mem_addr,
                     sizeof(manu_cont), manu_cont);

        // ASABUS_ID_set(ASAID);
        for (uint8_t i = 0; i < 2; i++) {
            printf("manu_cont=>%x\n", manu_cont[i]);
        }

        // for (uint8_t i = 0; i < sizeof(mem_addr); i++) {
        //     printf("mem_addr => %x\n", mem_addr[i]);
        // }

        _delay_ms(2000);
    }

}
