/**
 * @file adc_set.h
 * @author ya058764 (ya058764@gmail.com)
 * @date 2021.05.13
 * @brief
 *
 */

#ifndef C4MLIB_HARDWARESET_M128_ADC_SET_H
#define C4MLIB_HARDWARESET_M128_ADC_SET_H

#include "c4mlib/C4MBios/hardwareset/src/hardware_set.h"
#include "c4mlib/C4MBios/macro/src/bits_op.h"
#include "c4mlib/C4MBios/macro/src/std_def.h"

#include <stdint.h>
/* Public Section Start */

/**
 * @brief
 * @param ADCMuxReg_pADC Multiplex flag Reg address
 * @param ADCMuxMask Multiplex flag Mask, shift
 * @param ADCMuxShift
 * @param ADCTriReg_p ADC Triger flag Reg address
 * @param ADCTriMask ADC Triger flag Mask, shift
 * @param ADCTriShift
 * @param ADCEnReg_p ADC Enable flag Reg address
 * @param ADCEnMask ADC Enable flag Mask, shift
 * @param ADCEnShift
 * @param IntEnReg_p Interrupt Enable flag Reg address
 * @param IntEnMask Interrupt Enable flag Mask, shift
 * @param IntEnShift
 * @param IntSetReg_p Interrupt flag Set Reg address
 * @param IntSetMask Interrupt flag Set Mask, shift
 * @param IntSetShift
 * @param IntClrReg_p Interrupt flag Clear Reg address
 * @param IntClrMask Interrupt flag Clear Mask, shift
 * @param IntClrShift
 * @param Data8BitReg_p 8 bit ADC Data In Register addr
 * @param Data8BitBytes 8 bit ADC Data In bytes
 * @param Data10BitReg_p 10 bit ADC Data In Register addr
 * @param Data10BitBytes 10 bit ADC Data In bytes
 */
typedef struct {
    volatile uint8_t* ADCMuxReg_p;
    uint8_t ADCMuxMask;
    uint8_t ADCMuxShift;
    volatile uint8_t* ADCTriReg_p;
    uint8_t ADCTriMask;
    uint8_t ADCTriShift;
    volatile uint8_t* ADCEnReg_p;
    uint8_t ADCEnMask;
    uint8_t ADCEnShift;
    volatile uint8_t* IntEnReg_p;
    uint8_t IntEnMask;
    uint8_t IntEnShift;
    volatile uint8_t* IntSetReg_p;
    uint8_t IntSetMask;
    uint8_t IntSetShift;
    volatile uint8_t* IntClrReg_p;
    uint8_t IntClrMask;
    uint8_t IntClrShift;
    volatile uint8_t* Data8BitReg_p;
    uint8_t Data8BitBytes;
    volatile uint8_t* Data10BitReg_p;
    uint8_t Data10BitBytes;
} ADCOpStr_t;

/**
 * @defgroup adc_set_macro adc_set macros
 * @defgroup adc_set_struct adc_set structs
 * @defgroup adc_set_func adc_set functions
 */

/**
 * @brief   提供ADC硬體初始化所需之旗標設定結構
 * @ingroup adc_set_struct
 *
 * @param MUXn  ADC Input source
 * @param REFSn Reference source
 * @param ADCBIT Use 8bit or 10bit (ADLAR)
 * @param ADPCn  ADC prescaler (ADPSn)
 * @param ADFRUN  ADC free running (ADFR)
 * @param ADCEN   ADC convert enable (ADEN)
 * @param ADCINTEN  ADC interrupt enable (ADIE)
 * @param DDFn     ADC input pin
 * @param FlagTotalBytes  Total Bytes of Flag Group Datum
 * @param RegTotalBytes  Total Bytes of Registers
 */
typedef struct {
    uint8_t MUXn;
    uint8_t REFSn;
    uint8_t ADCBIT;
    uint8_t ADPCn;
    uint8_t ADFRUN;
    uint8_t ADCEN;
    uint8_t ADCINTEN;
    uint8_t DDFn;
    uint8_t FlagTotalBytes;
    uint8_t RegTotalBytes;
} ADCHWSetDataStr_t;

#define ADCFLAGPARALISTINI                                                     \
    {                                                                          \
        {.Reg_p = &ADMUX, .Mask = 0x1F, .Shift = 0},                           \
        {.Reg_p = &ADMUX, .Mask = 0xC0, .Shift = 6},                           \
        {.Reg_p = &ADMUX, .Mask = 0x20, .Shift = 5},                           \
        {.Reg_p = &ADCSRA, .Mask = 0x07, .Shift = 0},                          \
        {.Reg_p = &ADCSRA, .Mask = 0x20, .Shift = 5},                          \
        {.Reg_p = &ADCSRA, .Mask = 0x80, .Shift = 7},                          \
        {.Reg_p = &ADCSRA, .Mask = 0x08, .Shift = 3},                          \
        {.Reg_p = &DDRF, .Mask = 0x0F, .Shift = 0}                             \
    }

#define ADCREGPARALISTINI                                                      \
    { 0 }

#define ADCHWSETSTRINI                                                         \
    { .FlagNum = 8, .RegNum = 0 }

#define ADCOPSTRINI                                                            \
    {                                                                          \
        .ADCMuxReg_p = &ADCMUX, .ADCMuxMask = 0x1F, .ADCMuxShift = 0;          \
        .ADCTriReg_p = &ADCSRA, .ADCTriMask = 0x40, .ADCTriShift = 6;          \
        .ADCEnReg_p = &ADCSRA, .ADCEnMask = 0x80, .ADCEnShift = 7;             \
        .IntEnReg_p = &ADCSRA, .IntEnMask = 0x08, .IntEnShift = 3;             \
        .IntSetReg_p = &ADCSRA, .IntSetMask = 0x10, .IntSetShift = 4;          \
        .IntClrReg_p = &ADCSRA, .IntClrMask = 0x10, .IntClrShift = 4;          \
        .Data8BitReg_p = &ADCH, .Data8BitBytes = 1, .Data10BitReg_p = &ADCL,   \
        .Data10BitBytes = 2;                                                   \
    }

/* Public Section End */
#endif  // C4MLIB_HARDWAREINI_M128_ADC_SET_H
