/**
 * @file ext_set.h
 * @author ya058764 (ya058764@gmail.com)
 * @date 2021.05.13
 * @brief
 *
 */

#ifndef C4MLIB_HARDWARESET_M128_EXT_SET_H
#define C4MLIB_HARDWARESET_M128_EXT_SET_H

#include "c4mlib/C4MBios/hardwareset/src/hardware_set.h"
#include "c4mlib/C4MBios/macro/src/std_def.h"

#include <stdint.h>

/* Public Section Start */
typedef struct {
    volatile uint8_t *IntEnReg_p;
    uint8_t IntEnMask;
    uint8_t IntEnShift;
    volatile uint8_t *IntSetReg_p;
    uint8_t IntSetMask;
    uint8_t IntSetShift;
    volatile uint8_t *IntClrReg_p;
    uint8_t IntClrMask;
    uint8_t IntClrShift;
} EXTOpStr_t;

typedef struct {
    uint8_t DDXn;
    uint8_t ISCn0;
    uint8_t INTn;
    uint8_t FlagTotalBytes;
    uint8_t RegTotalBytes;
} EXTHWsetDataStr_t;

#define EXT0_3FLAGPARALISTINI(N)                                               \
    {                                                                          \
        {.Reg_p = &DDRD, .Mask = (char)(0x01 << N), .Shift = N},               \
        {.Reg_p = &EICRA, .Mask = (char)(0x03 << N * 2), .Shift = N * 2},      \
        {.Reg_p = &EIMSK, .Mask = (char)(0x01 << N), .Shift = N}               \
    }
#define EXT4_7FLAGPARALISTINI(N)                                               \
    {                                                                          \
        {.Reg_p = &DDRE, .Mask = (char)(0x10 << N), .Shift = N + 4},           \
        {.Reg_p = &EICRB, .Mask=(char)(0x03 << (N * 2)), .Shift = N * 2},      \
        {.Reg_p = &EIMSK, .Mask = (char)(0x10 << N), .Shift = N + 4}           \
    }

#define EXT0FLAGPARALISTINI EXT0_3FLAGPARALISTINI(0)
#define EXT1FLAGPARALISTINI EXT0_3FLAGPARALISTINI(1)
#define EXT2FLAGPARALISTINI EXT0_3FLAGPARALISTINI(2)
#define EXT3FLAGPARALISTINI EXT0_3FLAGPARALISTINI(3)
#define EXT4FLAGPARALISTINI EXT4_7FLAGPARALISTINI(0)
#define EXT5FLAGPARALISTINI EXT4_7FLAGPARALISTINI(1)
#define EXT6FLAGPARALISTINI EXT4_7FLAGPARALISTINI(2)
#define EXT7FLAGPARALISTINI EXT4_7FLAGPARALISTINI(3)

#define EXTHWSETSTRINI                                                         \
    { .FlagNum = 3, .RegNum = 0 }

#define EXTREGPARALISTINI                                                      \
    { 0 }
#define EXT0REGPARALISTINI EXTREGPARALISTINI
#define EXT1REGPARALISTINI EXTREGPARALISTINI
#define EXT2REGPARALISTINI EXTREGPARALISTINI
#define EXT3REGPARALISTINI EXTREGPARALISTINI
#define EXT4REGPARALISTINI EXTREGPARALISTINI
#define EXT5REGPARALISTINI EXTREGPARALISTINI
#define EXT6REGPARALISTINI EXTREGPARALISTINI
#define EXT7REGPARALISTINI EXTREGPARALISTINI

#define EXTOPSTRINI(N)                                                         \
    {                                                                          \
        .IntEnReg_p = &EIMSK, .IntEnMask = (0x01 << N), .IntEnShift = N,       \
        .IntSetReg_p = &EIFR, .IntSetMask = (0x01 << N), .IntSetShift = N,     \
        .IntClrReg_p = &EIFR, .IntClrMask = (0x01 << N), .IntClrShift = N      \
    }

#define EXT0OPSTRINI EXTOPSTRINI(0)
#define EXT1OPSTRINI EXTOPSTRINI(1)
#define EXT2OPSTRINI EXTOPSTRINI(2)
#define EXT3OPSTRINI EXTOPSTRINI(3)
#define EXT4OPSTRINI EXTOPSTRINI(4)
#define EXT5OPSTRINI EXTOPSTRINI(5)
#define EXT6OPSTRINI EXTOPSTRINI(6)
#define EXT7OPSTRINI EXTOPSTRINI(7)

/* Public Section End */

#endif  // C4MLIB_HARDWARESET_M128_EXT_SET_H
