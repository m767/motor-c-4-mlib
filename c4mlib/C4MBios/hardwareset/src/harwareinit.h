/**
 * @file harwareinit.h
 * @author Yi-Mou
 * @brief
 * @date 2020.04.29
 *
 *
 */
#if defined(__AVR_ATmega128__)
#    include "m128\spi_set.h"
#    include "m128\twi_set.h"
#    include "m128\uart_set.h"
#    include "m128\adc_set.h"
#    include "m128\ext_set.h"
#    include "m128\pwm_set.h"
#    include "m128\tim_set.h"
#endif
