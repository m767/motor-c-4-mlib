/**
 * @file hardware_set.c
 * @author ya058764
 * @date 2020.03.27
 * @brief
 */
#if defined(__AVR_ATmega128__)
#    include "m128/hardware_set.c"
#endif
