/**
 * @file test_ext7_init.c
 * @author ya058764 (ya058764@gmail.com)
 * @date 2021.05.14
 * @brief 
 * 
 */
#include "c4mlib/C4MBios/hardwareset/src/m128/ext_set.h"
#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/config/ext.cfg"
#include <avr/interrupt.h>
int i=0;


uint8_t EXT7_init(void){
    HardWareSet_t EXT7HWSet_str = EXTHWSETSTRINI;
    HWFlagPara_t EXT7FgGpData_str[3] = EXT7FLAGPARALISTINI;
    HWRegPara_t EXT7RegData_str = EXT7REGPARALISTINI;
    EXTHWsetDataStr_t EXT7HWSetData = EXT7SETDATALISTINI;
    HARDWARESET_LAY(EXT7HWSet_str, EXT7FgGpData_str[0], EXT7RegData_str, EXT7HWSetData);
    return HardwareSet_step(&EXT7HWSet_str);
}
int main(){
    C4M_DEVICE_set();
    uint8_t res = EXT7_init();
    printf("hardware_set report %d \n", res);
    printf("DDRE = %d\n", ((DDRE & 0x80) >> 7));
    printf("EICRB = %d\n", ((EICRB & 0xc0) >> 6));
    printf("EIMSK = %d\n", ((EIMSK & 0x80) >> 7));
    sei();
    while (1);
}

ISR(INT7_vect)
{
   i++;
   printf("ext complete, i=%d\n", i);
}
