/**
 * @file test_stdio.c
 * @author LiYu87
 * @brief 測試標準IO初始化韓式可以正常運作。
 * @date 2019.10.13
 */

#include "c4mlib/C4MBios/stdio/src/stdio.h"

int main() {
    C4M_STDIO_init();
    printf("test C4M_STDIO_init\n");

    return 0;
}
