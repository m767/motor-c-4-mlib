/**
 * @file rtupcount.c
 * @author Yi-Mou
 * @brief reatimeupcount定義
 * @date 2019-08-26
 *
 * 只做觸發計數
 */

#ifndef C4MLIB_RTPIO_RTUPCOUNT_H
#define C4MLIB_RTPIO_RTUPCOUNT_H

#include <stdint.h>

/**
 * @defgroup rtupcount_struct
 * @defgroup rtupcount_func
 */

/* Public Section Start */
/**
 * @brief RealTimeUpCount結構原型
 *
 * @ingroup rtupcount_struct
 * @param Task_Id 中斷中功能方塊名單編號。
 * @param ExCount 觸發次數計數值。
 *
 * 計數值紀錄執行step函式次數，用以追蹤次數。
 */
typedef struct {
    uint8_t Task_Id;             ///<中斷中功能方塊名單編號。
    volatile uint8_t ExCount;  ///<觸發次數計數值。
    uint8_t* ExCountOut_p; //Triggered Counter Pointer
    uint8_t NextTaskNum;         // Capacity of the next task list
    uint8_t* NextTask_p;       // points to next task list
} RealTimeISRCountStr_t;

/**
 * @brief 將觸發計數+1。
 *
 * @ingroup rtupcount_func
 * @param VoidStr_p 要執行的結構指標。
 *
 * 執行觸發次數值+1，可登錄在中斷服務常式中執行。
 */
void RealTimeISRCount_step(void* VoidStr_p);

#define RT_ISRCOUNT_LAY(RTCSTR, NEXTTASKNUM)                                     \
    RealTimeISRCountStr_t RTCSTR = {                                            \
        .Task_Id     = 0, /*Initial TaskId to be zero */                        \
        .ExCount   = 0, /*Initial Triger Counter */                           \
        .NextTaskNum = NEXTTASKNUM, /*Initial ListNum */                        \
        .ExCountOut_p = (uint8_t*)&(RTCSTR.ExCount)\
    }; 

/* Public Section End */
#endif  // C4MLIB_RTPIO_RTUPCOUNT_H
