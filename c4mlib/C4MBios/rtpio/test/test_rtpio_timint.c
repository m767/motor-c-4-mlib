/**
 * @file test_rtpio_timint.c
 * @author Yi-Mou
 * @brief rtpio在timint模組上實作
 * @date 2019-08-19
 *
 * 每次計時中斷觸發後改變PORTA輸出值，使其輸出100HZ方波。
 */

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/rtpio/src/rtpio.h"
#include <avr/io.h>

void init_timer2();  // timer2 CTC 100Hz

uint8_t data = 0;
RT_REG_IO_LAY(RealTimePortOut_1,0,&PORTA,1,&data);

int main() {
    C4M_DEVICE_set();

    init_timer2();
    sei();

    DDRA = 0xFF;
    while (1) {
        if (RealTimePortOut_1.ExCount == 1) {
            data = ~data;
            RealTimePortOut_1.ExCount = 0; 
        }
    }
}

void init_timer2() {
    OCR2 = 53;
    TCCR2 = 0b000001101;
    TCNT2 = 0;
    TIMSK |= 1 << OCIE2;
}

ISR(TIMER2_COMP_vect){
    RealTimeRegPut_step(&RealTimePortOut_1);
}
