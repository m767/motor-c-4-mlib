/**
 * @file test_rtfio_adcint.c
 * @author Yi-Mou
 * @brief rtfio實作
 * @date 2019-08-19
 *
 * timint呼叫RralTimeFlagOut使adc運作
 * adc處理完畢呼叫RealTimePort取值
 */

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/rtpio/src/rtpio.h"

#include <avr/io.h>

#include "c4mlib/config/adc.cfg"
#include "c4mlib/config/tim.cfg"

#define adcstart 1
void TimInt_init(void);
void ADCInt_init(void);

uint16_t data = 0;
uint8_t flag = 0;
RT_FLAG_IO_LAY(AdcStartFlag_str, 0, (uint8_t *)_SFR_ADDR(ADCSRA), 1 << (ADSC),ADSC, &flag);
RT_REG_IO_LAY(AdcPort_str, 1, &ADCL, 2, (uint8_t*)&data);

int main() {
    C4M_DEVICE_set();

    TimInt_init();
    ADCInt_init();

    sei();

    while (1) {
        if (AdcStartFlag_str.ExCount == 1) {
            flag =1 ;
            AdcStartFlag_str.ExCount  = 0;
        }
        else if (AdcStartFlag_str.ExCount > 1) {
            printf("too fast\n");
        }

        if (AdcPort_str.ExCount == 1) {
            AdcPort_str.ExCount = 0;
        }
        else if (AdcPort_str.ExCount > 1) {
            printf("too fast2\n");
        }
        printf("%u\n", data);
    }
}
void TimInt_init(void) {
    TCCR0 |= (1 << WGM01) | (7 << CS00);  // CTC mode Prescaler 1024
    TIMSK |= (1 << OCIE0);
    OCR0 = 53;  // 100Hz
}

void ADCInt_init(void) {
    ADMUX |= (1 << REFS0) | (1 << MUX0);
    ADCSRA |= (1 << ADEN) | (1 << ADIE);
}

ISR(TIMER0_COMP_vect) {
    RealTimeFlagPut_step(&AdcStartFlag_str);
}

ISR(ADC_vect) {
    RealTimeRegGet_step(&AdcPort_str);
}
