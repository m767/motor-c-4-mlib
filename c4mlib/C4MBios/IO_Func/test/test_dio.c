/**
 * @file test_dio.c
 * @author ya058764
 * @date 2019.07.23
 * @brief 測試dio函式運作
 */

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/IO_Func/src/dio.h"

#include <avr/io.h>

int main() {
    C4M_DEVICE_set();
    uint8_t data_p = 0xff,res;
    res = DIO_fpt(&DDRA, 0x0f, 0, data_p);
    printf("%d\n",res);
    res = DIO_fpt(&PORTA, 0x0f, 0, data_p);
    printf("%d\n",res);    

}
