/**
 * @file dio.c
 * @author LiYu87
 * @author ya058764
 * @date 2021.06.05
 * @brief 宣告 dio 及 ext 暫存器操作函式原型。
 * @date 2019.07.29
 *
 * 其中EXT函式與DIO函式，為了兼容就規格，所保留的重名函式。
 *  - EXT_fpt 與 DIO_fpt 相同。
 *  - EXT_fgt 與 DIO_fgt 相同。
 */

#ifndef C4MLIB_IOFUNC_DIO_H
#define C4MLIB_IOFUNC_DIO_H
#include <stdint.h>
/**
 * @defgroup hw_dio_func hardware dio functions
 */

/* Public Section Start */
/**
 * @brief dio flag put 函式
 *
 * @ingroup hw_dio_func
 * @param REG_p 暫存器編號。
 * @param Mask   遮罩。
 * @param Shift  向左位移。
 * @param Data   寫入資料。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *   - 2：參數 REG_p 錯誤。
 *   - 4：參數 Mask 錯誤。
 *   - 5：參數 Shift 超出範圍。
 *   - 6：警告操作方式與輸出入設定不同，但依然會執行操作。
 */
uint8_t DIO_fpt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift, uint8_t Data_p);

/**
 * @brief dio flag get 函式
 *
 * @ingroup hw_dio_func
 * @param REG_p 暫存器編號。
 * @param Mask   遮罩。
 * @param Shift  向右位移。
 * @param Data_p 資料指標。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *   - 2：參數 REG_p 錯誤。
 *   - 4：參數 Mask 錯誤。
 *   - 5：參數 Shift 超出範圍。
 *   - 6：警告操作方式與輸出入設定不同，但依然會執行操作。
 */
uint8_t DIO_fgt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift,
                void *Data_p);

/**
 * @brief dio put 函式
 *
 * @ingroup hw_dio_func
 * @param REG_p 暫存器編號。
 * @param Bytes  資料大小。
 * @param Data_p 資料指標。
 * @return char  錯誤代碼：
 *   - 0：成功無誤。
 *   - 3：參數 Bytes 錯誤。
 *   - 6：警告操作方式與輸出入設定不同，但依然會執行操作。
 */
uint8_t DIO_put(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p);

/**
 * @brief dio get 函式
 *
 * @ingroup hw_dio_func
 * @param REG_p 暫存器編號。
 * @param Bytes  資料大小。
 * @param Data_p 資料指標。
 * @return char  錯誤代碼：
 *   - 0：成功無誤。
 *   - 3：參數 Bytes 錯誤。
 *   - 6：警告操作方式與輸出入設定不同，但依然會執行操作。
 */
uint8_t DIO_get(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p);

/**
 * @brief 與 DIO_fpt 相同。
 */
uint8_t EXT_fpt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift, uint8_t Data);

/**
 * @brief 與 DIO_fgt 相同。
 */
uint8_t EXT_fgt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift, void *Data_p);
/* Public Section End */

#endif  // C4MLIB_IOFUNC_DIO_H
