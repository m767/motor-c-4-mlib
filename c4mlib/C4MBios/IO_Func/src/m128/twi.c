/**
 * @file twi.c
 * @author s915888
 * @date 2020.04.24
 * @author ya058764
 * @date 2021.06.05
 * @brief twi 暫存器操作相關函式實作。
 *
 */
#include "c4mlib/C4MBios/IO_Func/src/twi.h"

#include "c4mlib/C4MBios/macro/src/bits_op.h"
#include "c4mlib/C4MBios/macro/src/std_res.h"

#include <avr/io.h>

/* public functions declaration section ------------------------------------- */
uint8_t TWI_fpt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift,
                uint8_t Data);
uint8_t TWI_fgt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift,
                void *Data_p);
uint8_t TWI_put(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p);
uint8_t TWI_get(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p);
uint8_t TWI_set(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift,
                uint8_t Data) __attribute__((alias("TWI_fpt")));

/* public function contents section ----------------------------------------- */
uint8_t TWI_fpt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift,
                uint8_t Data) {
    if (REG_p != &TWBR && REG_p != &TWCR && REG_p != &TWSR && REG_p != &TWAR &&
        REG_p != &DDRD) {
        return RES_ERROR_LSBYTE;
    }
    else {
        REGFPT(REG_p, Mask, Shift, Data);
    }

    return RES_OK;
}

uint8_t TWI_fgt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift,
                void *Data_p) {
    if (REG_p != &TWSR) {
        return RES_ERROR_LSBYTE;
    }
    else {
        REGFGT(REG_p, Mask, Shift, Data_p);
    }

    return RES_OK;
}

uint8_t TWI_put(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p) {
    if (Bytes == 1) {
        if (REG_p != &TWDR) {
            return RES_ERROR_LSBYTE;
        }
        else {
            TWDR = *((uint8_t *)Data_p);
        }
    }
    else {
        return RES_ERROR_BYTES;
    }

    return RES_OK;
}

uint8_t TWI_get(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p) {
    if (Bytes == 1) {
        if (REG_p != &TWDR) {
            return RES_ERROR_LSBYTE;
        }
        else {
            *((uint8_t *)Data_p) = TWDR;
        }
    }
    else {
        return RES_ERROR_BYTES;
    }
    return RES_OK;
}
