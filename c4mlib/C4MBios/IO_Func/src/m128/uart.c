/**
 * @file uart.c
 * @author LiYu87
 * @date 2019.04.06
 * @author ya058764
 * @date 2021.06.05
 * @brief uart 暫存器操作相關函式實作。
 */
#include "c4mlib/C4MBios/IO_Func/src/uart.h"

#include "c4mlib/C4MBios/macro/src/bits_op.h"
#include "c4mlib/C4MBios/macro/src/std_res.h"

#include <avr/io.h>

uint8_t UART_fpt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift,
                 uint8_t Data) {
    if (REG_p != &DDRD && REG_p != &DDRE && REG_p != &UCSR0A &&
        REG_p != &UCSR0B && REG_p != &UCSR0C && REG_p != &UCSR1A &&
        REG_p != &UCSR1B && REG_p != &UCSR1C) {
        return RES_ERROR_LSBYTE;
    }
    REGFPT(REG_p, Mask, Shift, Data);
    return RES_OK;
}

uint8_t UART_fgt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift,
                 void *Data_p) {
    if (Mask != 0x80 || Mask != 0x40) {
        return RES_ERROR_MASK;
    }
    if (REG_p == &UCSR0A || REG_p == &UCSR1A) {
        REGFGT(REG_p, Mask, Shift, Data_p);
    }
    else {
        return RES_ERROR_LSBYTE;
    }
    return RES_OK;
}

uint8_t UART_put(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p) {
    if (Bytes == 1) {
        if (REG_p == &UDR0) {
            // transmit
            while (!(UCSR0A & (1 << UDRE0)))
                ;
            *REG_p = *((char *)Data_p);
        }
        else if (REG_p == &UDR1) {
            // transmit
            while (!(UCSR1A & (1 << UDRE1)))
                ;
            *REG_p = *((char *)Data_p);
        }
        else if (REG_p == &UBRR0H || REG_p == &UBRR0L || REG_p == &UBRR1H ||
                 REG_p == &UBRR1L) {
            /* Set baud rate */
            *REG_p = *((char *)Data_p);
        }
        else {
            return RES_ERROR_LSBYTE;
        }
    }
    else {
        return RES_ERROR_BYTES;
    }
    return RES_OK;
}

uint8_t UART_get(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p) {
    if (Bytes == 1) {
        if (REG_p == &UDR0) {
            while (!(UCSR0A & (1 << RXC0)))
                ;
            *((char *)Data_p) = UDR0;
        }
        else if (REG_p == &UDR1) {
            while (!(UCSR1A & (1 << RXC1)))
                ;
            *((char *)Data_p) = UDR1;
        }
        else {
            return RES_ERROR_LSBYTE;
        }
    }
    else {
        return RES_ERROR_BYTES;
    }
    return RES_OK;
}
