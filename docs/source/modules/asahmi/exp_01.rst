範例1—發送矩陣資料給HMI
***********************

開發板程式：

.. literalinclude:: exp_01.c
   :language: c
   :linenos:


執行結果：

.. image:: https://i.imgur.com/3srkj3k.png
  :width: 800
  :alt: asahmi_exp01_1



- - - - - - - - - - - - - - - - - - - - - - - - - - - - -

先宣告一個變數data，型態是float，而矩陣大小是2x5，並依序給其初始值
:code:`float data[2][5]`

接著可以利用 :code:`HMI_snput_matrix` 這個函式將矩陣資料data傳出。

HMI_snput_matrix 有四個參數，分別為Type、Dim1、Dim2、Data_p。

第一個參數是 Type ，代表要傳送資料的型態，並可以用數字或巨集的方式填入，
巨集可以參考資料型態對應編號表，而範例中的data型態是float，也就是32位元
的浮點數，所以要填入的巨集是 HMI_TYPE_F32 ，當然也可以填入 8 就好，
但是巨集的可讀性較好，所以盡量使用巨集來取代數字。

第二個參數是 Dim1 ，代表要傳送矩陣的維度1大小，這邊依照變數 data 填入 2。

第三個參數是 Dim2 ，代表要傳送矩陣的維度2大小，這邊依照變數 data 填入 3。

第四個參數是 Data_p ，代表要傳送矩陣的記憶體位置，而矩陣的名稱就是矩陣的記憶體位置，所以直接填入 data。

之後把回傳值存入變數res中。

最後會像這樣 :code:`res = HMI_put_matrix(HMI_TYPE_F32, 2, 5, data);`

這邊要特別注意， Dim1 與 Dim2 需要與變數宣告的大小一樣，若不一樣，就不保證
HMI_put_matrix 能正常執行，因為可能會存取到非法的記憶體位置。像此範例如果
改成 HMI_put_matrix(HMI_TYPE_F32, 2, 6, data) ，就會發生錯誤，會存取到
非法的記憶體位置，導致程式錯誤。

執行 HMI_put_matrix 的期間，人機會依序執行以下動作：

1. 接收到 ~GM, f32_2x5

  這個訊號的意思是叫人機做接收矩陣資料這個動作，而矩陣的型態是float32、
  大小是2乘5。

  其中， G 是 get 的縮寫，代表叫人機接收資料， M 是 matrix 的縮寫，代表
  要接收的資料是一個矩陣。在 , 後面的字串則代表了矩陣的資料型態以及大小，
  f32 代表資料型態是 float32 ， 2x5 代表大小是2乘5。

2. 回應 ~ACK

  這個訊號的意思是人機準備好執行動作了。人機告知開發版說準備好了，接著開發版
  便會開始傳送矩陣資料給人機。

3. 接收到矩陣資料

  人機接收到矩陣資料後便會把資料呈現在右半部分的接收頁面，如下圖

  .. image:: https://i.imgur.com/cHNYyjC.png
    :width: 400
    :alt: asahmi_exp01_2
  
