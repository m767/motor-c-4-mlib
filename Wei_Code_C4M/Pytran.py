## V3板下命令程式， prog(燒入模式) run(執行模式) reset(執行)
## 例：python Pytran.py -p COM4 -msg !prog
## 燒入後要執行：run->reset
import serial
import argparse
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--port", help="COM port choose, e.g. COM8", default="COM1", type=str)
    parser.add_argument("-msg", "--message", help="passing message (include\n)", default="!prog", type=str)
    args = parser.parse_args()
    print(args.port,args.message)
    ser = serial.Serial(
                port        = args.port,
                baudrate    = 38400,
                timeout     = 10,
            )
    ser.flushInput()
    ser.flushOutput()
    if(ser.isOpen() == False):
        ser.open()

    ser.write(str.encode(args.message+"\n"))

    if ser.isOpen():
            ser.close()
