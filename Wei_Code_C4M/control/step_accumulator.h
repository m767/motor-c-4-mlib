/**
 * @file step_accuimulator.h
 * @author Xiang-Guan Deng
 * @brief Collect and update command and sensor step and length
 * @date 2020.xx.xx
 *
 */
#ifndef STEP_ACCUMULATOR
#define STEP_ACCUMULATOR
#include <stdint.h>
#include "control_config.h"

#ifndef abs_int
#define abs_int(X) ((X < 0)? (-X) : (X))
#endif

#define SENSOR_RES                 (16384UL)
#define SENSOR_HALF                (SENSOR_RES>>1)

#ifdef USE_HAL /* Using Danny's Hardware abstract layer */

#define STEP_CACCUMUILATOR_INIT   {\
    .th_inc = 0,                   \
    .c_theta_total = 0,            \
    .c_length = 0.0,               \
    .k_theta2legnth = 0.0,         \
    }                              \

/**
 * @brief Define command step accumulator structure
 *
 */
typedef struct _step_caccumulator {
    int32_t th_inc;              /* theta increased */
    int32_t c_theta_total;       /* total command angle */
    float c_length;              /* total length */
    float k_theta2legnth;        /* theta to length gain */
}step_caccumulator_t;

/**
 * @brief Set command theta accumulated value to length
 */
void set_caccum_k(step_caccumulator_t *c_accum, float k_theta2legnth);

/**
 * @brief Update command theta accumulated value
 */
void update_step_caccum(step_caccumulator_t *c_accum, int32_t th_inc);

/**
 * @brief Get command length
 */
float get_step_caccum_length(step_caccumulator_t *c_accum);



#define STEP_SACCUMUILATOR_INIT {  \
    .th_s = 0,                     \
    .last_th_s = 0,                \
    .delta_th = 0,                 \
    .th_init = 0,                  \
    .cycles = 0,                   \
    .s_length = 0.0,               \
    .k_theta2legnth = 0.0,         \
    }                              \

/**
 * @brief Define sensor step accumulator structure
 *
 */
typedef struct _step_saccumulator {
    int32_t th_s;                  /* current sensor theta */
    int32_t last_th_s;             /* last sensor theta */
    int32_t delta_th;              /* delta theta */
    int32_t th_init;               /* initial theta position */
    int32_t cycles;                /* rounds */
    float s_length;                /* total length */
    float k_theta2legnth;          /* theta to length gain */
}step_saccumulator_t;

/**
 * @brief Set sensor theta accumulated value to length
 */
void set_saccum_k(step_saccumulator_t *s_accum, float k_theta2legnth);

/**
 * @brief Set sensor theta initial position
 */
void set_saccum_th_init(step_saccumulator_t *s_accum, int32_t th_init);

/**
 * @brief Update sensor theta accumulated value
 */
void update_step_saccum(step_saccumulator_t *s_accum, int32_t th_s);

/**
 * @brief Get sensor length
 */
float get_step_saccum_length(step_saccumulator_t *s_accum);

#else
/*==========STEP_CACCUMULATOR工作方塊結構體型態==========*/
/**
 * @brief STEP_CACCUMULATOR工作方塊結構體型態
 * @input th_inc (新增微步)
 * @output c_theta_total (命令總位移量)
 * 
 */
typedef struct{
    int16_t th_inc;              /*新增微步*/
    int16_t c_theta_total;       /*命令總位移量*/
    float c_length;              /*累積總微步量*/
    float k_theta2legnth;        /*總微步量至總位移量增益*/

    volatile int16_t* uIn_p[1];
    volatile int16_t* yOut_p[1];
} STEP_CACCUMULATORStr_t;
/*==========STEP_CACCUMULATOR工作方塊結構體定義及初始化==========*/
#define STEP_CACCUMULATORINI0 {\
    .k_theta2legnth = STEP_C_THETA_TO_LENGTH,\
    .yOut_p[0]=0}

// STEP_CACCUMULATORStr_t STEP_CACCUMULATOR_str = STEP_CACCUMULATORINI0;
/*==========STEP_CACCUMULATOR工作方塊佈線==========*/
#define STEP_CACCUMULATOR_LAY(STEP_CACCUMULATORSTR) {\
    STEP_CACCUMULATORSTR.uIn_p[0] = &STEP_CACCUMULATORSTR.th_inc;\
    STEP_CACCUMULATORSTR.yOut_p[0] =&STEP_CACCUMULATORSTR.c_theta_total;\
}

/*==========STEP_CACCUMULATOR工作方塊步級執行函式==========*/
void STEP_CACCUMULATOR_step(void* void_p);

/*==========STEP_SACCUMULATOR工作方塊結構體型態==========*/
/**
 * @brief STEP_SACCUMULATOR工作方塊結構體型態
 * @input th_s (編碼器讀值)
 * @output s_length (實際總位移量)
 * 
 */
typedef struct{
    int16_t th_s;                  /*編碼器讀值*/
    int16_t last_th_s;             /*前一狀態之編碼器讀值*/
    int16_t delta_th;              /*實際位移變化*/
    int16_t th_init;               /*位址原點編碼器初值*/
    int16_t cycles;                /*已跑圈數*/
    float s_length;                /*實際總位移量*/
    float k_theta2legnth;          /*角度實際總位移量轉換常數*/   
    int16_t th_inc; /*微步累加值*/
    
    volatile float* uIn_p[1]; /*pointer to input and output*/
    volatile float* yOut_p[1]; /*pointer to input and output*/
} STEP_SACCUMULATORStr_t;
/*==========STEP_SACCUMULATOR工作方塊結構體定義及初始化==========*/
#define STEP_SACCUMULATORINI0 {\
    .k_theta2legnth = STEP_S_THETA_TO_LENGTH,\
    .yOut_p[0]=0}

// STEP_SACCUMULATORStr_t STEP_SACCUMULATOR_str = STEP_SACCUMULATORINI0;
/*==========STEP_SACCUMULATOR工作方塊佈線==========*/
// #define STEP_SACCUMULATOR_LAY(STEP_SACCUMULATORSTR) {
//     STEP_SACCUMULATORSTR.uIn_p[0] = &STEP_SACCUMULATORSTR.th_s;
//     STEP_SACCUMULATORSTR.yOut_p[0] =&STEP_SACCUMULATORSTR.s_length;
// }
#define STEP_SACCUMULATOR_LAY(STEP_SACCUMULATORSTR) {\
    STEP_SACCUMULATORSTR.yOut_p[0] =&STEP_SACCUMULATORSTR.s_length;\
}
/*==========STEP_SACCUMULATOR工作方塊步級執行函式==========*/
void STEP_SACCUMULATOR_step(void* void_p);
#endif
#endif /* STEP_ACCUMULATOR */
