#include "pi_current.h"
#include "i_excite_angle.h"
#include "control_config.h"

#ifdef USE_HAL /* Using Danny's Hardware abstract layer */
    void init_current_para(fb_current_t *fb_current, float kp, float ki, float low_limit, float high_limit) {
        fb_current->pid.kp = kp;
        fb_current->pid.ki = ki;
        fb_current->i_svpwm = 0;
        fb_current->low_limit = low_limit;
        fb_current->high_limit = high_limit;
    }

    void cal_current_correct(fb_exc_angle_t *fb_exc_angle, fb_current_t *fb_current) {
    #ifdef ENABLE_CURRENT_PI
        /* C電子角(命令) 與 S電子角誤差(感測)、 累計誤差，作電流PI回饋 */
        fb_current->i_svpwm = I_SVPWM_EXI_BASE - (fb_current->pid.kp*fb_exc_angle->th_er) - (fb_current->pid.ki*fb_exc_angle->th_cum);

        /* 限制上下界 */
        if(fb_current->i_svpwm < fb_current->low_limit)  fb_current->i_svpwm = fb_current->low_limit;
        if(fb_current->i_svpwm > fb_current->high_limit) fb_current->i_svpwm = fb_current->high_limit;
    #else
        if(fb_exc_angle->th_er < 0) {
            fb_current->i_svpwm = -fb_current->low_limit;
        }
        else {
            fb_current->i_svpwm = fb_current->high_limit;
        }
    #endif
    }
#else
/*==========PI_CURRENT工作方塊步級執行函式==========*/
void PI_CURRENT_step(void* void_p){
    PI_CURRENTStr_t* Str_p =(PI_CURRENTStr_t*) void_p; /*typeset the str pointer */
    
#ifdef ENABLE_CURRENT_PI
    /* C電子角(命令) 與 S電子角誤差(感測)、 累計誤差，作電流PI回饋 */
    Str_p->i_svpwm = I_SVPWM_EXI_BASE - (Str_p->pid.kp*fb_exc_angle->th_er) - (Str_p->pid.ki*fb_exc_angle->th_cum);

    /* 限制上下界 */
    if(Str_p->i_svpwm < Str_p->low_limit)  Str_p->i_svpwm = Str_p->low_limit;
    if(Str_p->i_svpwm > Str_p->high_limit) Str_p->i_svpwm = Str_p->high_limit;
#else
    if(Str_p->th_er < 0) {
        Str_p->i_svpwm = -Str_p->low_limit;
    }
    else {
        Str_p->i_svpwm = Str_p->high_limit;
    }
#endif
}
#endif