/**
 * @file ele_angle.h
 * @author Xiang-Guan Deng
 * @brief Calculate and manage the machanical and electrical angle
 * @date 2020.xx.xx
 *
 * Encoder resolutio: 16 bits
 * C電子角累進器和S電子角除餘器實作
 */

#ifndef ELE_ANGLE_H
#define ELE_ANGLE_H
#include <stdint.h>

#include "control_config.h"
#ifndef abs_int
#    define abs_int(X) ((X < 0) ? (-X) : (X))
#endif

#define MAX_ELETRICAL_ROTOR_ANGLE (50)
#define MAX_ENCODER_VALUE         (16384UL)
#define HALF_ENCODER_VALUE        (8192UL)
// 327.68
#define ENC_ELE_ANGLE_TH_L  (327)
#define ENC_ELE_ANGLE_TH_H  (328)
#define ENC_ELE_ANGLE_ER    (32)
#define ENC_ELE_ANGLE_ER_TH (50)
#define EMC_ELE_AMGLE_COUNT                                                    \
    (100) /* per 100 error counters, plus or minus 1 count */

#define FULL_STEPS_NUM (200)
#define SENSOR2DEGREE  (-360.0 / 327.68) /* 順時鐘為正，encoder順時鐘為遞減 */

// #define N_STEP                    8

#ifdef USE_HAL /* Using Danny's Hardware abstract layer */
/* Define command angle type */
typedef struct _cangle_ {
    float K_degree;
    int16_t init_ele_angle;
    int16_t ele_angle;
    int16_t ele_limit;
    float ele_dangle;
} cangle_t;

/**
 * @brief Initialize command eletrical angle N_step parameter in run-time
 */
void init_cangle(cangle_t *cangle, uint16_t N_step, int16_t init_ele_angle);

/**
 * @brief Set command eletrical angle N_step parameter in run-time
 */
void set_cangle_step(cangle_t *cangle, uint16_t N_step);

/**
 * @brief Update command eletrical angle
 *
 */
void update_cangle(cangle_t *cangle, int16_t th_inc);

/* Define sensor angle type */
typedef struct _sangle_ {
    float K_degree;
    uint16_t init_mach_angle;
    int16_t mach_angle;
    int16_t poles;
    int16_t ele_angle;
    int16_t delta_angle;
    int16_t err_count;
    float ele_dangle;
} sangle_t;

/**
 * @brief get eletrical rotor angle(50) from encoder(16384)
 * 1. encoder is 14-bit resolution, 16384 per revolution
 * 2. 16384/50 = 327.68 (pulses per eletrical rotor angle)
 */
void init_sangle(sangle_t *sangle, uint16_t init_mach_angle);

/**
 * @brief Update sensor eletrical angle
 *
 */
void update_sangle(sangle_t *sangle, uint16_t enc_angle);

/**
 * @brief Initialize command eletrical angle parameters
 *
 */
#else /* Using C4MOS */

/*==========CANGLE工作方塊結構體型態==========*/
/**
 * @brief CANGLE工作方塊結構體型態
 * @input th_inc (微步累加值)
 * @output ele_dangle (命令電子角)
 */
typedef struct {
    float K_degree;    /*度量換算常數*/
    int16_t ele_angle; /*電子角狀態變數*/
    int16_t ele_limit; /*電子角轉動上限*/
    float ele_dangle;  /*命令電子角*/
    // int16_t th_inc; /*微步累加值*/
    volatile int16_t* uIn_p[1]; /*pointer to input and output*/
    volatile float* yOut_p[1];  /*pointer to input and output*/
} CANGLEStr_t;

/*==========CANGLE工作方塊結構體定義及初始化==========*/
// #    define CANGLEINI0                                                         \
//         {                                                                      \
//             .yOut_p[0] = 0                                                     \
//         }

// CANGLEStr_t CANGLE_str = CANGLEINI0;

/*==========CANGLE工作方塊佈線==========*/
#    define CANGLE_LAY(CANGLESTR)                                              \
        {                                                                      \
            CANGLESTR.yOut_p[0] = &CANGLESTR.ele_dangle;                       \
            CANGLESTR.K_degree =                                               \
                360.0 / (N_STEP * FULL_STEPS_NUM) * (90.0 / 1.8);              \
            CANGLESTR.ele_limit = N_STEP * FULL_STEPS_NUM;                     \
        }
//    CANGLESTR.uIn_p[0] = &CANGLESTR.th_inc;\
/*==========CANGLE工作方塊步級執行函式==========*/
void CANGLE_step(void* void_p);

/*==========SANGLE工作方塊結構體型態==========*/
/**
 * @brief SANGLE工作方塊結構體型態
 * @input enc_angle (角度編碼器感測值)
 * @output ele_dangle (電子角(度度量))
 */
typedef struct {
    float K_degree; /*度度量換算常數*/
    uint16_t init_mach_angle;
    int16_t enc_angle;  /*角度編碼器感測值*/
    int16_t mach_angle; /*感測機械角*/
    int16_t poles; /*感測電子圈數，馬達轉一圈電子角會轉50圈*/
    int16_t ele_angle;   /*感測電子角*/
    int16_t delta_angle; /*感測機械角位移*/
    int16_t err_count;   /*感測電子角累積誤差*/
    float ele_dangle;    /*電子角(度度量)*/

    volatile uint16_t* uIn_p[0]; /*pointer to input and output*/
    volatile float* yOut_p[1];   /*pointer to input and output*/
} SANGLEStr_t;
/*==========SANGLE工作方塊結構體定義及初始化==========*/
#    define SANGLEINI0                                                         \
        {                                                                      \
            .poles = 0, .ele_angle = 0, .err_count = 0, .mach_angle = 0,       \
            .delta_angle = 0, .yOut_p[0] = 0                                   \
        }

// SANGLEStr_t SANGLE_str = SANGLEINI0;
/*==========SANGLE工作方塊佈線==========*/
// #define SANGLE_LAY(SANGLESTR) {
//     SANGLESTR.uIn_p[0] = &SANGLESTR.enc_angle;
//     SANGLESTR.yOut_p[0] = &SANGLESTR.ele_dangle;
// }
#    define SANGLE_LAY(SANGLESTR)                                              \
        {                                                                      \
            SANGLESTR.yOut_p[0] = &SANGLESTR.ele_dangle;                       \
            SANGLESTR.K_degree  = -360.0 / 327.68;                             \
        }
/*==========SANGLE工作方塊步級執行函式==========*/
void SANGLE_step(void* void_p);
#endif

#endif
