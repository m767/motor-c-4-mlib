from asyncio.windows_events import NULL
import pickle
from turtle import color
import matplotlib.pyplot as plt
from mypickle import save2pickle, load_pickle
import os
from shutil import move
import numpy as np
from numpy import diff
import collections
from functools import cmp_to_key
import scipy.signal as signal
# def xcorr(x,y):
#     """
#     Perform Cross-Correlation on x and y
#     x    : 1st signal
#     y    : 2nd signal

#     returns
#     lags : lags of correlation
#     corr : coefficients of correlation
#     """
#     corr = signal.correlate(x, y, mode="full")
#     lags = signal.correlation_lags(len(x), len(y), mode="full")
#     return lags, corr

FIG_DIR = "data"
def plot_control_SampleHold(file , DELTA_T, micro_step):

    save_data = load_pickle("data/"+file+'.pickle')


    current_A_R = save_data["current_A_R"]
    current_A_F = save_data["current_A_F"]
    current_B_R = save_data["current_B_R"]
    current_B_F = save_data["current_B_F"]
    print('12121231231231321313123:',current_B_F.dtype)

    Sangle = save_data["Sangle"]
    Cangle = save_data["Cangle"]
    th_cum = save_data["th_cum"]

    

    PWMA = save_data["PWMA"] 
    PWMB = save_data["PWMB"]
    
    I_ki = save_data["I_ki"] 
    PI_kp = save_data["PI_kp"]
    PI_ki = save_data["PI_ki"]
    print('>>',I_ki,PI_kp,PI_ki)
    # power = (current_A*current_A*7.01)+(current_B*current_B*7.01)


    #開資料夾存figure
    file_marker=file+ '_' +str(micro_step) + 'step_'
    #沒回饋
    if PI_ki==0 and PI_kp==0 and I_ki==0:
        file_marker+='open'
        # os.mkdir(FIG_DIR+"/"+file_marker+ '_' +str(micro_step) + 'step' + '_open')
    #I回饋
    elif PI_ki==0 and PI_kp==0 and I_ki!=0:
        file_marker+='I'+ str(I_ki)
        # os.mkdir(FIG_DIR+"/"+file_marker+ '_' + str(micro_step) + 'step' + '_I' + str(I_ki))
    #I+PI回饋
    else:
        file_marker+='I'+ str(I_ki) + '_' + str(PI_kp) + '_' + str(PI_ki)
        # os.mkdir(FIG_DIR+"/"+file_marker+ '_' +str(micro_step) + 'step' + '_IPI_' + str(I_ki) + '_' + str(PI_kp) + '_' + str(PI_ki))
    
    try:
        os.mkdir(FIG_DIR)
    except:
        pass

    try:
        os.mkdir(FIG_DIR+"/"+file_marker)
    except:
        pass





    

    data_length = len(Sangle)


    t = np.linspace(0, data_length, data_length) #取樣週期是固定的1,1ms
    ### ==================================================================================== ###
    fig = plt.figure(figsize=(25, 30))        
    #========解決例外=======================
    #sensing command    error
    # 16300     10      16290
    # 10       16300    -16290
    error=np.zeros([len(Sangle)], dtype=np.int16)
    error=Sangle-Cangle
    Cangle_deg=Cangle*360/16384
    cut=0
    cut2=0
    for i in range(0,int(len(error)/2)):
        if error[i]>16300:
            cut+=1
            print('--__')
            
    for i in range(int(len(error)/2),len(error)):
        if error[i]>16300:
            cut2+=1
            print('__--')
            
    Sangle=np.where(error>16000,Sangle-16384,Sangle)
    Sangle=np.where(error<-16000,Sangle+16384,Sangle)
    error=Sangle-Cangle
    
    # Cangle=Cangle[start+cut:start+micro_step*200-cut2]
    # Sangle=Sangle[start+cut:start+micro_step*200-cut2]
    # error=error[start+cut:start+micro_step*200-cut2]

    # Cangle_deg=Cangle_deg[start:start+micro_step*8]
    # PWMA=PWMA[start:start+micro_step*8]
    # PWMB=PWMB[start:start+micro_step*8]
    # current_A=current_A[start:start+micro_step*8]
    # current_B=current_B[start:start+micro_step*8]
    current_A_deri = diff(-current_A_R+current_A_F)
    current_B_deri = diff(-current_B_R+current_B_F)
    print(Cangle[0:10])
    print(Cangle[-10:])

#==========================================================
    error_deg=error*360/16384
    #沒回饋
    if PI_ki==0 and PI_kp==0 and I_ki==0:
        if micro_step==1:
            fig.suptitle('Full-stepping Angle Control \n'  + ' max_er : ' + str(round(np.max(error_deg[5:]),3)) +' min_er : ' + str(round(np.min(error_deg[5:]),3)) + ' std_er : ' +  str(round(np.std(error_deg[5:]),3))  +'(deg)', fontsize=50)
        else:
            fig.suptitle(str(micro_step) +'Micro-stepping Angle Control \n' + ' max_er : ' + str(round(np.max(error_deg[5:]),3)) + ' min_er : ' + str(round(np.min(error_deg[5:]),3)) + ' std_er : ' +  str(round(np.std(error_deg[5:]),3)) +'(deg)', fontsize=50)
    #I回饋
    elif PI_ki==0 and PI_kp==0 and I_ki!=0:
        if micro_step==1:
            fig.suptitle('Full-stepping Angle Control \n' + 'i_ki : ' + str(round(float(I_ki),4)) + "\n" + ' max_er : ' + str(round(np.max(error_deg[5:]),3)) +' min_er : ' + str(round(np.min(error_deg[5:]),3)) + ' std_er : ' +  str(round(np.std(error_deg[5:]),3)) +'(deg)', fontsize=50)
        else:
            fig.suptitle(str(micro_step) +'Micro-stepping Angle Control \n' + 'i_ki : ' + str(round(float(I_ki),4)) + "\n" + ' max_er : ' + str(round(np.max(error_deg[5:]),3)) +' min_er : ' + str(round(np.min(error_deg[5:]),3)) + ' std_er : ' +  str(round(np.std(error_deg[5:]),3)) +'(deg)', fontsize=50)
    #I+PI回饋
    else:
        if micro_step==1:
            fig.suptitle('Full-stepping Angle Control \n' + 'i_ki : ' + str(round(float(I_ki),4)) + ' PI_kp : ' + str(round(float(PI_kp),4)) + ' PI_ki : ' + str(round(float(PI_ki),4)) + "\n" + ' max_er : ' + str(round(np.max(error_deg[5:]),3)) +' min_er : ' + str(round(np.min(error_deg[5:]),3)) + ' std_er : ' +  str(round(np.std(error_deg[5:]),3)) +'(deg)', fontsize=50)
        else:
            fig.suptitle(str(micro_step) +'Micro-stepping Angle Control \n' + 'i_ki : ' + str(round(float(I_ki),4)) + ' PI_kp : ' + str(round(float(PI_kp),4)) + ' PI_ki : ' + str(round(float(PI_ki),4)) + "\n" + ' max_er : ' + str(round(np.max(error_deg[5:]),3)) +' min_er : ' + str(round(np.min(error_deg[5:]),3)) + ' std_er : ' +  str(round(np.std(error_deg[5:]),3)) +'(deg)', fontsize=50)
   
#==========================================================
    x=np.arange(len(Cangle))/210
#==========================================================

# 1-1 Angle Control (step-encocder)
    plt.subplot(313)
    # x1=Cangle*200*micro_step/16384
    plt.plot(x , Cangle)
    plt.plot(x , Sangle)
    plt.grid(True)
    plt.xticks(fontsize=24)
    plt.yticks(fontsize=24)
    plt.ylabel('Angle(Encoder count) ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('t(s)', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Angle Control', fontsize=28)
    plt.legend(['Command Angle','Sensing Angle'], fontsize=20)
    # plt.xticks(np.arange(0,200*micro_step+1,200*micro_step/8),fontsize=20)
    # plt.yticks(np.arange(0,16384+1,16384/8),fontsize=20)
    fig.align_labels()

#==========================================================
# 1-2 Angle Control (deg-deg)
    plt.subplot(311)
    x1=Cangle*360/16384
    x2=Sangle*360/16384
    plt.plot(x ,x1 )
    plt.plot(x ,x2 )
    # plt.plot(t, Cangle)
    plt.grid(True)
    plt.xticks(fontsize=24)
    plt.yticks(fontsize=24)
    plt.ylabel('Sensing Angle(deg) ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('t(s)', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Angle Control', fontsize=28)
    plt.legend(['Command Angle','Sensing Angle'], fontsize=20)
    # plt.xticks(np.arange(0,361,45),fontsize=20)
    # plt.yticks(np.arange(0,360+1,360/8),fontsize=20)
    fig.align_labels()

#==========================================================
# 1-3 Error (deg-deg)
    plt.subplot(312)
    
    plt.plot(x ,error*360/16384)
    plt.grid(True)
    plt.xticks(fontsize=24)
    plt.yticks(fontsize=24)
    plt.ylabel('Error(deg)', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('t(s)', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Angle Error', fontsize=28)
    plt.legend(['Angle Error'], fontsize=20)
    fig.align_labels() 

    plt.subplots_adjust(hspace=0.3)

    fig.text(0.01,0.01,file_marker + '_angle' + '.png' ,fontsize=20)
    plt.savefig(FIG_DIR+"/"+file_marker +'/' + file_marker + '_angle' + '.png', dpi=300)

#==========================================================
#figure 2
    fig = plt.figure(figsize=(25, 30))
    #沒回饋
    if PI_ki==0 and PI_kp==0 and I_ki==0:
        if micro_step==1:
            fig.suptitle('Full-stepping Current-sensing', fontsize=50)
        else:
            fig.suptitle(str(micro_step) +'Micro-stepping Current-sensing', fontsize=50)
    #I回饋
    elif PI_ki==0 and PI_kp==0 and I_ki!=0:
        if micro_step==1:
            fig.suptitle('Full-stepping Current-sensing \n' + 'i_ki : ' + str(round(float(I_ki),4)) , fontsize=50)
        else:
            fig.suptitle(str(micro_step) +'Micro-stepping Current-sensing \n' + 'i_ki : ' + str(round(float(I_ki),4)), fontsize=50)
    #I+PI回饋
    else:
        if micro_step==1:
            fig.suptitle('Full-stepping Current-sensing \n' + 'i_ki : ' + str(round(float(I_ki),4)) + ' PI_kp : ' + str(round(float(PI_kp),4)) + ' PI_ki : ' + str(round(float(PI_ki),4))  , fontsize=50)
        else:
            fig.suptitle(str(micro_step) +'Micro-stepping Current-sensing \n' + 'i_ki : ' + str(round(float(I_ki),4)) + ' PI_kp : ' + str(round(float(PI_kp),4)) + ' PI_ki : ' + str(round(float(PI_ki),4)) , fontsize=50)

    
#==========================================================
# 2-1 pwm
    ax=plt.subplot(311)
    plt.plot(x, PWMA,'*-')
    plt.plot(x, PWMB,'*-')
    plt.grid(True)
    plt.ylabel('Duty(%) ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('t(s)', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Phase A B PWM', fontsize=28) 
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(['pwmA','pwmB'], fontsize=20)
    labels = ax.get_xticks().tolist()
    fig.align_labels()
    
    
#==========================================================
# 2-1 pwm
    # ax=plt.subplot(311)
    # plt.plot(Cangle_deg, PWMA,'*-')
    # plt.plot(Cangle_deg, PWMB,'*-')
    # plt.grid(True)
    # plt.ylabel('Duty(%) ', fontsize=24, position=(0,1), rotation="horizontal")
    # plt.xlabel('Command Angle(deg)', fontsize=24, position=(1,0), rotation="horizontal")
    # plt.title('Phase A B PWM', fontsize=28) 
    # plt.xticks(fontsize=20)
    # plt.yticks(fontsize=20)
    # plt.legend(['pwmA','pwmB'], fontsize=20)
    # labels = ax.get_xticks().tolist()
    # fig.align_labels()
    # 
#==========================================================
# 2-2 current A
    plt.subplot(312)
    plt.plot(x, current_A_R,'*-')
    plt.plot(x, current_A_F,'*-')
    plt.plot(x, -current_A_R + current_A_F,'*-')
    plt.grid(True)
    plt.ylabel('I(A) ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('t(s)', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Phase A current', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(['current_A_R','current_A_F','current_slope'], fontsize=20)
    fig.align_labels()
#==========================================================
# 2-2 current B
    plt.subplot(313)
    plt.plot(x, current_B_R,'*-')
    plt.plot(x, current_B_F,'*-')
    plt.plot(x, -current_B_R + current_B_F,'*-')
    plt.grid(True)
    plt.ylabel('I(A) ', fontsize=24, position=(0,1), rotation="horizontal")
    plt.xlabel('t(s)', fontsize=24, position=(1,0), rotation="horizontal")
    plt.title('Phase B current', fontsize=28)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(['B_R','B_F','slope'], fontsize=20)
    fig.align_labels()
   
#==========================================================
# # 2-2 current
#     plt.subplot(312)
#     (markers, stemlines, baseline)=plt.stem(Cangle_deg, current_A)
#     plt.setp(baseline, visible=False)
#     (markers, stemlines, baseline)=plt.stem(Cangle_deg, current_B,'darkorange')
#     plt.setp(baseline, visible=False)
#     plt.setp(markers,color='darkorange')
#     plt.grid(True)
#     plt.ylabel('I(A) ', fontsize=24, position=(0,1), rotation="horizontal")
#     plt.xlabel('Command Angle(deg)', fontsize=24, position=(1,0), rotation="horizontal")
#     plt.title('Phase A B current', fontsize=28)
#     plt.xticks(fontsize=20)
#     plt.yticks(fontsize=20)
#     plt.legend(['current_A','current_B'], fontsize=20)
#     fig.align_labels()
   
#==========================================================
# # 2-3 current 微分
#     plt.subplot(313)
#     plt.plot(Cangle_deg[:-1], current_A_deri)
#     plt.plot(Cangle_deg[:-1], current_B_deri)
#     plt.grid(True)
#     plt.ylabel('t(s)', fontsize=24, position=(0,1), rotation="horizontal")
#     plt.xlabel('Command Angle(deg)', fontsize=24, position=(1,0), rotation="horizontal")
#     plt.title('Phase A B current derivative', fontsize=28)
#     plt.xticks(fontsize=20)
#     plt.yticks(fontsize=20)
#     # plt.legend(['current_A_slope','current_B_slope'], fontsize=20)
#     plt.legend(['current_A_deri','current_B_deri'], fontsize=20)
#     fig.align_labels()
    
    plt.subplots_adjust(hspace=0.3)
    
    fig.text(0.01, 0.01,file_marker + '_current' + '.png' ,fontsize=20)
    plt.savefig(FIG_DIR+"/"+file_marker +'/' + file_marker + '_current' + '.png', dpi=300)

    ### ==================================================================================== ###

    # rms_ia_square = np.mean((current_A*current_A))
    # rms_ib_square = np.mean((current_B*current_B))
    # P_loss_A = rms_ia_square*6.485
    # P_loss_B = rms_ib_square*6.165
    # P_in_A = np.mean(np.abs(current_A*5))
    # P_in_B = np.mean(np.abs(current_A*5))
    # P_efficient_A = (P_in_A - P_loss_A) / P_in_A * 100
    # P_efficient_B = (P_in_B - P_loss_B) / P_in_B * 100
    # P_all = (P_in_A + P_in_B - P_loss_A - P_loss_B) / (P_in_A + P_in_B) * 100
    # omega = (abs(Sangle[0]-Sangle[-1]))/(current_A.shape[0]*0.0012)
    # rpm = omega*60/360
    report_str = ""
    report_str += "I_ki:" + str(I_ki) +"\n"
    report_str += "PI_ki:" + str(PI_ki) +"\n"
    report_str += "PI_kp:" + str(PI_kp) +"\n"
    # report_str += "Data size is " +  str(current_A.shape[0]) + "\n"
    report_str += "===== theta error (degree) =====" + "\n"
    report_str += "max: " + str(np.max(error*360/16384)) + "\n"
    report_str += "min: " + str(np.min(error*360/16384)) + "\n"
    report_str += "max-min: " + str(np.max(error*360/16384)-np.min(error*360/16384)) + "\n"
    report_str += "mean: " + str(np.mean(error*360/16384)) + "\n"
    report_str += "std: " + str(np.std(error*360/16384)) + "\n"
    report_str += "MSE: " + str(np.mean(np.square(error*360/16384))) + "\n"
    report_str += "N_STEP: " + str(1.8/(np.max(error*360/16384))) + ", " + str(1.8/(np.min(error*360/16384))) + "\n"
    report_str += "===== velocity =====" + "\n"
    # report_str += "theta/s: " + str(omega) + "\n"
    # report_str += "RPM: " + str(rpm) + "\n"
    report_str += "===== sensor ele angle =====" + "\n"
    report_str += "max: " + str(np.max(Sangle*360/16384)) + "\n"
    report_str += "min: " + str(np.min(Sangle*360/16384)) + "\n"
    report_str += "mean: " + str(np.mean(Sangle*360/16384)) + "\n"
    report_str += "===== command ele angle =====" + "\n"
    report_str += "max: " + str(np.max(Cangle*360/16384)) + "\n"
    report_str += "min: " + str(np.min(Cangle*360/16384)) + "\n"
    report_str += "mean: " + str(np.mean(Cangle*360/16384)) + "\n"
    # report_str += "===== consume power (watt) =====" + "\n"
    # report_str += "max: " + str(np.max(power)) + "\n"
    # report_str += "min: " + str(np.min(power)) + "\n"
    # report_str += "mean: " + str(np.mean(power)) + "\n"
    # report_str += "std: " + str(np.std(power)) + "\n"
    # report_str += "sum: " + str(np.sum(np.abs(power))) + " J\n"
    # report_str += "===== current  =====" + "\n"
    # report_str += "ia_max: " + str(np.max(current_A)) + " A\n"
    # report_str += "ia_min: " + str(np.min(current_A)) + " A\n"
    # report_str += "ia_mean: " + str(np.mean(current_A)) + " A\n"
    # report_str += "ib_max: " + str(np.max(current_B)) + " A\n"
    # report_str += "ib_min: " + str(np.min(current_B)) + " A\n"
    # report_str += "ib_mean: " + str(np.mean(current_B)) + " A\n"
    # report_str += "===== current RMS Square (watt) =====" + "\n"
    # report_str += "rms_ia_square: " + str(rms_ia_square) + " A\n"
    # report_str += "rms_ib_square: " + str(rms_ib_square) + " A\n"
    # report_str += "===== current RMS (watt) =====" + "\n"
    # report_str += "rms_ia: " + str(np.sqrt(rms_ia_square)) + " A\n"
    # report_str += "rms_ib: " + str(np.sqrt(rms_ib_square)) + " A\n"
    # report_str += "===== P_loss (watt) =====" + "\n"
    # report_str += "P_loss_A: " + str(rms_ia_square*6.485) + " W\n"
    # report_str += "P_loss_B: " + str(rms_ia_square*6.165) + " W\n"
    # report_str += "===== P_in (watt) =====" + "\n"
    # report_str += "P_in_A: " + str(P_in_A) + " W\n"
    # report_str += "P_in_B: " + str(P_in_B) + " W\n"
    # report_str += "===== Eta (H) (%) =====" + "\n"
    # report_str += "P_efficient_A: " + str(P_efficient_A) + " %\n"
    # report_str += "P_efficient_B: " + str(P_efficient_B) + " %\n"
    # report_str += "P_all: " + str(P_all) + " %\n"

    print(report_str)

    f = open(FIG_DIR + "/" + file_marker + "/report_" + file_marker + ".txt", "w")
    f.write(report_str)
    f.close()

    try:
        os.remove(FIG_DIR + "/" + file+'.pickle')
    except:
        pass
    try:
        move(FIG_DIR + "/" + file+'.txt' , FIG_DIR + "/" + file_marker + "/" + file_marker+'.txt')
    except:
        pass
        
    