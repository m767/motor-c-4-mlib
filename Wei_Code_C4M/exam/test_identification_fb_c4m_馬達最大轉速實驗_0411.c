// E:\mvmc\Motor\Wei_Code_C4M
// run ..\exam\test_identification_fb_c4m_馬達最大轉速實驗_0411 4 3 run
//
/**
 * @file test_identification_fb.c
 * @author nacmvmclab (nacmvmclab@gmail.com)
 * @date 2021.08.20
 * @brief 上升緣下降緣量測電流
 * ENABLE_EXI_ANGLE_I
 * ENABLE_CURRENT_PI
 *
 * PWM1     B5 OC1A
 * PWM2     B6 OC1B
 * S&H_HOLD D4
 * CS       D5
 * NSLEEP   D6
 *
 * TEST_PIN0 B4
 * TEST_PIN2 D7
 * TEST_PIN3 D0
 * TEST_PIN4 D1
 */
#ifndef __AVR_ATmega128__
#    define __AVR_ATmega128__
#endif
#include <avr/interrupt.h>
#include <avr/io.h>
#include <math.h>
#include <util/delay.h>

#include "../config/adc.cfg"
#include "../config/pwm.cfg"
#include "../config/spi.cfg"
#include "../config/tim.cfg"
#include "../../c4mlib/C4MBios/hardwareset/src/adc_set.h"
#include "../../c4mlib/C4MBios/hardwareset/src/pwm_set.h"
#include "../../c4mlib/C4MBios/hardwareset/src/spi_set.h"
#include "../../c4mlib/C4MBios/hardwareset/src/tim_set.h"
#include "../../c4mlib/C4MBios/macro/src/bits_op.h"
#include "../../c4mlib/ServiceProvi/asamotor/src/AS5047D.h"
// #include "E:/mvmc/Motor/c4mlib/ServiceProvi/databuffer/src/remobf.h"
// #include "E:/mvmc/Motor/c4mlib/ServiceProvi/hwimp/src/hardware_interrupt.h"
// #include "E:/mvmc/Motor/c4mlib/ServiceProvi/interrupt/src/frequency_set.h"
#include "../../c4mlib/ServiceProvi/pipeline/src/pipeline_executor.h"
#include "../../c4mlib/ServiceProvi/pipeline/src/pipeline_imp.h"

// #include "../bsp/control_board.h"
#include "../bsp/hal_as5047d.h"
#include "../control/control_config.h"

// #include "../lib/c4mlib.h"
#define N_STEP 1

#define ENABLE 1
#define OK     0

#define PWM_SET  8
#define Pi       3.14159
#define MAX_DUTY (5 / 12.0)

#define SAMPLE_SAMPLEHOLD() PORTD &= ~(1 << 4);  // Set Low(Sample)
#define HOLD_SAMPLEHOLD()   PORTD |= (1 << 4);   // Set High(Hold)
#define GET_PWM_CYCLE()     REGGET(&ICR1, 2, &tempDuty0);
#define NSLEEP_ON()         PORTD |= (1 << 6);

// #define measure_center      240  // degree
// #define eletrical_ang_revol 8
//一個完整的電子角 機械角理論上走1.8 * 4(2相共4個極)= 7.2度，多取到8
// #define upper_limit (45.5) * (measure_center + (eletrical_ang_revol / 2))
// #define lower_limit (45.5) * (measure_center - (eletrical_ang_revol / 2))

// #define print_data_num      64
#define COMMAND_CHANGE_FREQ 1

uint16_t ADC_DATA           = 0;  // ADC結果
uint16_t tempDuty0          = 0;
volatile uint16_t isr_count = 0;

uint8_t print_index = 0;

#define PWM              1
#define USE_WITHOUT_FREQ 1
// #define ENABLE_EXI_ANGLE_I 1
// #define ENABLE_CURRENT_PI 1

//新增微步計算器
#define ADJ_SPEED
#ifdef ADJ_SPEED
/*==========AdjSpeed==========*/
/* Define the adject velocity status */
#    define SM_READY             0
#    define SM_RUNNING           1
#    define SM_OVERLIMIT         2  //要調整的相位超過設定上限
#    define ADJSPEED_MAXNEXTTASK 2
/*==========AdjSpeed 工作方塊結構體型態==========*/
/**
 * @brief AdjSpeed 工作方塊結構體型態
 * @input PhaseAddAngle_p (新增相位調整角度), BackGroudSpeed_p (背景角速度)
 * @output M_IncCmdAngle (新增微步)
 *
 */

typedef struct {
    uint8_t TaskId;       // Task Identity of this Task in Pipeline Executor
    uint8_t* NextTask_p;  // The Array pointer to the Next Task List of this
    // Task
    uint8_t MaxNextTask;       // Maximum number of next task to connect
    int16_t PhaseUpLimitSpeed; /*最高相位調整速度 */
    int16_t PhaseSpeed;        /*相位角速度(w) Phase angle velocity */
    int16_t PhaseResidAngle; /*要調整的相位 Phase residual state variable  */
    uint8_t
        SpeedLimitationState; /*Current status 狀態2=要調整的相位超過設定上限
                                 狀態1=未超過*/
    int16_t* BackGroudSpeed_p; /*input_pin 背景角速度BackGroudSpeed_p*/
    int16_t* PhaseAddAngle_p;  /*input_pin */
    int16_t M_IncCmdAngle; /* Machine Increase Angle 新增相位調整角微步*/
    int16_t* M_IncCmdAngle_p; /*output_pin 新增相位調整角微步*/
    // volatile int16_t* uIn_p[2];  /*pointer to input and output*/
    // volatile int16_t* yOut_p[1]; /*pointer to input and output*/
} AdjSpeed_t;

/*w_back = 1 BackGroudSpeed_p
add = 0 PhaseAddAngle_p
ADJ_VELOCITYSTR.uIn_p[0]  = &ADJ_VELOCITYSTR.PhaseAddAngle_p;
ADJ_VELOCITYSTR.uIn_p[1]  = &ADJ_VELOCITYSTR.BackGroudSpeed_p;
ADJ_VELOCITYSTR.yOut_p[0] = &ADJ_VELOCITYSTR.M_IncCmdAngle;
*/
/*==========ADJ_VELOCITY工作方塊結構體定義及初始化==========*/
#    define ADJ_SPEED_INI                                                      \
        {                                                                      \
            .PhaseUpLimitSpeed = ADJ_MAX, .SpeedLimitationState = SM_READY,    \
            .PhaseSpeed = 0, .PhaseResidAngle = 0,                             \
            .MaxNextTask = ADJSPEED_MAXNEXTTASK                                \
        }

AdjSpeed_t AdjSpeed_str = ADJ_SPEED_INI;
/**
 * @brief 新增微步step
 * 新增微步計算器
 * @param void_p AdjSpeed_t
 */
uint8_t AdjSpeed_step(void* void_p) {  // get_cangle_inc
    AdjSpeed_t* Str_p = (AdjSpeed_t*)void_p;
    // if要調整的相位超過設定上限
    if (Str_p->PhaseResidAngle > Str_p->PhaseUpLimitSpeed) {
        Str_p->PhaseSpeed =
            Str_p->PhaseUpLimitSpeed;  //相位角速度(w)=PhaseUpLimitSpeed
        Str_p->SpeedLimitationState = SM_OVERLIMIT;  //狀態2
    }
    else {
        Str_p->PhaseSpeed =
            Str_p->PhaseResidAngle;  //相位角速度(w)=要調整的相位
        Str_p->SpeedLimitationState = SM_RUNNING;  //狀態1
    }
    //剩餘的相位=剩餘的相位-(相位角速度*單位時間)
    Str_p->PhaseResidAngle = Str_p->PhaseResidAngle - Str_p->PhaseSpeed;
    //計算下次要調整的相位
    Str_p->PhaseResidAngle += *Str_p->PhaseAddAngle_p;
    *Str_p->PhaseAddAngle_p = 0;
    Str_p->M_IncCmdAngle    = *Str_p->BackGroudSpeed_p + Str_p->PhaseSpeed;
    // printf("AdjSpeed_step M_IncCmdAngle: %x\n", Str_p->M_IncCmdAngle);
    return OK;
}

/*==========ADJ_VELOCITY工作方塊佈線==========*/
void AdjSpeed_lay(AdjSpeed_t* Str_p, PipelineStr_t* PLStr_p,
                  uint8_t* PreTaskId_p, uint16_t* BackGroudSpeed_p,
                  uint16_t* PhaseAddAngle_p) {
    /*Register and Enable in SysPipeline  上級*/
    Str_p->TaskId = Pipeline_reg(PLStr_p, AdjSpeed_step, Str_p, (uint8_t*)0);
    /*TaskId Connect to Pre-Task 前級*/
    *PreTaskId_p = Str_p->TaskId;
    /*Data UpStream Connect 上游*/
    Str_p->BackGroudSpeed_p = BackGroudSpeed_p;
    Str_p->PhaseAddAngle_p  = PhaseAddAngle_p;
    /*Internal Connect*/
    static uint8_t AdjSpeed_List_p[ADJSPEED_MAXNEXTTASK] = {0};
    Str_p->NextTask_p                                    = AdjSpeed_List_p;
    Str_p->M_IncCmdAngle_p = &Str_p->M_IncCmdAngle;
}

#endif

//兩相SVPWM
#define SVPWM_H
#ifdef SVPWM_H
#    include <stdint.h>
#    define DEGREE_TO_RADIAN (3.141593 / 180.0)
/*==========SVPWM==========*/
/* Define the adject velocity status */
#    define SVPWM_MAXNEXTTASK 1
/*==========SVPWM 工作方塊結構體型態==========*/
/**
 * @brief SVPWM工作方塊結構體型態
 * @input th_esvpwm_p (回饋激磁角), i_svpwm_p (電流PI回饋)
 * @output pwmA_p (A相PWWduty), pwmB_p (B相PWWduty)
 */
typedef struct {
    uint8_t TaskId;       // Task Identity of this Task in Pipeline Executor
    uint8_t* NextTask_p;  // The Array pointer to the Next Task List of this
    // Task
    uint8_t MaxNextTask;  // Maximum number of next task to connect
    float* th_esvpwm_p;   /*input_pin 回饋激磁角*/
    float* i_svpwm_p;     /*input_pin 電流PI回饋*/
    uint16_t pwmA;        /*A相PWWduty*/
    uint16_t* pwmA_p;     /*output_pin A相PWWduty*/
    uint16_t pwmB;        /*B相PWWduty*/
    uint16_t* pwmB_p;     /*output_pin B相PWWduty*/
} SVPWMStr_t;

/*==========SVPWM工作方塊結構體定義及初始化==========*/
#    define SVPWMINI                                                           \
        { .pwmA = 0, .pwmB = 0, .MaxNextTask = SVPWM_MAXNEXTTASK }

/*
SVPWMSTR.uIn_p[0]  = SVPWMSTR.th_esvpwm_p;
SVPWMSTR.uIn_p[1]   = &SVPWMSTR.i_svpwm_p;
SVPWMSTR.yOut_p[0] = &SVPWMSTR.pwmA_p;
SVPWMSTR.yOut_p[1] = &SVPWMSTR.pwmB_p;
*/

SVPWMStr_t SVPWM_str = SVPWMINI;

/*==========SVPWM工作方塊佈線==========*/

/**
 * @brief SVPWM_step
 *
 * @param void_p
 *
 * SVPWM_str.th_esvpwm_p = &I_EXCITE_str.th_esvpwm_p;//CANGLE_str.ele_dangle_p
 * SVPWM_str.i_svpwm_p = &PI_CURRENT_str.i_svpwm_p;//沒回饋=(+I_SVPWM_HIGH)or
 * (-I_SVPWM_HIGH) //0.9
 */
uint8_t SVPWM_step(void* void_p) {           // cal_pwmAB
    SVPWMStr_t* Str_p = (SVPWMStr_t*)void_p; /*typeset the str pointer */
    // printf("SVPWM_step th_esvpwm : %f, i_svpwm : %f  ", *Str_p->th_esvpwm_p,
    //        *Str_p->i_svpwm_p);
    float temp_ang =
        *Str_p->th_esvpwm_p * (3.14159 / 180);  //單位轉換：角度換弧度
    float ia_svpwm;
    float ib_svpwm;
#    ifdef ENABLE_CURRENT_PI
    /* 由角差I回饋 和 電流PI回饋計算SVPWM */
    if (*Str_p->i_svpwm_p < 0) {
        ia_svpwm = (-*Str_p->i_svpwm_p) * I_SVPWM_GAIN * sinf(temp_ang);
        ib_svpwm = (-*Str_p->i_svpwm_p) * I_SVPWM_GAIN * cosf(temp_ang);
    }
    else if (*Str_p->i_svpwm_p > 0) {
        ia_svpwm = (*Str_p->i_svpwm_p) * I_SVPWM_GAIN * sinf(temp_ang);
        ib_svpwm = (*Str_p->i_svpwm_p) * I_SVPWM_GAIN * cosf(temp_ang);
    }
    else {  //初始化時使用
        ia_svpwm = MAX_DUTY * sinf(temp_ang);
        ib_svpwm = MAX_DUTY * cosf(temp_ang);
    }

    // printf("using ENABLE_CURRENT_PI, ia_svpwm : %f, ib_svpwm : %f ",
    // ia_svpwm,
    //        ia_svpwm);
#    else
    ia_svpwm = MAX_DUTY * sinf(temp_ang);
    ib_svpwm = MAX_DUTY * cosf(temp_ang);

    // printf("Not using ENABLE_CURRENT_PI, ia_svpwm : %f, ib_svpwm : %f ",
    //        ia_svpwm, ia_svpwm);
#    endif
    *Str_p->pwmA_p = ((int8_t)(ia_svpwm * PERIOD_COUNT) + PERIOD_COUNT) >> 1;
    *Str_p->pwmB_p = ((int8_t)(ib_svpwm * PERIOD_COUNT) + PERIOD_COUNT) >> 1;
    // printf("SVPWM_step pwmA : %d, pwmB : %d\n", *Str_p->pwmA_p,
    // *Str_p->pwmB_p);
    return OK;
}

/*==========ADJ_VELOCITY工作方塊佈線==========*/
void SVPWM_lay(SVPWMStr_t* Str_p, PipelineStr_t* PLStr_p, uint8_t* PreTaskId_p,
               float* th_esvpwm_p, float* i_svpwm_p) {
    /*Register and Enable in SysPipeline  上級*/
    Str_p->TaskId = Pipeline_reg(PLStr_p, SVPWM_step, Str_p, (uint8_t*)0);
    /*TaskId Connect to Pre-Task 前級*/
    *PreTaskId_p = Str_p->TaskId;
    /*Data UpStream Connect 上游*/
    Str_p->th_esvpwm_p = th_esvpwm_p;
    Str_p->i_svpwm_p   = i_svpwm_p;
    /*Internal Connect*/
    static uint8_t SVPWM_List_p[SVPWM_MAXNEXTTASK] = {0};
    Str_p->NextTask_p                              = SVPWM_List_p;
    Str_p->pwmA_p                                  = &Str_p->pwmA;
    Str_p->pwmB_p                                  = &Str_p->pwmB;
}

#endif

// S電子角除餘器
#define SANGLE
#ifdef SANGLE
#    include <stdint.h>

#    include "E:/mvmc/Motor/Wei_Code_C4M/control/control_config.h"
#    ifndef abs_int
#        define abs_int(X) ((X < 0) ? (-X) : (X))
#    endif

#    define MAX_ELETRICAL_ROTOR_ANGLE (50)
#    define MAX_ENCODER_VALUE         (16384UL)
#    define HALF_ENCODER_VALUE        (8192UL)
// 327.68
#    define ENC_ELE_ANGLE_TH_L  (327)
#    define ENC_ELE_ANGLE_TH_H  (328)
#    define ENC_ELE_ANGLE_ER    (32)
#    define ENC_ELE_ANGLE_ER_TH (50)
#    define EMC_ELE_AMGLE_COUNT                                                \
        (100) /* per 100 error counters, plus or minus 1 count */

#    define FULL_STEPS_NUM (200)
// #    define SENSOR2DEGREE                                                      \
//         (-360.0 / 327.68) /* 順時鐘為正，encoder順時鐘為遞減 */

/*==========SANGLE==========*/
/* Define the adject velocity status */
#    define SANGLE_MAXNEXTTASK 1

/*==========SANGLE 工作方塊結構體型態==========*/
/**
 * @brief SANGLE工作方塊結構體型態
 * @input enc_angle_p (角度編碼器感測值)
 * @output ele_dangle_p (電子角(度度量))
 */
typedef struct {
    uint8_t TaskId;       // Task Identity of this Task in Pipeline Executor
    uint8_t* NextTask_p;  // The Array pointer to the Next Task List of this
    // Task
    uint8_t MaxNextTask;  // Maximum number of next task to connect
    float K_degree;       /*度度量換算常數*/
    uint16_t init_mach_angle;
    int16_t mach_angle; /*感測機械角*/
    int16_t poles; /*感測電子圈數，馬達轉一圈電子角會轉50圈*/
    int16_t ele_angle;    /*感測電子角*/
    int16_t delta_angle;  /*感測機械角位移*/
    int16_t err_count;    /*感測電子角累積誤差*/
    int16_t* enc_angle_p; /*input_pin 角度編碼器感測值*/
    float ele_dangle;     /*output_pin 電子角(度度量)*/
    float* ele_dangle_p;  /*output_pin 電子角(度度量)*/
} SANGLEStr_t;
/*==========SANGLE工作方塊結構體定義及初始化==========*/
#    define SANGLEINI0                                                         \
        {                                                                      \
            .poles = 0, .ele_angle = 0, .err_count = 0, .mach_angle = 0,       \
            .delta_angle = 0, .MaxNextTask = SANGLE_MAXNEXTTASK                \
        }

SANGLEStr_t SANGLE_str = SANGLEINI0;

// SANGLEStr_t SANGLE_str = SANGLEINI0;
/*==========SANGLE工作方塊佈線==========*/
// #define SANGLE_LAY(SANGLESTR) {
//     SANGLESTR.uIn_p[0] = &SANGLESTR.enc_angle_p;
//     SANGLESTR.yOut_p[0] = &SANGLESTR.ele_dangle_p;
// }
// #    define SANGLE_LAY(SANGLESTR) \
//         { \
//             SANGLESTR.yOut_p[0] = SANGLESTR.ele_dangle_p; \
//             SANGLESTR.K_degree  = -360.0 / 327.68; \
//         }

/*==========SANGLE工作方塊步級執行函式==========*/
uint8_t SANGLE_step(void* void_p) {
    SANGLEStr_t* Str_p = (SANGLEStr_t*)void_p;
    // printf("SANGLE_step enc_angle_p : %x ", *Str_p->enc_angle_p);
    //馬達目前位置-馬達初始位置
    int16_t temp_mach = *Str_p->enc_angle_p - Str_p->init_mach_angle;
    if (temp_mach < 0) {
        temp_mach += MAX_ENCODER_VALUE;
    }
    Str_p->delta_angle = temp_mach - Str_p->mach_angle;
    Str_p->mach_angle  = temp_mach;

    if (abs_int(Str_p->delta_angle) > HALF_ENCODER_VALUE) {
        if (Str_p->delta_angle < 0) {
            Str_p->delta_angle += MAX_ENCODER_VALUE;
        }
        else {
            Str_p->delta_angle -= MAX_ENCODER_VALUE;
        }
    }
    Str_p->ele_angle += Str_p->delta_angle;
    if (Str_p->ele_angle > ENC_ELE_ANGLE_TH_L) {
        Str_p->ele_angle -= ENC_ELE_ANGLE_TH_H;
        Str_p->err_count += ENC_ELE_ANGLE_ER;
        if (Str_p->err_count > ENC_ELE_ANGLE_ER_TH) {
            Str_p->ele_angle++;
            Str_p->err_count -= EMC_ELE_AMGLE_COUNT;
        }
        Str_p->poles++;
        if (Str_p->poles >= MAX_ELETRICAL_ROTOR_ANGLE) {
            Str_p->poles -= MAX_ELETRICAL_ROTOR_ANGLE;
        }
    }
    else if (Str_p->ele_angle < -ENC_ELE_ANGLE_TH_L) {
        Str_p->ele_angle += ENC_ELE_ANGLE_TH_H;
        Str_p->err_count -= ENC_ELE_ANGLE_ER;
        if (Str_p->err_count < -ENC_ELE_ANGLE_ER_TH) {
            Str_p->ele_angle--;
            Str_p->err_count += EMC_ELE_AMGLE_COUNT;
        }
        Str_p->poles--;
        if (Str_p->poles < 0) {
            Str_p->poles += MAX_ELETRICAL_ROTOR_ANGLE;
        }
    }

    /* 順時針為正 */
    Str_p->ele_dangle = (float)Str_p->ele_angle * Str_p->K_degree;
    // printf("SANGLE_step angle : %x, degree : %f\n", Str_p->ele_angle,
    //        Str_p->ele_dangle);

    // TODO:使用完SANGLE_step要在呼叫下式
    // STEP_SACCUMULATOR_step(&STEP_SACCUMULATOR_str);
    return OK;
}

void SANGLE_lay(SANGLEStr_t* Str_p, PipelineStr_t* PLStr_p,
                uint8_t* PreTaskId_p, int16_t* enc_angle_p) {
    /*Register and Enable in SysPipeline  上級*/
    Str_p->TaskId = Pipeline_reg(PLStr_p, SANGLE_step, Str_p, (uint8_t*)0);
    /*TaskId Connect to Pre-Task 前級*/
    *PreTaskId_p = Str_p->TaskId;
    /*Data UpStream Connect 上游*/
    Str_p->enc_angle_p = enc_angle_p;
    /*Internal Connect*/
    static uint8_t SANGLE_List_p[SANGLE_MAXNEXTTASK] = {0};
    Str_p->NextTask_p                                = SANGLE_List_p;
    Str_p->ele_dangle_p                              = &Str_p->ele_dangle;
}

#endif

// C電子角累進器
#define CANGLE
#ifdef CANGLE
#    include <stdint.h>

#    include "E:/mvmc/Motor/Wei_Code_C4M/control/control_config.h"
#    ifndef abs_int
#        define abs_int(X) ((X < 0) ? (-X) : (X))
#    endif

#    define MAX_ELETRICAL_ROTOR_ANGLE (50)
#    define MAX_ENCODER_VALUE         (16384UL)
#    define HALF_ENCODER_VALUE        (8192UL)
// 327.68
#    define ENC_ELE_ANGLE_TH_L  (327)
#    define ENC_ELE_ANGLE_TH_H  (328)
#    define ENC_ELE_ANGLE_ER    (32)
#    define ENC_ELE_ANGLE_ER_TH (50)
#    define EMC_ELE_AMGLE_COUNT                                                \
        (100) /* per 100 error counters, plus or minus 1 count */

#    define FULL_STEPS_NUM (200)
// #    define SENSOR2DEGREE                                                      \
//         (-360.0 / 327.68) /* 順時鐘為正，encoder順時鐘為遞減 */

/*==========CANGLE==========*/
/* Define the adject velocity status */
#    define CANGLE_MAXNEXTTASK 1
/*==========CANGLE工作方塊結構體型態==========*/
/**
 * @brief CANGLE工作方塊結構體型態
 * @input th_inc_p (微步累加值)
 * @output ele_dangle_p (命令電子角)
 */
typedef struct {
    uint8_t TaskId;       // Task Identity of this Task in Pipeline Executor
    uint8_t* NextTask_p;  // The Array pointer to the Next Task List of this
    // Task
    uint8_t MaxNextTask;  // Maximum number of next task to connect
    float K_degree;       /*度量換算常數*/
    int16_t ele_angle;    /*電子角狀態變數*/
    int16_t ele_limit;    /*電子角轉動上限*/
    int16_t* th_inc_p;    /*input_pin 微步累加值*/
    float ele_dangle;     /*output_pin 命令電子角*/
    float* ele_dangle_p;  /*output_pin 命令電子角*/

} CANGLEStr_t;

#    define CANGLE_INI                                                         \
        { .MaxNextTask = CANGLE_MAXNEXTTASK }

CANGLEStr_t CANGLE_str = CANGLE_INI;

/**
 * @brief Command電子角除餘器
 * C電子角除餘器
 * CANGLE
 * @param void_p CANGLEStr_t
 * Str_p->uIn_p[0]=AdjSpeed_str.add_step
 */
uint8_t CANGLE_step(void* void_p) {
    CANGLEStr_t* Str_p = (CANGLEStr_t*)void_p;
    Str_p->ele_angle += *Str_p->th_inc_p;  // AdjSpeed_str.add_step
    // Str_p->ele_limit:一圈的微步數
    //檢查:ele_angle不超過+-一圈(超過歸0)
    if (Str_p->ele_angle >= Str_p->ele_limit ||
        Str_p->ele_angle <= -Str_p->ele_limit) {
        Str_p->ele_angle = 0;
    }
    /* 360度餘數 */
    // Str_p->K_degree:(90/Nstep)(電子角/微步)
    Str_p->ele_dangle = (int16_t)(Str_p->ele_angle * Str_p->K_degree) % 360;
    // printf("Cele_degree ele_angle : %d, ele_dangle : %f\n", Str_p->ele_angle,
    //        Str_p->ele_dangle);

    // TODO:使用完CANGLE_step要再呼叫下式
    // STEP_CACCUMULATOR_step(&STEP_CACCUMULATOR_str);
    return OK;
}

void CANGLE_lay(CANGLEStr_t* Str_p, PipelineStr_t* PLStr_p,
                uint8_t* PreTaskId_p, int16_t* th_inc_p) {
    /*Register and Enable in SysPipeline  上級*/
    Str_p->TaskId = Pipeline_reg(PLStr_p, CANGLE_step, Str_p, (uint8_t*)0);
    /*TaskId Connect to Pre-Task 前級*/
    *PreTaskId_p = Str_p->TaskId;
    /*Data UpStream Connect 上游*/
    Str_p->th_inc_p = th_inc_p;
    /*Internal Connect*/
    static uint8_t CANGLE_List_p[CANGLE_MAXNEXTTASK] = {0};
    Str_p->NextTask_p                                = CANGLE_List_p;
    Str_p->ele_dangle_p                              = &Str_p->ele_dangle;

    //參數區段
    Str_p->K_degree  = 360.0 / (N_STEP * FULL_STEPS_NUM) * (90.0 / 1.8);
    Str_p->ele_limit = N_STEP * FULL_STEPS_NUM;
}

#endif

//激磁角回饋
#define I_EXCITE_ANGLE_H
#ifdef I_EXCITE_ANGLE_H
#    include <stdint.h>

#    include "E:/mvmc/Motor/Wei_Code_C4M/control/control_config.h"
#    include "E:/mvmc/Motor/Wei_Code_C4M/control/pid.h"

#    ifndef abs_float
#        define abs_float(X) ((X < 0) ? (-X) : (X))
#    endif
#    ifndef abs_int
#        define abs_int(X) ((X < 0) ? (-X) : (X))
#    endif
/*==========I_EXCITE==========*/
/* Define the adject velocity status */
#    define I_EXCITE_MAXNEXTTASK 1
/*==========I_EXCITE工作方塊結構體型態==========*/
/**
 * @brief I_EXCITE工作方塊結構體型態
 * @input e_sdegree_p (感測電子角(度度量)), e_cdegree_p (命令電子角(度度量))
 * @output th_cum_p(角度累計誤差), th_er_p (激磁角度誤差), th_esvpwm_p
 * (激磁角度)
 * @parameter ki
 */
typedef struct {
    uint8_t TaskId;       // Task Identity of this Task in Pipeline Executor
    uint8_t* NextTask_p;  // The Array pointer to the Next Task List of this
    // Task
    uint8_t MaxNextTask;  // Maximum number of next task to connect
    fb_pid_t pid;
    float cum_limit;    /*激磁角累計上限*/
    float last_th;      /*相位待調整角度*/
    float w;            /*角速度*/
    float* e_sdegree_p; /*input_pin 感測電子角(度度量)*/
    float* e_cdegree_p; /*input_pin 命令電子角(度度量)*/
    float th_er;        /*output_pin 激磁角度誤差*/
    float* th_er_p;     /*output_pin 激磁角度誤差*/
    float th_cum;       /*output_pin 角度累計誤差*/
    float* th_cum_p;    /*output_pin 角度累計誤差*/
    float th_esvpwm;    /*output_pin 激磁角度*/
    float* th_esvpwm_p; /*output_pin 激磁角度*/

} I_EXCITEStr_t;

/*==========I_EXCITE工作方塊結構體定義及初始化==========*/
#    define I_EXCITEINI0                                                       \
        {                                                                      \
            .pid.kp = 0, .pid.ki = EXC_KI, .pid.kd = 0, .th_cum = 0,           \
            .th_er = 0, .th_esvpwm = 0, .MaxNextTask = I_EXCITE_MAXNEXTTASK    \
        }

I_EXCITEStr_t I_EXCITE_str = I_EXCITEINI0;

/*==========I_EXCITE工作方塊步級執行函式==========*/
/**
 * @brief I_EXCITE_step
 *
 * @param void_p
 *
 * I_EXCITE_str.e_sdegree_p = &SANGLE_str.e_sdegree_p;
 * I_EXCITE_str.uIn_p[1] = &CANGLE_str.e_sdegree_p;
 */
uint8_t I_EXCITE_step(void* void_p) {  // cal_exc_ang_correct
    I_EXCITEStr_t* Str_p = (I_EXCITEStr_t*)void_p;
    // printf("I_EXCITE_step e_sdegree : %f, e_cdegree : %f  ",
    //        *Str_p->e_sdegree_p, *Str_p->e_cdegree_p);
    /* 計算C電子角(命令) 與 S電子角誤差(感測) (度度量) */
    /* th_er > 0 領先, th_er < 0 落後 */
    Str_p->th_er = (*Str_p->e_sdegree_p) - (*Str_p->e_cdegree_p);
    if (abs_float(Str_p->th_er) >= 180) {
        if ((*Str_p->e_sdegree_p) > (*Str_p->e_cdegree_p)) {
            /* 馬達落後 */
            Str_p->th_er -= 360;
        }
        else {
            /* 馬達領先 */
            Str_p->th_er += 360;
        }
    }

    Str_p->th_cum += Str_p->th_er; /* 累計誤差 */

    /* 限制上下界 */
    if (Str_p->th_cum >= Str_p->cum_limit)
        Str_p->th_cum = Str_p->cum_limit;
    if (Str_p->th_cum <= -Str_p->cum_limit)
        Str_p->th_cum = -Str_p->cum_limit;
        // printf("I_EXCITE_step the_er : %f, th_cum %f  ", Str_p->th_er,
        //        Str_p->th_cum);
#    ifdef ENABLE_EXI_ANGLE_I
    /* 計算 th_esvpwm 值 (角差I回饋) */
    Str_p->th_esvpwm = (*Str_p->e_cdegree_p) - Str_p->pid.ki * Str_p->th_cum;
    /* theta svpwm to positive */
    if (Str_p->th_esvpwm > 360)
        Str_p->th_esvpwm -= 360;
    else if (Str_p->th_esvpwm < 0)
        Str_p->th_esvpwm += 360;
        // printf("Using EXI angle I \n");
#    else
    Str_p->th_esvpwm = (*Str_p->e_cdegree_p);
    // printf("I_EXCITE_step Not Using EXI angle I \n");
#    endif

    /* Calculate and save omega (rad/s) */
    float delta_theta = (*Str_p->e_sdegree_p) - Str_p->last_th;
    if (delta_theta > 180) {
        delta_theta -= 360;
    }
    else if (delta_theta < -180) {
        delta_theta += 360;
    }
    Str_p->w = delta_theta * DELTA_THETA_TO_OMEGA;

    Str_p->last_th = (*Str_p->e_sdegree_p);
    return OK;
}

void I_EXCITE_lay(I_EXCITEStr_t* Str_p, PipelineStr_t* PLStr_p,
                  uint8_t* PreTaskId_p, float* e_sdegree_p,
                  float* e_cdegree_p) {
    /*Register and Enable in SysPipeline  上級*/
    Str_p->TaskId = Pipeline_reg(PLStr_p, I_EXCITE_step, Str_p, (uint8_t*)0);
    /*TaskId Connect to Pre-Task 前級*/
    *PreTaskId_p = Str_p->TaskId;
    /*Data UpStream Connect 上游*/
    Str_p->e_sdegree_p = e_sdegree_p;
    Str_p->e_cdegree_p = e_cdegree_p;
    /*Internal Connect*/
    static uint8_t I_EXCITE_List_p[I_EXCITE_MAXNEXTTASK] = {0};
    Str_p->NextTask_p                                    = I_EXCITE_List_p;
    Str_p->th_cum_p                                      = &Str_p->th_cum;
    Str_p->th_er_p                                       = &Str_p->th_er;
    Str_p->th_esvpwm_p                                   = &Str_p->th_esvpwm;

    //參數區段
    Str_p->cum_limit = 120.0 / EXC_KI;
}

#endif

//電流回饋
#define PI_CURRENT_H
#ifdef PI_CURRENT_H
/*==========PI_CURRENT==========*/
#    define PI_CURRENT_MAXNEXTTASK 1
/*==========PI_CURRENT工作方塊結構體型態==========*/
/**
 * @brief PI_CURRENT工作方塊結構體型態
 * @input th_cum_p (角度累積誤差) th_er_p (角度誤差)
 * @output i_svpwm_p 激磁電流)
 *
 */
typedef struct {
    uint8_t TaskId;       // Task Identity of this Task in Pipeline Executor
    uint8_t* NextTask_p;  // The Array pointer to the Next Task List of this
    // Task
    uint8_t MaxNextTask;  // Maximum number of next task to connect
    fb_pid_t pid;
    float low_limit;  /*激磁角下限*/
    float high_limit; /*激磁角上限*/
    float* th_cum_p;  /*input_pin 角度累積誤差*/
    float* th_er_p;   /*input_pin 角度誤差*/
    float i_svpwm;    /*output_pin 激磁電流*/
    float* i_svpwm_p; /*output_pin 激磁電流*/
} PI_CURRENTStr_t;
/*==========PI_CURRENT工作方塊結構體定義及初始化==========*/
#    define PI_CURRENTINI0                                                     \
        {                                                                      \
            .pid.kp = I_SVPWM_KP, .pid.ki = I_SVPWM_KI, .i_svpwm = 0,          \
            .low_limit = LOW_LIMIMT, .high_limit = HIGH_LIMIT,                 \
            .MaxNextTask = PI_CURRENT_MAXNEXTTASK                              \
        }

PI_CURRENTStr_t PI_CURRENT_str = PI_CURRENTINI0;

/*==========PI_CURRENT工作方塊佈線==========*/

/**
 * @brief PI_CURRENT_step
 *
 * @param void_p
 *
 */
uint8_t PI_CURRENT_step(void* void_p) {  // cal_current_correct
    PI_CURRENTStr_t* Str_p =
        (PI_CURRENTStr_t*)void_p; /*typeset the str pointer */
    // printf("PI_CURRENT_step th_er : %f, th_cum : %f  ", *Str_p->th_er_p,
    //        *Str_p->th_cum_p);
#    ifdef ENABLE_CURRENT_PI
    /* C電子角(命令) 與 S電子角誤差(感測)、 累計誤差，作電流PI回饋 */
    Str_p->i_svpwm = I_SVPWM_EXI_BASE - (Str_p->pid.kp * (*Str_p->th_er_p)) -
                     (Str_p->pid.ki * (*Str_p->th_cum_p));

    /* 限制上下界 */
    if (Str_p->i_svpwm < Str_p->low_limit)
        Str_p->i_svpwm = Str_p->low_limit;
    if (Str_p->i_svpwm > Str_p->high_limit)
        Str_p->i_svpwm = Str_p->high_limit;
        // printf("ENABLE_CURRENT_PI, isvpwm : %f\n", Str_p->i_svpwm);
#    else
    if (Str_p->th_er_p < 0) {
        Str_p->i_svpwm = -Str_p->low_limit;
    }
    else {
        Str_p->i_svpwm = Str_p->high_limit;
    }
    // printf("DISABLE_CURRENT_PI, isvpwm : %f\n", Str_p->i_svpwm);
#    endif
    return OK;
}

void PI_CURRENT_lay(PI_CURRENTStr_t* Str_p, PipelineStr_t* PLStr_p,
                    uint8_t* PreTaskId_p, float* th_cum_p, float* th_er_p) {
    /*Register and Enable in SysPipeline  上級*/
    Str_p->TaskId = Pipeline_reg(PLStr_p, PI_CURRENT_step, Str_p, (uint8_t*)0);
    /*TaskId Connect to Pre-Task 前級*/
    *PreTaskId_p = Str_p->TaskId;
    /*Data UpStream Connect 上游*/
    Str_p->th_cum_p = th_cum_p;
    Str_p->th_er_p  = th_er_p;
    /*Internal Connect*/
    static uint8_t PI_CURRENT_List_p[PI_CURRENT_MAXNEXTTASK] = {0};
    Str_p->NextTask_p = PI_CURRENT_List_p;
    Str_p->i_svpwm_p  = &Str_p->i_svpwm;

    //參數區段
}

#endif

//命令微步累加器
#define STEP_CACCUMULATOR
#ifdef STEP_CACCUMULATOR
#    ifndef abs_int
#        define abs_int(X) ((X < 0) ? (-X) : (X))
#    endif

#    define SENSOR_RES  (16384UL)
#    define SENSOR_HALF (SENSOR_RES >> 1)
/*==========STEP_CACCUMULATOR==========*/
#    define STEP_CACCUMULATOR_MAXNEXTTASK 1
/*==========STEP_CACCUMULATOR工作方塊結構體型態==========*/
/**
 * @brief STEP_CACCUMULATOR工作方塊結構體型態
 * @input th_inc_p (新增微步)
 * @output c_theta_total_p (命令總位移量)
 *
 */
typedef struct {
    uint8_t TaskId;       // Task Identity of this Task in Pipeline Executor
    uint8_t* NextTask_p;  // The Array pointer to the Next Task List of this
    // Task
    uint8_t MaxNextTask;      // Maximum number of next task to connect
    float c_length;           /*累積總微步量*/
    float k_theta2legnth;     /*總微步量至總位移量增益*/
    int16_t* th_inc_p;        /*input_pin 新增微步*/
    int16_t c_theta_total;    /*output_pin 命令總位移量*/
    int16_t* c_theta_total_p; /*output_pin 命令總位移量*/
} STEP_CACCUMULATORStr_t;
/*==========STEP_CACCUMULATOR工作方塊結構體定義及初始化==========*/
#    define STEP_CACCUMULATORINI                                               \
        {                                                                      \
            .k_theta2legnth = STEP_C_THETA_TO_LENGTH,                          \
            .MaxNextTask    = STEP_CACCUMULATOR_MAXNEXTTASK                    \
        }

STEP_CACCUMULATORStr_t STEP_CACCUMULATOR_str = STEP_CACCUMULATORINI;

/*==========STEP_CACCUMULATOR工作方塊佈線==========*/

/**
 * @brief Command微步累加器
 * 收數據用->累加AdjSpeed_str.add_step再乘上係數轉成長度
 * 命令微步累加器
 *
 * @param void_p STEP_CACCUMULATORStr_t
 */
uint8_t STEP_CACCUMULATOR_step(void* void_p) {
    STEP_CACCUMULATORStr_t* Str_p = (STEP_CACCUMULATORStr_t*)void_p;

    /* accumulate theta */
    Str_p->c_theta_total += *Str_p->th_inc_p;
    Str_p->c_theta_total = Str_p->c_theta_total % (200 * N_STEP);
    /* transfer theta to length */
    Str_p->c_length = (float)Str_p->c_theta_total * Str_p->k_theta2legnth;
    return OK;
}

void STEP_CACCUMULATOR_lay(STEP_CACCUMULATORStr_t* Str_p,
                           PipelineStr_t* PLStr_p, uint8_t* PreTaskId_p,
                           int16_t* th_inc_p) {
    /*Register and Enable in SysPipeline  上級*/
    Str_p->TaskId =
        Pipeline_reg(PLStr_p, STEP_CACCUMULATOR_step, Str_p, (uint8_t*)0);
    /*TaskId Connect to Pre-Task 前級*/
    *PreTaskId_p = Str_p->TaskId;
    /*Data UpStream Connect 上游*/
    Str_p->th_inc_p = th_inc_p;
    /*Internal Connect*/
    static uint8_t STEP_CACCUMULATOR_List_p[STEP_CACCUMULATOR_MAXNEXTTASK] = {
        0};
    Str_p->NextTask_p      = STEP_CACCUMULATOR_List_p;
    Str_p->c_theta_total_p = &Str_p->c_theta_total;

    //參數區段
}

#endif

//實際微步累加器
#define STEP_SACCUMULATOR
#ifdef STEP_SACCUMULATOR
/*==========STEP_SACCUMULATOR==========*/
/* Define the adject velocity status */
#    define STEP_SACCUMULATOR_MAXNEXTTASK 1
/*==========STEP_SACCUMULATOR工作方塊結構體型態==========*/
/**
 * @brief STEP_SACCUMULATOR工作方塊結構體型態
 * @input th_s_p (編碼器讀值)
 * @output s_length (實際總位移量)
 *
 */
typedef struct {
    uint8_t TaskId;       // Task Identity of this Task in Pipeline Executor
    uint8_t* NextTask_p;  // The Array pointer to the Next Task List of this
    // Task
    uint8_t MaxNextTask;  // Maximum number of next task to connect
    int16_t last_th_s;    /*前一狀態之編碼器讀值*/
    int16_t delta_th;     /*實際位移變化*/
    int16_t th_init;      /*位址原點編碼器初值*/
    int16_t cycles;       /*已跑圈數*/
    float k_theta2legnth; /*角度實際總位移量轉換常數*/
    int16_t th_inc;       /*微步累加值*/
    int16_t* th_s_p;      /*input_pin 編碼器讀值*/
    float s_length;       /*output_pin 實際總位移量*/
    float* s_length_p;    /*output_pin 實際總位移量*/

} STEP_SACCUMULATORStr_t;
/*==========STEP_SACCUMULATOR工作方塊結構體定義及初始化==========*/
#    define STEP_SACCUMULATORINI                                               \
        {                                                                      \
            .k_theta2legnth = STEP_S_THETA_TO_LENGTH,                          \
            .MaxNextTask    = STEP_SACCUMULATOR_MAXNEXTTASK                    \
        }

STEP_SACCUMULATORStr_t STEP_SACCUMULATOR_str = STEP_SACCUMULATORINI;

/**
 * @brief 實際微步累加器
 * STEP_SACCUMULATOR工作方塊步級執行函式
 * @param void_p
 */
uint8_t STEP_SACCUMULATOR_step(void* void_p) {  // update_step_saccum
    STEP_SACCUMULATORStr_t* Str_p =
        (STEP_SACCUMULATORStr_t*)void_p; /*typeset the str pointer */
    /* save last delta theta value */
    Str_p->delta_th = (uint16_t)roundf(*Str_p->th_s_p) - Str_p->last_th_s;
    if (abs_int(Str_p->delta_th) > SENSOR_HALF) {
        /* ZERO to MAX */
        if (Str_p->delta_th > 0) {
            Str_p->cycles++;
        }
        /* MAX to ZERO */
        else {
            Str_p->cycles--;
        }
    }
    /* transfer theta to length */
    Str_p->s_length =
        (float)(Str_p->cycles * SENSOR_RES + (uint16_t)roundf(*Str_p->th_s_p) -
                Str_p->th_init) *
        Str_p->k_theta2legnth;
    /* update last sensor theta */
    Str_p->last_th_s = (uint16_t)roundf(*Str_p->th_s_p);
    return OK;
}

void STEP_SACCUMULATOR_lay(STEP_SACCUMULATORStr_t* Str_p,
                           PipelineStr_t* PLStr_p, uint8_t* PreTaskId_p,
                           int16_t* th_s_p) {
    /*Register and Enable in SysPipeline  上級*/
    Str_p->TaskId =
        Pipeline_reg(PLStr_p, STEP_SACCUMULATOR_step, Str_p, (uint8_t*)0);

    /*TaskId Connect to Pre-Task 前級*/
    *PreTaskId_p = Str_p->TaskId;
    /*Data UpStream Connect 上游*/
    Str_p->th_s_p = th_s_p;
    /*Internal Connect*/
    static uint8_t STEP_SACCUMULATOR_List_p[STEP_SACCUMULATOR_MAXNEXTTASK] = {
        0};
    Str_p->NextTask_p = STEP_SACCUMULATOR_List_p;
    Str_p->s_length_p = &Str_p->s_length;

    //參數區段
}
#endif

// void macro_init(void);
void data_init(void);
void sbj_init(void);

uint8_t AdjSpeed_step(void* void_p);
uint8_t CANGLE_step(void* void_p);
uint8_t STEP_CACCUMULATOR_step(void* void_p);
uint8_t SANGLE_step(void* void_p);
uint8_t STEP_SACCUMULATOR_step(void* void_p);
uint8_t I_EXCITE_step(void* void_p);
uint8_t PI_CURRENT_step(void* void_p);
uint8_t SVPWM_step(void* void_p);

void control_print(void);
void control_handle(void);
void updateDuty(void);
void SenseCurrent(void);
void ADCTrigger1(void);
void ADCTrigger2(void);
void ADCTrigger3(void);
void ADCTrigger4(void);

extern as50474_t as5047d; /* AS5047D motor encoder IC */
uint16_t adcResult[4] = {0};

uint8_t PWM1_init(void) {
    HardWareSet_t PWM1HWSet_str       = PWM1HWSETSTRINI;
    HWFlagPara_t PWM1FgGpData_str[10] = PWM1FLAGPARALISTINI;
    HWRegPara_t PWM1RegData_str[4]    = PWM1REGPARALISTINI;
    PWM1HWSetDataStr_t PWM1HWSetData  = PWM1SETDATALISTINI;
    HARDWARESET_LAY(PWM1HWSet_str, PWM1FgGpData_str[0], PWM1RegData_str[0],
                    PWM1HWSetData);
    return HardwareSet_step(&PWM1HWSet_str);
}

#define MYTIM2SETDATALISTINI                                                   \
    {                                                                          \
        .WGMn0_1 = TIM0_2_SQUARE_WAVE, .CSn0_2 = TIM123_CLK_DIV_BY8,           \
        .COMn0_1 = ENABLE, .DDx = OUTPUT, .TIM2EN = ENABLE,                    \
        .FlagTotalBytes = 5, .OCRn = 100, .RegTotalBytes = 1                   \
    }

uint8_t TIM2_init(void) {
    HardWareSet_t TIM2HWSet_str      = TIM2HWSETSTRINI;
    HWFlagPara_t TIM2FgGpData_str[6] = TIM2FLAGPARALISTINI;
    HWRegPara_t TIM2RegData_str      = TIM2REGPARALISTINI;
    TIM2HWSetDataStr_t TIM2HWSetData = MYTIM2SETDATALISTINI;
    HARDWARESET_LAY(TIM2HWSet_str, TIM2FgGpData_str[0], TIM2RegData_str,
                    TIM2HWSetData)
    return HardwareSet_step(&TIM2HWSet_str);
}

uint8_t ADC_init(void) {
    HardWareSet_t ADCHWSet_str      = ADCHWSETSTRINI;
    HWFlagPara_t ADCFgGpData_str[8] = ADCFLAGPARALISTINI;
    HWRegPara_t ADCRegData_str      = ADCREGPARALISTINI;
    ADCHWSetDataStr_t ADCHWSetData  = ADCSETDATALISTINI;
    HARDWARESET_LAY(ADCHWSet_str, ADCFgGpData_str[0], ADCRegData_str,
                    ADCHWSetData);
    return HardwareSet_step(&ADCHWSet_str);
}

uint8_t SPI_init(void) {
    HardWareSet_t SPIHWSet_str       = SPIHWSETSTRINI;
    HWFlagPara_t SPIFgGpData_str[11] = SPIFLAGPARALISTINI;
    HWRegPara_t SPIRegData_str       = SPIREGPARALISTINI;
    SPIHWSetDataStr_t SPIHWSetData   = SPISETDATALISTINI;
    HARDWARESET_LAY(SPIHWSet_str, SPIFgGpData_str[0], SPIRegData_str,
                    SPIHWSetData);
    return HardwareSet_step(&SPIHWSet_str);
}

int main(void) {
    C4M_DEVICE_set();
    printf("start  this is test_identification_fb_c4m\n");

    /* 馬達硬體設定 */
    DDRD |= (1 << 4) | (1 << 6);  // set D4(S&H) D6(NSleep) output
    PWM1_init();
    TIM2_init();

    ADC_init();  // 32,64用212,16用215
    // AdjSpeed
    uint8_t pipeLineStart   = 0;
    uint16_t BackGroudSpeed = 1;
    uint16_t PhaseAddAngle  = 0;
    AdjSpeed_lay(&AdjSpeed_str, &SysPipeline_str, &pipeLineStart,
                 &BackGroudSpeed, &PhaseAddAngle);
    CANGLE_lay(&CANGLE_str, &SysPipeline_str, AdjSpeed_str.NextTask_p[0],
               AdjSpeed_str.M_IncCmdAngle_p);
    STEP_CACCUMULATOR_lay(&STEP_CACCUMULATOR_str, &SysPipeline_str,
                          CANGLE_str.NextTask_p[1],
                          AdjSpeed_str.M_IncCmdAngle_p);

    SANGLE_lay(&SANGLE_str, &SysPipeline_str,
               STEP_CACCUMULATOR_str.NextTask_p[0], &as5047d.angle);

    STEP_SACCUMULATOR_lay(&STEP_SACCUMULATOR_str, &SysPipeline_str,
                          SANGLE_str.NextTask_p[0], SANGLE_str.enc_angle_p);
    I_EXCITE_lay(&I_EXCITE_str, &SysPipeline_str,
                 STEP_SACCUMULATOR_str.NextTask_p[0], SANGLE_str.ele_dangle_p,
                 CANGLE_str.ele_dangle_p);
    PI_CURRENT_lay(&PI_CURRENT_str, &SysPipeline_str,
                   I_EXCITE_str.NextTask_p[0], I_EXCITE_str.th_cum_p,
                   I_EXCITE_str.th_er_p);

    SVPWM_lay(&SVPWM_str, &SysPipeline_str, PI_CURRENT_str.NextTask_p[0],
              I_EXCITE_str.th_esvpwm_p, PI_CURRENT_str.i_svpwm_p);

    // /* 周邊硬體設定 */
    SpiEncoder_Init();  // spi initial
    as5047d.init();     // Encoder  initial

    /*控制演算法初始化*/
    // macro_init();  //功能方塊"內"的連線

    // sbj_init();  //功能方塊 與 功能方塊 "之間"的連線

    data_init();  //功能方塊初始化

    SVPWM_step(&SVPWM_str);
    OCR1A = (uint8_t)(*SVPWM_str.pwmA_p);
    OCR1B = (uint8_t)(*SVPWM_str.pwmB_p);
    NSLEEP_ON();  // open DRV8847

    data_init();  //功能方塊初始化
    *SVPWM_str.pwmA_p = 176;
    *SVPWM_str.pwmB_p = 176;
    sei();

    SAMPLE_SAMPLEHOLD();
    GET_PWM_CYCLE();  //讀取pwm.cfg中設定的ICR1存成全域變數tempDuty0
    while (1) {
        ;
    }
}

// ISR(TIMER2_COMP_vect) {
//     isr_count++;
//     switch (isr_count) {
//         case 1: {
//             as5047d.update();
//             break;
//         }
//         case 2: {
//             break;
//         }
//         case 3: {
//             // SenseCurrent();  //將PWM duty 暫時設為50%
//             HOLD_SAMPLEHOLD();
//             break;
//         }
//         case 4: {
//             // updateDuty(); // SenseCurrent
//             ADCTrigger1();
//             break;
//         }
//         case 5: {
//             ADCTrigger2();
//             break;
//         }
//         case 6: {
//             ADCTrigger3();
//             break;
//         }
//         case (PWM_SET - 1): {
//             ADCTrigger4();
//             break;
//         }
//         case PWM_SET: {
//             SAMPLE_SAMPLEHOLD();
//             control_print();
//             break;
//         }
//         case (PWM_SET + 1): {
//             control_handle();
//             updateDuty();
//         }
//         case 5000: {
//             isr_count = 0;
//             break;
//         }
//         default: {
//             break;
//         }
//     }
// }

ISR(TIMER2_COMP_vect) {
    isr_count++;
    switch (isr_count) {
        case 1: {
            AdjSpeed_step(&AdjSpeed_str);
            CANGLE_step(&CANGLE_str);
            // STEP_CACCUMULATOR_step(&STEP_CACCUMULATOR_str);
            // SANGLE_step(&SANGLE_str);
            // STEP_SACCUMULATOR_step(&STEP_SACCUMULATOR_str);
            //激磁角回饋
            // I_EXCITE_step(&I_EXCITE_str);
            // //角差PI調電流
            // PI_CURRENT_step(&PI_CURRENT_str);
            SVPWM_str.th_esvpwm_p = CANGLE_str.ele_dangle_p;
            SVPWM_step(&SVPWM_str);
            break;
        }
        case 2: {
            updateDuty();
            break;
        }
        default: {
            isr_count = 0;
            break;
        }
    }
}

// void macro_init(void) {  // function block init
//  ADJ_VELOCITY_LAY(AdjSpeed_str);
//  SVPWM_lay(&SVPWM_str,&SysPipeline_str,(uint8_t
//  *)0,AdjSpeed_str.TaskId,);

// CANGLE_LAY(CANGLE_str);
// STEP_CACCUMULATOR_LAY(STEP_CACCUMULATOR_str);
// SANGLE_LAY(SANGLE_str);
// STEP_SACCUMULATOR_LAY(STEP_SACCUMULATOR_str);
// I_EXCITE_LAY(I_EXCITE_str);
// PI_CURRENT_LAY(PI_CURRENT_str);
//}

void data_init(void) {
    /* Initialize machanical angle */
    for (int i = 0; i < 16; i++) {
        as5047d.update();  // update encoder angle to be first machanical angle
    }
    CANGLE_str.ele_angle = as5047d.angle / 16384.0 * CANGLE_str.ele_limit;
    *CANGLE_str.ele_dangle_p =
        (int16_t)(CANGLE_str.ele_angle * CANGLE_str.K_degree) % 360;
    *I_EXCITE_str.th_esvpwm_p =
        (int16_t)(CANGLE_str.ele_angle * CANGLE_str.K_degree) % 360;

    STEP_SACCUMULATOR_str.th_init =
        as5047d.angle; /*set_saccum_th_init(&s_accum, as5047d.angle);*/
    SANGLE_str.init_mach_angle = as5047d.angle;
    *STEP_CACCUMULATOR_str.c_theta_total_p =
        as5047d.angle / 16384.0 * 360.0 / 1.8 * (float)N_STEP;

    // printf("encoder : %d\n", as5047d.angle);
    // printf("init_mach_angle : %x\n", SANGLE_str.init_mach_angle);
    // printf("Cele_degree ele_angle : %d, ele_dangle : %f\t%f\t%f\t%f\n",
    //        CANGLE_str.ele_angle, *CANGLE_str.ele_dangle_p,
    //        *I_EXCITE_str.e_sdegree_p, *I_EXCITE_str.th_esvpwm_p,
    //        *SVPWM_str.uIn_p[0]);
}

// void sbj_init(void) {                              //各funcion的傳參連結
//     SANGLE_str.enc_angle_p       = &as5047d.angle; /*fir.output是float*/
//     STEP_SACCUMULATOR_str.th_s_p = SANGLE_str.enc_angle_p;

//     I_EXCITE_str.e_sdegree_p = SANGLE_str.ele_dangle_p;
//     I_EXCITE_str.e_cdegree_p = CANGLE_str.ele_dangle_p;
//     PI_CURRENT_str.th_er_p   = I_EXCITE_str.th_er_p;
//     PI_CURRENT_str.th_cum_p  = I_EXCITE_str.th_cum_p;

//     SVPWM_str.th_esvpwm_p =
//         I_EXCITE_str.th_esvpwm_p;  //沒回饋=CANGLE_str.ele_dangle_p
//     SVPWM_str.i_svpwm_p =
//         PI_CURRENT_str
//             .i_svpwm_p;  //沒回饋=(+I_SVPWM_HIGH)or (-I_SVPWM_HIGH) //0.9

//     CANGLE_str.th_inc_p            = &AdjSpeed_str.M_IncCmdAngle;
//     STEP_CACCUMULATOR_str.th_inc_p = &AdjSpeed_str.M_IncCmdAngle;
// }

//==========================命令control_print==============================
//=========================================================================
void control_print(void) {  //沒有print是1.03ms
    uint16_t Cangle = (*STEP_CACCUMULATOR_str.c_theta_total_p) / (float)N_STEP *
                      81.92;  //激磁電流誤差 81.92=16384/200
    // as5047d.update();
    printf("[485],%d,%d,%d,%d,%d,%d,%.2f,%d,%d,%f,%f,%f\n", adcResult[0],
           adcResult[1], adcResult[2], adcResult[3], as5047d.angle, Cangle,
           *I_EXCITE_str.th_cum_p, *SVPWM_str.pwmA_p, *SVPWM_str.pwmB_p,
           I_EXCITE_str.pid.ki, PI_CURRENT_str.pid.kp, PI_CURRENT_str.pid.ki);
    // printf("[485],%d,%d,%d,%d\n", as5047d.angle,

    AdjSpeed_step(&AdjSpeed_str);
    CANGLE_step(&CANGLE_str);
    STEP_CACCUMULATOR_step(&STEP_CACCUMULATOR_str);
}

//==========================回饋control_handle==============================
//==========================================================================
void control_handle(void) {  // 0.7ms
    // S電子角除餘器
    /*原型是updare_sangle，後面緊跟STEP_SACCUMULATOR_step*/
    SANGLE_step(&SANGLE_str);
    STEP_SACCUMULATOR_step(&STEP_SACCUMULATOR_str);
    //激磁角回饋
    I_EXCITE_step(&I_EXCITE_str);
    //角差PI調電流
    PI_CURRENT_step(&PI_CURRENT_str);
    //調整*SVPWM_str->pwmA_p  *SVPWM_str->pwmB_p
    SVPWM_step(&SVPWM_str);

    // printf("control_handle pmwa : %d, pwmb: %d\n", mv_avg_str_PWMA.mv_output,
    //        mv_avg_str_PWMB.mv_output);
}

//==================================工具===================================
//=========================================================================
void updateDuty(void) {
    cli();
    /* 更改A相duty */
    OCR1A = (uint8_t)(*SVPWM_str.pwmA_p);
    /* 更改B相duty */
    OCR1B = (uint8_t)SVPWM_str.pwmB;
    sei();
}

void SenseCurrent(void) {
    cli();
    // TCNT1=0;
    /* 更改A相duty為總周期的一半 */
    OCR1A = (tempDuty0 >> 1);
    /* 更改B相duty為總周期的一半 */
    OCR1B = (tempDuty0 >> 1);
    sei();
}

//==================================ADC====================================
//=========================================================================
ISR(ADC_vect) {
    REGGET(&ADCL, 2, &ADC_DATA);  //讀取ADC轉換結果並記憶
    switch (isr_count) {
        case 4: {
            adcResult[0] = ADC_DATA;
            break;
        }
        case 5: {
            adcResult[1] = ADC_DATA;
            break;
        }
        case 6: {
            adcResult[2] = ADC_DATA;
            break;
        }
        case (PWM_SET - 1): {
            adcResult[3] = ADC_DATA;
            break;
        }
        default:
            break;
    }
}

/**
 * @brief A Rising
 *
 */
void ADCTrigger1(void) {
    REGFPT(&ADMUX, 0x1F, 0, 0x00);  //選擇輸入訊號為ADC0(Port F0)/sigleE_0_x1
    _delay_us(15);
    REGFPT(&ADCSRA, 0x40, 6, 1);  //觸發ADC轉換
}

/**
 * @brief A Falling
 *
 */
void ADCTrigger2(void) {
    REGFPT(&ADMUX, 0x1F, 0, 0x01);  //選擇輸入訊號為ADC1(Port F1)/sigleE_1_x1
    _delay_us(15);
    REGFPT(&ADCSRA, 0x40, 6, 1);  //觸發ADC轉換
}

/**
 * @brief B Rising
 *
 */
void ADCTrigger3(void) {
    REGFPT(&ADMUX, 0x1F, 0, 0x02);  //選擇輸入訊號為ADC2(Port F2)/sigleE_2_x1
    _delay_us(15);
    REGFPT(&ADCSRA, 0x40, 6, 1);  //觸發ADC轉換
}

/**
 * @brief B Falling
 *
 */
void ADCTrigger4(void) {
    REGFPT(&ADMUX, 0x1F, 0, 0x03);  //選擇輸入訊號為ADC3(Port F3)/sigleE_3_x1
    _delay_ms(1);
    REGFPT(&ADCSRA, 0x40, 6, 1);  //觸發ADC轉換
}
