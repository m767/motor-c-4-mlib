@REM run test_identification_fb_c4m 4 3
@REM run test_current_correction 4 3
@REM run FILLNAME(NO.hex) COMMAND_PORT NORMAL_PORT

make clean
set file_path=%cd%
cd ../c4mlib/ServiceProvi/asauart
make clean
@REM make clean_deps
@REM make deps
make %file_path%\test\%1.hex
cd %file_path%
taskkill /IM ttermpro.exe
python Pytran.py -p COM%2 -msg !prog
asaloader prog -p COM%3 -f ./test/%1.hex
IF "%4"=="run" (
    Start "" "C:\Program Files (x86)\teraterm\ttermpro.exe" /C=%3
    python Pytran.py -p COM%2 -msg !run
    python Pytran.py -p COM%2 -msg !reset
)

@REM if "%4%" == "true" ( 
@REM     E:\OP\CodeToll\Pytran.py -p COM%2 -msg !run
@REM     E:\OP\CodeToll\Pytran.py -p COM%2 -msg !reset
@REM )

@REM putty -serial COM10
@REM TTERMPRO /C=10


