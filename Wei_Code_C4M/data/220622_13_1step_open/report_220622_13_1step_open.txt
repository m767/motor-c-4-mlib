I_ki:0.0
PI_ki:0.0
PI_kp:0.0
Data size is 200
===== theta error (degree) =====
max: 0.1538086
min: -0.28564453
max-min: 0.43945312
mean: -0.045043945
std: 0.060324848
MSE: 0.005668044
N_STEP: 11.702857142857143, -6.3015384615384615
===== velocity =====
theta/s: 1000.0
RPM: 166.66666666666666
===== sensor ele angle =====
max: 25.202637
min: 19.797363
mean: 22.417053
===== command ele angle =====
max: 25.180664
min: 19.797363
mean: 22.462097
===== consume power (watt) =====
max: 2.605963
min: 1.7433643
mean: 2.475031
std: 0.12064534
sum: 495.0062 J
===== current  =====
ia_max: 0.6070101 A
ia_min: -0.60878336 A
ia_mean: -0.0011657142 A
ib_max: 0.58759624 A
ib_min: -0.5966497 A
ib_mean: -0.012838287 A
===== current RMS Square (watt) =====
rms_ia_square: 0.1836714 A
rms_ib_square: 0.16940007 A
===== current RMS (watt) =====
rms_ia: 0.42856902 A
rms_ib: 0.41158238 A
===== P_loss (watt) =====
P_loss_A: 1.1911090286821127 W
P_loss_B: 1.1323341806977987 W
===== P_in (watt) =====
P_in_A: 1.5845798 W
P_in_B: 1.5845798 W
===== Eta (H) (%) =====
P_efficient_A: 24.83123856632115 %
P_efficient_B: 34.09284966302973 %
P_all: 29.46204411467544 %
