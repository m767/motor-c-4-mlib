I_ki:0.0
PI_ki:0.0
PI_kp:0.0
Data size is 1600
===== theta error (degree) =====
max: 0.0439453125
min: -0.2197265625
std: 0.04387078259445791
RMS: 0.09397967338693192
MSE: 0.008832179009914398
===== current RMS (watt) =====
rms_ia: 0.40577453 A
rms_ib: 0.39222673 A
P(W): 2.232648316323757 W
===== velocity =====
theta/s: 8527.604166666668
RPM: 1421.2673611111113
===== sensor ele angle =====
max: 359.6484375
min: -0.10986328125
mean: 179.79384155273436
===== command ele angle =====
max: 359.7583
min: 0.0
mean: 179.87695
===== consume power (watt) =====
max: 2.5063715
min: 1.9104089
mean: 2.2326484
std: 0.14646392
sum: 3572.2373 J
===== current  =====
ia_max: 0.5851695 A
ia_min: -0.5869428 A
ia_mean: 0.0023909796 A
ib_max: 0.5609022 A
ib_min: -0.56995565 A
ib_mean: -0.007854385 A
===== current RMS Square (watt) =====
rms_ia_square: 0.16465297 A
rms_ib_square: 0.1538418 A
===== P_loss (watt) =====
P_loss_A: 1.1542173436284064 W
P_loss_B: 1.1542173436284064 W
===== P_in (watt) =====
P_in_A: 1.8197391 W
P_in_B: 1.8197391 W
===== Eta (H) (%) =====
P_efficient_A: 36.57237229642718 %
P_efficient_B: 40.73705561805841 %
P_all: 38.65471395724279 %
