I_ki:0.35
PI_ki:0.001
PI_kp:0.02
Data size is 2000
===== theta error (degree) =====
max: 0.06591797
min: -0.17578125
max-min: 0.24169922
mean: -0.023642577
std: 0.028751655
MSE: 0.0013856292
N_STEP: 27.30666666666667, -10.24
===== velocity =====
theta/s: 72.08333333333334
RPM: 12.013888888888891
===== sensor ele angle =====
max: 101.68945
min: 97.86621
mean: 99.886765
===== command ele angle =====
max: 101.68945
min: 97.976074
mean: 99.91041
===== consume power (watt) =====
max: 1.9560007
min: 0.13524793
mean: 0.42062914
std: 0.19423465
sum: 841.2583 J
===== current  =====
ia_max: 0.3594833 A
ia_min: -0.4364853 A
ia_mean: -0.07630345 A
ib_max: 0.43713877 A
ib_min: -0.50686055 A
ib_mean: -0.135557 A
===== current RMS Square (watt) =====
rms_ia_square: 0.028782222 A
rms_ib_square: 0.038006548 A
===== current RMS (watt) =====
rms_ia: 0.16965324 A
rms_ib: 0.19495268 A
===== P_loss (watt) =====
P_loss_A: 0.18665271239355208 W
P_loss_B: 0.17744240121915936 W
===== P_in (watt) =====
P_in_A: 0.76301295 W
P_in_B: 0.76301295 W
===== Eta (H) (%) =====
P_efficient_A: 75.53741211637582 %
P_efficient_B: 69.29142962000223 %
P_all: 72.41442086818903 %
