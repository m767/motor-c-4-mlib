I_ki:0.0
PI_ki:0.0
PI_kp:0.0
Data size is 200
===== theta error (degree) =====
max: -0.06591797
min: -0.21972656
max-min: 0.1538086
mean: -0.14622803
std: 0.04001181
MSE: 0.022983579
N_STEP: -27.30666666666667, -8.192
===== velocity =====
theta/s: 237.5
RPM: 39.583333333333336
===== sensor ele angle =====
max: 68.708496
min: 67.39014
mean: 68.01174
===== command ele angle =====
max: 68.84033
min: 67.5
mean: 68.15797
===== consume power (watt) =====
max: 2.4756575
min: 2.0604794
mean: 2.2768981
std: 0.1291578
sum: 455.37964 J
===== current  =====
ia_max: 0.4322853 A
ia_min: -0.19623868 A
ia_mean: 0.13258371 A
ib_max: 0.5681824 A
ib_min: 0.4031645 A
ib_mean: 0.5007192 A
===== current RMS Square (watt) =====
rms_ia_square: 0.07079665 A
rms_ib_square: 0.2540105 A
===== current RMS (watt) =====
rms_ia: 0.26607642 A
rms_ib: 0.5039945 A
===== P_loss (watt) =====
P_loss_A: 0.4591162987798453 W
P_loss_B: 0.43646136961877346 W
===== P_in (watt) =====
P_in_A: 1.1222268 W
P_in_B: 1.1222268 W
===== Eta (H) (%) =====
P_efficient_A: 59.088814778932175 %
P_efficient_B: -39.54172860065428 %
P_all: 9.773543089138952 %
