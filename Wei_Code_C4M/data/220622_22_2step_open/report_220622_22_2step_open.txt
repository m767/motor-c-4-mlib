I_ki:0.0
PI_ki:0.0
PI_kp:0.0
Data size is 1000
===== theta error (degree) =====
max: 0.06591797
min: -0.1977539
max-min: 0.26367188
mean: -0.0616333
std: 0.041077554
MSE: 0.0054860297
N_STEP: 27.30666666666667, -9.102222222222222
===== velocity =====
theta/s: 649.1666666666667
RPM: 108.19444444444447
===== sensor ele angle =====
max: 24.279785
min: 7.1191406
mean: 15.662482
===== command ele angle =====
max: 24.279785
min: 7.1850586
mean: 15.724115
===== consume power (watt) =====
max: 2.5247242
min: 1.7735822
mean: 2.2919497
std: 0.1564438
sum: 2291.9497 J
===== current  =====
ia_max: 0.5924497 A
ia_min: -0.5917962 A
ia_mean: 0.06985266 A
ib_max: 0.5681824 A
ib_min: -0.5820893 A
ib_mean: -0.033450972 A
===== current RMS Square (watt) =====
rms_ia_square: 0.16867135 A
rms_ib_square: 0.15828294 A
===== current RMS (watt) =====
rms_ia: 0.41069618 A
rms_ib: 0.3978479 A
===== P_loss (watt) =====
P_loss_A: 1.0938337349146605 W
P_loss_B: 1.039858901426196 W
===== P_in (watt) =====
P_in_A: 1.7765112 W
P_in_B: 1.7765112 W
===== Eta (H) (%) =====
P_efficient_A: 38.42798516315028 %
P_efficient_B: 45.07131155092824 %
P_all: 41.749648357039256 %
