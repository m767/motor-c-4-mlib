I_ki:0.35
PI_ki:0.0
PI_kp:0.0
Data size is 1600
===== theta error (degree) =====
max: 0.0439453125
min: -0.06591796875
std: 0.01772547948293668
RMS: 0.01902887850112292
MSE: 0.00036209821701049805
===== current RMS (watt) =====
rms_ia: 0.40505615 A
rms_ib: 0.3892533 A
P(W): 2.2122760403156283 W
===== velocity =====
theta/s: 8528.645833333334
RPM: 1421.4409722222224
===== sensor ele angle =====
max: 359.75830078125
min: -0.0439453125
mean: 179.87003173828126
===== command ele angle =====
max: 359.7583
min: 0.0
mean: 179.87695
===== consume power (watt) =====
max: 2.4692616
min: 1.8356404
mean: 2.212276
std: 0.14417282
sum: 3539.6416 J
===== current  =====
ia_max: 0.58031607 A
ia_min: -0.5820893 A
ia_mean: -0.00040583313 A
ib_max: 0.56575567 A
ib_min: -0.5748091 A
ib_mean: -0.006716856 A
===== current RMS Square (watt) =====
rms_ia_square: 0.16407047 A
rms_ib_square: 0.15151812 A
===== P_loss (watt) =====
P_loss_A: 1.1501340095698833 W
P_loss_B: 1.1501340095698833 W
===== P_in (watt) =====
P_in_A: 1.8240143 W
P_in_B: 1.8240143 W
===== Eta (H) (%) =====
P_efficient_A: 36.9449019262933 %
P_efficient_B: 41.768985735909965 %
P_all: 39.35694383110164 %
