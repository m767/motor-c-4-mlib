I_ki:0.0
PI_ki:0.0
PI_kp:0.0
Data size is 200
===== theta error (degree) =====
max: 0.02197265625
min: -0.17578125
std: 0.0517024352165131
RMS: 0.09477578683384145
MSE: 0.008982449769973755
===== current RMS (watt) =====
rms_ia: 0.37889448 A
rms_ib: 0.38442627 A
P(W): 2.042325422614813 W
===== velocity =====
theta/s: 67908.33333333334
RPM: 11318.055555555557
===== sensor ele angle =====
max: 358.08837890625
min: -0.02197265625
mean: 179.01002197265626
===== command ele angle =====
max: 358.19824
min: 0.0
mean: 179.08945
===== consume power (watt) =====
max: 2.2381852
min: 1.8976345
mean: 2.0423255
std: 0.10378881
sum: 408.4651 J
===== current  =====
ia_max: 0.52935463 A
ia_min: -0.5287012 A
ia_mean: 0.0019526505 A
ib_max: 0.53663486 A
ib_min: -0.55539525 A
ib_mean: -0.009986877 A
===== current RMS Square (watt) =====
rms_ia_square: 0.14356102 A
rms_ib_square: 0.14778355 A
===== P_loss (watt) =====
P_loss_A: 1.0063627536594868 W
P_loss_B: 1.0063627536594868 W
===== P_in (watt) =====
P_in_A: 1.5834436 W
P_in_B: 1.5834436 W
===== Eta (H) (%) =====
P_efficient_A: 36.444674936278915 %
P_efficient_B: 34.575336835633934 %
P_all: 35.51000588595642 %
