I_ki:0.0
PI_ki:0.0
PI_kp:0.0
Data size is 200
===== theta error (degree) =====
max: 0.0
min: -0.24169922
max-min: 0.24169922
mean: -0.09887695
std: 0.042606562
MSE: 0.011591971
N_STEP: inf, -7.447272727272727
===== velocity =====
theta/s: 500.0
RPM: 83.33333333333333
===== sensor ele angle =====
max: 61.083984
min: 58.381348
mean: 59.730137
===== command ele angle =====
max: 61.193848
min: 58.49121
mean: 59.829014
===== consume power (watt) =====
max: 2.521734
min: 2.1140218
mean: 2.3464744
std: 0.14425695
sum: 469.29486 J
===== current  =====
ia_max: 0.5997299 A
ia_min: 0.014887134 A
ia_mean: 0.36491916 A
ib_max: 0.57303584 A
ib_min: -0.3928041 A
ib_mean: 0.14596716 A
===== current RMS Square (watt) =====
rms_ia_square: 0.17477055 A
rms_ib_square: 0.1599619 A
===== current RMS (watt) =====
rms_ia: 0.41805568 A
rms_ib: 0.39995235 A
===== P_loss (watt) =====
P_loss_A: 1.1333870098739862 W
P_loss_B: 1.0774604342132807 W
===== P_in (watt) =====
P_in_A: 1.8245958 W
P_in_B: 1.8245958 W
===== Eta (H) (%) =====
P_efficient_A: 37.88284483094306 %
P_efficient_B: 45.95158705244718 %
P_all: 41.917215941695126 %
