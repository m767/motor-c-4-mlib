I_ki:0.35
PI_ki:0.0
PI_kp:0.0
Data size is 8000
===== theta error (degree) =====
max: 0.043945312
min: -0.21972656
max-min: 0.26367188
mean: -0.019465027
std: 0.02916948
MSE: 0.0012297459
N_STEP: 40.96, -8.192
===== velocity =====
theta/s: 85.41666666666667
RPM: 14.23611111111111
===== sensor ele angle =====
max: 107.68799
min: 89.626465
mean: 98.68644
===== command ele angle =====
max: 107.64404
min: 89.7583
mean: 98.70591
===== consume power (watt) =====
max: 2.5766697
min: 0.04702493
mean: 2.2783551
std: 0.15575941
sum: 18226.842 J
===== current  =====
ia_max: 0.590023 A
ia_min: -0.59422296 A
ia_mean: -0.07159679 A
ib_max: 0.5778893 A
ib_min: -0.57966256 A
ib_mean: 0.003438713 A
===== current RMS Square (watt) =====
rms_ia_square: 0.1693709 A
rms_ib_square: 0.1556441 A
===== current RMS (watt) =====
rms_ia: 0.41154698 A
rms_ib: 0.39451757 A
===== P_loss (watt) =====
P_loss_A: 1.0983703161031009 W
P_loss_B: 1.0441716266423464 W
===== P_in (watt) =====
P_in_A: 1.8490045 W
P_in_B: 1.8490045 W
===== Eta (H) (%) =====
P_efficient_A: 40.59666637337212 %
P_efficient_B: 48.10472896183597 %
P_all: 44.35069766760404 %
