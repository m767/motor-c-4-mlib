I_ki:0.35
PI_ki:0.0
PI_kp:0.0
Data size is 200
===== theta error (degree) =====
max: 0.021972656
min: -0.10986328
max-min: 0.13183594
mean: -0.010986328
std: 0.020845093
MSE: 0.00055521727
N_STEP: 81.92, -16.384
===== velocity =====
theta/s: 79.16666666666667
RPM: 13.194444444444445
===== sensor ele angle =====
max: 71.32324
min: 70.90576
mean: 71.12923
===== command ele angle =====
max: 71.32324
min: 70.97168
mean: 71.14021
===== consume power (watt) =====
max: 2.6846972
min: 2.4140103
mean: 2.5785403
std: 0.059694316
sum: 515.70807 J
===== current  =====
ia_max: -0.28602782 A
ia_min: -0.4728863 A
ia_mean: -0.38019723 A
ib_max: -0.3928041 A
ib_min: -0.5287012 A
ib_mean: -0.46772948 A
===== current RMS Square (watt) =====
rms_ia_square: 0.14752127 A
rms_ib_square: 0.22031616 A
===== current RMS (watt) =====
rms_ia: 0.38408497 A
rms_ib: 0.46937847 A
===== P_loss (watt) =====
P_loss_A: 0.9566754508763552 W
P_loss_B: 0.9094686437398195 W
===== P_in (watt) =====
P_in_A: 1.9009858 W
P_in_B: 1.9009858 W
===== Eta (H) (%) =====
P_efficient_A: 49.67477230683608 %
P_efficient_B: 28.55027744507066 %
P_all: 39.112524875953376 %
