I_ki:0.35
PI_ki:0.0
PI_kp:0.0
Data size is 6000
===== theta error (degree) =====
max: 0.021972656
min: -0.17578125
max-min: 0.1977539
mean: -0.01669922
std: 0.018550094
MSE: 0.0006229699
N_STEP: 81.92, -10.24
===== velocity =====
theta/s: 25.555555555555557
RPM: 4.2592592592592595
===== sensor ele angle =====
max: 134.1211
min: 130.05615
mean: 132.01317
===== command ele angle =====
max: 134.09912
min: 130.14404
mean: 132.02986
===== consume power (watt) =====
max: 2.3633518
min: 1.7752695
mean: 2.098321
std: 0.148984
sum: 12589.926 J
===== current  =====
ia_max: 0.59487647 A
ia_min: -0.44133875 A
ia_mean: 0.27568778 A
ib_max: 0.5827428 A
ib_min: -0.51656747 A
ib_mean: 0.17550132 A
===== current RMS Square (watt) =====
rms_ia_square: 0.17427418 A
rms_ib_square: 0.15730914 A
===== current RMS (watt) =====
rms_ia: 0.41746157 A
rms_ib: 0.39662218 A
===== P_loss (watt) =====
P_loss_A: 1.1301680336892606 W
P_loss_B: 1.074400297254324 W
===== P_in (watt) =====
P_in_A: 1.8965563 W
P_in_B: 1.8965563 W
===== Eta (H) (%) =====
P_efficient_A: 40.40946432240627 %
P_efficient_B: 48.86463962777634 %
P_all: 44.6370519750913 %
