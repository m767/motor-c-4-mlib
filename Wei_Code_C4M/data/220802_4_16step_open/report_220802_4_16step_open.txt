I_ki:0.0
PI_ki:0.0
PI_kp:0.0
===== theta error (degree) =====
max: -0.26367188
min: -0.5932617
max-min: 0.32958984
mean: -0.408208
std: 0.05656803
MSE: 0.1698337
N_STEP: -6.826666666666667, -3.0340740740740744
===== velocity =====
===== sensor ele angle =====
max: 359.47266
min: -0.41748047
mean: 116.66
===== command ele angle =====
max: 359.86816
min: 0.0
mean: 117.06819
